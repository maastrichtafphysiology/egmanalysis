classdef PaceBlankingPanel < ECGChannelPanel
    properties
        BlankedEcgData
        
        PaceLineListener
        PaceLines
        PaceLineMarkers
        BlankedDataLines
        
        PaceChannelListbox
        SelectedPaceChannel
        PaceIndices
        PaceInterval
        PaceIntervalEdit
        BlankingInterval
        BlankingIntervalEdit
        MinimalPaceAmplitude
        MinimalPaceAmplitudeEdit
    end
    
    properties (Constant)
        FILE_SUFFIX = '_PaceDetection';
    end
    
    events
        PaceBlankingCompleted
    end
    
    methods
        function self = PaceBlankingPanel(position)
            self = self@ECGChannelPanel(position);
            self.PaceLineListener = addlistener(UserInterfacePkg.KeyEventClass.Instance(),...
                'KeyPressed', @self.HandleKeyPressedEvent);
            self.PaceLineListener(end + 1) = addlistener(UserInterfacePkg.KeyEventClass.Instance(),...
                'KeyReleased', @self.HandleKeyReleasedEvent);
            
            self.PaceInterval = 0.2;
            self.BlankingInterval = 0.005;
            self.MinimalPaceAmplitude = 0.75;
        end
        
        function SetECGData(self, ecgData)
            numberOfReferenceChannels = size(ecgData.ReferenceData, 2);
            if numberOfReferenceChannels == 0
                numberOfReferenceChannels = ecgData.GetNumberOfChannels();
            end
            
            self.BlankedEcgData = ecgData.Copy();
            if isempty(self.SelectedPaceChannel) || self.SelectedPaceChannel <= numberOfReferenceChannels
                self.SelectedPaceChannel = 1;
            end
            
            self.PaceIndices = cell(numberOfReferenceChannels, 1);
            
            SetECGData@ECGChannelPanel(self, ecgData);
            
            self.SetPaceChannels();
        end
        
        function [blankedEcgData, paceIndices] = GetBlankedECGData(self)
            blankedEcgData = self.BlankedEcgData;
            paceIndices = self.PaceIndices;
        end
        
        function ecgData = LoadData(self, filename)
            blankingData = load(filename);
            ecgData = blankingData.ecgData;
            
            [pathString, nameString] = fileparts(filename);
            nameString = nameString(1:(end - length(PaceBlankingPanel.FILE_SUFFIX)));
            if ~isempty(blankingData.ecgData)
                blankingData.ecgData.Filename = fullfile(pathString, nameString);
            end
            
            if ~isempty(blankingData.blankedEcgData)
                blankingData.blankedEcgData.Filename = fullfile(pathString, nameString);
            end
            
            if isfield(blankingData, 'selectedPaceChannel')
                self.SelectedPaceChannel = blankingData.selectedPaceChannel;
            end
            
            if isfield(blankingData, 'paceInterval')
                self.PaceInterval = blankingData.paceInterval;
                set(self.PaceIntervalEdit,...
                    'string', num2str(1000 * self.PaceInterval));
            end
            
            if isfield(blankingData, 'minimalPaceAmplitude')
                self.MinimalPaceAmplitude = blankingData.minimalPaceAmplitude;
                set(self.MinimalPaceAmplitudeEdit,...
                    'string', num2str(100 * self.MinimalPaceAmplitude));
            end
            
            if isfield(blankingData, 'blankingInterval')
                self.BlankingInterval = blankingData.blankingInterval;
                set(self.BlankingIntervalEdit,...
                    'string', num2str(1000 * self.BlankingInterval)); 
            end
            
            self.SetECGData(blankingData.ecgData);
            
            self.PaceIndices = blankingData.paceIndices;
            self.BlankedEcgData = blankingData.blankedEcgData;
            
            self.ShowPaceLines();
            self.ShowBlankedECG();
        end
    end
    
    methods (Access = protected)
        function CreateControlPanel(self)
            CreateControlPanel@ECGChannelPanel(self);
            
            set(self.ChannelPanel, 'position', [0 .5 1 .49]);
            self.CreatePaceSettings();
            
            uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.1 .05 .8 .05],...
                'style', 'pushbutton',...
                'string', 'Pace detection completed',...
                'callback', @(src, event) notify(self, 'PaceBlankingCompleted'));
        end
        
        function ShowECGData(self)
            ShowECGData@ECGChannelPanel(self);
            
            self.ShowPaceLines();
            self.ShowBlankedECG();
        end
    end
    
    methods (Access = private)
        function CreatePaceSettings(self, varargin)
            paceSettingsPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [0 .1 1 .39],...
                'borderType', 'none');
            
            paceChannelPanel = uipanel('parent', paceSettingsPanel,...
                'units', 'normalized', ...
                'position', [0 .4 .5 .6],...
                'title', 'Available pacing channels',...
                'titlePosition', 'centertop',...
                'borderType', 'none');
            
            self.PaceChannelListbox = uicontrol(...
                'parent', paceChannelPanel,...
                'units', 'normalized',...
                'position', [0 0 1 1],...
                'style', 'listbox',...
                'backgroundColor', [1 1 1],...
                'callback', @self.SetSelectedPaceChannel,...
                'busyAction', 'cancel');
            
            uicontrol('parent', paceSettingsPanel,...
                'units', 'normalized',...
                'position', [.55 .8 .25 .1],...
                'style', 'text',...                
                'string', 'Pace interval (ms)');
            self.PaceIntervalEdit = uicontrol('parent', paceSettingsPanel,...
                'units', 'normalized',...
                'position', [.8 .8 .15 .1],...
                'style', 'edit',...
                'string', num2str(1000 * self.PaceInterval),...
                'callback', @self.SetPacePeriod);
            
            uicontrol('parent', paceSettingsPanel,...
                'units', 'normalized',...
                'position', [.55 .9 .25 .1],...
                'style', 'text',...                
                'string', 'Pace spike amplitude (%)');
            self.MinimalPaceAmplitudeEdit = uicontrol('parent', paceSettingsPanel,...
                'units', 'normalized',...
                'position', [.8 .9 .15 .1],...
                'style', 'edit',...
                'string', num2str(100 * self.MinimalPaceAmplitude),...
                'callback', @self.SetPaceAmplitude);
            
            uicontrol('parent', paceSettingsPanel,...
                'units', 'normalized',...
                'position', [.55 .7 .4 .1],...
                'style', 'pushbutton',...
                'string', 'Detect',...
                'callback', @self.DetectPacePulses);
            
            uicontrol('parent', paceSettingsPanel,...
                'units', 'normalized',...
                'position', [.55 .5 .25 .1],...
                'style', 'text',...                
                'string', 'Blanking interval (ms)');
            self.BlankingIntervalEdit = uicontrol('parent', paceSettingsPanel,...
                'units', 'normalized',...
                'position', [.8 .5 .15 .1],...
                'style', 'edit',...
                'string', num2str(1000 * self.BlankingInterval),...
                'callback', @self.SetBlankingInterval);
            
            uicontrol('parent', paceSettingsPanel,...
                'units', 'normalized',...
                'position', [.55 .4 .4 .1],...
                'style', 'pushbutton',...
                'string', 'Blank',...
                'callback', @self.BlankPacePulses);
            
            uicontrol('parent', paceSettingsPanel,...
                'units', 'normalized',...
                'position', [.05 .1 .4 .1],...
                'style', 'pushbutton',...
                'string', 'Save to .mat',...
                'callback', @self.SavePaceBlanking);
            uicontrol('parent', paceSettingsPanel,...
                'units', 'normalized',...
                'position', [.5 .1 .4 .1],...
                'style', 'pushbutton',...
                'string', 'Load from .mat',...
                'callback', @self.LoadPaceBlanking);
        end
        
        function HandleKeyPressedEvent(self, ~, eventData)
            switch eventData.Key
                case {'delete', 'd'}

                case 'i'

            end
        end
        
        function HandleKeyReleasedEvent(self, ~, eventData)
            switch eventData.Key
                case 'rightarrow'

                case 'leftarrow'

            end
        end
        
        function SetPaceChannels(self, varargin)
            if isempty(self.EcgData.ReferenceLabels)
                set(self.PaceChannelListbox, 'string', self.EcgData.ElectrodeLabels);
            else
                set(self.PaceChannelListbox, 'string', self.EcgData.ReferenceLabels);
            end
            set(self.PaceChannelListbox, 'value', self.SelectedPaceChannel);
        end
        
        function SetSelectedPaceChannel(self, varargin)
            self.SelectedPaceChannel = get(self.PaceChannelListbox, 'value');
            self.ShowPaceLines();
            self.ShowBlankedECG();
        end
        
        function SetPacePeriod(self, source, varargin)
            value = str2double(get(source, 'string')) / 1000;
            self.PaceInterval = value;
        end
        
        function SetPaceAmplitude(self, source, varargin)
            value = str2double(get(source, 'string')) / 100;
            self.MinimalPaceAmplitude = value;
        end 
        
        function SetBlankingInterval(self, source, varargin)
            value = str2double(get(source, 'string')) / 1000;
            self.BlankingInterval = value;
        end
        
        function DetectPacePulses(self, varargin)
            self.PaceIndices{self.SelectedPaceChannel} = self.DetectPacePeaks();            
            self.ShowPaceLines();
        end
        
        function BlankPacePulses(self, varargin)
            paceIndices = self.PaceIndices{self.SelectedPaceChannel};
            if isempty(paceIndices), return; end
            
            blankingSampleInterval = round(self.BlankingInterval * self.EcgData.SamplingFrequency);
            blankingRange = -blankingSampleInterval:1:blankingSampleInterval;
            blankingIndices = bsxfun(@plus, paceIndices, blankingRange);
            blankingIndices = blankingIndices(blankingIndices > 0 & blankingIndices < self.EcgData.GetNumberOfSamples());
            sampleRange = 1:self.EcgData.GetNumberOfSamples();
            sampleRange(blankingIndices) = [];
            for channelIndex = 1:self.EcgData.GetNumberOfChannels()
                signal = self.EcgData.Data(:, channelIndex);
                interpolant = pchip(sampleRange, signal(sampleRange));
                self.BlankedEcgData.Data(:, channelIndex) = ppval(interpolant, 1:self.EcgData.GetNumberOfSamples());
            end
            
            self.ShowBlankedECG();
        end
        
        function pacePeakIndices = DetectPacePeaks(self)
            samplingFrequency = self.EcgData.SamplingFrequency;
            if isempty(self.EcgData.ReferenceLabels)
                data = self.EcgData.Data(:, self.SelectedPaceChannel);
            else
                data = self.EcgData.ReferenceData(:, self.SelectedPaceChannel);
            end
            signal = abs(data - mean(data));
            
            minimumPacePeakDistance = floor(self.PaceInterval * samplingFrequency);
            [peakAmplitudes, pacePeakIndices] = findpeaks(signal, 'MINPEAKDISTANCE', minimumPacePeakDistance);
            
            validPeaks = peakAmplitudes > (max(peakAmplitudes) * self.MinimalPaceAmplitude);
            pacePeakIndices = pacePeakIndices(validPeaks);
        end
        
        function ShowPaceLines(self)
            validHandles = ishandle(self.PaceLines);
            if any(validHandles)
                delete(self.PaceLines(validHandles));
            end
            
            validHandles = ishandle(self.PaceLineMarkers);
            if any(validHandles)
                delete(self.PaceLineMarkers(validHandles));
            end
            
            paceIndices = self.PaceIndices{self.SelectedPaceChannel};
            if isempty(paceIndices), return; end
            
            time = self.EcgData.GetTimeRange();
            lineData = [self.EcgData.Data(:, self.SelectedChannels),...
                self.EcgData.ReferenceData(:, self.SelectedLeadChannels)];
            
            self.PaceLines = NaN(numel(self.AxesHandles), numel(paceIndices));
            self.PaceLineMarkers = NaN(numel(self.AxesHandles), 1);
            for axesIndex = 1:numel(self.AxesHandles);
                yLimits = get(self.AxesHandles(axesIndex), 'yLim');
                for rIndex = 1:numel(paceIndices)
                    self.PaceLines(axesIndex, rIndex) =...
                        line('xData', [time(paceIndices(rIndex)) time(paceIndices(rIndex))],...
                        'yData', yLimits,...
                        'color', [1 0 0],...
                        'parent', self.AxesHandles(axesIndex),...
                        'buttonDownFcn', @self.PaceSelectedCallback,...
                        'userData', paceIndices(rIndex));
                end
                
                self.PaceLineMarkers(axesIndex) =...
                        line('xData', time(paceIndices),...
                        'yData', lineData(paceIndices, axesIndex),...
                        'lineStyle', 'none',...
                        'marker', 'o', 'markerSize', 7,...
                        'markerEdgeColor', [0 0 0], 'markerFaceColor', [0 0 1],...
                        'parent', self.AxesHandles(axesIndex));
            end
        end
        
        function ShowBlankedECG(self)
            validHandles = ishandle(self.BlankedDataLines);
            if any(validHandles)
                delete(self.BlankedDataLines(validHandles));
            end
            
            lineData = self.BlankedEcgData.Data(:, self.SelectedChannels);
            time = self.EcgData.GetTimeRange();
            lineColor = [0 0 1];
            self.BlankedDataLines = NaN(numel(self.SelectedChannels), 1);
            for lineIndex = 1:size(lineData, 2);
                self.BlankedDataLines(lineIndex) = ...
                    line('xData', time, 'yData', lineData(:, lineIndex),...
                    'color', lineColor,...
                    'lineWidth', 1,...
                    'parent', self.AxesHandles(lineIndex),...
                    'buttonDownFcn', @self.LineSelectedCallback,...
                    'lineSmoothing', 'on');
            end
        end
        
        function PaceSelectedCallback(self, source, eventData)
        end
        
        function LineSelectedCallback(~, source, varargin)
            lineWidth = get(source, 'lineWidth');
            if lineWidth == 1
                set(source, 'lineWidth', 2);
                uistack(source, 'top');
            else
                set(source, 'lineWidth', 1);
                uistack(source, 'bottom');
            end
        end
        
        function SavePaceBlanking(self, varargin)
            [pathString, nameString] = fileparts(self.EcgData.Filename);
            filename = [pathString, filesep, nameString, PaceBlankingPanel.FILE_SUFFIX];
            ecgData = self.EcgData; %#ok<NASGU>
            blankedEcgData = self.BlankedEcgData; %#ok<NASGU>
            paceIndices = self.PaceIndices; %#ok<NASGU>
            selectedPaceChannel = self.SelectedPaceChannel; %#ok<NASGU>
            paceInterval = self.PaceInterval; %#ok<NASGU>
            minimalPaceAmplitude = self.MinimalPaceAmplitude; %#ok<NASGU>
            blankingInterval = self.BlankingInterval; %#ok<NASGU>
            save(filename,...
                'ecgData',...
                'blankedEcgData',...
                'paceIndices',...
                'selectedPaceChannel',...
                'paceInterval',...
                'minimalPaceAmplitude', ...
                'blankingInterval', '-v7.3');
        end
        
        function LoadPaceBlanking(self, varargin)
            if isempty(self.EcgData)
                [filename, pathname] = HelperFunctions.customUigetfile(['*', PaceBlankingPanel.FILE_SUFFIX, '.mat'], 'Select Pace detection file');
                if isequal(filename, 0) || isequal(pathname, 0), return; end
                filename = fullfile(pathname, filename);
            else
                [pathString, nameString] = fileparts(self.EcgData.Filename);
                filename = [pathString, filesep, nameString [PaceBlankingPanel.FILE_SUFFIX, '.mat']];
            end
            
            self.LoadData(filename);
        end
    end
end
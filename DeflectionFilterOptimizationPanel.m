classdef DeflectionFilterOptimizationPanel < UserInterfacePkg.CustomPanel
    properties (SetAccess = private)
		OptimizationProtocol
		Parameters
    end
    
    properties (Access = private)
		OptimizationSteps;
		CheckBoxes;
		DropDownBoxes;
		Values
		Percentages
    end
    
    methods
        function self = DeflectionFilterOptimizationPanel(position)
            self = self@UserInterfacePkg.CustomPanel(position);
			
			self.FillParameters();
			
			self.OptimizationSteps = 5;
			
			self.OptimizationProtocol = struct(...
                'Parameter', {},...
                'Action', {},...
                'Value', {},...
                'ParameterIndex', {},...
                'Factor', {},...
                'Percentage', {});
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            
			self.BuildGUI();
            
            self.SetInitialValues();
        end
    end        
	
    methods (Access = private)
		function BuildGUI(self)
            dropDownChoices = {'-', self.Parameters.Friendly};
            
			for i = 1 : self.OptimizationSteps
				parameterPanel = uipanel('parent', self.ControlHandle,... 
                    'units', 'normalized',...
					'position', [0, 1 - (i / self.OptimizationSteps), 1, 1 / self.OptimizationSteps]);
                
                self.DropDownBoxes(i) = uicontrol('style', 'popup',...
                    'parent', parameterPanel,... 
                    'String', dropDownChoices,...
                    'units', 'normalized',...
                    'position', [0.1 0.6 0.8 0.3],...
                    'callback', @self.UpdateOptimizationProtocol);
                
                self.CheckBoxes(i) = uicontrol('style', 'checkbox',...
                    'parent', parameterPanel,... 
                    'units', 'normalized',...
					'position', [0.1 .2 0.2 0.3],...
                    'string', 'Fixed',...
                    'min', 0,...
                    'max', 1,...
                    'value', 0,...
                    'tag', num2str(i),...
                    'callback', @self.CheckBoxChecked);
					
				self.Values(i) = uicontrol('style', 'edit',...
				    'parent', parameterPanel,... 
                    'units', 'normalized',...
					'backgroundcolor', 'white',...
					'enable', 'off',...
                    'position', [0.3 0.2 0.3 0.3],...
                    'tooltipString', 'Fixed threshold value',...
                    'callback', @self.UpdateOptimizationProtocol);
					
				self.Percentages(i) = uicontrol('style', 'edit',....
				    'parent', parameterPanel,... 
                    'units', 'normalized',...
					'backgroundcolor', 'white',...
					'enable', 'off',...
					'string', 0,...
                    'position', [0.6 0.2 0.3 0.3],...
                    'tooltipString', 'Maximum long interval percentage',...
					'callback', @self.UpdateOptimizationProtocol);
			end
        end
		
        function SetInitialValues(self)
            set(self.CheckBoxes(1), 'value', 1);
            set(self.Values(1), 'enable', 'on');
            set(self.DropDownBoxes(1), 'value', 2);
            set(self.Values(1), 'string', num2str(0));

			% set initial values to filter for template amplitude (10% long
			% intervals)
            set(self.Percentages(2), 'enable', 'off');
            set(self.CheckBoxes(2), 'value', 0);
            set(self.Values(2), 'enable', 'off');
            set(self.DropDownBoxes(2), 'value', 4);
            set(self.Values(2), 'string', num2str(10));

			set(self.Percentages(3), 'enable', 'off');
            set(self.CheckBoxes(3), 'value', 1);
            set(self.Values(3), 'enable', 'on');
            set(self.DropDownBoxes(3), 'value', 3);
            set(self.Values(3), 'string', inf);

			set(self.Percentages(4), 'enable', 'off');
            set(self.CheckBoxes(4), 'value', 1);
            set(self.Values(4), 'enable', 'on');
            set(self.DropDownBoxes(4), 'value', 6);
            set(self.Values(4), 'string', num2str(0));

			set(self.Percentages(5), 'enable', 'off');
            set(self.CheckBoxes(5), 'value', 1);
            set(self.Values(5), 'enable', 'on');
            set(self.DropDownBoxes(5), 'value', 5);
            set(self.Values(5), 'string', num2str(0));

			set(self.Percentages(5), 'enable', 'off');
            self.UpdateOptimizationProtocol();
        end
        
		function FillParameters(self)
			self.Parameters = struct('FilterSettingName', 'correlationThreshold',...
				'AnalysisResultName', 'templatePeakCorrelations', 'Min', [], 'Max', [],...
                'StepSize', [], 'Direction', 'increase', 'Friendly', 'Correlation');
				
			self.Parameters(2).FilterSettingName = 'durationThreshold';
			self.Parameters(2).Direction = 'decrease';
			self.Parameters(2).AnalysisResultName = 'templatePeakDurations';
            self.Parameters(2).Friendly = 'Duration';

			self.Parameters(3).FilterSettingName = 'amplitudeThreshold';
			self.Parameters(3).Direction = 'increase';
			self.Parameters(3).AnalysisResultName = 'templatePeakAmplitudes';
            self.Parameters(3).Friendly = 'Amplitude';

			self.Parameters(4).FilterSettingName = 'amplitudeDividedByDurationThreshold';
			self.Parameters(4).Direction = 'increase';
			self.Parameters(4).AnalysisResultName = 'peakAmplitudeDividedByDuration';
            self.Parameters(4).Friendly = 'Ampl./Durat.';

			self.Parameters(5).FilterSettingName = 'slopeThreshold';
			self.Parameters(5).Direction = 'decrease';
			self.Parameters(5).AnalysisResultName = 'deflectionPeakSlopes';	
            self.Parameters(5).Friendly = 'Slope';
		end
		
		function CheckBoxChecked(self, varargin)
            currentRow = str2num(get(varargin{1},'tag')); %#ok<ST2NM>
            if (get(self.CheckBoxes(currentRow), 'value'))
                set(self.Values(currentRow), 'enable', 'on');
				set(self.Percentages(currentRow), 'enable', 'off');
            else
                set(self.Values(currentRow), 'enable', 'off');
				set(self.Percentages(currentRow), 'enable', 'on');
            end
            self.UpdateOptimizationProtocol();
        end
        
        function UpdateOptimizationProtocol(self, varargin)
           self.OptimizationProtocol = struct('Parameter', {}, 'Action', {}, 'Value', {}, 'ParameterIndex', {});
           iOptimizationParamater = 1;
           for i = 1 : numel(self.CheckBoxes)
                currentParameterIndex = get(self.DropDownBoxes(i), 'value');
                if currentParameterIndex > 1
					if ~get(self.CheckBoxes(i), 'value')
                        set(self.Percentages(i), 'enable', 'on');
					end
					
                    self.OptimizationProtocol(i).Parameter = self.Parameters(currentParameterIndex - 1).FilterSettingName;
                    
                    if get(self.CheckBoxes(i), 'value')
                        self.OptimizationProtocol(i).Action = 'Fixed';
                    else
                        self.OptimizationProtocol(i).Action = 'Optimize';
                    end
                    
                    self.OptimizationProtocol(i).Value = str2double(get(self.Values(i), 'string'));
                    self.OptimizationProtocol(i).ParameterIndex = currentParameterIndex - 1;
                    self.OptimizationProtocol(i).Factor = 1;
					self.OptimizationProtocol(i).Percentage = str2double(get(self.Percentages(i), 'string'));
					
                    iOptimizationParamater = iOptimizationParamater + 1;
                else
                    set(self.Percentages(i), 'enable', 'off');
                end
           end
        end
    end
end
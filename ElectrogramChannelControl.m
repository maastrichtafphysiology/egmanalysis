classdef ElectrogramChannelControl < UserInterfacePkg.CustomPanel
    properties
        EcgData
        SelectedChannels
    end
    
    properties (Access = protected)
        AxesHandles
    end
    
    properties (Constant)
        NUMBER_OF_DISPLAYED_CHANNELS = 10;
    end
    
    methods
        function self = ElectrogramChannelControl(position)
            self = self@UserInterfacePkg.CustomPanel(position);
            self.SelectedChannels = [];
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            
            self.CreateElectrogramAxes();
        end
        
        function SetECGData(self, ecgData)
            self.EcgData = ecgData;
            
            if isempty(self.SelectedChannels)
                if self.EcgData.GetNumberOfChannels < ECGChannelPanel.NUMBER_OF_DISPLAYED_CHANNELS
                    self.SelectedChannels = 1:self.EcgData.GetNumberOfChannels;
                else
                    self.SelectedChannels = 1:ECGChannelPanel.NUMBER_OF_DISPLAYED_CHANNELS;
                end
            elseif numel(self.SelectedChannels) > self.EcgData.GetNumberOfChannels
                self.SelectedChannels = 1:self.EcgData.GetNumberOfChannels;
            end
            
            self.ShowECGData();
        end
        
        function delete(self)
            self.EcgData = [];
        end
    end
    
    methods (Access = protected)
        function CreateElectrogramAxes(self)
            self.AxesPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.2 0 .8 1]);
            
            uicontrol(...
                'parent', self.AxesPanel,...
                'units', 'normalized',...
                'position', [.95 0 .05 .05],...
                'style', 'pushbutton',...
                'string', 'Snapshot',...
                'callback', @self.Snapshot);
        end
        
        function ShowECGData(self)
            time = self.EcgData.GetTimeRange();
            validHandles = ishandle(self.AxesHandles);
            if any(validHandles)
                delete(self.AxesHandles(validHandles));
            end
            self.AxesHandles = [];

            lineData = [self.EcgData.Data(:, self.SelectedChannels),...
                self.EcgData.ReferenceData(:, self.SelectedLeadChannels)];
            lineLabels = [self.EcgData.ElectrodeLabels(self.SelectedChannels);...
                self.EcgData.ReferenceLabels(self.SelectedLeadChannels)];
            
            self.AxesHandles = ECGChannelPanel.TightSubplot(numel(lineLabels), 1, self.AxesPanel,...
                0.0025, [0.05, 0], [0, 0.05]);
            
            for lineIndex = 1:numel(lineLabels)
                lineColor = [0 0 0];
                line('xData', time, 'yData', lineData(:, lineIndex),...
                    'color', lineColor,...
                    'parent', self.AxesHandles(lineIndex),...
                    'lineSmoothing', 'on');
                text('parent', self.AxesHandles(lineIndex), 'string', lineLabels{lineIndex},...
                    'fontWeight', 'bold',...
                    'fontName', 'Helvetica',...
                    'fontSize', 11,...
                    'units', 'normalized', 'position', [0, 1, 0],...
                    'horizontalAlignment', 'left',...
                    'verticalAlignment', 'top',...
                    'interpreter', 'none');
                set(self.AxesHandles(lineIndex), 'xTick', [],...
                    'YAxisLocation', 'right', 'xLim', [time(1), time(end)]);
            end
            set(self.AxesHandles(end),'xTickMode', 'auto');
            xlabel(self.AxesHandles(end), 'time (seconds)');
            if numel(self.AxesHandles) > 1
                validHandles = ishandle(self.AxesHandles);
                if any(validHandles)
                    linkaxes(self.AxesHandles(validHandles), 'x');
                end
            end
        end
    end
    
    methods (Static)
        function axesHandles = TightSubplot(numberOfAxesRows, numberOfAxesColumns, parent, gap, heightMargins, widthMargins)
            
            % tight_subplot creates "subplot" axes with adjustable gaps and margins
            %
            % ha = tight_subplot(Nh, Nw, gap, marg_h, marg_w)
            %
            %   in:  Nh      number of axes in hight (vertical direction)
            %        Nw      number of axes in width (horizontaldirection)
            %        gap     gaps between the axes in normalized units (0...1)
            %                   or [gap_h gap_w] for different gaps in height and width
            %        marg_h  margins in height in normalized units (0...1)
            %                   or [lower upper] for different lower and upper margins
            %        marg_w  margins in width in normalized units (0...1)
            %                   or [left right] for different left and right margins
            %
            %  out:  ha     array of handles of the axes objects
            %                   starting from upper left corner, going row-wise as in
            %                   going row-wise as in
            %
            %  Example: ha = tight_subplot(3,2,[.01 .03],[.1 .01],[.01 .01])
            %           for ii = 1:6; axes(ha(ii)); plot(randn(10,ii)); end
            %           set(ha(1:4),'XTickLabel',''); set(ha,'YTickLabel','')
            
            
            if nargin<4; gap = 0; end
            if nargin<5 || isempty(heightMargins); heightMargins = .05; end
            if nargin<6; widthMargins = .05; end
            
            if numel(gap)==1;
                gap = [gap gap];
            end
            if numel(widthMargins)==1;
                widthMargins = [widthMargins widthMargins];
            end
            if numel(heightMargins)==1;
                heightMargins = [heightMargins heightMargins];
            end
            
            axesHeight = (1 - sum(heightMargins) - (numberOfAxesRows - 1) * gap(1)) / numberOfAxesRows;
            axesWidth = (1 - sum(widthMargins) - (numberOfAxesColumns - 1) * gap(2)) / numberOfAxesColumns;
            
            py = 1 - heightMargins(2) - axesHeight;
            
            axesHandles = zeros(numberOfAxesRows * numberOfAxesColumns, 1);
            ii = 0;
            for ih = 1:numberOfAxesRows
                px = widthMargins(1);
                
                for ix = 1:numberOfAxesColumns
                    ii = ii+1;
                    axesHandles(ii) = axes('units','normalized', ...
                        'parent', parent,...
                        'position',[px, py, axesWidth, axesHeight]);
                    px = px + axesWidth + gap(2);
                end
                py = py - axesHeight - gap(1);
            end
        end
    end
end
classdef WaveMapPanel < UserInterfacePkg.CustomPanel
    properties
        EcgData
        IntrinsicDeflectionData
        DeflectionData
        
        DetectedWaves
        
        WavemapCalculator
        TimeLag
        FrameRate
    end
    
    properties (Access = protected)
        ControlPanel
        
        AnimationPosition
        AnimationButton
        AnimationSlider
        AnimationPositionLabel
        
        SignalPanel
        SignalAxes
        SignalLine
        FarFieldDeflectionLine
        IntrinsicDeflectionLine
        ReferenceLine
        SelectedChannelIndex
        SelectedTime
        SelectedTimeLine
        AnimationPositionLine
        SelectedChannelText
        
        EcgMapControl
        DeflectionMapControl
        NewWavemapControl
        
        WaveMemberships
        WaveStartingPoints
    end
    
    events
        WavemapComputed
    end
    
    methods
        function self = WaveMapPanel(position)
            self = self@UserInterfacePkg.CustomPanel(position);
            
            self.WavemapCalculator = AlgorithmPkg.WavemapCalculator();
            self.SelectedChannelIndex = NaN;
            self.SelectedTime = NaN;
            self.TimeLag = 100;
            self.FrameRate = 0.01;
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            
            self.CreateControlPanel();
            self.CreateSignalPanel();
            self.CreateAnimationPanel();
        end
        
        function SetECGData(self, ecgData, intrinsicDeflectionData)
            self.EcgData = ecgData;
            
            minimumSpacing = self.EcgData.ComputeMinimumSpacing();
            self.WavemapCalculator.NeighborRadius = sqrt(2 * (minimumSpacing + 1e3 * eps)^2 );
            self.WavemapCalculator.MergeSearchRadius = 2 * self.WavemapCalculator.NeighborRadius;
            
            if nargin > 2
                self.IntrinsicDeflectionData = intrinsicDeflectionData.AnalysisResults;
                self.EcgData.SetActivations(self.IntrinsicDeflectionData);
            end
            self.SelectedChannelIndex = 1;
            self.InitializeSignalAxes();
            
            self.InitializeSignalMapAnimation();
            self.InitializeDeflectionAnimation();
            self.InitializeWavemapAnimation();
            
            set(self.AnimationSlider, 'enable', 'on');
            self.AnimationPosition = 1;
            numberOfSamples = self.EcgData.GetNumberOfSamples();
            set(self.AnimationSlider, 'min', 1, 'max', numberOfSamples,...
                'value', self.AnimationPosition,...
                'sliderStep', [1 / (numberOfSamples - 1), 1 / (numberOfSamples - 1)]);
            
            self.SetAnimationFrame();
        end
        
        function ecgData = LoadData(self, filename)
            try
                waveMapData = load(filename);
            catch %#ok<CTCH>
                [filename, pathname] = HelperFunctions.customUigetfile('*.mat', 'No associated file found. Select alternative WM file');
                if isequal(filename, 0) || isequal(pathname, 0), return; end
                filename = fullfile(pathname, filename);
                waveMapData = load(filename);
            end
            
            ecgData = waveMapData.ecgData;
            
            self.DetectedWaves = waveMapData.detectedWaves;
            self.WaveMemberships = waveMapData.waveMemberships;
            self.WavemapCalculator = waveMapData.wavemapCalculator;
            
            if isempty(self.WavemapCalculator.DetectionMethodIndex)
                self.WavemapCalculator.DetectionMethodIndex = 2;
            end
            
            self.SetECGData(waveMapData.ecgData, DeflectionDetectionResults(waveMapData.deflectionData));
            
            clear waveMapData;
            
            notify(self, 'WavemapComputed');
        end
        
        function delete(self)
            self.EcgData = [];
            self.IntrinsicDeflectionData = [];
            self.DeflectionData = [];
            self.DetectedWaves = [];
            delete(self.WavemapCalculator);
            self.WavemapCalculator = [];
        end
    end
    
    methods (Access = protected)
        function CreateControlPanel(self)
            % Animation controls
            self.ControlPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.2 0 .8 .05],...
                'borderType', 'none');
            
            self.AnimationButton = uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [0 0 .05 1],...
                'style', 'togglebutton',...
                'string', '>',...
                'callback', @self.ControlAnimation);
            
            self.AnimationSlider = uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.3 0 .7 1],...
                'style', 'slider',...
                'callback', @self.AnimationSliderCallback,...
                'enable', 'off');
            addlistener(handle(self.AnimationSlider), 'Value', 'PostSet', @self.ContinuousAnimationSliderCallback);
            
            self.AnimationPositionLabel = uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.25 0 .05 1],...
                'style', 'text');
            
            timeLagPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [.05, 0, .1, 1],...
                'title', 'Time lag (ms)');
            uicontrol('parent', timeLagPanel,...
                'units', 'normalized',...
                'position', [0, 0, 1, 1],...
                'style', 'edit',...
                'string', num2str(self.TimeLag),...
                'callback', @self.SetTimeLag);
            
            frameRatePanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [.15, 0, .1, 1],...
                'title', 'Frame rate (ms)');
            uicontrol('parent', frameRatePanel,...
                'units', 'normalized',...
                'position', [0, 0, 1, 1],...
                'style', 'edit',...
                'string', num2str(1000 * self.FrameRate),...
                'callback', @self.SetFrameRate);
            
            % Wavemap settings
            wavemapControlPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0 .5 .2 .5]);
            
            uicontrol('parent', wavemapControlPanel,...
                'units', 'normalized',...
                'position', [.1 .85 .8 .1],...
                'style', 'pushbutton',...
                'string', 'Compute wave map',...
                'callback', @self.ComputeWavemap);
            
            uicontrol('parent', wavemapControlPanel,...
                'units', 'normalized',...
                'position', [.1 .7 .5 .07],...
                'style', 'text',...
                'string', 'Minimal conduction (cm/s)');
            uicontrol('parent', wavemapControlPanel,...
                'units', 'normalized',...
                'position', [.7 .7 .2 .1],...
                'style', 'edit',...
                'string', num2str(100 * self.WavemapCalculator.ConductionThreshold),...
                'callback', @self.SetConductionThreshold);
            
            uicontrol('parent', wavemapControlPanel,...
                'units', 'normalized',...
                'position', [.1 .6 .5 .07],...
                'style', 'text',...
                'string', 'Merge threshold (%)');
            uicontrol('parent', wavemapControlPanel,...
                'units', 'normalized',...
                'position', [.7 .6 .2 .1],...
                'style', 'edit',...
                'string', num2str(100 * self.WavemapCalculator.MergeOverlapThreshold),...
                'callback', @self.SetMergeOverlapThreshold);
            
            uicontrol('parent', wavemapControlPanel,...
                'units', 'normalized',...
                'position', [.1 .4 .8 .1],...
                'style', 'pushbutton',...
                'string', 'Create deflection video',...
                'callback', @self.CreateDeflectionVideo);
            
            uicontrol('parent', wavemapControlPanel,...
                'units', 'normalized',...
                'position', [.1 .3 .8 .1],...
                'style', 'pushbutton',...
                'string', 'Create wavemap video',...
                'callback', @self.CreateWavemapVideo);
            
            uicontrol('parent', wavemapControlPanel,...
                'units', 'normalized',...
                'position', [.1 .2 .8 .1],...
                'style', 'pushbutton',...
                'string', 'Create signal video',...
                'callback', @self.CreateSignalVideo);
                       
            uicontrol('parent', wavemapControlPanel,...
                'units', 'normalized',...
                'position', [.5 .05 .4 .1],...
                'style', 'pushbutton',...
                'string', 'Load from .mat',...
                'callback', @self.LoadWavemap);
            
            uicontrol('parent', wavemapControlPanel,...
                'units', 'normalized',...
                'position', [.05 .05 .4 .1],...
                'style', 'pushbutton',...
                'string', 'Save to .mat',...
                'callback', @self.SaveWavemap);
        end
        
        function CreateSignalPanel(self)
            self.SignalPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.2 .85 .8 .15],...
                'borderType', 'none');
            self.SignalAxes = axes('parent', self.SignalPanel,...
                'units', 'normalized',...
                'outerPosition', [0 0 1 1]);
        end
        
        function CreateAnimationPanel(self)
            self.DeflectionMapControl = ExtendedUIPkg.GridActivationMapControl([.2 .05 .4 .8]);
            self.DeflectionMapControl.Name = 'Deflections';
            self.DeflectionMapControl.Create(self.ControlHandle);
            addlistener(self.DeflectionMapControl, 'MapClicked', @self.HandleMapClick);
            self.DeflectionMapControl.Show();
            
            deflectionColormap = hot(256 + 10);
            deflectionColormap = deflectionColormap(6:(end-5), :);
            self.DeflectionMapControl.SetColormap(deflectionColormap, 256);
            
            self.NewWavemapControl = ExtendedUIPkg.GridWavemapControl([.6 .05 .4 .8]);
            self.NewWavemapControl.Name = 'Waves';
            self.NewWavemapControl.Create(self.ControlHandle);
            addlistener(self.NewWavemapControl, 'MapClicked', @self.HandleMapClick);
            self.NewWavemapControl.Show();
            
            self.EcgMapControl = ExtendedUIPkg.SignalMapControl([0 0 .2 .5]);
            self.EcgMapControl.Name = 'Signal';
            self.EcgMapControl.Create(self.ControlHandle);
            self.EcgMapControl.Show();
        end
        
        function InitializeSignalMapAnimation(self)
            self.EcgMapControl.SamplingFrequency = self.EcgData.SamplingFrequency;
            if isempty(self.EcgData.MappedData)
                self.EcgData.CreateMappedDataFromPositions();
            end
            self.EcgMapControl.SetData(self.EcgData.MappedData, self.EcgData.ElectrodePositions);
            self.EcgMapControl.Show();
        end
        
        function InitializeDeflectionAnimation(self)
            self.DeflectionMapControl.SamplingFrequency = self.EcgData.SamplingFrequency;
            self.DeflectionMapControl.SetData(self.EcgData.Activations, self.EcgData.ElectrodePositions);
            self.DeflectionMapControl.Show();
        end
        
        function InitializeWavemapAnimation(self)
            self.NewWavemapControl.SamplingFrequency = self.EcgData.SamplingFrequency;
            self.NewWavemapControl.SetData(self.DetectedWaves, self.EcgData.ElectrodePositions);
            self.NewWavemapControl.Show();
        end
                      
        function InitializeSignalAxes(self)
            if isempty(self.EcgData), return; end
            
            signal = self.EcgData.Data(:, self.SelectedChannelIndex);
            time = self.EcgData.GetTimeRange();
            
            if ishandle(self.FarFieldDeflectionLine)
                delete(self.FarFieldDeflectionLine);
            end
            self.FarFieldDeflectionLine = line(...
                'xData', [], 'yData', [],...
                'color', [0 0 1],...
                'lineWidth', 2,...
                'parent', self.SignalAxes);
            
            if ishandle(self.IntrinsicDeflectionLine)
                delete(self.IntrinsicDeflectionLine);
            end
            self.IntrinsicDeflectionLine = line(...
                'xData', [], 'yData', [],...
                'color', [1 0 0],...
                'lineWidth', 3,...
                'parent', self.SignalAxes);
            
            if ishandle(self.ReferenceLine)
                delete(self.ReferenceLine);
            end
            self.ReferenceLine = line(...
                'xData', [], 'yData', [],...
                'color', [.7 .7 .7],...
                'lineWidth', 1,...
                'parent', self.SignalAxes);
            
            if ishandle(self.SignalLine)
                delete(self.SignalLine);
            end
            self.SignalLine = line('parent', self.SignalAxes,...
                'xData', time, 'yData', signal, 'color', [0 0 0], 'lineSmoothing', 'on');
            
            if ishandle(self.AnimationPositionLine)
                delete(self.AnimationPositionLine);
            end
            self.AnimationPositionLine = line(...
                'xData', [0 0], 'yData', [0 0],...
                'color', [1 0 0],...
                'lineWidth', 1,...
                'parent', self.SignalAxes);
            
            if ishandle(self.SelectedTimeLine)
                delete(self.SelectedTimeLine);
            end
            self.SelectedTimeLine = line(...
                'xData', [0 0], 'yData', [0 0],...
                'color', [1 0 0],...
                'lineWidth', 2,...
                'lineStyle', ':',...
                'parent', self.SignalAxes);
            
            self.SelectedChannelText = text(...
                'units', 'normalized',...
                'position', [0, 1],...
                'string', '',...
                'verticalAlignment', 'top',...
                'parent', self.SignalAxes,...
                'backgroundColor', [1 1 1],...
                'edgeColor', [0 0 0]);
            
            set(self.SignalAxes, 'LooseInset', get(self.SignalAxes, 'TightInset'));
            
            self.ShowSelectedChannel();
        end
        
        function ShowSelectedChannel(self)
            % signal
            signal = self.EcgData.Data(:, self.SelectedChannelIndex);
            time = self.EcgData.GetTimeRange();
            
            set(self.SignalLine, 'yData', signal / 1000);
            
            yLimits = [min(signal) max(signal)] / 1000;
            
            % reference signal
            if ~isempty(self.EcgData.ReferenceData)
                referenceData = self.EcgData.ReferenceData(:, 1);
                referenceData = (referenceData - min(referenceData)) / (max(referenceData) - min(referenceData));
                referenceData = referenceData * (yLimits(end) - yLimits(1)) + yLimits(1);
                set(self.ReferenceLine, 'xData', time, 'yData', referenceData);
                uistack(self.ReferenceLine, 'bottom');
            end
            
            deflectionData = self.IntrinsicDeflectionData{self.SelectedChannelIndex};
            
            % intrinsic deflections
            [xData yData] = DeflectionECGChannelPanel.ComputeDeflectionLineData(...
                time, signal,...
                deflectionData.templates,...
                deflectionData.templatePeakTemplates(deflectionData.PrimaryDeflections, :),...
                deflectionData.templatePeakRanges(deflectionData.PrimaryDeflections, :),...
                deflectionData.templatePeakCoefficients(deflectionData.PrimaryDeflections, :));
            
            set(self.IntrinsicDeflectionLine, 'xData', xData, 'yData', yData / 1000);
            
            % far-field deflections
            [xData yData] = DeflectionECGChannelPanel.ComputeDeflectionLineData(...
                time, signal,...
                deflectionData.templates,...
                deflectionData.templatePeakTemplates(~deflectionData.PrimaryDeflections, :),...
                deflectionData.templatePeakRanges(~deflectionData.PrimaryDeflections, :),...
                deflectionData.templatePeakCoefficients(~deflectionData.PrimaryDeflections, :));
            
            set(self.FarFieldDeflectionLine, 'xData', xData, 'yData', yData / 1000);
            
            set(self.SignalAxes, 'yLim', yLimits);
            
            yLimits = get(self.SignalAxes, 'yLim');
            set(self.AnimationPositionLine, 'yData', yLimits);
            set(self.SelectedTimeLine,...
                'xData', [self.SelectedTime, self.SelectedTime], 'yData', yLimits);
            
            set(self.SelectedChannelText, 'string',...
                ['Channel: ', self.EcgData.ElectrodeLabels{self.SelectedChannelIndex}]);
        end
        
        function SetSignalWindow(self, position)
            timePosition = position / self.EcgData.SamplingFrequency;
            timeWindow = [timePosition - 0.5, timePosition + 0.5];
            xTickSpacing = 0.1;
            xTicks = (timeWindow(1) + (xTickSpacing - mod(timeWindow(1), xTickSpacing))):(xTickSpacing):(timeWindow(end) - mod(timeWindow(end), xTickSpacing));
            
            set(self.SignalAxes,...
                'xLim', timeWindow,...
                'xTick', xTicks,...
                'xMinorTick', 'on');
            
            yLimits = get(self.SignalAxes, 'yLim');
            set(self.AnimationPositionLine, 'xData', [timePosition, timePosition], 'yData', yLimits);
        end
        
        function SetTimeLag(self, source, varargin)
            self.TimeLag = str2double(get(source, 'string'));
            
            self.NewWavemapControl.SetTimeLag(self.TimeLag);
            self.EcgMapControl.SetTimeLag(self.TimeLag);
            self.DeflectionMapControl.SetTimeLag(self.TimeLag);
        end
        
        function SetFrameRate(self, source, varargin)
            self.FrameRate = round(str2double(get(source, 'string'))) / 1000;
        end
        
        function SetConductionThreshold(self, source, varargin)
            value = str2double(get(source, 'string')) / 100;
            if value > 0
                self.WavemapCalculator.ConductionThreshold = value;
            end
            
            set(source, 'string', num2str(100 * self.WavemapCalculator.ConductionThreshold));
        end
        
        function SetMergeOverlapThreshold(self, source, varargin)
            value = str2double(get(source, 'string')) / 100;
            if value >= 0
                self.WavemapCalculator.MergeOverlapThreshold = value;
            end
            
            set(source, 'string', num2str(100 * self.WavemapCalculator.MergeOverlapThreshold));
        end
        
        function HandleMapClick(self, ~, eventData)
            self.SelectedChannelIndex = eventData.Position;
            self.SelectedTime = eventData.Time;
            self.ShowSelectedChannel();
        end
        
        function ControlAnimation(self, varargin)
            if isempty(self.EcgData), return; end
            
            set(self.AnimationButton, 'string', '||');
            numberOfSamples = self.EcgData.GetNumberOfSamples();
            
            frameStep = round(self.FrameRate * self.EcgData.SamplingFrequency);
            
%             set(self.EcgMapControl.SignalImage, 'eraseMode', 'none');
            startPosition = self.AnimationPosition;
            while get(self.AnimationButton, 'value')
                drawnow;
                self.AnimationPosition = self.AnimationPosition + frameStep;
                if self.AnimationPosition > numberOfSamples
                    self.AnimationPosition = 1;
                end
                set(self.AnimationSlider, 'value', self.AnimationPosition);
                self.SetAnimationFrame();
                
                if mod((self.AnimationPosition - startPosition) / frameStep, 10) == 0
                    set(self.AnimationSlider, 'value', self.AnimationPosition);
                    self.SetAnimationInfo();
                end
            end
            
%             set(self.EcgMapControl.SignalImage, 'eraseMode', 'normal');
            
            set(self.AnimationButton, 'string', '>');
            
            self.SetStaticFrame();
        end
        
        function AnimationSliderCallback(self, varargin)
            self.AnimationPosition = round(get(self.AnimationSlider, 'value'));
            self.SetStaticFrame();
        end
        
        function ContinuousAnimationSliderCallback(self, source, eventData)
            if verLessThan('matlab', '8.4.0')
                sliderObject = get(eventData, 'AffectedObject');
            else
                sliderObject = eventData.AffectedObject;
            end
            self.AnimationPosition = round(get(sliderObject, 'Value'));
            self.SetAnimationFrame();
            self.SetSignalWindow(self.AnimationPosition);
        end
        
        function SetStaticFrame(self)
            self.SetAnimationInfo();
            
            self.EcgMapControl.SetStaticFramePosition(self.AnimationPosition);
            self.DeflectionMapControl.SetStaticFramePosition(self.AnimationPosition);
            self.NewWavemapControl.SetStaticFramePosition(self.AnimationPosition);
            
            self.SetSignalWindow(self.AnimationPosition);
        end
        
        function SetAnimationFrame(self)
            self.SetAnimationInfo();
            
            self.EcgMapControl.SetFramePosition(self.AnimationPosition);
            self.DeflectionMapControl.SetFramePosition(self.AnimationPosition);
            self.NewWavemapControl.SetFramePosition(self.AnimationPosition);
        end
        
        function SetAnimationInfo(self)
            timePosition = round(1000 * self.AnimationPosition / self.EcgData.SamplingFrequency);
            set(self.AnimationPositionLabel, 'string', [ num2str(timePosition) ' ms']);
        end
        
        function ComputeWavemap(self, varargin)
            if isempty(self.IntrinsicDeflectionData), return; end
            
            [self.DetectedWaves, self.WaveMemberships, self.WaveStartingPoints] =...
                self.WavemapCalculator.DetectWaves(self.EcgData, self.IntrinsicDeflectionData);
            
            self.InitializeWavemapAnimation();
            
            notify(self, 'WavemapComputed');
        end
        
        function LoadPacemapWavemap(self, varargin)
            [fileName, pathName, filterIndex] = HelperFunctions.customUigetfile({...
                '*Wave_Info.ASC', 'Wave Info file';...
                '*.*', 'All Files'},...
                'Select a PACEMAP wave map file');
            
            [result, waveMemberships, waveActivations electrodeMapName] =...
                PACEMAPWaveMapPanel.ReadPacemapWaveData(fullfile(pathName, fileName));
            
            waveActivations = waveActivations(:, 3:end);
            % SHIFT?
            waveActivations = waveActivations + 5;
            waveMembership = waveMemberships(:, 3:end);
            startDC = (waveMembership > 3000);
            startBT = (waveMembership > 2000) & (waveMembership < 3000);
            startPW = (waveMembership > 1000) & (waveMembership < 2000);
            waveStarts = struct(...
                'peripheral', startPW,...
                'breakthrough', startBT,...
                'discontinuousConduction', startDC);
            
            waveMembership = waveMembership - startDC * 3000;
            waveMembership = waveMembership - startBT * 2000;
            waveMembership = waveMembership - startPW * 1000;
            
            electrodeLabels = cellstr(num2str((1:size(waveActivations, 1))'));
            self.PacemapData = DCMData(fileName, NaN(1, size(waveActivations, 1)), 1e3, electrodeLabels);
            self.PacemapData.SetElectrodeMap(electrodeMapName);
            self.PacemapActivations = waveActivations / 1e3;
            
            waveIDs = unique(waveMembership(~isnan(waveMembership)));
            self.PacemapWaves = AlgorithmPkg.Wave.empty(numel(waveIDs), 0);
            for waveIndex = 1:numel(waveIDs)
                waveMembers = (waveMembership == waveIDs(waveIndex));
                [rowPositions ~] = find(waveMembers);
                waveMembers = find(waveMembers);
                self.PacemapWaves(waveIndex) = AlgorithmPkg.Wave(waveIndex, self.PacemapData.ElectrodePositions);
                for waveMemberIndex = 1:numel(waveMembers)
                    self.PacemapWaves(waveIndex).Add(rowPositions(waveMemberIndex),...
                        NaN,...
                        waveActivations(waveMembers(waveMemberIndex)));
                end
            end
            
            self.InitializePacemapAnimation();
        end
        
        function SaveWavemap(self, varargin)
            [pathString, nameString] = fileparts(self.EcgData.Filename);
            filename = [pathString, filesep, nameString '_WM'];
            ecgData = self.EcgData; %#ok<NASGU>
            deflectionData = self.IntrinsicDeflectionData; %#ok<NASGU>
            detectedWaves = self.DetectedWaves; %#ok<NASGU>
            waveMemberships = self.WaveMemberships; %#ok<NASGU>
            wavemapCalculator = self.WavemapCalculator; %#ok<NASGU>
            
            save(filename,...
                'ecgData',...
                'deflectionData',...
                'detectedWaves',...
                'waveMemberships',...
                'wavemapCalculator');
        end
        
        function LoadWavemap(self, varargin)
            if isempty(self.EcgData)
                [filename pathname] = HelperFunctions.customUigetfile('*_WM.mat', 'Select WaveMap file');
                if isequal(filename, 0) || isequal(pathname, 0), return; end
                filename = fullfile(pathname, filename);
            else
                [pathString nameString] = fileparts(self.EcgData.Filename);
                filename = [pathString, filesep, nameString '_WM.mat'];
            end
            
            self.LoadData(filename);
        end
        
        function CreateDeflectionVideo(self, varargin)
            [filename, pathname] = HelperFunctions.customUiputfile('*.avi', 'Save video as...');
            
            if isequal(filename, 0) || isequal(pathname, 0), return; end
            filename = fullfile(pathname, filename);
            
            waitbarHandle = waitbar(0 ,'Writing deflection video'); 
            
            writer = VideoWriter(filename);
            writer.FrameRate = 25;
            writer.Quality = 100;
            open(writer);
            
            numberOfSamples = self.EcgData.GetNumberOfSamples();
            frameStep = round(self.FrameRate * self.EcgData.SamplingFrequency);
            for sampleIndex = 1:frameStep:numberOfSamples
                colorData = self.DeflectionMapControl.GetFrame(sampleIndex);
                colorData = imresize(colorData, 10, 'nearest');
                colorData(colorData > 1) = 1;
                colorData(colorData < 0) = 0;
                writeVideo(writer, im2frame(colorData));
                
                if mod(sampleIndex, 100) == 1
                    waitbar(sampleIndex / numberOfSamples,...
                        waitbarHandle,...
                        ['Writing deflection video, sample ' ...
                        num2str(sampleIndex) ' of ' ...
                        num2str(numberOfSamples)]);
                end
            end
            close(waitbarHandle);
            close(writer);
        end
               
        function CreateWavemapVideo(self, varargin)
            [filename, pathname] = HelperFunctions.customUiputfile('*.avi', 'Save video as...');
            
            if isequal(filename, 0) || isequal(pathname, 0), return; end
            filename = fullfile(pathname, filename);
            
            waitbarHandle = waitbar(0 ,'Writing wavemap video'); 
            
            writer = VideoWriter(filename);
            writer.FrameRate = 25;
            writer.Quality = 100;
            open(writer);
            
            numberOfSamples = self.EcgData.GetNumberOfSamples();
            frameStep = round(self.FrameRate * self.EcgData.SamplingFrequency);
            for sampleIndex = 1:frameStep:numberOfSamples
                colorData = self.NewWavemapControl.GetFrame(sampleIndex);
                
                colorData = imresize(colorData, 10, 'nearest');
                colorData(colorData > 1) = 1;
                colorData(colorData < 0) = 0;
                writeVideo(writer, im2frame(colorData));
                
                if mod(sampleIndex, 100) == 1
                    waitbar(sampleIndex / numberOfSamples,...
                        waitbarHandle,...
                        ['Writing wavemap video, sample ' ...
                        num2str(sampleIndex) ' of ' ...
                        num2str(numberOfSamples)]);
                end
            end
            close(waitbarHandle);
            close(writer);
        end
        
        function CreateSignalVideo(self, varargin)
            [filename, pathname] = HelperFunctions.customUiputfile('*.avi', 'Save video as...');
            
            if isequal(filename, 0) || isequal(pathname, 0), return; end
            filename = fullfile(pathname, filename);
            
            self.InitializeSignalAxes();
            self.ShowSelectedChannel();
            
            axis(self.SignalAxes, 'tight');
            axis(self.SignalAxes, 'manual');
            yLimits = get(self.SignalAxes, 'yLim');
            
            set(self.SignalAxes,...
                    'xTickMode', 'auto',...
                    'xMinorTick', 'off');
            
            writer = VideoWriter(filename);
            writer.FrameRate = 25;
            writer.Quality = 100;
            open(writer);
            
            waitbarHandle = waitbar(0 ,'Writing signal video');
            numberOfSamples = self.EcgData.GetNumberOfSamples();
            frameStep = round(self.FrameRate * self.EcgData.SamplingFrequency);
            for sampleIndex = 1:frameStep:numberOfSamples
                timePosition = sampleIndex / self.EcgData.SamplingFrequency;
                set(self.AnimationPositionLine, 'xData', [timePosition, timePosition], 'yData', yLimits);
            
                frame = getframe(self.SignalAxes);
                writeVideo(writer, frame);
                
                if mod(sampleIndex, 100) == 1
                    waitbar(sampleIndex / numberOfSamples,...
                        waitbarHandle,...
                        ['Writing signal video, sample ' ...
                        num2str(sampleIndex) ' of ' ...
                        num2str(numberOfSamples)]);
                end
            end
            close(waitbarHandle);
            close(writer);
        end
    end
end
classdef RepolarizationDetectionPanel < DeflectionDetectionTestPanel
    properties
        DetectedRepolarizations
    end
    
    properties (Access = protected)
        BeatRange
        RepolarizationLineHandles
    end
    
    methods
        function self = RepolarizationDetectionPanel(position)
            self = self@DeflectionDetectionTestPanel(position);
            self.BeatRange = [0.2, 0.2];
        end
        
        function Create(self, parentHandle)
            Create@DeflectionDetectionTestPanel(self, parentHandle);
            
        end

        function SetDepolarizations(self, detectedDepolarizations)
            self.DetectedDeflections = detectedDepolarizations;
            self.DetectedHighEnds = cell(size(detectedDepolarizations));
            self.DetectedLowEnds = cell(size(detectedDepolarizations));

            self.ShowDeflections();
            self.ShowAFCL();
        end
        
        function ecgData = LoadData(self, filename)
            ecgData = LoadData@DeflectionDetectionTestPanel(self, filename);
           
            ddData = load(filename);
            if isfield(ddData, 'detectedRepolarizations')
                self.DetectedRepolarizations = ddData.detectedRepolarizations;
                self.ShowRepolarization();

                notify(self, 'DeflectionDetectionCompleted');
            end
        end
    end
    
    methods (Access = protected)
        function CreateDetectionControls(self)
            CreateDetectionControls@DeflectionDetectionTestPanel(self);

            set(self.MinimumDeflectionDistancedEdit, 'Enable', 'off');
            set(self.MinimumAmplitudeThresholdEdit, 'Enable', 'off');
        end

        function ShowECGData(self)
            ShowECGData@DeflectionDetectionTestPanel(self);
            
            self.ShowRepolarization();
        end
    end
    
    methods (Access = protected)
        function DetectDeflections(self, varargin)
            switch self.SelectedDetectionMethod
                case 'DvDt'
                    [detectedDeflections, ~, ~] =...
                        self.DetectionDeflectionsDvDt();
                case 'NLE'
                    [detectedDeflections, ~, ~] =...
                        self.DetectDeflectionsNLE();
            end
            
            self.DetectedRepolarizations = detectedDeflections;
            
            self.ShowRepolarization();
            
            notify(self, 'DeflectionDetectionCompleted');
        end
        
        function [detectedDeflections, detectedHighEnds, detectedLowEnds] = ...
                DetectionDeflectionsDvDt(self)

            numberOfChannels = self.EcgData.GetNumberOfChannels();
            numberOfSamples = self.EcgData.GetNumberOfSamples();
            detectedDeflections = cell(numberOfChannels, 1);
            detectedHighEnds = cell(numberOfChannels, 1);
            detectedLowEnds = cell(numberOfChannels, 1);
            minPeakDistance = round(self.MinimumDeflectionDistance  * self.EcgData.SamplingFrequency);
            samplingFrequency = self.EcgData.SamplingFrequency;

            % SG filter to smooth signal
            sgWindowSampleLength = round(0.02 * samplingFrequency);
            if mod(sgWindowSampleLength, 2) == 0
                sgWindowSampleLength = sgWindowSampleLength + 1;
            end
            filteredData = sgolayfilt(self.EcgData.Data, 1, sgWindowSampleLength, [], 1);
            preprocessedData = filteredData;
            self.PreprocessedData = preprocessedData;
            
            for channelIndex = 1:numberOfChannels
                channelData = gradient(preprocessedData(:, channelIndex));
                [peakSlope, peakIndices] = findpeaks(channelData);

                beatIndices = self.DetectedDeflections{channelIndex};
                numberOfBeats = numel(beatIndices);
                channelRepolarizations = NaN(numberOfBeats - 1, 1);
                for beatIndex = 1:numberOfBeats - 1
                    beatStart = beatIndices(beatIndex);
                    beatEnd = beatIndices(beatIndex + 1);
                    % ignore edges - BeatRange
                    beatLength = beatEnd - beatStart + 1;
                    indicesToIgnore = beatLength * self.BeatRange;
                    validPeakIndices = peakSlope > 0 &...
                        peakIndices > beatStart + indicesToIgnore(1) &...
                        peakIndices < beatEnd - indicesToIgnore(end);
                    beatPeakIndices = peakIndices(validPeakIndices);
                    beatPeakSlope = peakSlope(validPeakIndices);
                    if any(validPeakIndices)
                        [~, maxIndex] = max(beatPeakSlope);
                        channelRepolarizations(beatIndex) = beatPeakIndices(maxIndex);
                    end
                end
                detectedDeflections{channelIndex} = channelRepolarizations;
            end
        end
        
        function ShowRepolarization(self)
            validHandles = ishandle(self.RepolarizationLineHandles);
            if any(validHandles)
                delete(self.RepolarizationLineHandles(validHandles));
            end
            
            if isempty(self.DetectedRepolarizations), return; end
            
            lineData = self.EcgData.Data(:, self.SelectedChannels);
            resultData = self.DetectedRepolarizations(self.SelectedChannels);
            time = self.EcgData.GetTimeRange();
            
            self.RepolarizationLineHandles = NaN(size(resultData));
            for channelIndex = 1:numel(resultData)
                currentResult = resultData{channelIndex};
                currentResult = currentResult(~isnan(currentResult));
                signal = lineData(:, channelIndex);
                
                self.RepolarizationLineHandles(channelIndex) = line('xData', time(currentResult),...
                    'yData', signal(currentResult),...
                    'lineStyle', 'none',...
                    'marker', 'o', 'markerSize', 7,...
                    'markerEdgeColor', [0 0 0], 'markerFaceColor', [.7 .7 .7],...
                    'parent', self.AxesHandles(channelIndex));
            end
        end

        function SaveDeflectionDetectionTest(self, varargin)
            SaveDeflectionDetectionTest@DeflectionDetectionTestPanel(self, varargin);
            [pathString, nameString] = fileparts(self.EcgData.Filename);
            filename = [pathString, filesep, nameString '_DDPreview'];
            
            detectedRepolarizations = self.DetectedRepolarizations;
            
            save(filename,...
                'detectedRepolarizations', '-append');
        end
    end
end
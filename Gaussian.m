function y = Gaussian(beta,x)
a = beta(1);
mu = beta(2);
sigma = beta(3);
y = a.*exp(-((x-mu).^2/(2*sigma^2)));
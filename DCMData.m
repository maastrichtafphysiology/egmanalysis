classdef DCMData < ECGData
    properties
        ElectrodeMap
        MappedData
        Activations % unit:milliseconds!
        ElectrodePositions
        Deflections
        Waves
    end
    
    properties (Access = protected)
        LeadIDs
    end
    
    properties (Constant)
        AvailableElectrodeMaps = DCMData.LoadAvailableElectrodeMaps();
    end
    
    methods
        function self = DCMData(filename, data, samplingFrequency, varargin)
            self = self@ECGData(filename, data, samplingFrequency, varargin{:});
            self.LeadIDs = 1:self.GetNumberOfChannels();
            self.Waves = AlgorithmPkg.Wave.empty(1, 0);
        end
        
        function SetElectrodeMap(self, electrodeMapName)
            self.CreateMappedData(electrodeMapName);
            if ispc
                pause(1);
            end
            self.SetElectrodePositions();
            
            validChannels = ~isnan(self.ElectrodePositions(:, 1));
            self.Data = self.Data(:, validChannels);
            self.ElectrodePositions = self.ElectrodePositions(validChannels, :);
            self.LeadIDs = self.LeadIDs(validChannels);
            self.ElectrodeLabels = self.ElectrodeLabels(validChannels);
        end
        
        function CreateMappedDataFromPositions(self)
            maxPosition = max(self.ElectrodePositions);
            minPosition = min(self.ElectrodePositions);
            minimumSpacing = self.ComputeMinimumSpacing();
            
            xData = minPosition(1):minimumSpacing:maxPosition(1);
            yData = minPosition(2):minimumSpacing:maxPosition(2);
            
            tolerance = 1e3 * eps;
            [xMembers, xIndices] = UtilityPkg.ismemberf(self.ElectrodePositions(:, 1), xData, 'tol', tolerance); %#ok<ASGLU>
            [yMembers, yIndices] = UtilityPkg.ismemberf(self.ElectrodePositions(:, 2), yData, 'tol', tolerance); %#ok<ASGLU>
            
            self.MappedData = NaN(numel(yData), numel(xData), self.GetNumberOfSamples);
            for channelIndex = 1:self.GetNumberOfChannels
                self.MappedData(yIndices(channelIndex), xIndices(channelIndex), :) = self.Data(:, channelIndex);
            end
        end
        
        function SetActivations(self, analysisResult, useAllDeflections)
            if nargin < 3
                useAllDeflections = false;
            end
            
            if isempty(self.ElectrodePositions), return; end
            
            self.CreateActivationData(analysisResult, useAllDeflections);
        end
        
        function AddReferenceChannel(self, selectedChannels)
            referenceData = self.Data(:, selectedChannels);
            referenceLabels = self.ElectrodeLabels(selectedChannels);
            
            self.Data(:, selectedChannels) = [];
            self.ElectrodeLabels(selectedChannels) = [];
            
            self.ReferenceData = [self.ReferenceData, referenceData];
            self.ReferenceLabels = [self.ReferenceLabels; referenceLabels(:)];
            
            self.LeadIDs(selectedChannels) = [];
            self.ElectrodePositions(selectedChannels, :) = [];
        end
        
        function dcmDataCopy = Copy(self, selectedChannels, timeRange)
            time = self.GetTimeRange();
            if nargin < 3
                timeRange = [time(1) time(end)];
                if nargin < 2
                    selectedChannels = true(size(self.Data, 2), 1);
                end
            end
            validTime = time >= timeRange(1) & time <= timeRange(2);
            
            data = self.Data(validTime, selectedChannels);
            labels = self.ElectrodeLabels(selectedChannels);
            dcmDataCopy = DCMData(self.Filename, data,...
                self.SamplingFrequency,...
                labels);
            
            if ~isempty(self.ReferenceData)
                dcmDataCopy.ReferenceData = self.ReferenceData(validTime, :);
                dcmDataCopy.ReferenceLabels = self.ReferenceLabels;
            end
            
            dcmDataCopy.Type = self.Type;
            
            dcmDataCopy.ElectrodeMap = self.ElectrodeMap;
            
            if ~isempty(self.MappedData)
                dcmDataCopy.MappedData = self.MappedData(:, :, validTime);
            end
            
            dcmDataCopy.LeadIDs = self.LeadIDs(selectedChannels);
            
            if ~isempty(self.ElectrodePositions)
                dcmDataCopy.ElectrodePositions = self.ElectrodePositions(selectedChannels, :);
            end
            
            if ~isempty(self.Activations)
                dcmDataCopy.Activations = self.Activations(selectedChannels);
                for channelIndex = 1: numel(dcmDataCopy.Activations)
                    channelActivations = dcmDataCopy.Activations{channelIndex};
                    validActivations = channelActivations / 1e3 >= timeRange(1) &...
                        channelActivations / 1e3 <= timeRange(end);
                    dcmDataCopy.Activations{channelIndex} = channelActivations(validActivations);
                end
            end
        end
        
        function edgeElectrodes = GetEdgeElectrodes(self)
            if isempty(self.ElectrodePositions),
                edgeElectrodes = [];
                return;
            end
            
            minimumSpacing = self.ComputeMinimumSpacing();
            maxPosition = max(self.ElectrodePositions);
            minPosition = min(self.ElectrodePositions);
            xData = minPosition(1):minimumSpacing:maxPosition(1);
            yData = minPosition(2):minimumSpacing:maxPosition(2);
            
            tolerance = 1e3 * eps;
            [xMembers, xIndices] = UtilityPkg.ismemberf(self.ElectrodePositions(:, 1), xData, 'tol', tolerance); %#ok<ASGLU>
            [yMembers, yIndices] = UtilityPkg.ismemberf(self.ElectrodePositions(:, 2), yData, 'tol', tolerance); %#ok<ASGLU>
            electrodeMapIndices = sub2ind([numel(yData), numel(xData)], yIndices, xIndices);
            pixelMap = zeros(numel(yData), numel(xData));
            pixelMap(electrodeMapIndices) = 1;
            [~, minXPosition] = min(self.ElectrodePositions(:, 1));
            [initialPointX, initialPointY] = ind2sub([numel(yData), numel(xData)], electrodeMapIndices(minXPosition));
            boundaryPoints = bwtraceboundary(pixelMap, [initialPointX, initialPointY], 'N', 4);
            boundaryPoints = sub2ind([numel(yData), numel(xData)], boundaryPoints(:, 1), boundaryPoints(:, 2));
            edgeElectrodes = ismember(electrodeMapIndices, boundaryPoints);
            
%             validElectrodes = find(~(any(isnan(self.ElectrodePositions), 2) | any(isinf(self.ElectrodePositions), 2)));
%             edgeElectrodes = false(numel(self.ElectrodePositions), 1);
%             electrodeIndices = convhull(self.ElectrodePositions(validElectrodes, 1), self.ElectrodePositions(validElectrodes, 2));
%             edgeElectrodes(validElectrodes(electrodeIndices)) = true;
        end
        
        function [electrodeGrid, electrodeGridIndices] = GetElectrodeGrid(self)
            if isempty(self.ElectrodePositions),
                electrodeGrid = [];
                return;
            end
            
            minimumSpacing = self.ComputeMinimumSpacing();
            maxPosition = max(self.ElectrodePositions);
            minPosition = min(self.ElectrodePositions);
            xData = minPosition(1):minimumSpacing:maxPosition(1);
            yData = minPosition(2):minimumSpacing:maxPosition(2);
            
            tolerance = 1e3 * eps;
            [xMembers, xIndices] = UtilityPkg.ismemberf(self.ElectrodePositions(:, 1), xData, 'tol', tolerance); %#ok<ASGLU>
            [yMembers, yIndices] = UtilityPkg.ismemberf(self.ElectrodePositions(:, 2), yData, 'tol', tolerance); %#ok<ASGLU>
            electrodeGridIndices = sub2ind([numel(yData), numel(xData)], yIndices, xIndices);
            electrodeGrid = false(numel(yData), numel(xData));
            electrodeGrid(electrodeGridIndices) = true;
        end
        
        function edgeElectrodes = GetEdgeElectrodesWaveBased(self)
            waveIncidenceMap = self.ComputeWaveIncidenceMap();
            minimumSpacing = self.ComputeMinimumSpacing();
            radius = sqrt(minimumSpacing^2 + minimumSpacing^2) + 1e3 * eps;
            
            neighbors = self.GetElectrodeNeighbors(1:self.GetNumberOfChannels, radius);
            
            validElectrodes = waveIncidenceMap > 0;
            neighbors = neighbors(validElectrodes, :);
            
            numberOfNeighbors = sum(neighbors);
            edgeElectrodes = numberOfNeighbors < 8;
        end
        
        function [electrodeBTCount, BTInfo] = ComputeBTCount(self)
            electrodeBTCount = zeros(size(self.ElectrodePositions, 1), 1);
            BTInfo(numel(electrodeBTCount), 1) = struct(...
                'waveID', [],...
                'time', []);
            
            if isempty(self.Waves), return; end
                
            for waveIndex = 1:numel(self.Waves)
                if self.Waves(waveIndex).Peripheral, continue; end
                startingPoints = self.Waves(waveIndex).GetAllStartingPoints();
                electrodeBTCount(startingPoints(:, 1)) = electrodeBTCount(startingPoints(:, 1)) + 1;
                
                for pointIndex = 1:size(startingPoints, 1)
                    BTInfo(startingPoints(pointIndex, 1)).waveID =...
                        [BTInfo(startingPoints(pointIndex, 1)).waveID; self.Waves(waveIndex).ID];
                    BTInfo(startingPoints(pointIndex, 1)).time =...
                        [BTInfo(startingPoints(pointIndex, 1)).time; startingPoints(pointIndex, 1)];
                end
            end
        end
        
        function minimumSpacing = ComputeMinimumSpacing(self)
            distanceMatrix = pdist(self.ElectrodePositions);
            distanceMatrix = distanceMatrix(distanceMatrix ~= 0);
            minimumSpacing = min(distanceMatrix);
        end
        
        function [neighbors, electrodeDistances] = GetElectrodeNeighbors(self, electrodeIndex, radius)
            neighbors = false(size(self.ElectrodePositions, 1), numel(electrodeIndex));
            electrodeDistances = NaN(size(self.ElectrodePositions, 1), numel(electrodeIndex));
            for index = 1:numel(electrodeIndex)
                electrodePosition = self.ElectrodePositions(electrodeIndex(index), : );
                positionDifferences = bsxfun(@minus, self.ElectrodePositions, electrodePosition);
                electrodeDistances(:, index) = sqrt(positionDifferences(:, 1).^2 + positionDifferences(:, 2).^2 + positionDifferences(:, 3).^2);
                neighbors(:, index) = electrodeDistances(:, index) <= radius + 1e3*eps;
            end
        end
        
        function SetWaveType(self)
            if isempty(self.Waves), return; end
            
            waveIncidenceMap = self.ComputeWaveIncidenceMap();
            minimumSpacing = self.ComputeMinimumSpacing();
            radius = sqrt(minimumSpacing^2 + minimumSpacing^2) + 1e3 * eps;
            
            neighbors = self.GetElectrodeNeighbors(1:self.GetNumberOfChannels, radius);
            
            validElectrodes = waveIncidenceMap > 0;
            neighbors = neighbors(validElectrodes, :);
            
            numberOfNeighbors = sum(neighbors);
            edgeElectrodes = numberOfNeighbors < 8;
            
            for waveIndex = 1:numel(self.Waves)
                self.Waves(waveIndex).Peripheral = self.Waves(waveIndex).IsPeripheral(edgeElectrodes);
            end
        end
        
        function waveIncidenceMap = ComputeWaveIncidenceMap(self)
            waveIncidenceMap = zeros(size(self.ElectrodePositions, 1), 1);
            for waveIndex = 1:numel(self.Waves)
                waveIncidenceMap(self.Waves(waveIndex).Members(:,1)) =...
                    waveIncidenceMap(self.Waves(waveIndex).Members(:,1)) + 1;
            end
        end
        
        function delete(self)
            delete(self.Waves);
        end
        
        function phaseData = ComputePhase(self, varargin)
            % Sinusoidal recomposition + Hilbert transform
            
            afComplexityCalculator = AlgorithmPkg.AFComplexityCalculator();
            afComplexityCalculator.DFParameters.DFRange = [0.5, 20];
            afComplexityCalculator.DFParameters.NumberOfFFTPoints = 2^14;
            
            dcmFilter = true;
            dominantFrequencies = afComplexityCalculator.ComputeDCMDominantFrequency(self, dcmFilter);
            signalPeriodSamples = round((1 ./ dominantFrequencies) * self.SamplingFrequency);
            
            signals = self.Data;
            recomposedSignals = zeros(size(self.Data));
            
            for channelIndex = 1:self.GetNumberOfChannels()
                % create sinusoid
                period = signalPeriodSamples(channelIndex);
                sinusoidWavelet = zeros(period + 1, 1);
                for sampleIndex = 1:period
                    sinusoidWavelet(sampleIndex) = sin(2 * pi * (sampleIndex - period / 2) / period);
                end
                
                currentSignal = signals(:, channelIndex);
                recomposedSignal = zeros(size(currentSignal));
                for sampleIndex = 2:numel(recomposedSignal) - 1
                    
                    localDifference = currentSignal(sampleIndex + 1) - currentSignal(sampleIndex -  1);
                    if localDifference < 0
                        for tt = -floor(period / 2):floor(period / 2)
                            if (sampleIndex + tt) > 0 && (sampleIndex + tt) < numel(currentSignal)
                                recomposedSignal(sampleIndex + tt)= recomposedSignal(sampleIndex + tt) + ...
                                    localDifference * sinusoidWavelet(floor(tt + period / 2 + 1));
                            end
                        end
                    end
                end
                
                recomposedSignals(:, channelIndex) = recomposedSignal;
            end
            
            hilbertTransform = hilbert(recomposedSignals);
            phaseData = atan2(real(hilbertTransform), -imag(hilbertTransform)); 
        end
    end
    
    methods (Access = protected)
        function CreateMappedData(self, electrodeMapName)
            electrodeMapPosition = find(strcmpi(electrodeMapName, DCMData.AvailableElectrodeMaps(:, 1)), 1, 'first');
            if isempty(electrodeMapPosition)
                figurePosition = get(gcf, 'position');
                figurePosition = [...
                    figurePosition(1) + figurePosition(3) / 3,...
                    figurePosition(2) + figurePosition(4) / 3,...
                    figurePosition(3) / 3,...
                    figurePosition(4) / 3];
                [electrodeMapPosition, ~] = listdlg(...
                    'listString', DCMData.AvailableElectrodeMaps(:, 1),...
                    'selectionMode', 'single',...
                    'name', 'Select electrode array configuration',...
                    'listSize', figurePosition(3:4));
                if isempty(electrodeMapPosition)
                    self.ElectrodeMap = DCMData.LoadElectrodeMapFile('');
                else
                    self.ElectrodeMap = DCMData.AvailableElectrodeMaps{electrodeMapPosition, 2};
                end
            else
                self.ElectrodeMap = DCMData.AvailableElectrodeMaps{electrodeMapPosition, 2};
            end
            
            self.MappedData = NaN([self.ElectrodeMap.size self.GetNumberOfSamples()]);
            for dataIndex = 1:self.GetNumberOfChannels()
                if dataIndex > size(self.ElectrodeMap.map, 1), break; end
                
                mapIndex = self.ElectrodeMap.map(dataIndex, 1);
                if mapIndex > 0
                    [mapRow, mapColumn] = ind2sub(self.ElectrodeMap.size, mapIndex);
                    self.MappedData(mapRow, mapColumn, :) = self.Data(:, dataIndex);
                end
            end
        end
        
        function SetElectrodePositions(self)
            if isempty(self.ElectrodeMap), return; end
            
            if isfield(self.ElectrodeMap, 'electrodeSpacing')
                xDistance = self.ElectrodeMap.electrodeSpacing(1);
                yDistance = self.ElectrodeMap.electrodeSpacing(2);
            else
                xDistance = 1;
                yDistance = 1;
            end
            
            if isfield(self.ElectrodeMap, 'epiEndoDistance')
                zDistance = self.ElectrodeMap.epiEndoDistance;
            else
                zDistance = 1;
            end
            
            self.ElectrodePositions = NaN(self.GetNumberOfChannels, 3);
            for dataIndex = 1:self.GetNumberOfChannels()
                if dataIndex > size(self.ElectrodeMap.map, 1), break; end
                mapIndex = self.ElectrodeMap.map(dataIndex, 1);
                if mapIndex > 0
                    [mapRow, mapColumn] = ind2sub(self.ElectrodeMap.size, mapIndex);
                    mapLayer = self.ElectrodeMap.map(dataIndex, 2);
                    self.ElectrodePositions(dataIndex, 1) = mapColumn * xDistance;
                    self.ElectrodePositions(dataIndex, 2) = mapRow * yDistance;
                    self.ElectrodePositions(dataIndex, 3) = mapLayer * zDistance;
                end
            end
        end
        
        function CreateActivationData(self, analysisResult, useAllDeflections)
            self.Activations = cell(self.GetNumberOfChannels(), 1);
            
            for channelIndex = 1:self.GetNumberOfChannels()
                timeRange = self.GetTimeRange() * 1000;
                if useAllDeflections
                    activationIndices = analysisResult{channelIndex}.templatePeakIndices;
                else
                    activationIndices = analysisResult{channelIndex}.templatePeakIndices(analysisResult{channelIndex}.PrimaryDeflections);
                end
                
                self.Activations{channelIndex} = timeRange(activationIndices);
            end
        end
    end
    
    methods (Static)
        function maps = LoadAvailableElectrodeMaps()
            if isdeployed
                [pathString, ~] = fileparts(which('electrodeMapSpoon16x16.mat'));
                mapLocation = pathString;
%                 mapLocation = [ctfroot filesep 'Data'];
                disp(mapLocation);
            else
                mFilename = mfilename('fullpath');
                [pathString, ~] = fileparts(mFilename);
                mapLocation = fullfile(pathString, 'Data');
            end
            
            listing = dir(fullfile(mapLocation, '*.mat'));
            maps = cell(numel(listing), 2);
            for mapIndex = 1:numel(listing)
                currentMap = DCMData.LoadElectrodeMapFile(fullfile(mapLocation, listing(mapIndex).name));
                if isempty(currentMap.aliases)
                    currentMap.aliases = {listing(mapIndex).name};
                end
                maps{mapIndex, 1} = currentMap.aliases{1};
                maps{mapIndex, 2} = currentMap;
            end
        end
        
        function electrodeMap = LoadElectrodeMapFile(mapLocation)
            electrodeMap = [];
            try
                electrodeMapData = load(mapLocation);
            catch e
                disp(e);
                try
                    [electrodeFilename, electrodePathname] = ...
                        uigetfile('*.mat', 'Electrode map not found. Select electrode map file');
                    if 0 ~= electrodeFilename
                        electrodeMapData = load([electrodePathname electrodeFilename]);
                    end
                catch e
                    disp(e);
                    return;
                end
            end
            map = electrodeMapData.electrodeMap;
            if isfield(electrodeMapData, 'electrodeMapSize')
                mapSize = electrodeMapData.electrodeMapSize;
            else
                mapSize = [12, 13];
            end
            
            electrodeMap = struct(...
                'map', map,...
                'size', mapSize);
            
            if isfield(electrodeMapData, 'electrodeSpacing')
                electrodeMap.electrodeSpacing = electrodeMapData.electrodeSpacing;
            end
            
            if isfield(electrodeMapData, 'epiEndoDistance')
                electrodeMap.epiEndoDistance = electrodeMapData.epiEndoDistance;
            end
            
            if isfield(electrodeMapData, 'aliases')
                electrodeMap.aliases = electrodeMapData.aliases;
            else
                electrodeMap.aliases = {};
            end
        end
        
        function electrodePositions = ComputeElectrodePositions(electrodeMap)
            if isfield(electrodeMap, 'electrodeSpacing')
                xDistance = electrodeMap.electrodeSpacing(1);
                yDistance = electrodeMap.electrodeSpacing(2);
            else
                xDistance = 1;
                yDistance = 1;
            end
            
            if isfield(electrodeMap, 'epiEndoDistance')
                zDistance = electrodeMap.epiEndoDistance;
            else
                zDistance = 1;
            end
            
            numberOfElectrodes = size(electrodeMap.map, 1);
            electrodePositions = NaN(numberOfElectrodes, 3);
            for dataIndex = 1:numberOfElectrodes
                mapIndex = electrodeMap.map(dataIndex, 1);
                if mapIndex > 0
                    [mapRow, mapColumn] = ind2sub(electrodeMap.size, mapIndex);
                    mapLayer = electrodeMap.map(dataIndex, 2);
                    electrodePositions(dataIndex, 1) = mapColumn * xDistance;
                    electrodePositions(dataIndex, 2) = mapRow * yDistance;
                    electrodePositions(dataIndex, 3) = mapLayer * zDistance;
                end
            end
        end
    end
end
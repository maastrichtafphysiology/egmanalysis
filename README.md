# README #

This README contains information about the software package for AF electrogram analysis (formerly known as *Wavemap*)

### What is this repository for? ###

* Algorithms and user interface to import, process and annotate electrogram recordings during AF
* Version 1.0

### How do I get set up? ###

* Add the folder EGMAnalysis to the path
* Dependencies: CustomMatlabPackages
* Run: *AFStart()*

### Questions / contact ###

* [Stef Zeemering](mailto:s.zeemering@maastrichtuniversity.nl)

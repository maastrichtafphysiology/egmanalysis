classdef EGMPreProcessor < ECGPreProcessor
    properties
        
    end
    
    methods
        function self = EGMPreProcessor()
            self = self@ECGPreProcessor();

            self.NotchOrder = 6;
        end
        
        function filteredSignal = NotchFilter(self, signal)
            filteredSignal = double(signal);
            nyquistFrequency = self.SamplingFrequency / 2;
            for notchIndex = 1:numel(self.NotchFrequency)
                notchRange = self.NotchFrequency(notchIndex) + [-1, 1];
                
                relativeFrequency = notchRange / nyquistFrequency;
                quality = self.NotchOrder;
                
                [z,p,k] = butter(quality, relativeFrequency, 'stop');
                sos = zp2sos(z,p,k);

                filteredSignal = filtfilt(sos, 1, filteredSignal);
            end
        end
    end
end
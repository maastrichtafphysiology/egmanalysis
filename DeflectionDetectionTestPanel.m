classdef DeflectionDetectionTestPanel < ECGChannelPanel
    properties
        DetectedDeflections
        DetectedHighEnds
        DetectedLowEnds
        DeflectionLineHandles
        HighEndLineHandles
        LowEndLineHandles
        ProcessedLineHandles
    end
    
    properties (Access = protected)
        PreprocessedData
        MinimumDeflectionDistance
        MinimumAmplitudeThreshold
        WindowLength
        
        MinimumDeflectionDistancedEdit
        MinimumAmplitudeThresholdEdit
        AFCLAxes

        SelectedDetectionMethod
    end

    properties (Constant)
        DETECTION_METHODS = {...
            'DvDt', 'NLE'};
    end
    
    events
        DeflectionDetectionCompleted
    end
    
    methods
        function self = DeflectionDetectionTestPanel(position)
            self = self@ECGChannelPanel(position);
            self.MinimumDeflectionDistance = 0.05;
            self.MinimumAmplitudeThreshold = 0.25;
            self.WindowLength = 0.5;

            self.SelectedDetectionMethod =...
                DeflectionDetectionTestPanel.DETECTION_METHODS{1};
        end
        
        function Create(self, parentHandle)
            Create@ECGChannelPanel(self, parentHandle);
            
            self.CreateDetectionControls();
        end
        
        function SetECGData(self, ecgData)
            self.DetectedDeflections = cell(ecgData.GetNumberOfChannels, 1);
            self.DetectedHighEnds = cell(ecgData.GetNumberOfChannels, 1);
            self.DetectedLowEnds = cell(ecgData.GetNumberOfChannels, 1);
            self.PreprocessedData = ecgData.Data;

            % estimate dominant deflection frequency
            samplingFrequency = ecgData.SamplingFrequency; 
            nyquistFrequency = samplingFrequency / 2;
            numberOfChannels = ecgData.GetNumberOfChannels();
            
            [bHigh, aHigh] = butter(2, [40, 250] / nyquistFrequency);
            [bLow, aLow] = butter(2, 20 / nyquistFrequency, 'low');
            
            filteredSignals = filtfilt(bHigh, aHigh, ecgData.Data);
            filteredSignals = abs(filteredSignals);
            filteredSignals = [zeros(100, numberOfChannels); filteredSignals; zeros(100, numberOfChannels)];
            filteredSignals = filtfilt(bLow, aLow, filteredSignals);
            filteredSignals = filteredSignals(101:(end-100), :);
            channelDF = NaN(numberOfChannels, 1);
            parfor channelIndex = 1:numberOfChannels
                channelData = filteredSignals(:, channelIndex);
                % compute DF
                nFFT = 2^14;
                [frequencyPower, frequencies] = pwelch(channelData, [], [], nFFT, samplingFrequency);
                [peakValues, peakIndices] = findpeaks(frequencyPower);

                minimalFrequency = 1;
                maximalFrequency = 20;
                validDFFrequencies = frequencies >= minimalFrequency &...
                    frequencies < maximalFrequency;
                peakDFValues = peakValues(validDFFrequencies(peakIndices));
                peakDFIndices = peakIndices(validDFFrequencies(peakIndices));
                [~, maxPosition] = max(peakDFValues);

                channelDF(channelIndex) = frequencies(peakDFIndices(maxPosition));
            end
            
            medianAFCL = median(1 ./ channelDF(~isnan(channelDF)));
            self.MinimumDeflectionDistance = 0.75 * medianAFCL;
            set(self.MinimumDeflectionDistancedEdit, 'string',...
                num2str(round(1e3 * self.MinimumDeflectionDistance)));

            SetECGData@ECGChannelPanel(self, ecgData);
        end
        
        function ecgData = LoadData(self, filename)
            try
                ddData = load(filename);
            catch %#ok<CTCH>
                [filename, pathname] = HelperFunctions.customUigetfile('*.mat', 'No associated file found. Select alternative file');
                if isequal(filename, 0) || isequal(pathname, 0), return; end
                filename = fullfile(pathname, filename);
                ddData = load(filename);
            end
            ecgData = ddData.ecgData;
            
            [pathString, nameString] = fileparts(filename);
            nameParts = strsplit(nameString, '_');
            nameString = strjoin(nameParts(1:(end-1)), '_');
            if ~isempty(ddData.ecgData)
                ddData.ecgData.Filename = fullfile(pathString, nameString);
            end
            
            self.SetECGData(ddData.ecgData);
            
            if isfield(ddData, 'detectedDeflections')
                self.DetectedDeflections = ddData.detectedDeflections;
                self.DetectedHighEnds = ddData.detectedHighEnds;
                self.DetectedLowEnds = ddData.detectedLowEnds;
                self.SetSettings(ddData.settings);
                
                self.ShowDeflections();
                self.ShowAFCL();

                notify(self, 'DeflectionDetectionCompleted');
            end
        end
    end
    
    methods (Access = protected)
        function CreateDetectionControls(self)
            set(self.ChannelPanel, 'position', [0 .6 1 .39]);
            
            deflectionDetectionControlPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [0 0 1 .6],...
                'borderType', 'none');
            
            deflectionDetectionSettingsPanel = uipanel('parent', deflectionDetectionControlPanel,...
                'units', 'normalized', ...
                'position', [0 .8 1 .2],...
                'borderType', 'none');
            deflectionDistancePanel = uipanel('parent', deflectionDetectionSettingsPanel,...
                'units', 'normalized', ...
                'position', [0 0 .5 1],...
                'title', 'Deflection distance (ms)');
            self.MinimumDeflectionDistancedEdit = uicontrol(...
                'parent', deflectionDistancePanel,...
                'units', 'normalized',...
                'position', [.1 .1 .8 .8],...
                'style', 'edit',...
                'string', num2str(1e3 * self.MinimumDeflectionDistance),...
                'callback', @self.SetMinimumDeflectionDistance);
            deflectionAmplitudePanel = uipanel('parent', deflectionDetectionSettingsPanel,...
                'units', 'normalized', ...
                'position', [.5 0 .5 1],...
                'title', 'Deflection specificity (0-100%)');
            self.MinimumAmplitudeThresholdEdit = uicontrol(...
                'parent', deflectionAmplitudePanel,...
                'units', 'normalized',...
                'position', [.1 .1 .8 .8],...
                'style', 'edit',...
                'string', num2str(100 * self.MinimumAmplitudeThreshold),...
                'callback', @self.SetMinimumAmplitudeThreshold);
            
            AFCLPanel = uipanel('parent', deflectionDetectionControlPanel,...
                'units', 'normalized', ...
                'position', [0 .2 1 .6],...
                'title', 'Deflection Cycle length');
            self.AFCLAxes = subplot(1,1,1, 'parent', AFCLPanel);
            xlabel(self.AFCLAxes, 'ms')
            
            uicontrol('parent', deflectionDetectionControlPanel,...
                'units', 'normalized',...
                'position', [.1 .1 .8 .1],...
                'style', 'pushbutton',...
                'string', 'Detect deflections',...
                'callback', @self.DetectDeflections);
            uicontrol('parent', deflectionDetectionControlPanel,...
                'units', 'normalized',...
                'position', [0 0 1/3 .1],...
                'style', 'pushbutton',...
                'string', 'Load DD.mat',...
                'callback', @self.LoadDeflectionDetection);
            uicontrol('parent', deflectionDetectionControlPanel,...
                'units', 'normalized',...
                'position', [1/3 0 1/3 .1],...
                'style', 'pushbutton',...
                'string', 'Load DDPreview.mat',...
                'callback', @self.LoadDeflectionDetectionTest);
             uicontrol('parent', deflectionDetectionControlPanel,...
                'units', 'normalized',...
                'position', [2/3 0 1/3 .1],...
                'style', 'pushbutton',...
                'string', 'Save DDPreview.mat',...
                'callback', @self.SaveDeflectionDetectionTest);
        end
        
        function ShowECGData(self)
            ShowECGData@ECGChannelPanel(self);
            
            self.ShowDeflections();
        end
    end
    
    methods (Access = protected)
        function DetectDeflections(self, varargin)
            switch self.SelectedDetectionMethod
                case 'DvDt'
                    [detectedDeflections, detectedHighEnds, detectedLowEnds] =...
                        self.DetectionDeflectionsDvDt();
                case 'NLE'
                    [detectedDeflections, detectedHighEnds, detectedLowEnds] =...
                        self.DetectDeflectionsNLE();
            end
            
            self.DetectedDeflections = detectedDeflections;
            self.DetectedHighEnds = detectedHighEnds;
            self.DetectedLowEnds = detectedLowEnds;
            
            self.ShowDeflections();
            self.ShowAFCL();
            
            notify(self, 'DeflectionDetectionCompleted');
        end
        
        function [detectedDeflections, detectedHighEnds, detectedLowEnds] = ...
                DetectionDeflectionsDvDt(self)
            numberOfChannels = self.EcgData.GetNumberOfChannels();
            numberOfSamples = self.EcgData.GetNumberOfSamples();
            detectedDeflections = cell(numberOfChannels, 1);
            detectedHighEnds = cell(numberOfChannels, 1);
            detectedLowEnds = cell(numberOfChannels, 1);
            minPeakDistance = round(self.MinimumDeflectionDistance  * self.EcgData.SamplingFrequency);
            samplingFrequency = self.EcgData.SamplingFrequency;

            % SG filter to smooth signal
            sgWindowSampleLength = round(0.01 * samplingFrequency);
            if mod(sgWindowSampleLength, 2) == 0
                sgWindowSampleLength = sgWindowSampleLength + 1;
            end
            filteredData = sgolayfilt(self.EcgData.Data, 5, sgWindowSampleLength, [], 1);
            preprocessedData = filteredData;
            self.PreprocessedData = preprocessedData;

            minimumAmplitudeThreshold = self.MinimumAmplitudeThreshold;
            
            for channelIndex = 1:numberOfChannels
                originalChannelData = self.EcgData.Data(:, channelIndex);
                channelData = gradient(preprocessedData(:, channelIndex));
                
                [peakSlope, peakIndices] = findpeaks(...
                    -channelData, 'MINPEAKDISTANCE', minPeakDistance);
                peakWidths = ones(numel(peakSlope), 1) * round(0.01 * self.EcgData.SamplingFrequency);
                
                amplitudes = NaN(size(peakIndices));
                highEnds = NaN(size(peakIndices));
                lowEnds = NaN(size(peakIndices));
                slopes = NaN(size(peakIndices));
                slopeIndex = NaN(size(peakIndices));
                for pIndex = 1:numel(peakIndices)
                    peakWidth = 2 * round(peakWidths(pIndex));
                    peakInterval = peakIndices(pIndex) + (-peakWidth:peakWidth);
                    peakInterval = peakInterval(peakInterval > 0 & peakInterval <= numberOfSamples);
                    [highEndValue, highEndIndex] = max(originalChannelData(peakInterval));
                    [lowEndValue, lowEndIndex] = min(originalChannelData(peakInterval));
                    amplitudes(pIndex) = highEndValue - lowEndValue;
                    highEnds(pIndex) = peakInterval(highEndIndex);
                    lowEnds(pIndex) = peakInterval(lowEndIndex);
                    slopes(pIndex) = peakSlope(pIndex);
                    slopeIndex(pIndex) = peakIndices(pIndex);
                end
                
                validPeaks = slopes > 0 &...
                    amplitudes > minimumAmplitudeThreshold * std(originalChannelData);
                
                detectedDeflections{channelIndex} = slopeIndex(validPeaks);
                detectedHighEnds{channelIndex} = highEnds(validPeaks);
                detectedLowEnds{channelIndex} = lowEnds(validPeaks);
            end

        end

        function [detectedDeflections, detectedHighEnds, detectedLowEnds] =...
                DetectDeflectionsNLE(self)
            numberOfChannels = self.EcgData.GetNumberOfChannels();
            numberOfSamples = self.EcgData.GetNumberOfSamples();
            detectedDeflections = cell(numberOfChannels, 1);
            detectedHighEnds = cell(numberOfChannels, 1);
            detectedLowEnds = cell(numberOfChannels, 1);
            minPeakDistance = round(self.MinimumDeflectionDistance  * self.EcgData.SamplingFrequency);
            samplingFrequency = self.EcgData.SamplingFrequency; 
            orginalDataRange = [min(self.EcgData.Data); max(self.EcgData.Data)];
            
            % moving average for deflection amplitude & slope
            windowSampleLength = round(self.WindowLength * samplingFrequency);
            movingAverage = ones(windowSampleLength, 1) / windowSampleLength;
            
            % SG filter to smooth signal
            sgWindowSampleLength = round(0.01 * samplingFrequency);
            if mod(sgWindowSampleLength, 2) == 0
                sgWindowSampleLength = sgWindowSampleLength + 1;
            end
            filteredData = sgolayfilt(self.EcgData.Data, 5, sgWindowSampleLength, [], 1);
            
            % transform data (nonlinear energy operator)
            preprocessedData = self.EcgData.Data(2:(end-1), :).^2 -...
                self.EcgData.Data(1:(end-2), :).* self.EcgData.Data(3:(end), :);
            preprocessedData = [zeros(1, numberOfChannels);...
                preprocessedData; zeros(1, numberOfChannels);];
            self.PreprocessedData = preprocessedData;
            
            minimumAmplitudeThreshold = self.MinimumAmplitudeThreshold;
            
            for channelIndex = 1:numberOfChannels
                originalChannelData = self.EcgData.Data(:, channelIndex);
                channelData = preprocessedData(:, channelIndex);
                channelGradient = gradient(filteredData(:, channelIndex));
                
                [peakEnergy, peakIndices] = findpeaks(...
                    channelData, 'MINPEAKDISTANCE', minPeakDistance);
                peakWidths = ones(numel(peakEnergy), 1) * round(0.01 * self.EcgData.SamplingFrequency);
                
                amplitudes = NaN(size(peakIndices));
                highEnds = NaN(size(peakIndices));
                lowEnds = NaN(size(peakIndices));
                slopes = NaN(size(peakIndices));
                slopeIndex = NaN(size(peakIndices));
                for pIndex = 1:numel(peakIndices)
                    peakWidth = 2 * round(peakWidths(pIndex));
                    peakInterval = peakIndices(pIndex) + (-peakWidth:peakWidth);
                    peakInterval = peakInterval(peakInterval > 0 & peakInterval <= numberOfSamples);
                    [highEndValue, highEndIndex] = max(originalChannelData(peakInterval));
                    [lowEndValue, lowEndIndex] = min(originalChannelData(peakInterval));
                    amplitudes(pIndex) = highEndValue - lowEndValue;
                    highEnds(pIndex) = peakInterval(highEndIndex);
                    lowEnds(pIndex) = peakInterval(lowEndIndex);
                    [minimumSlope, minimumSlopeIndex] = min(channelGradient(peakInterval));
                    slopes(pIndex) = minimumSlope;
                    slopeIndex(pIndex) = peakInterval(minimumSlopeIndex);
                end
                
                validPeaks = slopes < 0;
%                 interpolatedAmplitude = interp1(peakIndices(validPeaks), amplitudes(validPeaks), 1:numel(channelData),...
%                     'linear', mean(amplitudes(validPeaks)));
%                 [upperAmplitudeEnvelope, ~] = envelope(interpolatedAmplitude, windowSampleLength, 'analytic');
                interpolatedEnergy = interp1(peakIndices(validPeaks), peakEnergy(validPeaks), 1:numel(channelData),...
                    'linear', mean(peakEnergy(validPeaks)));
                [upperEnergyEnvelope, ~] = envelope(interpolatedEnergy, windowSampleLength, 'analytic');
                validPeaks = validPeaks & peakEnergy > (minimumAmplitudeThreshold * upperEnergyEnvelope(peakIndices)');
                
                detectedDeflections{channelIndex} = slopeIndex(validPeaks);
                detectedHighEnds{channelIndex} = highEnds(validPeaks);
                detectedLowEnds{channelIndex} = lowEnds(validPeaks);
            end
        end

        function ShowDeflections(self)
            validHandles = ishandle(self.DeflectionLineHandles);
            if any(validHandles)
                delete(self.DeflectionLineHandles(validHandles));
            end
            
            validHandles = ishandle(self.HighEndLineHandles);
            if any(validHandles)
                delete(self.HighEndLineHandles(validHandles));
            end
            
            validHandles = ishandle(self.LowEndLineHandles);
            if any(validHandles)
                delete(self.LowEndLineHandles(validHandles));
            end
            
            validHandles = ishandle(self.ProcessedLineHandles);
            if any(validHandles)
                delete(self.ProcessedLineHandles(validHandles));
            end
            
            if isempty(self.DetectedDeflections), return; end
            
            lineData = self.EcgData.Data(:, self.SelectedChannels);
            resultData = self.DetectedDeflections(self.SelectedChannels);
            highEndData = self.DetectedHighEnds(self.SelectedChannels);
            lowEndData = self.DetectedLowEnds(self.SelectedChannels);
            time = self.EcgData.GetTimeRange();
            self.ProcessedLineHandles = NaN(size(resultData));
            self.DeflectionLineHandles = NaN(size(resultData));
            for channelIndex = 1:numel(resultData)
                currentResult = resultData{channelIndex};
                currentHighEnds = highEndData{channelIndex};
                currentLowEnds = lowEndData{channelIndex};
                signal = lineData(:, channelIndex);
                
                self.ProcessedLineHandles(channelIndex) = line('xData', time, 'yData', self.PreprocessedData(:, self.SelectedChannels(channelIndex)),...
                        'color', [0 0 1],...
                        'parent', self.AxesHandles(channelIndex),...
                        'lineSmoothing', 'on');
                
                self.DeflectionLineHandles(channelIndex) = line('xData', time(currentResult),...
                    'yData', signal(currentResult),...
                    'lineStyle', 'none',...
                    'marker', 'o', 'markerSize', 7,...
                    'markerEdgeColor', [0 0 0], 'markerFaceColor', [1 0 0],...
                    'parent', self.AxesHandles(channelIndex));
                
                self.HighEndLineHandles(channelIndex) = line('xData', time(currentHighEnds),...
                    'yData', signal(currentHighEnds),...
                    'lineStyle', 'none',...
                    'marker', 'x', 'markerSize', 7,...
                    'markerEdgeColor', [0 0 1], 'markerFaceColor', [0 0 1],...
                    'parent', self.AxesHandles(channelIndex));
                self.LowEndLineHandles(channelIndex) = line('xData', time(currentLowEnds),...
                    'yData', signal(currentLowEnds),...
                    'lineStyle', 'none',...
                    'marker', 'x', 'markerSize', 7,...
                    'markerEdgeColor', [0 0 1], 'markerFaceColor', [0 0 1],...
                    'parent', self.AxesHandles(channelIndex));
            end
        end
        
        function ShowAFCL(self)
            if isempty(self.DetectedDeflections), return; end
            
            afcl = cellfun(@diff, self.DetectedDeflections, 'uniformOutput', false);
            afcl = vertcat(afcl{:});
            afcl = 1000 * afcl / self.EcgData.SamplingFrequency;
            afclPercentiles = prctile(afcl, [1, 99]);
            bins = 0:5:max(afcl);
            
            histogram(self.AFCLAxes, afcl, bins);
            xlim(self.AFCLAxes, [afclPercentiles(1), afclPercentiles(end)])
        end
        
        function SetSettings(self, settings)
            self.MinimumDeflectionDistance = settings.minimumDeflectionDistance;
            self.MinimumAmplitudeThreshold = settings.minimumAmplitudeThreshold;
            self.WindowLength = settings.windowLength;
            
            set(self.MinimumDeflectionDistancedEdit, 'string',...
                num2str(1e3 * self.MinimumDeflectionDistance));
            
            set(self.MinimumAmplitudeThresholdEdit, 'string',...
                num2str(100 * self.MinimumAmplitudeThreshold));
        end
        
        function SetMinimumDeflectionDistance(self, source, varargin)
            self.MinimumDeflectionDistance = str2double(get(source, 'string')) / 1000;
        end
        
        function SetMinimumAmplitudeThreshold(self, source, varargin)
            self.MinimumAmplitudeThreshold = str2double(get(source, 'string')) / 100;
        end
        
        function LoadDeflectionDetection(self, varargin)
            if isempty(self.EcgData)
                [filename, pathname] = HelperFunctions.customUigetfile('*_DD.mat', 'Select DD file');
                if isequal(filename, 0) || isequal(pathname, 0), return; end
                filename = fullfile(pathname, filename);
            else
                [pathString, nameString] = fileparts(self.EcgData.Filename);
                filename = [pathString, filesep, nameString '_DD.mat'];
            end
            
            self.LoadData(filename);
        end
        
        function LoadDeflectionDetectionTest(self, varargin)
            if isempty(self.EcgData)
                [filename, pathname] = HelperFunctions.customUigetfile('*_DDPreview.mat', 'Select DDPreview file');
                if isequal(filename, 0) || isequal(pathname, 0), return; end
                filename = fullfile(pathname, filename);
            else
                [pathString, nameString] = fileparts(self.EcgData.Filename);
                filename = [pathString, filesep, nameString '_DDPreview.mat'];
            end
            
            self.LoadData(filename);
        end
        
        function SaveDeflectionDetectionTest(self, varargin)
            [pathString, nameString] = fileparts(self.EcgData.Filename);
            filename = [pathString, filesep, nameString '_DDPreview'];
            ecgData = self.EcgData; %#ok<NASGU>
            detectedDeflections = self.DetectedDeflections;
            detectedHighEnds = self.DetectedHighEnds;
            detectedLowEnds = self.DetectedLowEnds;
            
            settings = struct(...
                'minimumDeflectionDistance', self.MinimumDeflectionDistance,...
                'minimumAmplitudeThreshold', self.MinimumAmplitudeThreshold,...
                'windowLength', self.WindowLength);
            
            save(filename,...
                'ecgData',...
                'detectedDeflections',...
                'detectedHighEnds', 'detectedLowEnds',...
                'settings');
        end
    end
end
classdef EGMPreProcessingPanel < PreProcessingPanel

    methods (Access = public)

        function self = EGMPreProcessingPanel(position)
            self = self@PreProcessingPanel(position);

            self.EcgPreProcessor = EGMPreProcessor();
        end

    end
end
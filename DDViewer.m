classdef DDViewer < UserInterfacePkg.CustomFigure
    properties (Access = private)
        EcgData
        DeflectionData
        IntrinsicDeflectionResult  
        
        AnimationPosition
        TimeLag
        FrameRate
        
        SelectedChannelIndex
        SelectedTime
        
        DeflectionMapControl
        
        ControlPanel
        AnimationButton
        AnimationSlider
        AnimationPositionLabel
        
        ZoomToggleButton
        PanToggleButton
    end
    
    methods
        function self = DDViewer(position)
            if nargin < 1
                screenSize = get(0,'Screensize');
                position = [screenSize(3) / 5, screenSize(4) / 5, 2 * screenSize(3) / 5, 3 * screenSize(4) / 5];            
            end
            self = self@UserInterfacePkg.CustomFigure(position);
            self.Name = 'DD viewer';
            
            self.SelectedChannelIndex = NaN;
            self.SelectedTime = NaN;
            self.TimeLag = 100;
            self.FrameRate = 0.001;
            
            if nargin < 1
               self.Create(); 
            end
        end
        
        function Create(self)
            Create@UserInterfacePkg.CustomFigure(self);
            
            set(self.ControlHandle,...
                'CloseRequestFcn', @self.Close);
            
            set(self.ControlHandle,...
                'colormap', flipud(jet(256)));
            
            self.CreateFileMenu();
            
            self.CreateToolbar();
            
            self.InitializeEvents();
            
            self.CreateControlPanel();
            
            self.InitializeView();
            
            self.Show();
        end
        
        function delete(self)
        end
    end
    
    methods (Access = private)
        function CreateControlPanel(self)
            self.ControlPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0 0 1 .1],...
                'borderType', 'none');
            
            self.AnimationButton = uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [0 .5 .1 .5],...
                'style', 'togglebutton',...
                'string', '>',...
                'callback', @self.ControlAnimation);
            
            self.AnimationSlider = uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.1 .5 .8 .3],...
                'style', 'slider',...
                'callback', @self.AnimationSliderCallback,...
                'enable', 'off');
            addlistener(handle(self.AnimationSlider), 'Value', 'PostSet', @self.ContinuousAnimationSliderCallback);
            
            self.AnimationPositionLabel = uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [0 0 .1 .5],...
                'style', 'text');
            
            timeLagPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [.1, 0, .1, .5],...
                'title', 'Time lag (ms)');
            uicontrol('parent', timeLagPanel,...
                'units', 'normalized',...
                'position', [0, 0, 1, 1],...
                'style', 'edit',...
                'string', num2str(self.TimeLag),...
                'callback', @self.SetTimeLag);
            
            frameRatePanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [.2, 0, .1, .5],...
                'title', 'Frame rate (ms)');
            uicontrol('parent', frameRatePanel,...
                'units', 'normalized',...
                'position', [0, 0, 1, 1],...
                'style', 'edit',...
                'string', num2str(1000 * self.FrameRate),...
                'callback', @self.SetFrameRate);
        end
        
        function InitializeView(self)
            self.DeflectionMapControl = ExtendedUIPkg.GridActivationMapControl([0 .1 1 .9]);
            self.DeflectionMapControl.Name = 'Activations';
            self.DeflectionMapControl.Create(self.ControlHandle);
            addlistener(self.DeflectionMapControl, 'MapClicked', @self.HandleMapClick);
            self.DeflectionMapControl.Show();
            
            deflectionColormap = hot(256 + 10);
            deflectionColormap = deflectionColormap(6:(end-5), :);
            self.DeflectionMapControl.SetColormap(deflectionColormap, 256);
        end
        
        function CreateFileMenu(self)
            fileMenu = uimenu(...
                'parent', self.ControlHandle,...
                'label', 'File');
            uimenu('parent', fileMenu,...
                'label', 'Load Deflection Detection (_DD) file',...
                'callback', @self.LoadSignal);
        end
        
        function CreateToolbar(self)
            toolbar = uitoolbar(...
                'parent', self.ControlHandle);
            toolbarIconData = load('toolbarIcons.mat');
            uipushtool(...
                'parent', toolbar,...
                'CData', toolbarIconData.opendoc,...
                'tooltipString', 'Load signal',...
                'clickedCallback', @self.LoadSignal);
            self.ZoomToggleButton = uitoggletool(...
                'parent', toolbar,...
                'CData', toolbarIconData.zoomCData,...
                'offCallback', 'zoom off',...
                'onCallback', @self.ZoomOn,...
                'separator', 'on');
            self.PanToggleButton = uitoggletool(...
                'parent', toolbar,...
                'CData', toolbarIconData.panCData,...
                'offCallback', 'pan off',...
                'onCallback', @self.PanOn);
        end
        
        function NotifyMouseUp(self, varargin)
            UserInterfacePkg.MouseEventClass.Instance().NotifyMouseUp();
        end
        
        function NotifyMouseDown(self, varargin)
            UserInterfacePkg.MouseEventClass.Instance().NotifyMouseDown();
        end
        
        function InitializeEvents(self)
            set(self.ControlHandle, 'WindowButtonUpFcn', @self.NotifyMouseUp)
            set(self.ControlHandle, 'WindowButtonDownFcn', @self.NotifyMouseDown)
        end
        
        function LoadSignal(self, varargin)
            [fileName, pathName] = HelperFunctions.customUigetfile({...
                '*_DD.mat', 'Deflection detection file (*_DD.mat)'},...
                'Select a Deflection Detection file');
            if isequal(fileName, 0), return; end
            
            ddData = load(fullfile(pathName, fileName));
            
            self.EcgData = ddData.ecgData;
            self.EcgData.SetActivations(ddData.intrinsicDeflectionResult);
            self.DeflectionData = ddData.deflectionData;
            self.IntrinsicDeflectionResult = ddData.intrinsicDeflectionResult;
            
            self.InitializeECGData();
        end
        
        function InitializeECGData(self)
            self.DeflectionMapControl.SamplingFrequency = self.EcgData.SamplingFrequency;
            self.DeflectionMapControl.SetData(self.EcgData.Activations, self.EcgData.ElectrodePositions);
            self.DeflectionMapControl.Show();
            
            set(self.AnimationSlider, 'enable', 'on');
            self.AnimationPosition = 1;
            numberOfSamples = self.EcgData.GetNumberOfSamples();
            set(self.AnimationSlider, 'min', 1, 'max', numberOfSamples,...
                'value', self.AnimationPosition,...
                'sliderStep', [1 / (numberOfSamples - 1), 1 / (numberOfSamples - 1)]);
            
            self.SetAnimationFrame();
        end
        
        function ShowSelectedChannel(self)
            
        end
        
        function SetSignalWindow(self, position)
            timePosition = position / self.EcgData.SamplingFrequency;
            timeWindow = [timePosition - 0.5, timePosition + 0.5];
            xTickSpacing = 0.1;
            xTicks = (timeWindow(1) + (xTickSpacing - mod(timeWindow(1), xTickSpacing))):(xTickSpacing):(timeWindow(end) - mod(timeWindow(end), xTickSpacing));
            
            % update electrogram axes limits
%             set(self.SignalAxes,...
%                 'xLim', timeWindow,...
%                 'xTick', xTicks,...
%                 'xMinorTick', 'on');
%             
%             yLimits = get(self.SignalAxes, 'yLim');
        end
        
        function SetTimeLag(self, source, varargin)
            self.TimeLag = str2double(get(source, 'string'));
            
            self.DeflectionMapControl.SetTimeLag(self.TimeLag);
        end
        
        function SetFrameRate(self, source, varargin)
            self.FrameRate = round(str2double(get(source, 'string'))) / 1000;
        end
        
        function HandleMapClick(self, ~, eventData)
            self.SelectedChannelIndex = eventData.Position;
            self.SelectedTime = eventData.Time;
            self.ShowSelectedChannel();
        end
        
        function ControlAnimation(self, varargin)
            if isempty(self.EcgData), return; end
            
            set(self.AnimationButton, 'string', '||');
            numberOfSamples = self.EcgData.GetNumberOfSamples();
            
            frameStep = round(self.FrameRate * self.EcgData.SamplingFrequency);
            
            startPosition = self.AnimationPosition;
            while get(self.AnimationButton, 'value')
                drawnow;
                self.AnimationPosition = self.AnimationPosition + frameStep;
                if self.AnimationPosition > numberOfSamples
                    self.AnimationPosition = 1;
                end
                set(self.AnimationSlider, 'value', self.AnimationPosition);
                self.SetAnimationFrame();
                
                if mod((self.AnimationPosition - startPosition) / frameStep, 10) == 0
                    set(self.AnimationSlider, 'value', self.AnimationPosition);
                    self.SetAnimationInfo();
                end
            end
            
            set(self.AnimationButton, 'string', '>');
            
            self.SetStaticFrame();
        end
        
        function AnimationSliderCallback(self, varargin)
            self.AnimationPosition = round(get(self.AnimationSlider, 'value'));
            self.SetStaticFrame();
        end
        
        function ContinuousAnimationSliderCallback(self, source, eventData)
            if verLessThan('matlab', '8.4.0')
            sliderObject = get(eventData, 'AffectedObject');
            else
                sliderObject = eventData.AffectedObject;
            end
            self.AnimationPosition = round(get(sliderObject, 'Value'));
            self.SetAnimationFrame();
            self.SetSignalWindow(self.AnimationPosition);
        end
        
        function SetStaticFrame(self)
            self.SetAnimationInfo();
            
            self.DeflectionMapControl.SetStaticFramePosition(self.AnimationPosition);
            
            self.SetSignalWindow(self.AnimationPosition);
        end
        
        function SetAnimationFrame(self)
            self.SetAnimationInfo();
            
            self.DeflectionMapControl.SetFramePosition(self.AnimationPosition);
        end
        
        function SetAnimationInfo(self)
            timePosition = round(1000 * self.AnimationPosition / self.EcgData.SamplingFrequency);
            set(self.AnimationPositionLabel, 'string', [ num2str(timePosition) ' ms']);
        end
        
        function PanOn(self, varargin)
            pan on;
            set(self.ZoomToggleButton, 'state', 'off');
            set(self.RotateToggleButton, 'state', 'off');
        end
        
        function ZoomOn(self, varargin)
            zoom on;
            hManager = uigetmodemanager(self.ControlHandle);
            set(hManager.WindowListenerHandles, 'Enable','off');
            set(self.PanToggleButton, 'state', 'off');
            set(self.RotateToggleButton, 'state', 'off');
        end
        
        function Close(self, varargin)
            delete(self.ControlHandle);
        end
    end
end
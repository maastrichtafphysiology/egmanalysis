function DCMPreviewStart()
warning off MATLAB:uitabgroup:OldVersion;
screenSize = get(0,'Screensize');
figurePosition = [screenSize(3) / 5, screenSize(4) / 5, 3 * screenSize(3) / 5, 3 * screenSize(4) / 5];
applicationInstance = DCMPreviewFigure(figurePosition); 
applicationInstance.Create();
end
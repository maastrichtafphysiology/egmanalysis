classdef QRSTCancellationPanel < ECGChannelPanel
    properties
        ReferenceChannel
        RWaveIndices
        ClusterMemberIndices
    end
    
    properties (Access = protected)
        QrstCancellator
        CancelledEcgData
        CancelledDataLines
        
        ReferenceChannelPopup
        QRSTCancellationButton
        CancellationMethodPopup
        
        RPeakMarkers
        RPeakLines
        RPeakListener
        SelectedRPeakIndex
        RPeakDistanceEdit
        
        QRSTSettingsPanel
        QRSTRangePanel
        ClusterSensitivityPanel
        
        HighpassFilterButton
        HighpassFilterEdit
        
        MouseListeners
        LineMode
        TouchedAxesIndex
        TouchedRPeakIndex
    end
    
    events
        QRSTCancellationCompleted
    end
    
    methods
        function self = QRSTCancellationPanel(position)
            self = self@ECGChannelPanel(position);
            
            self.RPeakListener = addlistener(UserInterfacePkg.KeyEventClass.Instance(),...
                'KeyPressed', @self.HandleKeyPressedEvent);
%             self.RPeakListener(end + 1) = addlistener(UserInterfacePkg.KeyEventClass.Instance(),...
%                 'KeyReleased', @self.HandleKeyReleasedEvent);
            
            self.LineMode = 'Initial';
            self.MouseListeners = event.listener.empty(0, 0);
            
            self.SelectedRPeakIndex = NaN;
            self.TouchedRPeakIndex = NaN;
            self.TouchedAxesIndex = NaN;
        end
        
        function Create(self, parentHandle)
            Create@ECGChannelPanel(self, parentHandle);
            
            self.CreateMouseListeners();
        end
        
        function SetECGData(self, ecgData)
            self.ReferenceChannel = 1;
            SetECGData@ECGChannelPanel(self, ecgData);
            
            self.SelectedRPeakIndex = NaN;
            self.TouchedRPeakIndex = NaN;
            self.TouchedAxesIndex = NaN;
        end
        
        function ecgData = LoadData(self, filename)
            qrstData = load(filename);
            ecgData = qrstData.ecgData;
            
            [pathString, nameString] = fileparts(filename);
            nameString = nameString(1:(end - 5));
            if ~isempty(qrstData.ecgData)
                qrstData.ecgData.Filename = fullfile(pathString, nameString);
            end
            
            if ~isempty(qrstData.cancelledEcgData)
                qrstData.cancelledEcgData.Filename = fullfile(pathString, nameString);
            end
            self.CancelledEcgData = qrstData.cancelledEcgData;
            
            self.RWaveIndices = qrstData.rWaveIndices;
            
            if isfield(qrstData, 'clusterMemberIndices')
                self.ClusterMemberIndices = qrstData.clusterMemberIndices;
            else
                self.ClusterMemberIndices = ones(size(self.RWaveIndices, 2), 1);
            end
            
            self.SetECGData(qrstData.ecgData);
            
            self.ReferenceChannel = qrstData.referenceChannel;
            set(self.ReferenceChannelPopup, 'value', self.ReferenceChannel);
        end
        
        function cancelledEcgData = GetCancelledECGData(self)
            if isempty(self.CancelledEcgData)
                cancelledEcgData = self.EcgData;
            else
                cancelledEcgData = self.CancelledEcgData;
            end
        end
        
        function delete(self)
            delete@ECGChannelPanel(self);
            self.CancelledEcgData = [];
            
            if ~isempty(self.MouseListeners)
                delete(self.MouseListeners);
            end
            
            if ~isempty(self.RPeakListener)
                delete(self.RPeakListener);
            end
        end
    end
    
    methods (Access = protected)
        function CreateControlPanel(self)
            CreateControlPanel@ECGChannelPanel(self);
            
            set(self.ChannelPanel, 'position', [0 .5 1 .49]);
            self.CreateQRSTSettings();
            
            uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.1 .05 .8 .05],...
                'style', 'pushbutton',...
                'string', 'QRST cancellation completed',...
                'callback', @(src, event) notify(self, 'QRSTCancellationCompleted'));
        end
        
        function CreateQRSTSettings(self, varargin)
            if ishandle(self.QRSTSettingsPanel)
                delete(self.QRSTSettingsPanel)
            end
            
            self.QRSTSettingsPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [0 .1 1 .39],...
                'borderType', 'none');
            
            uicontrol('parent', self.QRSTSettingsPanel,...
                'units', 'normalized',...
                'position', [.05 .9 .4 .1],...
                'style', 'pushbutton',...
                'string', 'R-peak detection',...
                'callback', @self.ComputeRDetection);
            self.ReferenceChannelPopup = uicontrol(...
                'parent', self.QRSTSettingsPanel,...
                'units', 'normalized',...
                'position', [.5 .925 .4 .05],...
                'style', 'popupmenu',...
                'string', 'Select reference channel',...
                'backgroundColor', [1 1 1],...
                'callback', @self.SetReferenceChannel,...
                'busyAction', 'cancel');
            uicontrol('parent', self.QRSTSettingsPanel,...
                'units', 'normalized',...
                'position', [.05 .8 .4 .075],...
                'style', 'text',...                
                'string', 'R-peak distance (ms)',...
                'tooltipString', 'Change minimal R-peak distance to increase / decrease R-detection sensitivity');
            self.RPeakDistanceEdit = uicontrol('parent', self.QRSTSettingsPanel,...
                'units', 'normalized',...
                'position', [.5 .8 .4 .1],...
                'style', 'edit',...
                'string', num2str(1000 * self.QrstCancellator.VentricularRefractoryPeriod),...
                'callback', @self.SetVentricularRefractoryPeriod);
            
            self.QRSTCancellationButton = uicontrol('parent', self.QRSTSettingsPanel,...
                'units', 'normalized',...
                'position', [.05 .7 .4 .1],...
                'style', 'pushbutton',...
                'string', 'QRST cancellation',...
                'callback', @self.ComputeQRSTCancellation);
            self.CancellationMethodPopup = uicontrol(...
                'parent', self.QRSTSettingsPanel,...
                'units', 'normalized',...
                'position', [.5 .725 .4 .05],...
                'style', 'popupmenu',...
                'string', 'Abstract class',...
                'value', 1,...
                'backgroundColor', [1 1 1],...
                'callback', @self.SetCancellationMethod,...
                'busyAction', 'cancel');
            
            % QRST window range
            self.QRSTRangePanel = uipanel('parent', self.QRSTSettingsPanel,...
                'units', 'normalized', ...
                'position', [0 .5 1 .15],...
                'borderType', 'none');
            
            uicontrol('parent', self.QRSTRangePanel,...
                'units', 'normalized',...
                'position', [.1, 2/3, .2, 1/3],...
                'style', 'text',...                
                'string', '< R (%)');
            uicontrol('parent', self.QRSTRangePanel,...
                'units', 'normalized',...
                'position', [.1, 0, .2, 2/3],...
                'style', 'edit',...                
                'string', num2str(100 * self.QrstCancellator.QRSTRange(1)),...
                'callback', @self.SetQRSTRangeBefore);
            
            uicontrol('parent', self.QRSTRangePanel,...
                'units', 'normalized',...
                'position', [.3, 2/3, .2, 1/3],...
                'style', 'text',...                
                'string', '> R (%)');
            uicontrol('parent', self.QRSTRangePanel,...
                'units', 'normalized',...
                'position', [.3, 0, .2, 2/3],...
                'style', 'edit',...
                'string', num2str(100 * self.QrstCancellator.QRSTRange(2)),...
                'callback', @self.SetQRSTRangeAfter);
            
            self.ClusterSensitivityPanel = uipanel('parent', self.QRSTRangePanel,...
                'units', 'normalized', ...
                'position', [.5, 0, .3, 1],...
                'borderType', 'none');
            uicontrol('parent', self.ClusterSensitivityPanel,...
                'units', 'normalized',...
                'position', [0, 2/3, 1, 1/3],...
                'style', 'pushbutton',...                
                'string', 'Clusters...',...
                'callback', @self.ComputeQRSClusters);
            uicontrol('parent', self.ClusterSensitivityPanel,...
                'units', 'normalized',...
                'position', [0, 0, 1, 2/3],...
                'style', 'edit',...
                'string', num2str(self.QrstCancellator.ClusterSensitivity),...
                'tooltipString', 'Cluster similarity (0-1)',...
                'callback', @self.SetClusterSensitivity);
            
            self.HighpassFilterButton = uicontrol('parent', self.QRSTSettingsPanel,...
                'units', 'normalized',...
                'position', [.05 .4 .4 .075],...
                'style', 'pushbutton',...                
                'string', 'High-pass filter (Hz)',...
                'tooltipString', 'High-pass filter frequency (after QRST cancellation)',...
                'callback', @self.ApplyHighpassPostFilter);
            self.HighpassFilterEdit = uicontrol('parent', self.QRSTSettingsPanel,...
                'units', 'normalized',...
                'position', [.5 .4 .3 .1],...
                'style', 'edit',...
                'string', num2str(self.QrstCancellator.HighpassPostFilter),...
                'callback', @self.SetHighpassPostFilter);
            
            uicontrol('parent', self.QRSTSettingsPanel,...
                'units', 'normalized',...
                'position', [.1 .3 .4 .05],...
                'style', 'checkbox',...
                'string', 'Show diagnostics',...
                'value', self.QrstCancellator.ShowDiagnostics,...
                'callback', @self.SetShowDiagnostics);
            
            uicontrol('parent', self.QRSTSettingsPanel,...
                'units', 'normalized',...
                'position', [.05 .1 .4 .1],...
                'style', 'pushbutton',...
                'string', 'Save to .mat',...
                'callback', @self.SaveQRSTCancellation);
            uicontrol('parent', self.QRSTSettingsPanel,...
                'units', 'normalized',...
                'position', [.5 .1 .4 .1],...
                'style', 'pushbutton',...
                'string', 'Load from .mat',...
                'callback', @self.LoadQRSTCancellation);
        end
        
        function SetChannelList(self)
            SetChannelList@ECGChannelPanel(self);
            
            if isempty(self.EcgData.ReferenceLabels)
                set(self.ReferenceChannelPopup, 'string', self.EcgData.ElectrodeLabels);
            else
                set(self.ReferenceChannelPopup, 'string', self.EcgData.ReferenceLabels);
            end
            set(self.ChannelListbox, 'value', self.ReferenceChannel);
        end
        
        function ShowECGData(self)
            ShowECGData@ECGChannelPanel(self);
            
            self.ShowRWaves();
            self.ShowQRSTCancellation();
        end
        
        function ShowRWaves(self)
            if isempty(self.RWaveIndices), return; end
            
            validHandles = ishandle(self.RPeakLines);
            if any(validHandles)
                delete(self.RPeakLines(validHandles));
            end
            
            validHandles = ishandle(self.RPeakMarkers);
            if any(validHandles)
                delete(self.RPeakMarkers(validHandles));
            end
            
            time = self.EcgData.GetTimeRange();
            lineData = [self.EcgData.Data(:, self.SelectedChannels),...
                self.EcgData.ReferenceData(:, self.SelectedLeadChannels)];
            
            self.RPeakLines = NaN(numel(self.AxesHandles), numel(self.RWaveIndices));
            self.RPeakMarkers = NaN(numel(self.AxesHandles), 1);
            for axesIndex = 1:numel(self.AxesHandles)
                yLimits = get(self.AxesHandles(axesIndex), 'yLim');
                for rIndex = 1:numel(self.RWaveIndices)
                    self.RPeakLines(axesIndex, rIndex) =...
                        line('xData', [time(self.RWaveIndices(rIndex)) time(self.RWaveIndices(rIndex))],...
                        'yData', yLimits,...
                        'color', [1 0 0],...
                        'parent', self.AxesHandles(axesIndex),...
                        'buttonDownFcn', @self.RPeakSelectedCallback,...
                        'userData', self.RWaveIndices(rIndex));
                end

                self.RPeakMarkers(axesIndex) =...
                        line('xData', time(self.RWaveIndices),...
                        'yData', lineData(self.RWaveIndices, axesIndex),...
                        'lineStyle', 'none',...
                        'marker', 'o', 'markerSize', 7,...
                        'markerEdgeColor', [0 0 0], 'markerFaceColor', [0 0 1],...
                        'parent', self.AxesHandles(axesIndex));
            end
        end
        
        function ShowQRSTCancellation(self)
            if isempty(self.CancelledEcgData), return; end
            
            validHandles = ishandle(self.CancelledDataLines);
            if any(validHandles)
                delete(self.CancelledDataLines(validHandles));
            end
            
            lineData = self.CancelledEcgData.Data(:, self.SelectedChannels);
            time = self.EcgData.GetTimeRange();
            lineColor = [0 0 1];
            self.CancelledDataLines = NaN(numel(self.SelectedChannels), 1);
            for lineIndex = 1:size(lineData, 2);
                self.CancelledDataLines(lineIndex) = ...
                    line('xData', time, 'yData', lineData(:, lineIndex),...
                    'color', lineColor,...
                    'lineWidth', 1,...
                    'parent', self.AxesHandles(lineIndex),...
                    'buttonDownFcn', @self.LineSelectedCallback,...
                    'lineSmoothing', 'on');
            end
        end
        
        function SetReferenceChannel(self, varargin)
            self.ReferenceChannel = get(self.ReferenceChannelPopup, 'value');
        end
        
        function SetCancellationMethod(self, varargin)
            methodIndex = get(self.CancellationMethodPopup, 'value');
            self.QrstCancellator.CancellationMethod = QRSTCancellator.CANCELLATION_METHODS{methodIndex};
            
            switch self.QrstCancellator.CancellationMethod
                case QRSTCancellator.CANCELLATION_METHODS{2}
                    set(self.ClusterSensitivityPanel, 'visible', 'on');
                    set(self.QRSTCancellationButton, 'enable', 'off');
                otherwise
                    set(self.ClusterSensitivityPanel, 'visible', 'off');
                    set(self.QRSTCancellationButton, 'enable', 'on');
            end
        end
        
        function ComputeRDetection(self, varargin)
            self.CancelledEcgData = [];
            referenceChannelIndex = self.ReferenceChannel;
            noReferenceData = false;
            if isempty(self.EcgData.ReferenceData)
                noReferenceData = true;
                self.EcgData.ReferenceData = self.EcgData.Data(:, self.ReferenceChannel);
                referenceChannelIndex = 1;
            end
            
            self.RWaveIndices = self.QrstCancellator.DetectRWaves(self.EcgData, referenceChannelIndex);
%             self.RWaveIndices = self.QrstCancellator.DetectWindowRWaves(self.EcgData, referenceChannelIndex);
            
            if noReferenceData
                self.EcgData.ReferenceData = [];
            end

            self.ShowRWaves();
        end
        
        function ComputeQRSClusters(self, varargin)
            if isempty(self.RWaveIndices), return; end
            rWaveWindowIndices = self.QrstCancellator.GetRWaveWindowIndices(self.RWaveIndices);
            
            if isempty(self.EcgData.ReferenceData)
                signal = self.EcgData.Data(:, self.ReferenceChannel);
            else
                signal = self.EcgData.ReferenceData(:, self.ReferenceChannel);
            end
            
            [~, self.ClusterMemberIndices] = self.QrstCancellator.ComputeQRSTClusters(signal, rWaveWindowIndices);
            set(self.QRSTCancellationButton, 'enable', 'on');
        end
        
        function ComputeQRSTCancellation(self, varargin)
            referenceChannelIndex = self.ReferenceChannel;
            noReferenceData = false;
            if isempty(self.EcgData.ReferenceData)
                noReferenceData = true;
                self.EcgData.ReferenceData = self.EcgData.Data(:, self.ReferenceChannel);
                referenceChannelIndex = 1;
            end
            self.QrstCancellator.ReferenceChannelIndex = referenceChannelIndex;
            
            self.CancelledEcgData = self.QrstCancellator.CancelQRST(self.EcgData, self.RWaveIndices, self.ClusterMemberIndices);
            
            if noReferenceData
                self.EcgData.ReferenceData = [];
            end
            
            set(self.HighpassFilterButton, 'enable', 'on');
            
            self.ShowQRSTCancellation();
        end
        
        function ApplyHighpassPostFilter(self, varargin)
            if isempty(self.CancelledEcgData), return; end
            
            nyquistFrequency = self.CancelledEcgData.SamplingFrequency;
            self.CancelledEcgData.Data = self.QrstCancellator.FilterCancelledSignal(...
                self.CancelledEcgData.Data, nyquistFrequency);
            
            set(self.HighpassFilterButton, 'enable', 'off');
            
            self.ShowQRSTCancellation();
        end
        
        function LineSelectedCallback(~, source, varargin)
            lineWidth = get(source, 'lineWidth');
            if lineWidth == 1
                set(source, 'lineWidth', 2);
                uistack(source, 'top');
            else
                set(source, 'lineWidth', 1);
                uistack(source, 'bottom');
            end
        end
        
        function RPeakSelectedCallback(self, source, varargin)
            selectedRPeakPosition = get(source, 'userData');
            selectedRPeakIndex = find(self.RWaveIndices == selectedRPeakPosition);
            if ~isnan(self.SelectedRPeakIndex)
                for axesIndex = 1:numel(self.AxesHandles);
                    set(self.RPeakLines(axesIndex, self.SelectedRPeakIndex), 'lineWidth', 1);
                end
            end
            
            if selectedRPeakIndex == self.SelectedRPeakIndex
                if strcmp(self.LineMode, 'Moving')
                    for axesIndex = 1:numel(self.AxesHandles);
                        set(self.RPeakLines(axesIndex, self.SelectedRPeakIndex), 'lineWidth', 2);
                    end
                end
                self.SelectedRPeakIndex = NaN;
            else
                self.SelectedRPeakIndex = selectedRPeakIndex;
                for axesIndex = 1:numel(self.AxesHandles);
                    set(self.RPeakLines(axesIndex, self.SelectedRPeakIndex), 'lineWidth', 2);
                end
            end
        end
        
        function HandleKeyPressedEvent(self, ~, eventData)
            switch eventData.Key
                case {'delete', 'd'}
                    self.DeleteRPeak(self.SelectedRPeakIndex);
                    self.SelectedRPeakIndex = NaN;
                case 'i'
                    newPeakIndex = self.InsertRPeak();
                    self.SelectedRPeakIndex = newPeakIndex;
                    self.ShowRWaves();
                case {'rightarrow', 'leftarrow'}
                    if isnan(self.SelectedRPeakIndex), return; end
                    switch eventData.Key
                        case 'rightarrow'
                            self.MoveRPeak(self.SelectedRPeakIndex, 1);
                        case 'leftarrow'
                            self.MoveRPeak(self.SelectedRPeakIndex, -1);
                    end
            end
        end
        
        function HandleKeyReleasedEvent(self, ~, eventData)
            if isnan(self.SelectedRPeakIndex), return; end
            
            switch eventData.Key
                case 'rightarrow'
                    self.MoveRPeak(self.SelectedRPeakIndex, 1);
                case 'leftarrow'
                    self.MoveRPeak(self.SelectedRPeakIndex, -1);
            end
        end
        
        function SaveQRSTCancellation(self, varargin)
            [pathString, nameString] = fileparts(self.EcgData.Filename);
            filename = [pathString, filesep, nameString '_QRST.mat'];
            ecgData = self.EcgData; %#ok<NASGU>
            cancelledEcgData = self.CancelledEcgData; %#ok<NASGU>
            rWaveIndices = self.RWaveIndices; %#ok<NASGU>
            clusterMemberIndices = self.ClusterMemberIndices; %#ok<NASGU>
            qrstCancellator = self.QrstCancellator; %#ok<NASGU>
            referenceChannel = self.ReferenceChannel; %#ok<NASGU>
            save(filename,...
                'ecgData',...
                'cancelledEcgData',...
                'rWaveIndices',...
                'clusterMemberIndices',...
                'qrstCancellator',...
                'referenceChannel', '-v7.3');
        end
        
        function LoadQRSTCancellation(self, varargin)
            if isempty(self.EcgData)
                [filename, pathname] = HelperFunctions.customUigetfile('*_QRST.mat', 'Select QRST file');
                if isequal(filename, 0) || isequal(pathname, 0), return; end
                filename = fullfile(pathname, filename);
            else
                [pathString, nameString] = fileparts(self.EcgData.Filename);
                filename = [pathString, filesep, nameString '_QRST.mat'];
            end
            
            self.LoadData(filename);
        end
        
        function ExportQRSTCancellation(self, varargin)
            if isempty(self.CancelledEcgData), return; end
            
            [filename, pathname] = uiputfile('*.ASC', 'Save QRST cancelled signals for ELAN');
            if isequal(filename,0) || isequal(pathname,0), return; end

            time = self.CancelledEcgData.GetTimeRange();
            beginTime = time(1) * 1000;
            endTime = time(end) * 1000;
            fileID = fopen(fullfile(pathname, filename), 'w');
            fprintf(fileID, 'BeginTime: %.0f ms\t EndTime: %.0f ms\n', beginTime, endTime);
            fprintf(fileID, 'QRST analysis signal\n');
            fprintf(fileID, '%s\t', self.CancelledEcgData.ElectrodeLabels{:});
            fprintf(fileID, '\n');
            fclose(fileID);
            
            dlmwrite(fullfile(pathname, filename), self.CancelledEcgData.Data, '-append', 'delimiter', '\t');
        end
        
        function DeleteRPeak(self, rPeakIndex)
            if isnan(self.SelectedRPeakIndex), return; end
            self.RWaveIndices(rPeakIndex) = [];
            delete(self.RPeakLines(:, rPeakIndex));
            self.RPeakLines(:, rPeakIndex) = [];
        end
        
        function newPeakIndex = InsertRPeak(self)
            point = get(self.AxesHandles(1), 'currentPoint');
            mouseXLocation = point(1);
            time = self.EcgData.GetTimeRange();
            [value, newIndex] = min(abs(time - mouseXLocation)); %#ok<ASGLU>
            if ismember(newIndex, self.RWaveIndices), return; end
            
            previousIndexPosition = find(self.RWaveIndices < newIndex, 1, 'last');
            if isempty(previousIndexPosition)
                self.RWaveIndices = [newIndex; self.RWaveIndices];
                newPeakIndex = 1;
            else
                if previousIndexPosition == numel(self.RWaveIndices)
                    self.RWaveIndices = [self.RWaveIndices; newIndex];
                    newPeakIndex = numel(self.RWaveIndices);
                else
                    self.RWaveIndices = [self.RWaveIndices(1:previousIndexPosition);...
                        newIndex; self.RWaveIndices((previousIndexPosition + 1):end)];
                    newPeakIndex = previousIndexPosition + 1;
                end
            end
        end
        
        function MoveRPeak(self, rPeakIndex, amount)
            currentValue = self.RWaveIndices(rPeakIndex);
            newIndex = currentValue + amount;
            if newIndex > 0 &&...
                    newIndex < self.EcgData.GetNumberOfSamples();
                self.RWaveIndices(rPeakIndex) = newIndex;
                
                time = self.EcgData.GetTimeRange();
                newTime = time(self.RWaveIndices(rPeakIndex));
                self.SetRPeak(rPeakIndex, newTime, true);
            end
        end
        
        function SetRPeak(self, rPeakIndex, timePosition, updateIndex)
            for axesIndex = 1:numel(self.AxesHandles)
                    set(self.RPeakLines(axesIndex, rPeakIndex), 'xData', [timePosition, timePosition]);
            end
            
            if updateIndex
                time = self.EcgData.GetTimeRange();
                [~, self.RWaveIndices(rPeakIndex)] = min(abs(time - timePosition));
                
                lineData = [self.EcgData.Data(:, self.SelectedChannels),...
                    self.EcgData.ReferenceData(:, self.SelectedLeadChannels)];
            
                for axesIndex = 1:numel(self.AxesHandles)
                    set(self.RPeakLines(axesIndex, rPeakIndex), 'userData', self.RWaveIndices(rPeakIndex));
                    
                    markerXData = get(self.RPeakMarkers(axesIndex), 'xData');
                    markerYData = get(self.RPeakMarkers(axesIndex), 'yData');
                    markerXData(rPeakIndex) = timePosition;
                    markerYData(rPeakIndex) = lineData(self.RWaveIndices(rPeakIndex), axesIndex);
                    set(self.RPeakMarkers(axesIndex), 'xData', markerXData, 'yData', markerYData);
                end
            end
        end
        
        function SetQRSTRangeBefore(self, source, varargin)
            value = str2double(get(source, 'string'));
            self.QrstCancellator.QRSTRange(1) = value / 100;
        end
        
        function SetQRSTRangeAfter(self, source, varargin)
            value = str2double(get(source, 'string'));
            self.QrstCancellator.QRSTRange(2) = value / 100;
        end
        
        function SetQRSRangeBefore(self, source, varargin)
            value = str2double(get(source, 'string'));
            self.QrstCancellator.QRSRange(1) = value / 1000;
        end
        
        function SetQRSRangeAfter(self, source, varargin)
            value = str2double(get(source, 'string'));
            self.QrstCancellator.QRSRange(2) = value / 1000;
        end
        
        function SetClusterSensitivity(self, source, varargin)
            value = str2double(get(source, 'string'));
            self.QrstCancellator.ClusterSensitivity = value;
        end
        
        function SetShowDiagnostics(self, source, varargin)
            self.QrstCancellator.ShowDiagnostics = get(source, 'value');
        end
        
        function SetQRSResidueReduction(self, source, varargin)
            self.QrstCancellator.QRSResidueReduction = get(source, 'value');
        end
        
        function SetVentricularRefractoryPeriod(self, source, varargin)
            value = str2double(get(source, 'string')) / 1000;
            self.QrstCancellator.VentricularRefractoryPeriod = value;
        end
        
        function SetHighpassPostFilter(self, source, varargin)
            value = str2double(get(source, 'string'));
            self.QrstCancellator.HighpassPostFilter = value;
        end
    end
    
    methods (Access = private)
        function CreateMouseListeners(self)
            mouseEventSingleton = UserInterfacePkg.MouseEventClass.Instance();
            self.MouseListeners(end + 1) = addlistener(mouseEventSingleton, 'MouseMotion', @self.MouseMotionCallback);
            self.MouseListeners(end + 1) = addlistener(mouseEventSingleton, 'MouseDown', @self.MouseDownCallback);
            self.MouseListeners(end + 1) = addlistener(mouseEventSingleton, 'MouseUp', @self.MouseUpCallback);
        end
        
        function MouseMotionCallback(self, varargin)
            persistent previousPointerShape;
            if isempty(self.RPeakLines), return; end
            
            switch self.LineMode
                case {'Moving'}
                    currentPoint = get(self.AxesHandles(self.TouchedAxesIndex), 'currentPoint');
                    self.SetRPeak(self.TouchedRPeakIndex, currentPoint(1, 1), false);
                otherwise
                    selectedObject = hittest(gcf);
                    [axesIndex, rPeakIndex] = find(selectedObject == self.RPeakLines);
                    if isempty(axesIndex)
                        if ~isnan(self.TouchedRPeakIndex)
                            set(gcf, 'Pointer', previousPointerShape);
                            self.TouchedRPeakIndex = NaN;
                            self.TouchedAxesIndex = NaN;
                        end
                    else
                        if isnan(self.TouchedRPeakIndex)
                            previousPointerShape = get(gcf, 'Pointer');
                            set(gcf, 'Pointer', 'hand');
                            self.TouchedRPeakIndex = rPeakIndex;
                            self.TouchedAxesIndex = axesIndex;
                        end
                    end
            end
        end
        
        function MouseDownCallback(self, varargin)
            if isnan(self.TouchedRPeakIndex), return; end
            
            self.LineMode = 'Moving';
        end
        
        function MouseUpCallback(self, varargin)
            switch(self.LineMode)
                case 'Moving'
                    currentPoint = get(self.AxesHandles(self.TouchedAxesIndex), 'currentPoint');
                    self.SetRPeak(self.TouchedRPeakIndex, currentPoint(1, 1), true);
                    self.LineMode = 'None';
                otherwise
                    self.LineMode = 'None';
            end
        end
    end
end
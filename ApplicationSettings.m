classdef ApplicationSettings < handle
    properties
        TemplateMatching
        DeflectionClustering
        VelocityComputation
        SupranormalThreshold
        
        CurrentFileLocation
    end
    
    properties (Constant)
        VELOCITY_METHODS = {'Planar', 'Biquadratic'};
    end
    
    methods (Access = public)
        function settingsStruct = ToStruct(self)
            settingsStruct = struct(...
                'TemplateMatching', self.TemplateMatching,...
                'DeflectionClustering', self.DeflectionClustering);
        end
        
        function delete(self)
            %clear all handles to the Class: ApplicationSettings in the base workspace
            s = evalin('base','whos');
            for i = 1:numel(s)
                if (strcmpi(s(i).class, 'ApplicationSettings'))
                    evalstr = sprintf('clear %s',s(i).name);
                    evalin('base', evalstr);
                end
            end
        end
    end
    
    methods (Access = private)
        function self = ApplicationSettings() 
             self.TemplateMatching = struct(...
                 'Durations', (5:5:50),...
                 'AmplitudeThreshold', 1000,...
                 'SlopeCalculationStepsize', 1,...
                 'FastModeEnabled', true,...
                 'OverlapFilterEnabled', true);
            
             self.DeflectionClustering = struct(...
                'CycleLengthCutoff', .25,...
                'Method', 'AdvancedWindow',...
                'InitialAssignment', 1,...
                'FractionationDiagnosticsWindowSize', 50,...
                'IntrinsicAmplitudeMedianPercentage', 10,...
                'IntrinsicSlopeMedianPercentage', 10);
            
            criteriaProtocol = struct('Parameter', 'Amplitude');
            criteriaProtocol(2).Parameter = 'Duration';
            criteriaProtocol(3).Parameter = 'Correlation';
            
            self.DeflectionClustering.CriteriaProtocol = criteriaProtocol;
            
            self.VelocityComputation = struct(...
                'conductionBlock', 0.2,...
                'hierarchical', true,...
                'waveBased', true,...
                'fullArray', false,...
                'radius', sqrt(18),...
                'method', ApplicationSettings.VELOCITY_METHODS{1});
            
            self.SupranormalThreshold = 1.2;
            
            if ispc
                self.CurrentFileLocation = getenv('USERPROFILE'); 
            else
                self.CurrentFileLocation = [getenv('HOME') '/Documents'];
            end
        end
    end
    
    methods (Static)
        function self = Instance()
            persistent uniqueInstance;
            if isempty(uniqueInstance) || ~isvalid(uniqueInstance)
                uniqueInstance = ApplicationSettings();
            end
            self = uniqueInstance;
        end
    end
end
classdef DepolarizationRepolarizationPanel < UserInterfacePkg.CustomPanel
    properties
        EcgData
        Depolarizations
        Repolarizations
    end

    properties (Access = private)
        ControlPanel
        AnimationButton
        AnimationSlider
        AnimationPosition
        AnimationPositionLabel
        TimeLag
        FrameRate
        FrameRateEdit

        EcgMapControl
        DepolarizationMapControl
        RepolarizationMapControl

        SignalAxes
        SignalLine
        DepolarizationLine
        RepolarizationLine
        AnimationPositionLine

        SelectedChannelText
        SelectedChannelIndex
    end

    methods
        function self = DepolarizationRepolarizationPanel(position)
            self = self@UserInterfacePkg.CustomPanel(position);
            self.TimeLag = 100;
            self.SelectedChannelIndex = 1;
            self.FrameRate = 5e-3;
        end

        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);

            self.CreateControlPanel();
            self.CreateAnimationPanel();
            self.CreateSignalPanel();
        end

        function SetECGData(self, ecgData)
            self.EcgData = ecgData;

            self.SelectedChannelIndex = 1;

            self.InitializeSignalMapAnimation();
            
            self.InitializeSignalAxes();

            self.AnimationPosition = 1;
            numberOfSamples = self.EcgData.GetNumberOfSamples();
            set(self.AnimationSlider, 'min', 1, 'max', numberOfSamples,...
                'value', self.AnimationPosition,...
                'sliderStep', [1 / (numberOfSamples - 1), 1 / (numberOfSamples - 1)]);            

            self.SetAnimationFrame();
        end

        function SetDepolarizations(self, deflections)
            self.Depolarizations = deflections;
            
            depolarizationTimes = cellfun(@(x) 1e3 * x / self.EcgData.SamplingFrequency,...
                deflections, 'UniformOutput', false);
            afcl = cellfun(@(x) mean(diff(x)), depolarizationTimes);
            self.TimeLag = round(median(afcl(~isnan(afcl))) / 2);

            self.FrameRate = round(median(afcl) / 60) / 1e3;
            set(self.FrameRateEdit, 'string', num2str(1000 * self.FrameRate));

            self.InitializeDepolarizationAnimation();
        end

        function SetRepolarizations(self, deflections)
            self.Repolarizations = deflections;
            
            self.InitializeRepolarizationAnimation();
        end
    end

    methods (Access = private)
        function CreateControlPanel(self)
            self.ControlPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0 .95 .7 .05]);

            self.AnimationButton = uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [0 0 .1 1],...
                'style', 'togglebutton',...
                'string', '>',...
                'callback', @self.ControlAnimation);

            self.AnimationSlider = uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.2 .1 .7 .8],...
                'style', 'slider',...
                'callback', @self.AnimationSliderCallback);
            addlistener(handle(self.AnimationSlider), 'Value', 'PostSet', @self.ContinuousAnimationSliderCallback);

            self.AnimationPositionLabel = uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.1 .1 .1 .9],...
                'style', 'text',...
                'FontSize', 12,...
                'string', '0');

            frameRatePanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [.9 0 .1 1],...
                'title', 'Frame rate (ms)');
            self.FrameRateEdit = uicontrol('parent', frameRatePanel,...
                'units', 'normalized',...
                'position', [.1 .1 .8 .8],...
                'style', 'edit',...
                'string', num2str(1000 * self.FrameRate),...
                'callback', @self.SetFrameRate);
        end

        function CreateAnimationPanel(self)
            self.EcgMapControl = ExtendedUIPkg.SignalMapControl([.7 .7 .3 .3]);
            self.EcgMapControl.Name = 'Signal';
            self.EcgMapControl.Create(self.ControlHandle);
            addlistener(self.EcgMapControl, 'MapClicked', @self.HandleMapClick);

            self.DepolarizationMapControl = ExtendedUIPkg.GridActivationMapControl([0 0 .5 .7]);
            self.DepolarizationMapControl.Name = 'Depolarizations';
            self.DepolarizationMapControl.Create(self.ControlHandle);
            addlistener(self.DepolarizationMapControl, 'MapClicked', @self.HandleMapClick);

            self.RepolarizationMapControl = ExtendedUIPkg.GridActivationMapControl([.5 0 .5 .7]);
            self.RepolarizationMapControl.Name = 'Repolarizations';
            self.RepolarizationMapControl.Create(self.ControlHandle);
            addlistener(self.RepolarizationMapControl, 'MapClicked', @self.HandleMapClick);

            deflectionColormap = flipud(jet(256 + 10));
            deflectionColormap = deflectionColormap(6:(end-5), :);
            self.DepolarizationMapControl.SetColormap(deflectionColormap, 256);
            self.RepolarizationMapControl.SetColormap(deflectionColormap, 256);
        end

        function CreateSignalPanel(self)
            signalPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0 .7 .7 .25]);
            self.SignalAxes = axes('parent', signalPanel, 'OuterPosition', [0,0,1,1]);
        end

        function InitializeSignalMapAnimation(self)
            self.EcgMapControl.SamplingFrequency = self.EcgData.SamplingFrequency;
            self.EcgMapControl.TimeLag = self.TimeLag;
            self.EcgMapControl.SetData(self.EcgData.MappedData, self.EcgData.ElectrodePositions);
            self.EcgMapControl.Show();
            self.EcgMapControl.MaximizeAxes();
        end

        function InitializeDepolarizationAnimation(self)
            samplingFrequency = self.EcgData.SamplingFrequency;
            self.DepolarizationMapControl.SamplingFrequency = samplingFrequency;
            self.DepolarizationMapControl.TimeLag = self.TimeLag;
            depolarizationTimes = cellfun(@(x) 1e3 * x / samplingFrequency,...
                self.Depolarizations, 'UniformOutput', false);
            self.DepolarizationMapControl.SetData(depolarizationTimes, self.EcgData.ElectrodePositions);
            self.DepolarizationMapControl.Show();
        end

        function InitializeRepolarizationAnimation(self)
            samplingFrequency = self.EcgData.SamplingFrequency;
            self.RepolarizationMapControl.SamplingFrequency = samplingFrequency;
            self.RepolarizationMapControl.TimeLag = self.TimeLag;
            repolarizationTimes = cellfun(@(x) 1e3 * x / samplingFrequency,...
                self.Repolarizations, 'UniformOutput', false);
            self.RepolarizationMapControl.SetData(repolarizationTimes, self.EcgData.ElectrodePositions);
            self.RepolarizationMapControl.Show();
        end

        function InitializeSignalAxes(self)
            if isempty(self.EcgData), return; end

            signal = self.EcgData.Data(:, self.SelectedChannelIndex);
            time = self.EcgData.GetTimeRange();

            if ishandle(self.DepolarizationLine)
                delete(self.DepolarizationLine);
            end

            self.DepolarizationLine = line(...
                'xData', [], 'yData', [],...
                'lineStyle', 'none',...
                'marker', 'o', 'markerSize', 7,...
                'markerEdgeColor', [0 0 0], 'markerFaceColor', [1 0 0],...
                'parent', self.SignalAxes);

            if ishandle(self.RepolarizationLine)
                delete(self.RepolarizationLine);
            end

            self.RepolarizationLine = line(...
                'xData', [], 'yData', [],...
                'lineStyle', 'none',...
                'marker', 'o', 'markerSize', 7,...
                'markerEdgeColor', [0 0 0], 'markerFaceColor', [.7 .7 .7],...
                'parent', self.SignalAxes);

            if ishandle(self.SignalLine)
                delete(self.SignalLine);
            end
            self.SignalLine = line('parent', self.SignalAxes,...
                'xData', time, 'yData', signal, 'color', [0 0 0], 'lineSmoothing', 'on');

            if ishandle(self.AnimationPositionLine)
                delete(self.AnimationPositionLine);
            end
            self.AnimationPositionLine = line(...
                'xData', [0 0], 'yData', [0 0],...
                'color', [1 0 0],...
                'lineWidth', 1,...
                'parent', self.SignalAxes);

            self.SelectedChannelText = text(...
                'units', 'normalized',...
                'position', [0, 1],...
                'string', '',...
                'verticalAlignment', 'top',...
                'parent', self.SignalAxes,...
                'backgroundColor', [1 1 1],...
                'edgeColor', [0 0 0]);

            set(self.SignalAxes, 'LooseInset', get(self.SignalAxes, 'TightInset'));

            self.ShowSelectedChannel();
        end

        function ControlAnimation(self, varargin)
            set(self.AnimationButton, 'string', '||');
            numberOfSamples = self.EcgData.GetNumberOfSamples();
            frameStep = round(self.FrameRate * self.EcgData.SamplingFrequency);

            while get(self.AnimationButton, 'value')
                drawnow;
                self.AnimationPosition = self.AnimationPosition + frameStep;
                if self.AnimationPosition > numberOfSamples
                    self.AnimationPosition = 1;
                end

                self.SetAnimationFrame();

                if mod(self.AnimationPosition, 100) == 0
                    set(self.AnimationSlider, 'value', self.AnimationPosition);
                    self.SetAnimationInfo();
                end
            end

            set(self.AnimationSlider, 'value', self.AnimationPosition);

            set(self.AnimationButton, 'string', '>');

            self.SetStaticFrame();
        end

        function AnimationSliderCallback(self, varargin)
            self.AnimationPosition = round(get(self.AnimationSlider, 'value'));
            self.SetStaticFrame();
        end

        function ContinuousAnimationSliderCallback(self, ~, eventData)
            if verLessThan('matlab', '8.4.0')
                sliderObject = get(eventData, 'AffectedObject');
            else
                sliderObject = eventData.AffectedObject;
            end
            self.AnimationPosition = round(get(sliderObject, 'Value'));
            self.SetAnimationInfo();
            self.SetAnimationFrame();
        end

        function SetStaticFrame(self)
            self.SetAnimationInfo();

            self.EcgMapControl.SetStaticFramePosition(self.AnimationPosition);
            firstActivation = self.DepolarizationMapControl.SetRelativeStaticFramePosition(self.AnimationPosition);
            self.RepolarizationMapControl.SetRelativeStaticFramePosition(self.AnimationPosition, firstActivation);
        end

        function SetAnimationFrame(self)
            self.SetAnimationInfo();

            self.EcgMapControl.SetFramePosition(self.AnimationPosition);
            self.DepolarizationMapControl.SetFramePosition(self.AnimationPosition);
            self.RepolarizationMapControl.SetFramePosition(self.AnimationPosition);
        end

        function SetAnimationInfo(self)
            timePosition = self.AnimationPosition / self.EcgData.SamplingFrequency;
            set(self.AnimationPositionLine, 'xData', [timePosition, timePosition]);

            set(self.AnimationPositionLabel, 'string', [ num2str(round(1e3 * timePosition)) ' ms']);

            signalAxesXLimits = get(self.SignalAxes, 'XLim');
            xlimitRange = signalAxesXLimits(end) - signalAxesXLimits(1);
            
            xMargin = 0.1;
            if timePosition < signalAxesXLimits(1) + xMargin * xlimitRange
                timePosition = timePosition - xMargin * xlimitRange;
                signalAxesXLimits = [timePosition,...
                    timePosition + xlimitRange];
                set(self.SignalAxes, 'XLim', signalAxesXLimits);
            elseif timePosition > signalAxesXLimits(end) - xMargin * xlimitRange
                timePosition = timePosition + xMargin * xlimitRange;
                signalAxesXLimits = [timePosition - xlimitRange, timePosition];
                set(self.SignalAxes, 'XLim', signalAxesXLimits);
            end
        end

        function HandleMapClick(self, ~, eventData)
            self.SelectedChannelIndex = eventData.Position;

            self.ShowSelectedChannel();
        end

        function ShowSelectedChannel(self)
            channelLabel = self.EcgData.ElectrodeLabels{self.SelectedChannelIndex};
            set(self.SelectedChannelText, 'string',...
                ['Channel: ', channelLabel]);

            signal = self.EcgData.Data(:, self.SelectedChannelIndex);
            time = self.EcgData.GetTimeRange();

            set(self.SignalLine, 'yData', signal);

            yLimits = [min(signal), max(signal)];
            set(self.AnimationPositionLine, 'yData', yLimits);

            if ~isempty(self.Depolarizations)
                deflectionIndices = self.Depolarizations{self.SelectedChannelIndex};

                set(self.DepolarizationLine, 'xData', time(deflectionIndices),...
                    'yData', signal(deflectionIndices));
            end
            
            if ~isempty(self.Repolarizations)
                deflectionIndices = self.Repolarizations{self.SelectedChannelIndex};

                set(self.RepolarizationLine, 'xData', time(deflectionIndices),...
                    'yData', signal(deflectionIndices));
            end



        end

        function SetFrameRate(self, source, varargin)
            self.FrameRate = round(str2double(get(source, 'string'))) / 1000;
        end
    end

end
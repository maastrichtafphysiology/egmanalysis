function y = DoubleSigmoidal(beta,x)
a = beta(1);
x01 = beta(2);
x02 = beta(3);
k1 = beta(4);
k2 = beta(5);
y = a.* (1./(1+exp((x-x01)/k1)) .* (1 - 1./(1+exp((x-x02)/k2))));

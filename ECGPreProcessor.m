classdef ECGPreProcessor < handle
    properties
        BandpassFrequencies
        BandpassMethod
        BandpassOrder
        NotchFrequency
        NotchOrder
        NotchLeads
        MedianBaselineWindowInSec
        
        ApplyBandpassFilter
        ApplyNotchFilter
        ApplyCombFilter
        ApplyCombFilterPerLead
        ApplyMedianBaselineFilter
    end
    
    properties (Access = protected)
        SamplingFrequency;
    end
    
    properties (Constant)
        FILTERMETHODS = {'FIR', 'Chebyshev Type II'}
    end
    
    methods
        function self = ECGPreProcessor()
            self.BandpassMethod = ECGPreProcessor.FILTERMETHODS{2};
            self.BandpassFrequencies = [0.1, 100];
            self.BandpassOrder = 3;
            self.NotchFrequency = 50;
            self.NotchOrder = 70;
            self.MedianBaselineWindowInSec = .75;
            
            self.NotchLeads = [];
            
            self.ApplyBandpassFilter = true;
            self.ApplyNotchFilter = false;
            self.ApplyCombFilter = false;
            self.ApplyCombFilterPerLead = false;
            self.ApplyMedianBaselineFilter = false;
        end
        
        function filteredEcgData = Filter(self, ecgData)
            self.SamplingFrequency = ecgData.SamplingFrequency;
            filteredData = ecgData.Data;
            for channelIndex = 1:ecgData.GetNumberOfChannels
                if self.ApplyCombFilter
                    filteredData(:, channelIndex) =...
                        self.CombFilter(filteredData(:, channelIndex));
                elseif self.ApplyNotchFilter
                    filteredData(:, channelIndex) =...
                        self.NotchFilter(filteredData(:, channelIndex));
                elseif self.ApplyCombFilterPerLead
                    filteredData(:, channelIndex) =...
                        self.CombFilterPerLead(filteredData(:, channelIndex),channelIndex);
                end
                
                if self.ApplyBandpassFilter
                    filteredData(:, channelIndex) =...
                        self.BandpassFilter(filteredData(:, channelIndex));
                end
            end
            
            if self.ApplyMedianBaselineFilter
                MedianBaselineWindowInSamples = round(self.MedianBaselineWindowInSec * ecgData.SamplingFrequency);
                baselineEstimation = medfilt1(filteredData,MedianBaselineWindowInSamples);
                filteredData = filteredData - baselineEstimation;
            end
            
            filteredLeadData = ecgData.ReferenceData;
            parfor leadIndex = 1:numel(ecgData.ReferenceLabels)
                if self.ApplyNotchFilter
                    filteredLeadData(:, leadIndex) =...
                        self.NotchFilter(filteredLeadData(:, leadIndex));
                end
                
                if self.ApplyBandpassFilter
                    filteredLeadData(:, leadIndex) =...
                        self.BandpassFilter(filteredLeadData(:, leadIndex));
                end
            end
            
            filteredEcgData = ecgData.Copy();
            filteredEcgData.Data = filteredData;
            filteredEcgData.ReferenceData = filteredLeadData;
        end
        
        function [psdEstimate, psdLeadEstimate] = EstimatePSD(self, ecgData)
            self.SamplingFrequency = ecgData.SamplingFrequency;
            numberOfChannels = ecgData.GetNumberOfChannels();
            if numberOfChannels > 0
                psdEstimate(numberOfChannels, 1) = struct(...
                    'Frequencies', [],...
                    'Data', []);
                samplingFrequency = self.SamplingFrequency;
                data = ecgData.Data;
                try
                    for channelIndex = 1:ecgData.GetNumberOfChannels
                        [frequencyPower, frequencies] = pwelch(data(:, channelIndex), [], [], [], samplingFrequency);
                        psdEstimate(channelIndex).Frequencies = frequencies;
                        psdEstimate(channelIndex).Data = frequencyPower;
                    end
                catch ME
                    disp(ME);
                end
            else
                psdEstimate = [];
            end
            
            if isempty(ecgData.ReferenceLabels)
                psdLeadEstimate = [];
                return;
            end
            
            psdLeadEstimate(numel(ecgData.ReferenceLabels), 1) = struct(...
                'Frequencies', [],...
                'Data', []);
            referenceData = ecgData.ReferenceData;
            try
                for channelIndex = 1:numel(ecgData.ReferenceLabels)
                    [frequencyPower, frequencies] = pwelch(referenceData(:, channelIndex), [], [], [], samplingFrequency);
                    psdLeadEstimate(channelIndex).Frequencies = frequencies;
                    psdLeadEstimate(channelIndex).Data = frequencyPower;
                end
            catch ME
                disp(ME);
            end
        end
        
        function filteredSignal = NotchFilter(self, signal)
            filteredSignal = double(signal);
            for notchIndex = 1:numel(self.NotchFrequency)
                nyquistFrequency = self.SamplingFrequency / 2;
                relativeFrequency = self.NotchFrequency(notchIndex) / nyquistFrequency;
                quality = self.NotchOrder;
                bandWidth = relativeFrequency / quality;
                try
                    [num, den] = iirnotch(relativeFrequency, bandWidth);
                    filteredSignal = filtfilt(num, den, filteredSignal);
                catch ME
                    [num, den] = HelperFunctions.iirnotchCustom(relativeFrequency, bandWidth);
                    filteredSignal = filtfilt(num, den, filteredSignal);
                end
            end
        end
        
        
        
        function filteredSignal = CombFilter(self, signal)
            filteredSignal = double(signal);
            for notchIndex = 1:numel(self.NotchFrequency)
                combOrder = round(self.SamplingFrequency / self.NotchFrequency(notchIndex));
                quality = self.NotchOrder;
                bandWidth = (self.NotchFrequency(notchIndex) / (self.SamplingFrequency / 2)) / quality;
                try
                    [num, den] = iircomb(combOrder, bandWidth, 'notch');
                    filteredSignal = filtfilt(num, den, filteredSignal);
                catch ME
                    [num, den] = HelperFunctions.iircombCustom(combOrder, bandWidth);
                    filteredSignal = filtfilt(num, den, filteredSignal);
                end
            end
        end
        
        
        function filteredSignal = CombFilterPerLead(self, signal, channelIndex)
            filteredSignal = double(signal);
            if self.NotchLeads(channelIndex)
                for notchIndex = 1:numel(self.NotchFrequency)
                    nyquistFrequency = self.SamplingFrequency / 2;
                    relativeFrequency = self.NotchFrequency(notchIndex) / nyquistFrequency;
                    quality = self.NotchOrder;
                    bandWidth = relativeFrequency / quality;
                    try
                        [num, den] = iirnotch(relativeFrequency, bandWidth);
                        filteredSignal = filtfilt(num, den, filteredSignal);
                    catch ME
                        [num, den] = HelperFunctions.iirnotchCustom(relativeFrequency, bandWidth);
                        filteredSignal = filtfilt(num, den, filteredSignal);
                    end
                end
            end
        end
        
        function filteredSignal = BandpassFilter(self, signal)
            
            nyquistFrequency = self.SamplingFrequency / 2;
            
            bandpassFrequencies(1) = max([0 self.BandpassFrequencies(1)]);
            bandpassFrequencies(2) = min([nyquistFrequency self.BandpassFrequencies(2)]);
            if bandpassFrequencies(1) > bandpassFrequencies(2)
                ME = MException('AFAnalysis:filterSignal', 'Invalid bandpass bounds');
                throw(ME);
            end
            
            try
                switch self.BandpassMethod
                    case ECGPreProcessor.FILTERMETHODS{1}
                        [a, b] = ECGPreProcessor.ComputeFIRCoefficients(...
                            bandpassFrequencies, nyquistFrequency, self.BandpassOrder);
                    case ECGPreProcessor.FILTERMETHODS{2}
                        [a, b] = ECGPreProcessor.ComputeCheby2Coefficients(...
                            bandpassFrequencies, nyquistFrequency, self.BandpassOrder);
                end
                
                filteredSignal = filtfilt(b, a, double(signal));
                
            catch ME
                disp(ME);
                filteredSignal = signal;
            end
        end
    end
    
    methods (Static)
        function [a, b] = ComputeFIRCoefficients(bandpassFrequencies, nyquistFrequency, order)
            MIN_EDGE_DEVIATION = 1e-3;
            
            frequencyPoints = [0 bandpassFrequencies(1) / nyquistFrequency...
                bandpassFrequencies(1) / nyquistFrequency...
                bandpassFrequencies(2) / nyquistFrequency...
                bandpassFrequencies(2) / nyquistFrequency 1];
            magnitudeResponse = [0 0 1 1 0 0];
            
            if bandpassFrequencies(1) < MIN_EDGE_DEVIATION
                frequencyPoints = [0 bandpassFrequencies(2) / nyquistFrequency ...
                    bandpassFrequencies(2) / nyquistFrequency 1];
                magnitudeResponse = [1 1 0 0];
            end
            
            if nyquistFrequency - bandpassFrequencies(2) < MIN_EDGE_DEVIATION
                frequencyPoints = [0 bandpassFrequencies(1) / nyquistFrequency...
                    bandpassFrequencies(1) / nyquistFrequency 1];
                magnitudeResponse = [0 0 1 1];
            end
            
            if (bandpassFrequencies(1) < MIN_EDGE_DEVIATION)...
                    && (nyquistFrequency - bandpassFrequencies(2) < MIN_EDGE_DEVIATION)
                frequencyPoints = [0 1];
                magnitudeResponse = [1 1];
            end
            
            lap = 5;
            npt = 2^(nextpow2(min([4 * nyquistFrequency, numel(signal) / 4])));
            filterCoefficients = fir2(order, frequencyPoints,...
                magnitudeResponse, fix(npt), fix(lap));
            a = 1;
            b = filterCoefficients;
        end
        
        function [a, b] = ComputeCheby2Coefficients(bandpassFrequencies, nyquistFrequency, order)
            if bandpassFrequencies(2) >= nyquistFrequency
                [b, a] = cheby2(order, 20,...
                    bandpassFrequencies(1) / nyquistFrequency, 'high');
            else
                [b, a] = cheby2(order, 20,...
                    [bandpassFrequencies(1) / nyquistFrequency bandpassFrequencies(2) / nyquistFrequency]);
            end
        end
    end
end
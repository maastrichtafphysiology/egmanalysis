classdef DCMPreview < UserInterfacePkg.CustomFigure
    properties (Access = private)
        EcgData
        
        TabGroup
        
        ChannelSelectionView
        PaceBlankingView
        DeflectionDetectionView
        DCMPreviewView
        
        ParallelProcessing
        
        ZoomToggleButton
        PanToggleButton
        
        MappedFileTypeIndices
    end
    
    properties (Constant)
        FILETYPEFILTER = {...
            '*.DATA', 'IDEEQ Data (*.DATA)';...
            '*.mat', 'MAT-file containing ecgData (*.mat)';...
            '*.*', 'DCM (PACEMAP Binary) (*.E**)';...
            '*.bdf', 'BioSemi data format (*.bdf)';...
            '*.csv', 'Basket Export (*.csv)'};
    end
    
    methods
        function self = DCMPreview(position)
            if nargin < 1
                screenSize = get(0,'Screensize');
                position = [screenSize(3) / 5, screenSize(4) / 5,...
                    3 * screenSize(3) / 5, 3 * screenSize(4) / 5];
            end
            
            self = self@UserInterfacePkg.CustomFigure(position);
            self.Name = 'Direct Contact Map Preview';
            self.ParallelProcessing = false;
            self.MappedFileTypeIndices = 1:size(DCMPreview.FILETYPEFILTER, 1);
            
            if nargin < 1            
                self.Create();
            end
        end
        
        function Create(self)
            Create@UserInterfacePkg.CustomFigure(self);
            
            set(self.ControlHandle,...
                'CloseRequestFcn', @self.Close);
            
            set(self.ControlHandle,...
                'colormap', flipud(gray(256)));
            
            self.Hide();
            
            self.CreateFileMenu();
            
            self.CreateSettingsMenu();
            
            self.CreateToolbar();
            
            self.InitializeEvents();
            
            self.Show();
        end
        
        function delete(self)
            %             self.RemoveTabs();
            %
            %             delete(ApplicationSettings.Instance()); %#ok<PROP>
            %
            %             if isobject(self.ApplicationSettings)
            %                 delete(self.ApplicationSettings);
            %             end
            %
            %             if isobject(self.BSPMView)
            %                 delete(self.BSPMView);
            %             end
            %
            %             if isobject(self.EcgData)
            %                 delete(self.EcgData)
            %             end
            %
            %             delete(timerfindall);
        end
    end
    
    methods (Access = private)
        function InitializeTabs(self)
            self.RemoveTabs();
            if ishandle(self.TabGroup)
                delete(self.TabGroup);
            end
            
            self.TabGroup = uitabgroup(...
                'parent', self.ControlHandle,...
                'position', [0 .025 1 .975]);
            
            channelSelectionTab = uitab(self.TabGroup, 'title','Data');
            self.ChannelSelectionView = DCMVisualizationPanel([0 0 1 1]);
            self.ChannelSelectionView.Create(channelSelectionTab);
            
            
            addlistener(self.ChannelSelectionView, 'ValidChannelsSelectionCompleted',...
                @self.HandleChannelSelectionEvent);
            
            paceBlankingTab = uitab(self.TabGroup, 'title','Pacing artifacts');
            self.PaceBlankingView = PaceBlankingPanel([0 0 1 1]);
            self.PaceBlankingView.Create(paceBlankingTab);
            
            addlistener(self.PaceBlankingView, 'PaceBlankingCompleted',...
                @self.HandlePaceBlankingEvent);
            
            deflectionDetectionTab = uitab(self.TabGroup, 'title','Deflection Detection');
            self.DeflectionDetectionView = DeflectionDetectionTestPanel([0 0 1 1]);
            self.DeflectionDetectionView.Create(deflectionDetectionTab);
            
            addlistener(self.DeflectionDetectionView, 'DeflectionDetectionCompleted',...
                @self.HandleDeflectionDetectionEvent);
            
            previewTab = uitab(self.TabGroup, 'title','Propagation Preview');
            self.DCMPreviewView = DCMPreviewPanel([0 0 1 1]);
            self.DCMPreviewView.Create(previewTab);
            
            self.ChannelSelectionView.Show();
            self.PaceBlankingView.Show();
            self.DeflectionDetectionView.Show();
            self.DCMPreviewView.Show();
        end
        
        function ShowEmptyPreviewTabs(self, varargin)
            self.EcgData = DCMData('Empty tabs', [], NaN);
            self.InitializeTabs();
            self.SetFilename();
        end
        
        function RemoveTabs(self)
            if isobject(self.ChannelSelectionView)
                delete(self.ChannelSelectionView);
            end
        end
        
        function CreateFileMenu(self)
            fileMenu = uimenu(...
                'parent', self.ControlHandle,...
                'label', 'File');
            uimenu('parent', fileMenu,...
                'label', 'Load signal',...
                'callback', @self.LoadSignal);
        end
        
        function CreateSettingsMenu(self)
            settingsMenu = uimenu(...
                'parent', self.ControlHandle,...
                'label', 'Settings');
            uimenu(...
                'parent', settingsMenu,...
                'label', 'Parallel processing',...
                'callback', @self.SetParallelProcessing);
        end
        
        function CreateToolbar(self)
            toolbar = uitoolbar(...
                'parent', self.ControlHandle);
            toolbarIconData = load('toolbarIcons.mat');
            uipushtool(...
                'parent', toolbar,...
                'CData', toolbarIconData.opendoc,...
                'tooltipString', 'Load signal',...
                'clickedCallback', @self.LoadSignal);
            self.ZoomToggleButton = uitoggletool(...
                'parent', toolbar,...
                'CData', toolbarIconData.zoomCData,...
                'offCallback', 'zoom off',...
                'onCallback', @self.ZoomOn,...
                'separator', 'on');
            self.PanToggleButton = uitoggletool(...
                'parent', toolbar,...
                'CData', toolbarIconData.panCData,...
                'offCallback', 'pan off',...
                'onCallback', @self.PanOn,...
                'separator', 'on');
            
            uipushtool(...
                'parent', toolbar,...
                'CData', toolbarIconData.DCMButtonData,...
                'tooltipString', 'Show empty DCM tabs',...
                'clickedCallback', @self.ShowEmptyPreviewTabs,...
                'separator', 'on');
        end
        
        function SetFilename(self)
            if isempty(self.EcgData), return; end
            
            self.Name = ['Direct Contact Map Preview - ', self.EcgData.Filename];
        end
        
        function InitializeEvents(self)
            set(self.ControlHandle, 'KeyPressFcn', @self.NotifyKeyPress);
            set(self.ControlHandle, 'KeyReleaseFcn', @self.NotifyKeyRelease);
            
            set(self.ControlHandle, 'WindowButtonMotionFcn', @self.NotifyMouseMotion)
            set(self.ControlHandle, 'WindowButtonUpFcn', @self.NotifyMouseUp)
            set(self.ControlHandle, 'WindowButtonDownFcn', @self.NotifyMouseDown)
        end
        
        function NotifyKeyPress(self, ~, eventData)
            UserInterfacePkg.KeyEventClass.Instance().NotifyKeyPressed(UserInterfacePkg.KeyPressedEventData(eventData));
        end
        
        function NotifyKeyRelease(self, ~, eventData)
            UserInterfacePkg.KeyEventClass.Instance().NotifyKeyReleased(UserInterfacePkg.KeyPressedEventData(eventData));
        end
        
        function NotifyMouseMotion(self, varargin)
            UserInterfacePkg.MouseEventClass.Instance().NotifyMouseMotion();
        end
        
        function NotifyMouseUp(self, varargin)
            UserInterfacePkg.MouseEventClass.Instance().NotifyMouseUp();
        end
        
        function NotifyMouseDown(self, varargin)
            UserInterfacePkg.MouseEventClass.Instance().NotifyMouseDown();
        end
        
        function LoadSignal(self, varargin)
            fileTypeFilters = DCMPreview.FILETYPEFILTER;
            mappedFiletypeFilters = fileTypeFilters(self.MappedFileTypeIndices, :);

            % MAC OS (until 11.4) is not compatible with the uigetfile
            % filter option. Workaround for mac users:
            if ismac
                filterIndex = menu('Select file type',fileTypeFilters(:,2));
                [fileName, pathName, ~] = HelperFunctions.customUigetfile(...
                    '*.*',...
                    'Select a signal file');
            elseif ispc
                [fileName, pathName, filterIndex] = HelperFunctions.customUigetfile(...
                    mappedFiletypeFilters,...
                    'Select a signal file');
            end
            
            if isequal(fileName, 0), return; end
            
            mappedFilterIndex = self.MappedFileTypeIndices(filterIndex);
            if filterIndex > 1
                fileTypeIndices = 1:size(DCMPreview.FILETYPEFILTER, 1);
                fileTypeIndices(mappedFilterIndex) = [];
                self.MappedFileTypeIndices = [mappedFilterIndex, fileTypeIndices];
            end
            
            switch mappedFilterIndex
                case 1
                    ecgData = InputOutputPkg.ECGReader.ReadIDEEQ(fullfile(pathName, fileName));
                case 2
                    fileData = load(fullfile(pathName, fileName));
                    ecgData = fileData.ecgData;
                    ecgData.Filename = fullfile(pathName, fileName);
                    if isempty(ecgData.ElectrodePositions)
                        ecgData.SetElectrodeMap([]);
                    end
                case 3
                    ecgData = InputOutputPkg.ECGReader.ReadPACEMAPBinary(fullfile(pathName, fileName));
                case 4
                    ecgData = InputOutputPkg.ECGReader.ReadDCMBDF(fullfile(pathName, fileName));
                    ecgData.SetElectrodeMap([]);
                case 5
                    ecgData = InputOutputPkg.ECGReader.ReadBasketExportData(fullfile(pathName, fileName));
                otherwise
                    errordlg('File Error', 'Unkown file type');
                    return;
            end
            
            self.EcgData = ecgData;
            self.SetFilename();
            self.InitializeTabs();
            
            self.ChannelSelectionView.SetECGData(self.EcgData);
        end
        
        function SetParallelProcessing(self, source, varargin)
            if self.ParallelProcessing
                self.ParallelProcessing = false;
                set(source, 'checked', 'off');
                
                if verLessThan('matlab', '7.0.1')
                    if matlabpool('size') > 0
                        matlabpool('close');
                    end
                else
                    delete(gcp('nocreate'));
                end
            else
                self.ParallelProcessing = true;
                set(source, 'checked', 'on');
                
                if verLessThan('matlab', '7.0.1')
                    if matlabpool('size') == 0
                        matlabpool('open');
                    end
                else
                    if isempty(gcp('nocreate'))
                        parpool;
                    end
                end
            end
        end
        
        function PanOn(self, varargin)
            pan on;
            set(self.ZoomToggleButton, 'state', 'off');
        end
        
        function ZoomOn(self, varargin)
            set(self.PanToggleButton, 'state', 'off');
            zoom on;
        end
        
        function HandleChannelSelectionEvent(self, varargin)
            disp('Channels selected');
            
            ecgData = self.ChannelSelectionView.GetECGDataToAnalyze();
            
            self.PaceBlankingView.SetECGData(ecgData);
            self.DeflectionDetectionView.SetECGData(ecgData); 
        end
        
        function HandlePaceBlankingEvent(self, varargin)
            disp('Pace detection completed');
            
            ecgData = self.PaceBlankingView.GetBlankedECGData();
            
            self.DCMPreviewView.SetECGData(ecgData);
            self.DeflectionDetectionView.SetECGData(ecgData);
        end
        
        function HandleDeflectionDetectionEvent(self, varargin)
            disp('Deflections detected');
            
            deflections = self.DeflectionDetectionView.DetectedDeflections;
            
            self.DCMPreviewView.SetECGData(self.DeflectionDetectionView.EcgData);
            self.DCMPreviewView.SetDeflections(deflections);
        end
        
        function Close(self, varargin)
            self.RemoveTabs();
            
            if isobject(self.EcgData)
                delete(self.EcgData)
            end
            
            delete(timerfindall);
            
            delete(self.ControlHandle);
        end
    end
end
classdef DCMPreviewPanel < UserInterfacePkg.CustomPanel
    properties
        EcgData
    end
    
    properties (Access = private)
        ControlPanel
        AnimationButton
        AnimationSlider
        AnimationPosition
        AnimationPositionLabel
        TimeLag
        FrameRate
        
        EcgMapControl
        DeflectionMapControl
        
        SignalAxes
        SignalLine
        IntrinsicDeflectionLine
        AnimationPositionLine

        SelectedChannelText
        SelectedChannelIndex

        VideoRange
        VideoRangeStartEdit
        VideoRangeStopEdit
    end
    
    methods
        function self = DCMPreviewPanel(position)
            self = self@UserInterfacePkg.CustomPanel(position);
            self.TimeLag = 100;
            self.VideoRange = [NaN, NaN];
            self.SelectedChannelIndex = 1;
            self.FrameRate = 5e-3;
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            
            self.CreateControlPanel();
            self.CreateAnimationPanel();
            self.CreateSignalPanel();
        end
        
        function SetECGData(self, ecgData)
            self.EcgData = ecgData;
            
            if isempty(self.EcgData.Activations)
                self.ComputeActivations();
            end
            
            afcl = cellfun(@(x) mean(diff(x)), self.EcgData.Activations);
            self.TimeLag = round(median(afcl(~isnan(afcl))) / 2);
            
            self.SelectedChannelIndex = 1;
            
            self.InitializeSignalMapAnimation();
            self.InitializeDeflectionAnimation();
            self.InitializeSignalAxes();
            
            self.AnimationPosition = 1;
            numberOfSamples = self.EcgData.GetNumberOfSamples();
            set(self.AnimationSlider, 'min', 1, 'max', numberOfSamples,...
                'value', self.AnimationPosition,...
                'sliderStep', [1 / (numberOfSamples - 1), 1 / (numberOfSamples - 1)]);
            
            time = self.EcgData.GetTimeRange();
            self.VideoRange = [time(1), time(end)];
            set(self.VideoRangeStartEdit, 'string', num2str(self.VideoRange(1)));
            set(self.VideoRangeStopEdit, 'string', num2str(self.VideoRange(2)));
            
            self.SetAnimationFrame();
        end
        
        function SetDeflections(self, deflections)
            self.EcgData.Activations = deflections;
            self.SetECGData(self.EcgData);
        end
    end
    
    methods (Access = private)
        function CreateControlPanel(self)
            self.ControlPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0 .9 1 .1]);
            
            self.AnimationButton = uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [0 0 .1 1],...
                'style', 'togglebutton',...
                'string', '>',...
                'callback', @self.ControlAnimation);
            
            self.AnimationSlider = uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.2 .3 .6 .4],...
                'style', 'slider',...
                'callback', @self.AnimationSliderCallback);
            addlistener(handle(self.AnimationSlider), 'Value', 'PostSet', @self.ContinuousAnimationSliderCallback);
            
            self.AnimationPositionLabel = uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.1 .5 .1 .5],...
                'style', 'text',...
                'string', '0');
            
            frameRatePanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [.1 0 .1 .5],...
                'title', 'Frame rate (ms)');
            uicontrol('parent', frameRatePanel,...
                'units', 'normalized',...
                'position', [.1 .1 .4 .8],...
                'style', 'edit',...
                'string', num2str(1000 * self.FrameRate),...
                'callback', @self.SetFrameRate);
            
            videoSettingsPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [.8 0 .1 1]);
            videoRangePanel = uipanel('parent', videoSettingsPanel,...
                'units', 'normalized', ...
                'position', [0 .5 1 .5],...
                'title', 'Interval (s)');
            self.VideoRangeStartEdit = uicontrol('parent', videoRangePanel,...
                'units', 'normalized',...
                'position', [.1 .1 .4 .8],...
                'style', 'edit',...
                'string', num2str(self.VideoRange(1)),...
                'callback', @self.SetVideoRangeStart);
            self.VideoRangeStopEdit = uicontrol('parent', videoRangePanel,...
                'units', 'normalized',...
                'position', [.5 .1 .4 .8],...
                'style', 'edit',...
                'string', num2str(self.VideoRange(2)),...
                'callback', @self.SetVideoRangeStop);
            uicontrol(...
                'parent', videoSettingsPanel,...
                'units', 'normalized',...
                'position', [0 0 1 .5],...
                'style', 'pushbutton',...
                'string', 'Create video',...
                'callback', @self.CreateVideo);
        end
        
        function CreateAnimationPanel(self)
            self.EcgMapControl = ExtendedUIPkg.SignalMapControl([.9 0 .1 1]);
            self.EcgMapControl.Name = 'Signal';
            self.EcgMapControl.Create(self.ControlPanel);
            addlistener(self.EcgMapControl, 'MapClicked', @self.HandleMapClick);
            
            self.DeflectionMapControl = ExtendedUIPkg.GridActivationMapControl([0 0 1 .8]);
            self.DeflectionMapControl.Name = 'Deflections';
            self.DeflectionMapControl.Create(self.ControlHandle);
            addlistener(self.DeflectionMapControl, 'MapClicked', @self.HandleMapClick);
            
             deflectionColormap = hot(256 + 10);
            deflectionColormap = deflectionColormap(6:(end-5), :);
            self.DeflectionMapControl.SetColormap(deflectionColormap, 256);
        end
        
        function CreateSignalPanel(self)
            signalPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0 .8 1 .1]);
            self.SignalAxes = subplot(1,1,1, 'parent', signalPanel);
        end
        
        function InitializeSignalMapAnimation(self)
            self.EcgMapControl.SamplingFrequency = self.EcgData.SamplingFrequency;
            self.EcgMapControl.TimeLag = self.TimeLag;
            self.EcgMapControl.SetData(self.EcgData.MappedData, self.EcgData.ElectrodePositions);
            self.EcgMapControl.Show();
            self.EcgMapControl.MaximizeAxes();
        end
        
        function InitializeDeflectionAnimation(self)
            self.DeflectionMapControl.SamplingFrequency = self.EcgData.SamplingFrequency;
            self.DeflectionMapControl.TimeLag = self.TimeLag;
            self.DeflectionMapControl.SetData(self.EcgData.Activations, self.EcgData.ElectrodePositions);
            self.DeflectionMapControl.Show();
        end
        
        function InitializeSignalAxes(self)
            if isempty(self.EcgData), return; end
            
            signal = self.EcgData.Data(:, self.SelectedChannelIndex);
            time = self.EcgData.GetTimeRange();
                        
            if ishandle(self.IntrinsicDeflectionLine)
                delete(self.IntrinsicDeflectionLine);
            end
            self.IntrinsicDeflectionLine = line(...
                'xData', [], 'yData', [],...
                'lineStyle', 'none',...
                'marker', 'o', 'markerSize', 7,...
                'markerEdgeColor', [0 0 0], 'markerFaceColor', [1 0 0],...
                'parent', self.SignalAxes);
            
            if ishandle(self.SignalLine)
                delete(self.SignalLine);
            end
            self.SignalLine = line('parent', self.SignalAxes,...
                'xData', time, 'yData', signal, 'color', [0 0 0], 'lineSmoothing', 'on');
            
            if ishandle(self.AnimationPositionLine)
                delete(self.AnimationPositionLine);
            end
            self.AnimationPositionLine = line(...
                'xData', [0 0], 'yData', [0 0],...
                'color', [1 0 0],...
                'lineWidth', 1,...
                'parent', self.SignalAxes);
            
            self.SelectedChannelText = text(...
                'units', 'normalized',...
                'position', [0, 1],...
                'string', '',...
                'verticalAlignment', 'top',...
                'parent', self.SignalAxes,...
                'backgroundColor', [1 1 1],...
                'edgeColor', [0 0 0]);
            
            set(self.SignalAxes, 'LooseInset', get(self.SignalAxes, 'TightInset'));
            
            self.ShowSelectedChannel();
        end
        
        function ControlAnimation(self, varargin)
            set(self.AnimationButton, 'string', '||');
            numberOfSamples = self.EcgData.GetNumberOfSamples();
            frameStep = round(self.FrameRate * self.EcgData.SamplingFrequency);
            
            while get(self.AnimationButton, 'value')
                drawnow;
                self.AnimationPosition = self.AnimationPosition + frameStep;
                if self.AnimationPosition > numberOfSamples
                    self.AnimationPosition = 1;
                end

                self.SetAnimationFrame();
                
                if mod(self.AnimationPosition, 100) == 0
                    set(self.AnimationSlider, 'value', self.AnimationPosition);
                    self.SetAnimationInfo();
                end
            end
            
            set(self.AnimationSlider, 'value', self.AnimationPosition);
            
            set(self.AnimationButton, 'string', '>');
            
            self.SetStaticFrame();
        end
        
        function AnimationSliderCallback(self, varargin)
            self.AnimationPosition = round(get(self.AnimationSlider, 'value'));
            self.SetStaticFrame();
        end
        
        function ContinuousAnimationSliderCallback(self, ~, eventData)
            if verLessThan('matlab', '8.4.0')
                sliderObject = get(eventData, 'AffectedObject');
            else
                sliderObject = eventData.AffectedObject;
            end
            self.AnimationPosition = round(get(sliderObject, 'Value'));
            self.SetAnimationInfo();
            self.SetAnimationFrame();
        end
        
        function SetStaticFrame(self)
            self.SetAnimationInfo();
            
            self.EcgMapControl.SetStaticFramePosition(self.AnimationPosition);
            self.DeflectionMapControl.SetStaticFramePosition(self.AnimationPosition);
        end
        
        function SetAnimationFrame(self)
            self.SetAnimationInfo();
            
            self.EcgMapControl.SetFramePosition(self.AnimationPosition);
            self.DeflectionMapControl.SetFramePosition(self.AnimationPosition);
        end
        
        function SetAnimationInfo(self)
            timePosition = round(1000 * self.AnimationPosition / self.EcgData.SamplingFrequency);
            set(self.AnimationPositionLabel, 'string', [ num2str(timePosition) ' ms']);
            set(self.AnimationPositionLine, 'xData', [timePosition / 1000, timePosition / 1000]);
        end
        
        function ComputeActivations(self)
            numberOfChannels = self.EcgData.GetNumberOfChannels();
            activations = cell(numberOfChannels, 1);
            minPeakDistance = round(0.050  * self.EcgData.SamplingFrequency);
            
            for channelIndex = 1:numberOfChannels
                channelData = self.EcgData.Data(:, channelIndex);
                [peakValues, peakIndices] = findpeaks(-diff(channelData), 'MINPEAKDISTANCE', minPeakDistance);
                validPeaks = peakValues > (median(peakValues) - std(peakValues));
                activations{channelIndex} = peakIndices(validPeaks);
            end
            
            self.EcgData.Activations = activations;
        end
        
        function SetVideoRangeStart(self, source, varargin)
            self.VideoRange(1) = str2double(get(source, 'string'));
        end
        
        function SetVideoRangeStop(self, source, varargin)
            self.VideoRange(2) = str2double(get(source, 'string'));
        end
        
        function CreateVideo(self, varargin)
           [filename, pathname] = HelperFunctions.customUiputfile('*.mp4', 'Save video as...');
            
            if isequal(filename, 0) || isequal(pathname, 0), return; end
            filename = fullfile(pathname, filename);
            
            warning('off', 'MATLAB:hg:EraseModeIgnored')
            waitbarHandle = waitbar(0 ,'Writing preview video'); 
            
            writer = VideoWriter(filename, 'MPEG-4');
            writer.FrameRate = 100;
            writer.Quality = 75;
            open(writer);
            
            sampleRange = round(self.VideoRange * self.EcgData.SamplingFrequency + 1);
            numberOfSamples = self.EcgData.GetNumberOfSamples();
            if sampleRange(1) < 1
                sampleRange(1) = 1;
            end
            
            if sampleRange(2) > numberOfSamples
                sampleRange(2) = numberOfSamples;
            end
            
            numberOfVideoSamples = sampleRange(2) - sampleRange(1) + 1;
            
            for sampleIndex = sampleRange(1):sampleRange(2)
                colorData = self.DeflectionMapControl.GetFrame(sampleIndex);
                colorData = imresize(colorData, 20, 'nearest');
                colorData(colorData > 1) = 1;
                colorData(colorData < 0) = 0;
                writeVideo(writer, im2frame(colorData));
                
                if mod(sampleIndex, 100) == 0
                    waitbar((sampleIndex -  sampleRange(1) + 1) / numberOfVideoSamples,...
                        waitbarHandle,...
                        ['Writing map videos, sample ' ...
                        num2str(sampleIndex -  sampleRange(1) + 1) ' of ' ...
                        num2str(numberOfVideoSamples)]);
                end
            end
            close(waitbarHandle);
            close(writer);
        end
        
        function HandleMapClick(self, ~, eventData)
            self.SelectedChannelIndex = eventData.Position;
            
            self.ShowSelectedChannel();
        end
        
        function ShowSelectedChannel(self)
            channelLabel = self.EcgData.ElectrodeLabels{self.SelectedChannelIndex};
            set(self.SelectedChannelText, 'string',...
                ['Channel: ', channelLabel]);
            
            signal = self.EcgData.Data(:, self.SelectedChannelIndex);
            time = self.EcgData.GetTimeRange();
            
            set(self.SignalLine, 'yData', signal);
            
            deflectionIndices = self.EcgData.Activations{self.SelectedChannelIndex};
            
            set(self.IntrinsicDeflectionLine, 'xData', time(deflectionIndices),...
                    'yData', signal(deflectionIndices));

            yLimits = [min(signal), max(signal)];
            set(self.AnimationPositionLine, 'yData', yLimits);
            
            axis(self.SignalAxes, 'tight');
        end
        
        function SetFrameRate(self, source, varargin)
            self.FrameRate = round(str2double(get(source, 'string'))) / 1000;
        end
    end
end
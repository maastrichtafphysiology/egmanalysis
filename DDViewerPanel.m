classdef DDViewerPanel < UserInterfacePkg.CustomPanel
    properties (Access = private)
        EcgData
        IntrinsicDeflectionResult  
        
        AnimationPosition
        TimeLag
        FrameRate
        
        SelectedChannelIndex
        SelectedTime
        
        DeflectionMapControl
        
        ControlPanel
        AnimationButton
        AnimationSlider
        AnimationPositionLabel
    end
    
    methods
        function self = DDViewerPanel(position)
            self = self@UserInterfacePkg.CustomPanel(position);
            
            self.SelectedChannelIndex = NaN;
            self.SelectedTime = NaN;
            self.TimeLag = 100;
            self.FrameRate = 0.001;
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            
            self.CreateControlPanel();
            
            self.InitializeView();
        end
        
        function SetECGData(self, ecgData)
            self.EcgData = ecgData;
        end
        
        function SetDeflections(self, deflections)
            self.EcgData.Activations = deflections;
            self.InitializeECGData();
        end
    end
    
    methods (Access = private)
        function CreateControlPanel(self)
            self.ControlPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0 0 1 .1],...
                'borderType', 'none');
            
            self.AnimationButton = uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [0 .5 .1 .5],...
                'style', 'togglebutton',...
                'string', '>',...
                'callback', @self.ControlAnimation);
            
            self.AnimationSlider = uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.1 .5 .8 .3],...
                'style', 'slider',...
                'callback', @self.AnimationSliderCallback,...
                'enable', 'off');
            addlistener(handle(self.AnimationSlider), 'Value', 'PostSet', @self.ContinuousAnimationSliderCallback);
            
            self.AnimationPositionLabel = uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [0 0 .1 .5],...
                'style', 'text');
            
            timeLagPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [.1, 0, .1, .5],...
                'title', 'Time lag (ms)');
            uicontrol('parent', timeLagPanel,...
                'units', 'normalized',...
                'position', [0, 0, 1, 1],...
                'style', 'edit',...
                'string', num2str(self.TimeLag),...
                'callback', @self.SetTimeLag);
            
            frameRatePanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [.2, 0, .1, .5],...
                'title', 'Frame rate (ms)');
            uicontrol('parent', frameRatePanel,...
                'units', 'normalized',...
                'position', [0, 0, 1, 1],...
                'style', 'edit',...
                'string', num2str(1000 * self.FrameRate),...
                'callback', @self.SetFrameRate);
        end
        
        function InitializeView(self)
            self.DeflectionMapControl = ExtendedUIPkg.GridActivationMapControl([0 .1 1 .9]);
            self.DeflectionMapControl.Name = 'Activations';
            self.DeflectionMapControl.Create(self.ControlHandle);
            addlistener(self.DeflectionMapControl, 'MapClicked', @self.HandleMapClick);
            self.DeflectionMapControl.Show();
            
            deflectionColormap = hot(256 + 10);
            deflectionColormap = deflectionColormap(6:(end-5), :);
            self.DeflectionMapControl.SetColormap(deflectionColormap, 256);
        end
        
        function InitializeECGData(self)
            self.DeflectionMapControl.SamplingFrequency = self.EcgData.SamplingFrequency;
            self.DeflectionMapControl.SetData(self.EcgData.Activations, self.EcgData.ElectrodePositions);
            self.DeflectionMapControl.Show();
            
            set(self.AnimationSlider, 'enable', 'on');
            self.AnimationPosition = 1;
            numberOfSamples = self.EcgData.GetNumberOfSamples();
            set(self.AnimationSlider, 'min', 1, 'max', numberOfSamples,...
                'value', self.AnimationPosition,...
                'sliderStep', [1 / (numberOfSamples - 1), 1 / (numberOfSamples - 1)]);
            
            self.SetAnimationFrame();
        end
        
        function ShowSelectedChannel(self)
            
        end
        
        function SetSignalWindow(self, position)
            timePosition = position / self.EcgData.SamplingFrequency;
            timeWindow = [timePosition - 0.5, timePosition + 0.5];
            xTickSpacing = 0.1;
            xTicks = (timeWindow(1) + (xTickSpacing - mod(timeWindow(1), xTickSpacing))):(xTickSpacing):(timeWindow(end) - mod(timeWindow(end), xTickSpacing));
            
            % update electrogram axes limits
%             set(self.SignalAxes,...
%                 'xLim', timeWindow,...
%                 'xTick', xTicks,...
%                 'xMinorTick', 'on');
%             
%             yLimits = get(self.SignalAxes, 'yLim');
        end
        
        function SetTimeLag(self, source, varargin)
            self.TimeLag = str2double(get(source, 'string'));
            
            self.DeflectionMapControl.SetTimeLag(self.TimeLag);
        end
        
        function SetFrameRate(self, source, varargin)
            self.FrameRate = round(str2double(get(source, 'string'))) / 1000;
        end
        
        function HandleMapClick(self, ~, eventData)
            self.SelectedChannelIndex = eventData.Position;
            self.SelectedTime = eventData.Time;
            self.ShowSelectedChannel();
        end
        
        function ControlAnimation(self, varargin)
            if isempty(self.EcgData), return; end
            
            set(self.AnimationButton, 'string', '||');
            numberOfSamples = self.EcgData.GetNumberOfSamples();
            
            frameStep = round(self.FrameRate * self.EcgData.SamplingFrequency);
            
            startPosition = self.AnimationPosition;
            while get(self.AnimationButton, 'value')
                drawnow;
                self.AnimationPosition = self.AnimationPosition + frameStep;
                if self.AnimationPosition > numberOfSamples
                    self.AnimationPosition = 1;
                end
                set(self.AnimationSlider, 'value', self.AnimationPosition);
                self.SetAnimationFrame();
                
                if mod((self.AnimationPosition - startPosition) / frameStep, 10) == 0
                    set(self.AnimationSlider, 'value', self.AnimationPosition);
                    self.SetAnimationInfo();
                end
            end
            
            set(self.AnimationButton, 'string', '>');
            
            self.SetStaticFrame();
        end
        
        function AnimationSliderCallback(self, varargin)
            self.AnimationPosition = round(get(self.AnimationSlider, 'value'));
            self.SetStaticFrame();
        end
        
        function ContinuousAnimationSliderCallback(self, source, eventData)
            if verLessThan('matlab', '8.4.0')
            sliderObject = get(eventData, 'AffectedObject');
            else
                sliderObject = eventData.AffectedObject;
            end
            self.AnimationPosition = round(get(sliderObject, 'Value'));
            self.SetAnimationFrame();
            self.SetSignalWindow(self.AnimationPosition);
        end
        
        function SetStaticFrame(self)
            self.SetAnimationInfo();
            
            self.DeflectionMapControl.SetStaticFramePosition(self.AnimationPosition);
            
            self.SetSignalWindow(self.AnimationPosition);
        end
        
        function SetAnimationFrame(self)
            self.SetAnimationInfo();
            
            self.DeflectionMapControl.SetFramePosition(self.AnimationPosition);
        end
        
        function SetAnimationInfo(self)
            timePosition = round(1000 * self.AnimationPosition / self.EcgData.SamplingFrequency);
            set(self.AnimationPositionLabel, 'string', [ num2str(timePosition) ' ms']);
        end
    end
end
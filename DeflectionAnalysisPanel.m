classdef DeflectionAnalysisPanel < DeflectionECGChannelPanel
    
    properties (Access = protected)
        TemplateMatchingResult
        FilterOptimizationPanel
        
        IntrinsicDeflectionResult
        IntrinsicDeflectionLineHandles
        FarFieldDeflectionLineHandles
        
        VisibleTemplateMatchingDeflections;
        VisibleFarFieldDeflections
        VisibleInstrinsicDeflections
        
        DeterminePDFButton
        IntervalHistogramPopup
        DetermineIntrinsicDeflectionsButton
        IntrinsicDeflectionPopup
        
        ComputeDeflectionAssignmentButton
        DeflectionAssignmentPopup
        AFComplexityLabel
        
        SaveToMatButton
        LoadFromMatButton
    end
    
    properties (Constant)
        DEFLECTIONTYPES = {'Template matching'; 'Far-field'; 'Intrinsic'}
        ASSIGNMENT_TYPES = {'Slope * amplitude assignment';...
            'Shortest path assignment'}
    end
    
    events
        IntrinsicDeflectionsDetermined
    end
    
    methods
        function self = DeflectionAnalysisPanel(position)
            self = self@DeflectionECGChannelPanel(position);
            
            self.VisibleTemplateMatchingDeflections = true;
            self.VisibleFarFieldDeflections = false;
            self.VisibleInstrinsicDeflections = true;
        end
        
        function SetTemplateMatchingResult(self, result)
            self.DeflectionData = result;
            
            self.ShowDeflections();
        end
        
        function intrinsicDeflectionData = GetIntrinsicDeflections(self)
            intrinsicDeflectionData = DeflectionDetectionResults(self.IntrinsicDeflectionResult);
        end
        
        function ecgData = LoadData(self, filename)
            try
                ddData = load(filename);
            catch %#ok<CTCH>
                [filename, pathname] = HelperFunctions.customUigetfile('*.mat', 'No associated file found. Select alternative DD file');
                if isequal(filename, 0) || isequal(pathname, 0), return; end
                filename = fullfile(pathname, filename);
                ddData = load(filename);
            end
            ecgData = ddData.ecgData;
            
            self.DeflectionData = ddData.deflectionData;
            self.IntrinsicDeflectionResult = ddData.intrinsicDeflectionResult;
            if ~isempty(self.IntrinsicDeflectionResult)
                set(self.ComputeDeflectionAssignmentButton, 'enable', 'on');
            end
            
            if ~isempty(self.DeflectionData.Fit)
                set(self.DetermineIntrinsicDeflectionsButton, 'enable', 'on');
                set(self.ComputeDeflectionAssignmentButton, 'enable', 'on');
            end
            
            [pathString, nameString] = fileparts(filename);
            nameString = nameString(1:(end - 3));
            if ~isempty(ddData.ecgData)
                ddData.ecgData.Filename = fullfile(pathString, nameString);
            end
            
            self.SetECGData(ddData.ecgData);
        end
    end
    
    methods (Access = protected)
        function CreateControlPanel(self)
            CreateControlPanel@ECGChannelPanel(self);
            
            set(self.ChannelPanel, 'position', [0 .4 1 .59]);
            self.CreateDeflectionVisualizationPanel();
            
            self.DeterminePDFButton = uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [0 .3 .5 .05],...
                'style', 'pushbutton',...
                'string', 'Determine cycle length PDF',...
                'callback', @self.DetermineCycleLengthPDFCallback,...
                'enable', 'on');
            
            uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [0 .25 .5 .05],...
                'style', 'pushbutton',...
                'string', 'Examine interval histogram',...
                'callback', @self.CreateCycleLengthHistogramFigure);
            self.IntervalHistogramPopup = uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.5 .25 .5 .05],...
                'style', 'popupmenu',...
                'string', DeflectionAnalysisPanel.DEFLECTIONTYPES,...
                'value', 1,...
                'backgroundColor', [1 1 1]);
            
            self.ComputeDeflectionAssignmentButton = uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [0 .2 .5 .05],...
                'style', 'pushbutton',...
                'string', 'Assign deflections',...
                'callback', @self.ComputeDeflectionAssignment,...
                'enable', 'off');
            self.DeflectionAssignmentPopup = uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.5 .2 .5 .05],...
                'style', 'popupmenu',...
                'string', DeflectionAnalysisPanel.ASSIGNMENT_TYPES,...
                'value', 1,...
                'backgroundColor', [1 1 1]);
            self.AFComplexityLabel = uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.5 .2 .5 .025],...
                'style', 'text');
            
            self.SaveToMatButton = uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [0 .15 .5 .05],...
                'style', 'pushbutton',...
                'string', 'Save to .mat',...
                'callback', @self.SaveDeflectionDetection,...
                'enable', 'on');
            
            self.LoadFromMatButton = uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.5 .15 .5 .05],...
                'style', 'pushbutton',...
                'string', 'Load from .mat',...
                'callback', @self.LoadDeflectionDetection,...
                'enable', 'on');
            
            uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.1 .05 .8 .05],...
                'style', 'pushbutton',...
                'string', 'Apply wave map to intrinsic deflections',...
                'callback', @self.ApplyWavemap,...
                'enable', 'on');
        end
        
        function CreateDeflectionVisualizationPanel(self)
            deflectionPanel = uipanel('parent', self.ChannelPanel,...
                'position', [.5, .5, .5, .49],...
                'title', 'Visible deflections');
            
            uicontrol('parent', deflectionPanel,...
                'units', 'normalized',...
                'position', [.05 .7 .9 .2],...
                'style', 'checkbox',...
                'string', 'Template matching',...
                'value', self.VisibleTemplateMatchingDeflections,...
                'callback', @self.EnableTemplateMatchingDeflections);
            
            uicontrol('parent', deflectionPanel,...
                'units', 'normalized',...
                'position', [.05 .4 .9 .2],...
                'style', 'checkbox',...
                'string', 'Far-field',...
                'value', self.VisibleFarFieldDeflections,...
                'callback', @self.EnableFarFieldDeflections);
            
            uicontrol('parent', deflectionPanel,...
                'units', 'normalized',...
                'position', [.05 .1 .9 .2],...
                'style', 'checkbox',...
                'string', 'Intrinsic',...
                'value', self.VisibleTemplateMatchingDeflections,...
                'callback', @self.EnableIntrinsicDeflections);
        end
        
        function CreateCycleLengthHistogramFigure(self, varargin)
            deflectionTypeIndex = get(self.IntervalHistogramPopup, 'value');
            switch deflectionTypeIndex
                case 1
                    deflectionData = self.DeflectionData.AnalysisResults;
                    deflectionName = DeflectionAnalysisPanel.DEFLECTIONTYPES{1};
                    cycleLengths = DeflectionDetectionResults.CalculateCycleLengths(...
                        deflectionData, false(size(deflectionData)));
                case 2
                    deflectionData = self.IntrinsicDeflectionResult;
                    deflectionName = DeflectionAnalysisPanel.DEFLECTIONTYPES{2};
                    cycleLengths = DeflectionDetectionResults.CalculateCycleLengths(...
                        deflectionData, false(size(deflectionData)));
                case 3
                    deflectionData = self.IntrinsicDeflectionResult;
                    deflectionName = DeflectionAnalysisPanel.DEFLECTIONTYPES{3};
                    cycleLengths = DeflectionDetectionResults.CalculatePrimaryCycleLengths(...
                        deflectionData, false(size(deflectionData)));
                otherwise
                    deflectionData = self.DeflectionData.AnalysisResults;
                    deflectionName = DeflectionAnalysisPanel.DEFLECTIONTYPES{1};
                    cycleLengths = DeflectionDetectionResults.CalculateCycleLengths(...
                        deflectionData, false(size(deflectionData)));
            end
            
            if isempty(cycleLengths), return; end
            
            figureHandle = figure('name', [deflectionName ' deflections interval histogram'],...
                'numberTitle', 'off',...
                'menuBar', 'none',...
                'visible', 'on');
            
            histogram = ExtendedUIPkg.HistogramControl.BasicHistogramControl([0.01 0.01 .98 .98], 'Interval (s)');
            histogram.Create(figureHandle);
            histogram.Show((0:0.002:0.4), cycleLengths / self.EcgData.SamplingFrequency);
        end
        
        function DetermineCycleLengthPDFCallback(self, varargin)
            position = get(gcf, 'position');
            figurePosition = [position(1:2) + 1/5 * position(3:4), 3/5 * position(3:4)];
            
            determineCycleProbabilityControl = DetermineCycleProbabilityControl(figurePosition,...
                self.DeflectionData, false(size(self.DeflectionData.AnalysisResults)));
            
            determineCycleProbabilityControl.Create();
            determineCycleProbabilityControl.Show();
            
            set(self.DetermineIntrinsicDeflectionsButton, 'enable', 'on');
            set(self.ComputeDeflectionAssignmentButton, 'enable', 'on');
        end
        
        function DetermineIntrinsicDeflections(self, varargin)
            cycleLengthFit = self.DeflectionData.Fit;
            deflectionCalculator = AlgorithmPkg.DeflectionAssignmentCalculator(cycleLengthFit);
            algorithmIndex = get(self.IntrinsicDeflectionPopup, 'value');
            deflectionCalculator.AssignmentAlgorithm =...
                AlgorithmPkg.DeflectionAssignmentCalculator.ASSIGNMENT_ALGORITHMS{algorithmIndex};
            
            analysisResult = self.DeflectionData.AnalysisResults;
            [filteredResult, ~] = deflectionCalculator.Apply(analysisResult);
            self.IntrinsicDeflectionResult = filteredResult;
            self.ShowIntrinsicDeflections();
        end
        
        function ComputeDeflectionAssignment(self, varargin)
            complexityTypeIndex = get(self.DeflectionAssignmentPopup, 'value');
            switch complexityTypeIndex
                case 1
                    complexity = self.ComputeAutomatedDeflectionAssignment(4);
                case 2
                    complexity = self.ComputeAutomatedDeflectionAssignment(5);
            end
            
            set(self.AFComplexityLabel, 'string',...
                ['AF Complexity: ' num2str(complexity)]);
        end
        
        function complexity = ComputeExpectedAFComplexity(self)
            deflectionData = self.DeflectionData.AnalysisResults;
            cycleLengthFit = self.DeflectionData.Fit;
            
            time = self.EcgData.GetTimeRange();
            duration = time(end) - time(1);
            numberOfChannels = self.EcgData.GetNumberOfChannels();
            expectedNumberOfIntrinsicDeflections =...
                round(numberOfChannels * duration / cycleLengthFit.Beta(2));
            
            numberOfCandidateDeflections = 0;
            for channelIndex = 1:numel(deflectionData)
                numberOfCandidateDeflections = numberOfCandidateDeflections +...
                    numel(deflectionData{channelIndex}.templatePeakIndices);
            end
            
            complexity = numberOfCandidateDeflections / expectedNumberOfIntrinsicDeflections - 1;
            disp(['AF complexity (based on expected number of intrinsic deflections): ' num2str(complexity)]);
            disp(['AFCL: ' num2str(round(1000 * cycleLengthFit.Beta(2))) ' ms']);
        end
        
        function complexity = ComputeAssignmentAFComplexity(self)
            intrinsicDeflectionData = self.IntrinsicDeflectionResult;
            
            channelComplexities = NaN(size(intrinsicDeflectionData));
            for channelIndex = 1:numel(intrinsicDeflectionData)
                intrinsicDeflections = intrinsicDeflectionData{channelIndex}.PrimaryDeflections;
                numberOfIntrinsicDeflections = numel(find(intrinsicDeflections));
                numberOfFarFieldDeflections = numel(find(~intrinsicDeflections));
                channelComplexities(channelIndex) = numberOfFarFieldDeflections / numberOfIntrinsicDeflections;
            end
            
            complexity = mean(channelComplexities);
            cycleLengthFit = self.DeflectionData.Fit;
            disp(['AF complexity (based on interval assignment): ' num2str(complexity)]);
            disp(['AFCL: ' num2str(round(1000 * cycleLengthFit.Beta(2))) ' ms']);
        end
        
        function complexity = ComputeAmplitudeAFComplexity(self)
            intrinsicDeflectionData = self.IntrinsicDeflectionResult;
            
            amplitudePercentage = ApplicationSettings.Instance.DeflectionClustering.IntrinsicAmplitudeMedianPercentage / 100;
            channelComplexities = NaN(size(intrinsicDeflectionData));
            for channelIndex = 1:numel(intrinsicDeflectionData)
                intrinsicDeflections = intrinsicDeflectionData{channelIndex}.PrimaryDeflections;
                numberOfIntrinsicDeflections = numel(find(intrinsicDeflections));
                intrinsicAmplitudes = intrinsicDeflectionData{channelIndex}.templatePeakAmplitudes(intrinsicDeflections);
                amplitudeMean = median(intrinsicAmplitudes);
                farFieldDeflections = ~intrinsicDeflections(:) &...
                    intrinsicDeflectionData{channelIndex}.templatePeakAmplitudes(:) > amplitudeMean * amplitudePercentage;
                numberOfFarFieldDeflections = numel(find(farFieldDeflections));
                channelComplexities(channelIndex) = numberOfFarFieldDeflections / numberOfIntrinsicDeflections;
            end
            
            complexity = mean(channelComplexities);
            cycleLengthFit = self.DeflectionData.Fit;
            disp(['AF complexity (based on amplitude threshold): ' num2str(complexity)]);
            disp(['AFCL: ' num2str(round(1000 * cycleLengthFit.Beta(2))) ' ms']);
        end
        
        function complexity = ComputeAutomatedDeflectionAssignment(self, methodIndex)
            cycleLengthFit = self.DeflectionData.Fit;
            deflectionCalculator = AlgorithmPkg.DeflectionAssignmentCalculator(cycleLengthFit);
            deflectionCalculator.Time = self.EcgData.GetTimeRange();
            templateMatchingAnalysisResult = self.DeflectionData.AnalysisResults;
            
            deflectionCalculator.AssignmentAlgorithm = AlgorithmPkg.DeflectionAssignmentCalculator.ASSIGNMENT_ALGORITHMS{methodIndex};
            intrinsicDeflectionData = deflectionCalculator.Apply(templateMatchingAnalysisResult);
            %             intrinsicDeflectionData = deflectionCalculator.SpatialDetection(self.EcgData, templateMatchingAnalysisResult);
            
            filterSettings = FilterSettings();
            filterSettings.OverlapFilterEnabled = true;
            
            channelComplexities = NaN(size(intrinsicDeflectionData));
            filteredResults = cell(size(intrinsicDeflectionData));
            
            amplitudePercentage = ApplicationSettings.Instance.DeflectionClustering.IntrinsicAmplitudeMedianPercentage / 100;
            slopePercentage = ApplicationSettings.Instance.DeflectionClustering.IntrinsicSlopeMedianPercentage / 100;
            for channelIndex = 1:numel(intrinsicDeflectionData)
                intrinsicDeflections = intrinsicDeflectionData{channelIndex}.PrimaryDeflections;
                numberOfIntrinsicDeflections = numel(find(intrinsicDeflections));
                intrinsicAmplitudes = intrinsicDeflectionData{channelIndex}.templatePeakAmplitudes(intrinsicDeflections);
                amplitudeMedian = median(intrinsicAmplitudes);
                intrinsicSlopes = intrinsicDeflectionData{channelIndex}.deflectionPeakSlopes(intrinsicDeflections);
                slopeMedian = median(intrinsicSlopes);
                
                filterSettings.amplitudeThreshold = amplitudeMedian * amplitudePercentage;
                filterSettings.slopeThreshold = slopeMedian * slopePercentage;
                deflectionFilter = AlgorithmPkg.DeflectionFilter(filterSettings);
                filteredResult = deflectionFilter.Apply(intrinsicDeflectionData{channelIndex});
                
                farFieldDeflections = ~filteredResult.PrimaryDeflections;
                numberOfFarFieldDeflections = numel(find(farFieldDeflections));
                channelComplexities(channelIndex) = numberOfFarFieldDeflections / numberOfIntrinsicDeflections;
                
                filteredResults{channelIndex} = filteredResult;
            end
            
            complexity = mean(channelComplexities);
            disp(['AF complexity (based on amplitude threshold (automatic)): ' num2str(complexity)]);
            disp(['AFCL: ' num2str(round(1000 * cycleLengthFit.Beta(2))) ' ms']);
            
            self.IntrinsicDeflectionResult = filteredResults;
            self.ShowIntrinsicDeflections();
        end
        
        function ShowDeflections(self)
            ShowDeflections@DeflectionECGChannelPanel(self);
            if ~self.VisibleTemplateMatchingDeflections
                DeflectionECGChannelPanel.EnableDeflections(...
                    self.DeflectionLineHandles, self.VisibleTemplateMatchingDeflections);
            end
            
            self.ShowFarFieldDeflections();
            if ~self.VisibleFarFieldDeflections
                DeflectionECGChannelPanel.EnableDeflections(...
                    self.FarFieldDeflectionLineHandles, self.VisibleFarFieldDeflections);
            end
            
            
            self.ShowIntrinsicDeflections();
            if ~self.VisibleInstrinsicDeflections
                DeflectionECGChannelPanel.EnableDeflections(...
                    self.IntrinsicDeflectionLineHandles, self.VisibleInstrinsicDeflections);
            end
        end
        
        function ShowFarFieldDeflections(self)
            % Far-field deflections
            validHandles = ishandle(self.FarFieldDeflectionLineHandles);
            if any(validHandles)
                delete(self.FarFieldDeflectionLineHandles(validHandles));
            end
            
            if isempty(self.IntrinsicDeflectionResult), return; end
            
            lineData = self.EcgData.Data(:, self.SelectedChannels);
            resultData = self.IntrinsicDeflectionResult(self.SelectedChannels);
            time = self.EcgData.GetTimeRange();
            self.FarFieldDeflectionLineHandles = NaN(size(resultData));
            farFieldColor = [1 0 1];
            for channelIndex = 1:numel(resultData)
                currentResult = resultData{channelIndex};
                signal = lineData(:, channelIndex);
                
                % Far-field deflections
                validDeflections = ~currentResult.PrimaryDeflections;
                templates = currentResult.templates;
                ranges = currentResult.templatePeakRanges(validDeflections, :);
                coefficients = currentResult.templatePeakCoefficients(validDeflections);
                templatePeakTemplates = currentResult.templatePeakTemplates(validDeflections, :);
                
                [xData yData] = DeflectionECGChannelPanel.ComputeDeflectionLineData(...
                    time, signal, templates, templatePeakTemplates, ranges, coefficients);
                
                self.FarFieldDeflectionLineHandles(channelIndex) = line('xData', xData, 'yData', yData,...
                    'color', farFieldColor,...
                    'lineWidth', 2,...
                    'parent', self.AxesHandles(channelIndex));
            end
        end
        
        function ShowIntrinsicDeflections(self)
            % Intrinsic deflections
            validHandles = ishandle(self.IntrinsicDeflectionLineHandles);
            if any(validHandles)
                delete(self.IntrinsicDeflectionLineHandles(validHandles));
            end
            
            if isempty(self.IntrinsicDeflectionResult), return; end
            
            lineData = self.EcgData.Data(:, self.SelectedChannels);
            resultData = self.IntrinsicDeflectionResult(self.SelectedChannels);
            time = self.EcgData.GetTimeRange();
            self.IntrinsicDeflectionLineHandles = NaN(size(resultData));
            intrinsicColor = [0 1 0];
            for channelIndex = 1:numel(resultData)
                currentResult = resultData{channelIndex};
                signal = lineData(:, channelIndex);
                
                % Intrinsic deflections
                validDeflections = currentResult.PrimaryDeflections;
                templates = currentResult.templates;
                ranges = currentResult.templatePeakRanges(validDeflections, :);
                coefficients = currentResult.templatePeakCoefficients(validDeflections);
                templatePeakTemplates = currentResult.templatePeakTemplates(validDeflections, :);
                
                [xData yData] = DeflectionECGChannelPanel.ComputeDeflectionLineData(...
                    time, signal, templates, templatePeakTemplates, ranges, coefficients);
                
                self.IntrinsicDeflectionLineHandles(channelIndex) = line('xData', xData, 'yData', yData,...
                    'color', intrinsicColor,...
                    'lineWidth', 2,...
                    'parent', self.AxesHandles(channelIndex));
            end
        end
        
        function EnableTemplateMatchingDeflections(self, source, varargin)
            self.VisibleTemplateMatchingDeflections = get(source, 'value');
            DeflectionECGChannelPanel.EnableDeflections(...
                self.DeflectionLineHandles, self.VisibleTemplateMatchingDeflections);
        end
        
        function EnableFarFieldDeflections(self, source, varargin)
            self.VisibleFarFieldDeflections = get(source, 'value');
            DeflectionECGChannelPanel.EnableDeflections(...
                self.FarFieldDeflectionLineHandles, self.VisibleFarFieldDeflections);
        end
        
        function EnableIntrinsicDeflections(self, source, varargin)
            self.VisibleInstrinsicDeflections = get(source, 'value');
            DeflectionECGChannelPanel.EnableDeflections(...
                self.IntrinsicDeflectionLineHandles, self.VisibleInstrinsicDeflections);
        end
        
        function LoadDeflectionDetection(self, varargin)
            if isempty(self.EcgData)
                [filename, pathname] = HelperFunctions.customUigetfile('*_DD.mat', 'Select DD file');
                if isequal(filename, 0) || isequal(pathname, 0), return; end
                filename = fullfile(pathname, filename);
            else
                [pathString, nameString] = fileparts(self.EcgData.Filename);
                filename = [pathString, filesep, nameString '_DD.mat'];
            end
            
            self.LoadData(filename);
        end
        
        function FilterDeflections(self, varargin)
            optimizationProtocol = self.FilterOptimizationPanel.OptimizationProtocol;
            deflectionOptimizationFilter = AlgorithmPkg.TemplateMatchingOptimizationFilter(...
                self.FilterOptimizationPanel.Parameters, optimizationProtocol);
            
            self.DeflectionData.PreFilter(deflectionOptimizationFilter);
            self.ShowDeflections();
        end
        
        function SaveDeflectionDetection(self, varargin)
            [pathString, nameString] = fileparts(self.EcgData.Filename);
            filename = [pathString, filesep, nameString '_DD'];
            ecgData = self.EcgData; %#ok<NASGU>
            deflectionData = self.DeflectionData;
            intrinsicDeflectionResult = self.IntrinsicDeflectionResult; %#ok<NASGU>
            
            if isempty(deflectionData.Fit)
                msgbox({'No AFCL distribution estimated!'; 'DD file will not be saved.'}, 'Warning','warn');
                return;
            end
            
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Saving deflection detection...', 0, self));
            
            save(filename,...
                'ecgData',...
                'deflectionData',...
                'intrinsicDeflectionResult');
            
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Done', 1, self));
        end
        
        function ApplyWavemap(self, varargin)
            notify(self, 'IntrinsicDeflectionsDetermined');
        end
    end
end
classdef PACEMAPWaveMapPanel < WaveMapPanel
    properties
        PacemapData
        PacemapWaves
        PacemapActivations
    end
    
    properties (Access = private)
        
        PACEMAPActivationLine
        
        PacemapControl
        
        CompareButton
        ComparisonView
        ComparisonFigure
        
        CreatePacemapVideoButton
    end
    
    methods
        function self = PACEMAPWaveMapPanel(position)
            self = self@WaveMapPanel(position);
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            
            self.CreateControlPanel();
            self.CreateSignalPanel();
            self.CreateAnimationPanel();
        end
        
        function SetECGData(self, ecgData, intrinsicDeflectionData)
            self.EcgData = ecgData;
            
            minimumSpacing = self.EcgData.ComputeMinimumSpacing();
            self.WavemapCalculator.NeighborRadius = sqrt(2 * (minimumSpacing + 1e3 * eps)^2 );
            self.WavemapCalculator.MergeSearchRadius = 2 * self.WavemapCalculator.NeighborRadius;
            
            if nargin > 2
                self.IntrinsicDeflectionData = intrinsicDeflectionData.AnalysisResults;
                self.EcgData.SetActivations(self.IntrinsicDeflectionData);
            end
            self.SelectedChannelIndex = 1;
            self.InitializeSignalAxes();
            
            self.InitializeSignalMapAnimation();
            self.InitializeDeflectionAnimation();
            self.InitializeWavemapAnimation();
            self.InitializePacemapAnimation();
            self.InitializePreviewAnimation();
            
            self.AnimationPosition = 1;
            numberOfSamples = self.EcgData.GetNumberOfSamples();
            set(self.AnimationSlider, 'min', 1, 'max', numberOfSamples,...
                'value', self.AnimationPosition,...
                'sliderStep', [1 / (numberOfSamples - 1), 1 / (numberOfSamples - 1)]);
            
            self.SetAnimationFrame();
        end
        
        function ecgData = LoadData(self, filename)
            try
                waveMapData = load(filename);
            catch %#ok<CTCH>
                [filename, pathname] = HelperFunctions.customUigetfile('*.mat', 'No associated file found. Select alternative WM file');
                if isequal(filename, 0) || isequal(pathname, 0), return; end
                filename = fullfile(pathname, filename);
                waveMapData = load(filename);
            end
            
            ecgData = waveMapData.ecgData;
            
            self.DetectedWaves = waveMapData.detectedWaves;
            self.WaveMemberships = waveMapData.waveMemberships;
            self.WavemapCalculator = waveMapData.wavemapCalculator;
            
            if isempty(self.WavemapCalculator.DetectionMethodIndex)
                self.WavemapCalculator.DetectionMethodIndex = 2;
            end
            
            self.SetECGData(waveMapData.ecgData, DeflectionDetectionResults(waveMapData.deflectionData));
            
            clear waveMapData;
            
            notify(self, 'WavemapComputed');
        end
        
        function delete(self)
            self.EcgData = [];
            self.IntrinsicDeflectionData = [];
            self.DeflectionData = [];
            self.DetectedWaves = [];
            delete(self.WavemapCalculator);
            self.WavemapCalculator = [];
        end
    end
    
    methods (Access = protected)
        function CreateControlPanel(self)
            self.ControlPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0 .975 1 .025]);
            
            self.AnimationButton = uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [0 0 .1 1],...
                'style', 'togglebutton',...
                'string', '>',...
                'callback', @self.ControlAnimation);
            
            self.AnimationSlider = uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.3 0 .7 1],...
                'style', 'slider',...
                'callback', @self.AnimationSliderCallback);
            addlistener(handle(self.AnimationSlider), 'Value', 'PostSet', @self.ContinuousAnimationSliderCallback);
            
            self.AnimationPositionLabel = uicontrol(...
                'parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.1 0 .1 1],...
                'style', 'text');
            uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.2 0 .1 1],...
                'style', 'edit',...
                'string', num2str(self.TimeLag),...
                'callback', @self.SetTimeLag);
            
            
            wavemapControlPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.9 .425 .1 .425]);
            
            uicontrol('parent', wavemapControlPanel,...
                'units', 'normalized',...
                'position', [.1 .9 .8 .1],...
                'style', 'pushbutton',...
                'string', 'Compute wave map',...
                'callback', @self.ComputeWavemap);
            uicontrol('parent', wavemapControlPanel,...
                'units', 'normalized',...
                'position', [.1 .8 .8 .1],...
                'style', 'popupmenu',...
                'string', AlgorithmPkg.WavemapCalculator.METHODS,...
                'value', self.WavemapCalculator.DetectionMethodIndex,...
                'callback', @self.SetDetectionMethod);
            uicontrol('parent', wavemapControlPanel,...
                'units', 'normalized',...
                'position', [.1 .7 .8 .1],...
                'style', 'pushbutton',...
                'string', 'Improve wave map',...
                'callback', @self.ImproveWaveMap);
            
            uicontrol('parent', wavemapControlPanel,...
                'units', 'normalized',...
                'position', [.1 .6 .5 .07],...
                'style', 'text',...
                'string', 'Minimal conduction (cm/s)');
            uicontrol('parent', wavemapControlPanel,...
                'units', 'normalized',...
                'position', [.7 .6 .2 .1],...
                'style', 'edit',...
                'string', num2str(100 * self.WavemapCalculator.ConductionThreshold),...
                'callback', @self.SetConductionThreshold);
            
            uicontrol('parent', wavemapControlPanel,...
                'units', 'normalized',...
                'position', [.1 .5 .5 .07],...
                'style', 'text',...
                'string', 'Merge threshold (%)');
            uicontrol('parent', wavemapControlPanel,...
                'units', 'normalized',...
                'position', [.7 .5 .2 .1],...
                'style', 'edit',...
                'string', num2str(100 * self.WavemapCalculator.MergeOverlapThreshold),...
                'callback', @self.SetMergeOverlapThreshold);
            
            uicontrol('parent', wavemapControlPanel,...
                'units', 'normalized',...
                'position', [.1 .4 .8 .1],...
                'style', 'pushbutton',...
                'string', 'Create video',...
                'callback', @self.CreateWavemapVideo);
            
            uicontrol('parent', wavemapControlPanel,...
                'units', 'normalized',...
                'position', [.1 .3 .8 .1],...
                'style', 'pushbutton',...
                'string', 'Create signal video',...
                'callback', @self.CreateSignalVideo);
            
%             uicontrol('parent', wavemapControlPanel,...
%                 'units', 'normalized',...
%                 'position', [.1 .2 .8 .1],...
%                 'style', 'pushbutton',...
%                 'string', 'Create map videos (4)',...
%                 'callback', @self.CreateMapVideos);
            
            uicontrol('parent', wavemapControlPanel,...
                'units', 'normalized',...
                'position', [.1 0 .8 .1],...
                'style', 'pushbutton',...
                'string', 'Load Wave map',...
                'callback', @self.LoadWavemap);
            
            uicontrol('parent', wavemapControlPanel,...
                'units', 'normalized',...
                'position', [.1 .1 .8 .1],...
                'style', 'pushbutton',...
                'string', 'Save Wave map',...
                'callback', @self.SaveWavemap);
            
            pacemapControlPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.9 0 .1 .425]);
            uicontrol('parent', pacemapControlPanel,...
                'units', 'normalized',...
                'position', [.1 .7 .8 .2],...
                'style', 'pushbutton',...
                'string', 'Load PACEMAP Wave map',...
                'callback', @self.LoadPacemapWavemap);
            self.CompareButton = uicontrol('parent', pacemapControlPanel,...
                'units', 'normalized',...
                'position', [.1 .5 .8 .2],...
                'style', 'pushbutton',...
                'string', 'Compare Wave maps',...
                'callback', @self.CompareWavemap);
            self.CreatePacemapVideoButton = uicontrol('parent', pacemapControlPanel,...
                'units', 'normalized',...
                'position', [.1 .3 .8 .2],...
                'style', 'pushbutton',...
                'string', 'Create PACEMAP video',...
                'callback', @self.CreatePacemapVideo);
        end
        
        function CreateSignalPanel(self)
            self.SignalPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0 .85 1 .125]);
            self.SignalAxes = axes('parent', self.SignalPanel,...
                'units', 'normalized',...
                'outerPosition', [0 0 1 1]);
        end
        
        function CreateAnimationPanel(self)
            self.NewWavemapControl = ExtendedUIPkg.GridWavemapControl([.225 0 .45 .85]);
            self.NewWavemapControl.Name = 'Waves';
            self.NewWavemapControl.Create(self.ControlHandle);
            addlistener(self.NewWavemapControl, 'MapClicked', @self.HandleMapClick);
            
            self.EcgMapControl = ExtendedUIPkg.SignalMapControl([0 .425 .225 .425]);
            self.EcgMapControl.Name = 'Signal';
            self.EcgMapControl.Create(self.ControlHandle);
            
            self.DeflectionMapControl = ExtendedUIPkg.GridActivationMapControl([0 0 .225 .425]);
            self.DeflectionMapControl.Name = 'Deflections';
            self.DeflectionMapControl.Create(self.ControlHandle);
            
            self.AnimatedWavesControl = ExtendedUIPkg.AnimatedGridWavemapControl([.675 .425 .225 .425]);
            self.AnimatedWavesControl.Name = 'Preview';
            self.AnimatedWavesControl.Create(self.ControlHandle);
            
            self.PacemapControl = ExtendedUIPkg.GridWavemapControl([.675 0 .225 .425]);
            self.PacemapControl.Name = 'PACEMAP';
            self.PacemapControl.Create(self.ControlHandle);
        end
        
        function InitializeSignalMapAnimation(self)
            self.EcgMapControl.SamplingFrequency = self.EcgData.SamplingFrequency;
            self.EcgMapControl.SetData(self.EcgData.MappedData, self.EcgData.ElectrodePositions);
            self.EcgMapControl.Show();
        end
        
        function InitializeDeflectionAnimation(self)
            self.DeflectionMapControl.SamplingFrequency = self.EcgData.SamplingFrequency;
            self.DeflectionMapControl.SetData(self.EcgData.Activations, self.EcgData.ElectrodePositions);
            self.DeflectionMapControl.Show();
        end
        
        function InitializeWavemapAnimation(self)
            self.NewWavemapControl.SamplingFrequency = self.EcgData.SamplingFrequency;
            self.NewWavemapControl.SetData(self.DetectedWaves, self.EcgData.ElectrodePositions);
            self.NewWavemapControl.Show();
        end
        
        function InitializePreviewAnimation(self)
            self.AnimatedWavesControl.SamplingFrequency = self.EcgData.SamplingFrequency;
            self.AnimatedWavesControl.SetData(self.DetectedWaves, self.EcgData.ElectrodePositions);
            self.AnimatedWavesControl.Show();
        end
        
        function InitializePacemapAnimation(self)
            if isempty(self.PacemapData), return; end
            
            self.PacemapControl.SamplingFrequency = self.PacemapData.SamplingFrequency;
            self.PacemapControl.SetData(self.PacemapWaves, self.PacemapData.ElectrodePositions);
            self.PacemapControl.Show();
            
            self.NewWavemapControl.MatchWaveColors(self.PacemapWaves, self.PacemapControl.WaveColors);
        end
        
        function InitializeSignalAxes(self)
            if isempty(self.EcgData), return; end
            
            signal = self.EcgData.Data(:, self.SelectedChannelIndex);
            time = self.EcgData.GetTimeRange();
            
            if ishandle(self.FarFieldDeflectionLine)
                delete(self.FarFieldDeflectionLine);
            end
            self.FarFieldDeflectionLine = line(...
                'xData', [], 'yData', [],...
                'color', [0 0 1],...
                'lineWidth', 2,...
                'parent', self.SignalAxes);
            
            if ishandle(self.IntrinsicDeflectionLine)
                delete(self.IntrinsicDeflectionLine);
            end
            self.IntrinsicDeflectionLine = line(...
                'xData', [], 'yData', [],...
                'color', [1 0 0],...
                'lineWidth', 3,...
                'parent', self.SignalAxes);
            
            if ishandle(self.PACEMAPActivationLine)
                delete(self.PACEMAPActivationLine);
            end
            self.PACEMAPActivationLine = line(...
                'xData', [], 'yData', [],...
                'color', [0 .5 0],...
                'lineWidth', 3,...
                'parent', self.SignalAxes);
            
            if ishandle(self.ReferenceLine)
                delete(self.ReferenceLine);
            end
            self.ReferenceLine = line(...
                'xData', [], 'yData', [],...
                'color', [.7 .7 .7],...
                'lineWidth', 1,...
                'parent', self.SignalAxes);
            
            if ishandle(self.SignalLine)
                delete(self.SignalLine);
            end
            self.SignalLine = line('parent', self.SignalAxes,...
                'xData', time, 'yData', signal, 'color', [0 0 0], 'lineSmoothing', 'on');
            
            if ishandle(self.AnimationPositionLine)
                delete(self.AnimationPositionLine);
            end
            self.AnimationPositionLine = line(...
                'xData', [0 0], 'yData', [0 0],...
                'color', [1 0 0],...
                'lineWidth', 1,...
                'parent', self.SignalAxes);
            
            if ishandle(self.SelectedTimeLine)
                delete(self.SelectedTimeLine);
            end
            self.SelectedTimeLine = line(...
                'xData', [0 0], 'yData', [0 0],...
                'color', [1 0 0],...
                'lineWidth', 2,...
                'lineStyle', ':',...
                'parent', self.SignalAxes);
            
            self.SelectedChannelText = text(...
                'units', 'normalized',...
                'position', [0, 1],...
                'string', '',...
                'verticalAlignment', 'top',...
                'parent', self.SignalAxes,...
                'backgroundColor', [1 1 1],...
                'edgeColor', [0 0 0]);
            
            set(self.SignalAxes, 'LooseInset', get(self.SignalAxes, 'TightInset'));
            
            self.ShowSelectedChannel();
        end
        
        function ShowSelectedChannel(self)
            % signal
            signal = self.EcgData.Data(:, self.SelectedChannelIndex);
            time = self.EcgData.GetTimeRange();
            
            set(self.SignalLine, 'yData', signal / 1000);
            
            yLimits = [min(signal) max(signal)] / 1000;
            
            % reference signal
            if ~isempty(self.EcgData.ReferenceData)
                referenceData = self.EcgData.ReferenceData(:, 1);
                referenceData = (referenceData - min(referenceData)) / (max(referenceData) - min(referenceData));
                referenceData = referenceData * (yLimits(end) - yLimits(1)) + yLimits(1);
                set(self.ReferenceLine, 'xData', time, 'yData', referenceData);
                uistack(self.ReferenceLine, 'bottom');
            end
            
            deflectionData = self.IntrinsicDeflectionData{self.SelectedChannelIndex};
            
            % intrinsic deflections
            [xData yData] = DeflectionECGChannelPanel.ComputeDeflectionLineData(...
                time, signal,...
                deflectionData.templates,...
                deflectionData.templatePeakTemplates(deflectionData.PrimaryDeflections, :),...
                deflectionData.templatePeakRanges(deflectionData.PrimaryDeflections, :),...
                deflectionData.templatePeakCoefficients(deflectionData.PrimaryDeflections, :));
            
            set(self.IntrinsicDeflectionLine, 'xData', xData, 'yData', yData / 1000);
            
            % far-field deflections
            [xData yData] = DeflectionECGChannelPanel.ComputeDeflectionLineData(...
                time, signal,...
                deflectionData.templates,...
                deflectionData.templatePeakTemplates(~deflectionData.PrimaryDeflections, :),...
                deflectionData.templatePeakRanges(~deflectionData.PrimaryDeflections, :),...
                deflectionData.templatePeakCoefficients(~deflectionData.PrimaryDeflections, :));
            
            set(self.FarFieldDeflectionLine, 'xData', xData, 'yData', yData / 1000);
            
            % PACEMAP activations
            [xData yData] = self.GetPACEMAPActivationLineData(yLimits);
            set(self.PACEMAPActivationLine, 'xData', xData, 'yData', yData);
            
            set(self.SignalAxes, 'yLim', yLimits);
            
            yLimits = get(self.SignalAxes, 'yLim');
            set(self.AnimationPositionLine, 'yData', yLimits);
            set(self.SelectedTimeLine,...
                'xData', [self.SelectedTime, self.SelectedTime], 'yData', yLimits);
            
            set(self.SelectedChannelText, 'string',...
                ['Channel: ', self.EcgData.ElectrodeLabels{self.SelectedChannelIndex}]);
        end
        
        function SetSignalWindow(self, position)
            timePosition = position / self.EcgData.SamplingFrequency;
            timeWindow = [timePosition - 0.5, timePosition + 0.5];
            xTickSpacing = 0.1;
            xTicks = (timeWindow(1) + (xTickSpacing - mod(timeWindow(1), xTickSpacing))):(xTickSpacing):(timeWindow(end) - mod(timeWindow(end), xTickSpacing));
            
            set(self.SignalAxes,...
                'xLim', timeWindow,...
                'xTick', xTicks,...
                'xMinorTick', 'on');
            
            yLimits = get(self.SignalAxes, 'yLim');
            set(self.AnimationPositionLine, 'xData', [timePosition, timePosition], 'yData', yLimits);
        end
        
        function SetTimeLag(self, source, varargin)
            self.TimeLag = str2double(get(source, 'string'));
            
            self.NewWavemapControl.SetTimeLag(self.TimeLag);
            self.EcgMapControl.SetTimeLag(self.TimeLag);
            self.DeflectionMapControl.SetTimeLag(self.TimeLag);
            self.AnimatedWavesControl.SetTimeLag(self.TimeLag);
            self.PacemapControl.SetTimeLag(self.TimeLag);
        end
        
        function SetDetectionMethod(self, source, varargin)
            self.WavemapCalculator.DetectionMethodIndex = get(source, 'value');
        end
        
        function SetConductionThreshold(self, source, varargin)
            value = str2double(get(source, 'string')) / 100;
            if value > 0
                self.WavemapCalculator.ConductionThreshold = value;
            end
            
            set(source, 'string', num2str(100 * self.WavemapCalculator.ConductionThreshold));
        end
        
        function SetMergeOverlapThreshold(self, source, varargin)
            value = str2double(get(source, 'string')) / 100;
            if value >= 0
                self.WavemapCalculator.MergeOverlapThreshold = value;
            end
            
            set(source, 'string', num2str(100 * self.WavemapCalculator.MergeOverlapThreshold));
        end
        
        function HandleMapClick(self, ~, eventData)
            self.SelectedChannelIndex = eventData.Position;
            self.SelectedTime = eventData.Time;
            self.ShowSelectedChannel();
        end
        
        function ControlAnimation(self, varargin)
            set(self.AnimationButton, 'string', '||');
            numberOfSamples = self.EcgData.GetNumberOfSamples();
            
            set(self.EcgMapControl.SignalImage, 'eraseMode', 'none');
            while get(self.AnimationButton, 'value')
                drawnow;
                self.AnimationPosition = self.AnimationPosition + 1;
                if self.AnimationPosition > numberOfSamples
                    self.AnimationPosition = 1;
                end
                set(self.AnimationSlider, 'value', self.AnimationPosition);
                self.SetAnimationFrame();
            end
            
            set(self.EcgMapControl.SignalImage, 'eraseMode', 'normal');
            
            set(self.AnimationButton, 'string', '>');
            
            self.SetStaticFrame();
        end
        
        function AnimationSliderCallback(self, varargin)
            self.AnimationPosition = round(get(self.AnimationSlider, 'value'));
            self.SetStaticFrame();
        end
        
        function ContinuousAnimationSliderCallback(self, source, eventData)
            sliderObject = get(eventData, 'AffectedObject');
            self.AnimationPosition = round(get(sliderObject, 'Value'));
            self.SetAnimationFrame();
            self.SetSignalWindow(self.AnimationPosition);
        end
        
        function SetStaticFrame(self)
            self.SetAnimationInfo();
            
            self.EcgMapControl.SetStaticFramePosition(self.AnimationPosition);
            self.DeflectionMapControl.SetStaticFramePosition(self.AnimationPosition);
            self.NewWavemapControl.SetStaticFramePosition(self.AnimationPosition);
            self.PacemapControl.SetStaticFramePosition(self.AnimationPosition);
            self.AnimatedWavesControl.SetStaticFramePosition(self.AnimationPosition);
            
            self.SetSignalWindow(self.AnimationPosition);
        end
        
        function SetAnimationFrame(self)
            self.SetAnimationInfo();
            
            self.EcgMapControl.SetFramePosition(self.AnimationPosition);
            self.DeflectionMapControl.SetFramePosition(self.AnimationPosition);
            self.NewWavemapControl.SetFramePosition(self.AnimationPosition);
            self.PacemapControl.SetFramePosition(self.AnimationPosition);
        end
        
        function SetAnimationInfo(self)
            timePosition = round(1000 * self.AnimationPosition / self.EcgData.SamplingFrequency);
            set(self.AnimationPositionLabel, 'string', [ num2str(timePosition) ' ms']);
        end
        
        function ComputeWavemap(self, varargin)
            if isempty(self.IntrinsicDeflectionData), return; end
            
            [self.DetectedWaves, self.WaveMemberships, self.WaveStartingPoints] =...
                self.WavemapCalculator.DetectWaves(self.EcgData, self.IntrinsicDeflectionData);
            
            self.InitializeWavemapAnimation();
            self.InitializePreviewAnimation();
            
            notify(self, 'WavemapComputed');
        end
        
        function ImproveWaveMap(self, varargin)
            if isempty(self.IntrinsicDeflectionData), return; end
            
            self.WavemapCalculator.WaveMerging = true;
            self.WavemapCalculator.StartPointElimination = true;
            self.WavemapCalculator.WaveConductionLikelihoodImprovement = true;
            
            [self.DetectedWaves self.WaveMemberships, self.IntrinsicDeflectionData] = self.WavemapCalculator.ImproveWaves();
            
            self.InitializeWavemapAnimation();
            self.InitializePreviewAnimation();
        end
        
        function LoadPacemapWavemap(self, varargin)
            [fileName, pathName, filterIndex] = HelperFunctions.customUigetfile({...
                '*Wave_Info.ASC', 'Wave Info file';...
                '*.*', 'All Files'},...
                'Select a PACEMAP wave map file');
            
            [result, waveMemberships, waveActivations electrodeMapName] =...
                WaveMapPanel.ReadPacemapWaveData(fullfile(pathName, fileName));
            
            waveActivations = waveActivations(:, 3:end);
            % SHIFT?
            waveActivations = waveActivations + 5;
            waveMembership = waveMemberships(:, 3:end);
            startDC = (waveMembership > 3000);
            startBT = (waveMembership > 2000) & (waveMembership < 3000);
            startPW = (waveMembership > 1000) & (waveMembership < 2000);
            waveStarts = struct(...
                'peripheral', startPW,...
                'breakthrough', startBT,...
                'discontinuousConduction', startDC);
            
            waveMembership = waveMembership - startDC * 3000;
            waveMembership = waveMembership - startBT * 2000;
            waveMembership = waveMembership - startPW * 1000;
            
            electrodeLabels = cellstr(num2str((1:size(waveActivations, 1))'));
            self.PacemapData = DCMData(fileName, NaN(1, size(waveActivations, 1)), 1e3, electrodeLabels);
            self.PacemapData.SetElectrodeMap(electrodeMapName);
            self.PacemapActivations = waveActivations / 1e3;
            
            waveIDs = unique(waveMembership(~isnan(waveMembership)));
            self.PacemapWaves = AlgorithmPkg.Wave.empty(numel(waveIDs), 0);
            for waveIndex = 1:numel(waveIDs)
                waveMembers = (waveMembership == waveIDs(waveIndex));
                [rowPositions ~] = find(waveMembers);
                waveMembers = find(waveMembers);
                self.PacemapWaves(waveIndex) = AlgorithmPkg.Wave(waveIndex, self.PacemapData.ElectrodePositions);
                for waveMemberIndex = 1:numel(waveMembers)
                    self.PacemapWaves(waveIndex).Add(rowPositions(waveMemberIndex),...
                        NaN,...
                        waveActivations(waveMembers(waveMemberIndex)));
                end
            end
            
            self.InitializePacemapAnimation();
        end
        
        function SaveWavemap(self, varargin)
            [pathString, nameString] = fileparts(self.EcgData.Filename);
            filename = [pathString, filesep, nameString '_WM'];
            ecgData = self.EcgData; %#ok<NASGU>
            deflectionData = self.IntrinsicDeflectionData; %#ok<NASGU>
            detectedWaves = self.DetectedWaves; %#ok<NASGU>
            waveMemberships = self.WaveMemberships; %#ok<NASGU>
            wavemapCalculator = self.WavemapCalculator; %#ok<NASGU>
            
            save(filename,...
                'ecgData',...
                'deflectionData',...
                'detectedWaves',...
                'waveMemberships',...
                'wavemapCalculator');
        end
        
        function LoadWavemap(self, varargin)
            if isempty(self.EcgData)
                [filename pathname] = HelperFunctions.customUigetfile('*_WM.mat', 'Select WaveMap file');
                if isequal(filename, 0) || isequal(pathname, 0), return; end
                filename = fullfile(pathname, filename);
            else
                [pathString nameString] = fileparts(self.EcgData.Filename);
                filename = [pathString, filesep, nameString '_WM.mat'];
            end
            
            self.LoadData(filename);
        end
        
        function CompareWavemap(self, varargin)
            if isempty(self.DetectedWaves) || isempty(self.PacemapWaves), return; end
            
            deflectionComparer = AlgorithmPkg.DeflectionComparer();
            deflectionComparer.BlankingPeriod = 0.2;
            
            deflectionData = self.IntrinsicDeflectionData;
            activationData = self.PacemapActivations;
            deflectionActivationIndices = NaN(numel(deflectionData), 1);
            for electrodeIndex = 1:numel(deflectionData)
                index = find(strcmp(self.EcgData.ElectrodeLabels{electrodeIndex}, self.PacemapData.ElectrodeLabels));
                if ~isempty(index)
                    deflectionActivationIndices(electrodeIndex) = index;
                end
                deflectionActivationIndices = deflectionActivationIndices(~isnan(deflectionActivationIndices));
            end
            activationData = activationData(deflectionActivationIndices, :);
            
            time = self.EcgData.GetTimeRange();
            [electrodeSpecificity, electrodeSensitivity] = deflectionComparer.Compare(deflectionData, activationData, time);
            specificity = sum(cellfun(@(x) numel(find(x)), electrodeSpecificity)) / sum(cellfun(@(x) numel(x), electrodeSpecificity));
            sensitivity = sum(cellfun(@(x) numel(find(x)), electrodeSensitivity)) / sum(cellfun(@(x) numel(x), electrodeSensitivity));
            
%             if isobject(self.ComparisonView)
%                 delete(self.ComparisonView);
%             end
%             
%             self.ComparisonView = AnalysisUIPkg.WavemapComparisonPanel([0 0 1 1]);
%             
%             screenSize = get(0,'Screensize');
%             pcaPosition = [...
%                 screenSize(1) + screenSize(3) / 3,...
%                 screenSize(2) + screenSize(4) / 3,...
%                 screenSize(3) / 3,...
%                 screenSize(4) / 3];
%             self.ComparisonFigure = UserInterfacePkg.CustomFigure(pcaPosition);
%             self.ComparisonFigure.Create();
%             self.ComparisonFigure.Name = 'Wavemap Comparison';
%             self.ComparisonFigure.AddCustomControl(self.ComparisonView);
%             
%             self.ComparisonView.SetWavemapData(...
%                 self.PacemapWaves, self.DetectedWaves,...
%                 self.PacemapData.ElectrodePositions, self.EcgData.ElectrodePositions);
%             
%             self.ComparisonView.Show();
        end
        
        function CreatePacemapVideo(self, varargin)
            waitbarHandle = waitbar(0 ,'Writing PACEMAP video'); 
            
            writer = VideoWriter('pacemap.avi');
            writer.Quality = 100;
            open(writer);
            
            numberOfSamples = self.EcgData.GetNumberOfSamples();
            
            for sampleIndex =1000:2000
                colorData1 = self.EcgMapControl.GetFrame(sampleIndex);
                colorData2 = self.PacemapControl.GetFrame(sampleIndex);
                
                colorData = [colorData1,...
                    ones(size(colorData1, 1), 1, 3),...
                    colorData2];
                
                colorData = imresize(colorData, 20, 'nearest');
                colorData(colorData > 1) = 1;
                colorData(colorData < 0) = 0;
                writeVideo(writer, im2frame(colorData));
                
                if mod(sampleIndex, 100) == 0
                    waitbar(sampleIndex / numberOfSamples,...
                        waitbarHandle,...
                        ['Writing PACEMAP video, sample ' ...
                        num2str(sampleIndex) ' of ' ...
                        num2str(numberOfSamples)]);
                end
            end
            close(waitbarHandle);
            close(writer);
        end
        
        function CreateMapVideos(self, varargin)
            waitbarHandle = waitbar(0 ,'Writing wavemap videos (4)'); 
            
            writer = VideoWriter('maps.avi');
            writer.Quality = 100;
            open(writer);
            
            numberOfSamples = self.EcgData.GetNumberOfSamples();
            
            for sampleIndex =1:numberOfSamples
                colorData1 = self.EcgMapControl.GetFrame(sampleIndex);
                colorData2 = self.DeflectionMapControl.GetFrame(sampleIndex);
                colorData3 = self.PacemapControl.GetFrame(sampleIndex);
                colorData4 = self.NewWavemapControl.GetFrame(sampleIndex);
                
                colorData12 = [colorData1,...
                    ones(size(colorData1, 1), 1, 3),...
                    colorData2];
                
                colorData = [colorData12;...
                    ones(1, size(colorData12, 2), 3);...
                    colorData3,...
                    ones(size(colorData3, 1), 1, 3),...
                    colorData4];
                
                colorData = imresize(colorData, 20, 'nearest');
                colorData(colorData > 1) = 1;
                colorData(colorData < 0) = 0;
                writeVideo(writer, im2frame(colorData));
                
                if mod(sampleIndex, 100) == 0
                    waitbar(sampleIndex / numberOfSamples,...
                        waitbarHandle,...
                        ['Writing map videos, sample ' ...
                        num2str(sampleIndex) ' of ' ...
                        num2str(numberOfSamples)]);
                end
            end
            close(waitbarHandle);
            close(writer);
        end
        
        function CreateWavemapVideo(self, varargin)
            waitbarHandle = waitbar(0 ,'Writing wavemap video'); 
            
            writer = VideoWriter('wavemap.avi');
            writer.FrameRate = 100;
            writer.Quality = 75;
            open(writer);
            
            numberOfSamples = self.EcgData.GetNumberOfSamples();
            
            for sampleIndex = 1:numberOfSamples
                colorData = self.NewWavemapControl.GetFrame(sampleIndex);
                
                colorData = imresize(colorData, 10, 'nearest');
                colorData(colorData > 1) = 1;
                colorData(colorData < 0) = 0;
                writeVideo(writer, im2frame(colorData));
                
                if mod(sampleIndex, 100) == 1
                    waitbar(sampleIndex / numberOfSamples,...
                        waitbarHandle,...
                        ['Writing wavemap video, sample ' ...
                        num2str(sampleIndex) ' of ' ...
                        num2str(numberOfSamples)]);
                end
            end
            close(waitbarHandle);
            close(writer);
        end
        
        function CreateSignalVideo(self, varargin)
            self.InitializeSignalAxes();
            self.ShowSelectedChannel();
            
            axis(self.SignalAxes, 'tight');
            axis(self.SignalAxes, 'manual');
            yLimits = get(self.SignalAxes, 'yLim');
            
            set(self.SignalAxes,...
                    'xTickMode', 'auto',...
                    'xMinorTick', 'off');
            
            writer = VideoWriter('signal.avi');
            writer.FrameRate = 25;
            writer.Quality = 100;
            open(writer);
            
            waitbarHandle = waitbar(0 ,'Writing signal video');
            numberOfSamples = self.EcgData.GetNumberOfSamples();
            for sampleIndex = 1:4:numberOfSamples
                timePosition = sampleIndex / self.EcgData.SamplingFrequency;
                set(self.AnimationPositionLine, 'xData', [timePosition, timePosition], 'yData', yLimits);
            
                frame = getframe(self.SignalAxes);
                writeVideo(writer, frame);
                
                if mod(sampleIndex, 100) == 1
                    waitbar(sampleIndex / numberOfSamples,...
                        waitbarHandle,...
                        ['Writing signal video, sample ' ...
                        num2str(sampleIndex) ' of ' ...
                        num2str(numberOfSamples)]);
                end
            end
            close(waitbarHandle);
            close(writer);
        end
        
        function [xData, yData] = GetPACEMAPActivationLineData(self, yLimits)
            xData = [];
            yData = [];
            if isempty(self.PacemapActivations), return; end
            
            ecgDataElectrodeLabel = self.EcgData.ElectrodeLabels{self.SelectedChannelIndex};
            pacemapElectrodeIndex = find(strcmp(ecgDataElectrodeLabel, self.PacemapData.ElectrodeLabels));
            if isempty(pacemapElectrodeIndex), return; end
            
            activations = self.PacemapActivations(pacemapElectrodeIndex, :);
            activations = activations(~isnan(activations));
            
            xData = NaN(3 * numel(activations) - 1, 1);
            yData = xData;
            for activationIndex = 1:numel(activations)
                xData(3 * activationIndex - 2) = activations(activationIndex);
                xData(3 * activationIndex - 1) = activations(activationIndex);
                
                yData(3 * activationIndex - 2) = yLimits(1);
                yData(3 * activationIndex - 1) = yLimits(end);
            end
        end
    end
    
    methods (Static)
        function [result, waveMemberships, waveActivations, electrodeMapName] = ReadPacemapWaveData(filename)
            infile = fopen(filename, 'rt');
            
            sampleOffset = 2;
            linesToSkip = 7;
            
            line = 1;
            electrode = 1;
            wave = [];
            activations = [];
            electrodeMapName = '';
            while ~feof(infile)
                lineText = fgetl(infile);
                
                if line == 2
                    try
                        if ispc
                            [pathString, filename] = fileparts(lineText);
                            electrodeMapName = filename;
                        else
                            fileParts = textscan(lineText, '%s', 'Delimiter', '\\');
                            electrodeMapName = fileParts{1}{end};
                            extensionPosition = strfind(lower(electrodeMapName), '.mat');
                            electrodeMapName = electrodeMapName(1:(extensionPosition - 1));
                        end
                    catch ME
                        errordlg(ME.message, 'No electrodemap information found in file');
                    end
                end
                
                if line > linesToSkip
                    % parse first electrode line
                    rem = lineText;
                    [token, rem] = strtok(rem);
                    sample = 0;
                    
                    % sanity check
                    if 0 == strcmp(token,'Ele:')
                        %             result = -1;
                        %             waveMemberships = [];
                        %             waveActivations = [];
                        %             return;
                        break;
                    end
                    
                    % set electrode number
                    [token, rem] = strtok(rem);
                    wave(electrode, 1) = str2num(token);
                    activations(electrode, 1) = str2num(token);
                    
                    % get samples for the current electrode
                    [token, rem] = strtok(rem);
                    while ~isempty(rem)
                        sample = sample + 1;
                        activations(electrode, sample + sampleOffset) = str2num(token);
                        [token, rem] = strtok(rem);
                    end
                    if ~isempty(token)
                        if ~isempty(str2num(token))
                            sample = sample + 1;
                            activations(electrode, sample + sampleOffset) = str2num(token);
                        end
                    end
                    
                    % parse second electrode line
                    sample = 0;
                    lineText = fgetl(infile);
                    if -1 ~= lineText
                        rem = lineText;
                        [token, rem] = strtok(rem);
                        while 0 == isempty(rem)
                            sample = sample + 1;
                            wave(electrode, sample + sampleOffset) = str2num(token);
                            [token, rem] = strtok(rem);
                        end
                        if ~isempty(token)
                            if ~isempty(str2num(token))
                                sample = sample + 1;
                                wave(electrode, sample + sampleOffset) = str2num(token);
                            end
                        end
                    end
                    
                    % parse third electrode line
                    lineText = fgetl(infile);
                    
                    % write number of samples for current electrode
                    wave(electrode, sampleOffset) = sample;
                    activations(electrode, sampleOffset) = sample;
                    electrode = electrode + 1;
                end
                line = line + 1;
            end
            
            % replace zeros with NaN in wave and activations matrix
            wave(0 == wave) = NaN;
            activations(0 == activations) = NaN;
            
            waveMemberships = wave;
            result = fclose(infile);
            waveActivations = activations;
        end
    end
end
classdef VentricularAnalysis < UserInterfacePkg.CustomFigure
    properties (Access = protected)
        EcgData

        TabGroup

        ChannelSelectionView
        PaceBlankingView
        PreProcessingView
        DepolarizationDetectionView
        RepolarizationDetectionView
        DepolarizationRepolarizationView

        ApplicationSettings
        ParallelProcessing

        ZoomToggleButton
        PanToggleButton

        AnalysisStatusBar

        GUIMode
        MappedFileTypeIndices
    end

    properties (Constant)
        FILETYPEFILTER = {...
            '*.DATA', 'IDEEQ Data (*.DATA)';...
            '*.*', 'DCM (PACEMAP Binary) (*.E**)';...
            '*.bdf', 'BioSemi data format (*.bdf)';...
            '*.csv', 'Basket Export (*.csv)';...
            '*.*', 'DCM (PACEMAP Binary Series) (*.E**)';...
            '*.ASC', 'DCM (PACEMAP ASCII) (*.ASC)';...
            '*.mat', 'MAT-file containing ecgData (*.mat)'}
    end

    methods
        function self = VentricularAnalysis(position)
            if nargin < 1
                warning off MATLAB:uitabgroup:OldVersion;
                screenSize = get(0,'Screensize');
                position = [screenSize(3) / 5, screenSize(4) / 5,...
                    3 * screenSize(3) / 5, 3 * screenSize(4) / 5];
            end

            self = self@UserInterfacePkg.CustomFigure(position);
            self.Name = 'Ventricular Analysis';
            self.ParallelProcessing = false;
            self.GUIMode = 'Release';

            self.MappedFileTypeIndices = 1:size(VentricularAnalysis.FILETYPEFILTER, 1);

            if nargin < 1
                self.Create();
            end
        end

        function Create(self)
            Create@UserInterfacePkg.CustomFigure(self);

            set(self.ControlHandle,...
                'CloseRequestFcn', @self.Close);

            set(self.ControlHandle,...
                'colormap', flipud(gray(256)));

            self.Hide();

            self.CreateFileMenu();

            self.CreateToolbar();

            self.CreateStatusBar();

            self.InitializeEvents();

            self.Show();
        end

        function delete(self)
            self.EcgData = [];
            if ishandle(self.ApplicationSettings)
                delete(self.ApplicationSettings);
            end
            clear self.ApplicationSettings;
        end
    end

    methods (Access = protected)
        function InitializeTabs(self)
            self.RemoveTabs();
            if ishandle(self.TabGroup)
                delete(self.TabGroup);
            end

            self.InitializeDCMTabs();
        end

        function ShowEmptyDCMTabs(self, varargin)
            self.EcgData = DCMData('Empty tabs', [], NaN);
            self.InitializeTabs();
            self.SetFilename();
        end

        function InitializeDCMTabs(self)
            self.Hide();

            self.TabGroup = uitabgroup(...
                'parent', self.ControlHandle,...
                'position', [0 .025 1 .975]);

            channelSelectionTab = uitab(self.TabGroup, 'title', 'Electrograms');
            self.ChannelSelectionView = DCMVisualizationPanel([0 0 1 1]);
            self.ChannelSelectionView.Create(channelSelectionTab);
            self.ChannelSelectionView.Show();

            addlistener(self.ChannelSelectionView, 'ValidChannelsSelectionCompleted',...
                @self.HandleChannelSelectionEvent);

            paceBlankingTab = uitab(self.TabGroup, 'title','Pace detection');
            self.PaceBlankingView = PaceBlankingPanel([0 0 1 1]);
            self.PaceBlankingView.Create(paceBlankingTab);
            self.PaceBlankingView.Show();

            addlistener(self.PaceBlankingView, 'PaceBlankingCompleted',...
                @self.HandlePaceBlankingEvent);

            preProcessingTab = uitab(self.TabGroup, 'title','Pre-processing');
            self.PreProcessingView = EGMPreProcessingPanel([0 0 1 1]);
            self.PreProcessingView.Create(preProcessingTab);
            self.PreProcessingView.Show();

            addlistener(self.PreProcessingView, 'PreProcessingCompleted',...
                @self.HandlePreProcessingEvent);

            deflectionAnalysisTab = uitab(self.TabGroup, 'title','Depolarization');
            self.DepolarizationDetectionView = DeflectionDetectionTestPanel([0 0 1 1]);
            self.DepolarizationDetectionView.Create(deflectionAnalysisTab);
            self.DepolarizationDetectionView.Show();

            addlistener(self.DepolarizationDetectionView, 'DeflectionDetectionCompleted',...
                @self.HandleIntrinsicDeflectionEvent);

            repolarizationDetectionTab = uitab(self.TabGroup, 'title','Repolarization');
            self.RepolarizationDetectionView = RepolarizationDetectionPanel([0 0 1 1]);
            self.RepolarizationDetectionView.Create(repolarizationDetectionTab);
            self.RepolarizationDetectionView.Show();

            addlistener(self.RepolarizationDetectionView, 'DeflectionDetectionCompleted',...
                @self.HandleRepolarizationEvent);

            depolarizationRepolarizationTab = uitab(self.TabGroup, 'title','Depolarization & repolarization viewer');
            self.DepolarizationRepolarizationView = DepolarizationRepolarizationPanel([0 0 1 1]);
            self.DepolarizationRepolarizationView.Create(depolarizationRepolarizationTab);
            self.DepolarizationRepolarizationView.Show();

            self.Show();
        end

        function RemoveTabs(self)
            if isobject(self.ChannelSelectionView)
                delete(self.ChannelSelectionView);
            end

            if isobject(self.PreProcessingView)
                delete(self.PreProcessingView);
            end

            if isobject(self.DepolarizationDetectionView)
                delete(self.DepolarizationDetectionView);
            end

            if isobject(self.RepolarizationDetectionView)
                delete(self.RepolarizationDetectionView);
            end

            if isobject(self.DepolarizationRepolarizationView)
                delete(self.DepolarizationRepolarizationView);
            end

        end

        function CreateFileMenu(self)
            fileMenu = uimenu(...
                'parent', self.ControlHandle,...
                'label', 'File');
            uimenu('parent', fileMenu,...
                'label', 'Import New Signal',...
                'callback', @self.LoadSignal);
            uimenu('parent', fileMenu,...
                'label', 'Load Analysis',...
                'callback', @self.LoadAnalysis);
            uimenu('parent', fileMenu,...
                'label', 'Show Empty Tabs',...
                'callback', @self.ShowEmptyDCMTabs);
        end

        function CreateToolbar(self)
            toolbar = uitoolbar(...
                'parent', self.ControlHandle);
            toolbarIconData = load('toolbarIcons.mat');
            uipushtool(...
                'parent', toolbar,...
                'CData', toolbarIconData.newDataCData,...
                'tooltipString', 'Import new signal',...
                'clickedCallback', @self.LoadSignal);
            uipushtool(...
                'parent', toolbar,...
                'CData', toolbarIconData.opendoc,...
                'tooltipString', 'Load analysis',...
                'clickedCallback', @self.LoadAnalysis);
            self.ZoomToggleButton = uitoggletool(...
                'parent', toolbar,...
                'CData', toolbarIconData.zoomCData,...
                'offCallback', 'zoom off',...
                'onCallback', @self.ZoomOn,...
                'separator', 'on');
            self.PanToggleButton = uitoggletool(...
                'parent', toolbar,...
                'CData', toolbarIconData.panCData,...
                'offCallback', 'pan off',...
                'onCallback', @self.PanOn);
            uipushtool(...
                'parent', toolbar,...
                'CData', toolbarIconData.DCMButtonData,...
                'tooltipString', 'Show empty tabs',...
                'clickedCallback', @self.ShowEmptyDCMTabs,...
                'separator', 'on');
        end

        function CreateStatusBar(self)
            self.AnalysisStatusBar = UserInterfacePkg.StatusBar([0 0 1 .025]);
            self.AddCustomControl(self.AnalysisStatusBar);
            self.AnalysisStatusBar.AddListener(UserInterfacePkg.StatusEventClass.Instance(), 'StatusChange');
            self.AnalysisStatusBar.Show();
        end

        function InitializeEvents(self)
            set(self.ControlHandle, 'KeyPressFcn', @self.NotifyKeyPress);
            set(self.ControlHandle, 'KeyReleaseFcn', @self.NotifyKeyRelease);

            set(self.ControlHandle, 'WindowButtonMotionFcn', @self.NotifyMouseMotion)
            set(self.ControlHandle, 'WindowButtonUpFcn', @self.NotifyMouseUp)
            set(self.ControlHandle, 'WindowButtonDownFcn', @self.NotifyMouseDown)
        end

        function NotifyKeyPress(self, ~, eventData)
            UserInterfacePkg.KeyEventClass.Instance().NotifyKeyPressed(UserInterfacePkg.KeyPressedEventData(eventData));
        end

        function NotifyKeyRelease(self, ~, eventData)
            UserInterfacePkg.KeyEventClass.Instance().NotifyKeyReleased(UserInterfacePkg.KeyPressedEventData(eventData));
        end

        function NotifyMouseMotion(self, varargin)
            UserInterfacePkg.MouseEventClass.Instance().NotifyMouseMotion();
        end

        function NotifyMouseUp(self, varargin)
            UserInterfacePkg.MouseEventClass.Instance().NotifyMouseUp();
        end

        function NotifyMouseDown(self, varargin)
            UserInterfacePkg.MouseEventClass.Instance().NotifyMouseDown();
        end

        function LoadSignal(self, varargin)
            try
                fileTypeFilters = VentricularAnalysis.FILETYPEFILTER;
                mappedFiletypeFilters = fileTypeFilters(self.MappedFileTypeIndices, :);

                % MAC OS (until 11.4) is not compatible with the uigetfile
                % filter option. Workaround for mac users:
                if ismac
                    filterIndex = menu('Select file type',fileTypeFilters(:,2));
                    [fileName, pathName, ~] = HelperFunctions.customUigetfile(...
                        '*.*',...
                        'Select a signal file');
                elseif ispc
                    [fileName, pathName, filterIndex] = HelperFunctions.customUigetfile(...
                        mappedFiletypeFilters,...
                        'Select a signal file');
                end

                if isequal(fileName, 0), return; end

                switch filterIndex
                    case 1
                        ecgData = InputOutputPkg.ECGReader.ReadIDEEQ(fullfile(pathName, fileName));
                    case 2
                        ecgData = InputOutputPkg.ECGReader.ReadPACEMAPBinary(fullfile(pathName, fileName));
                    case 4
                        ecgData = InputOutputPkg.ECGReader.ReadBasketExportData(fullfile(pathName, fileName));
                    case 5
                        ecgData = InputOutputPkg.ECGReader.ReadPACEMAPBinarySeries(fullfile(pathName, fileName));
                    case 6
                        ecgData = InputOutputPkg.ECGReader.ReadPACEMAPEcg(fullfile(pathName, fileName));
                    case 3
                        ecgData = InputOutputPkg.ECGReader.ReadDCMBDF(fullfile(pathName, fileName));
                        ecgData.SetElectrodeMap([]);
                    case 7
                        fileData = load(fullfile(pathName, fileName));
                        ecgData = fileData.ecgData;
                        ecgData.Filename = fullfile(pathName, fileName);
                        if isempty(ecgData.ElectrodePositions)
                            ecgData.SetElectrodeMap([]);
                        end
                    otherwise
                        errordlg('File Error', 'Unkown file type');
                        return;
                end
            catch loadFileError
                errordlg(loadFileError.message, 'File error');
                disp(loadFileError);
            end

            self.EcgData = ecgData;
            self.SetFilename();
            self.InitializeTabs();

            self.ChannelSelectionView.SetECGData(self.EcgData);
        end

        function LoadAnalysis(self, varargin)
            try
                [fileName, pathName, filterIndex] = HelperFunctions.customUigetfile({...
                    '*.mat', 'Any analysis file';...
                    '*_PP.mat', 'Preprocessed file';...
                    '*_TM.mat', 'Template matching file';...
                    '*_DD.mat', 'Deflection detection file'},...
                    'Select an analysis file');
                if isequal(fileName, 0), return; end

                patterns = {'_PaceDetection.mat'; '_PP.mat'; '_TM.mat'; '_DD.mat'};
                types = {'PaceDetection'; 'PP'; 'TM'; 'DD'};

                type = '';
                switch filterIndex
                    case 1
                        for typeIndex = 1:numel(patterns)
                            if ~isempty(strfind(fileName, patterns{typeIndex}))
                                type = types{typeIndex};
                                break;
                            end
                        end
                    otherwise
                        type = types{filterIndex - 1};
                end

                if isempty(type)
                    errordlg('File Error', 'Unkown file type');
                    return;
                end

                self.ShowEmptyDCMTabs();
                drawnow;
                switch(type)
                    case 'PaceDetection'
                        self.TabGroup.SelectedTab = self.TabGroup.Children(2);
                        ecgData = self.PaceBlankingView.LoadData(fullfile(pathName, fileName));
                    case 'PP'
                        self.TabGroup.SelectedTab = self.TabGroup.Children(3);
                        ecgData = self.PreProcessingView.LoadData(fullfile(pathName, fileName));
                    case 'TM'
                        self.TabGroup.SelectedTab = self.TabGroup.Children(4);
                        ecgData = self.TemplateMatchingView.LoadData(fullfile(pathName, fileName));
                    case 'DD'
                        self.TabGroup.SelectedTab = self.TabGroup.Children(5);
                        ecgData = self.DepolarizationDetectionView.LoadData(fullfile(pathName, fileName));
                end
            catch loadFileError
                errordlg(loadFileError.message, 'File error');
                disp(loadFileError);
            end

            self.EcgData = ecgData;
            self.SetFilename();
        end

        function SetFilename(self)
            if isempty(self.EcgData), return; end

            self.Name = ['Ventricular Analysis - ', self.EcgData.Filename];
        end

        function SetParallelProcessing(self, source, varargin)
            if self.ParallelProcessing
                self.ParallelProcessing = false;
                set(source, 'checked', 'off');

                if verLessThan('matlab', '7.0.1')
                    if matlabpool('size') > 0
                        matlabpool('close');
                    end
                else
                    delete(gcp('nocreate'));
                end
            else
                self.ParallelProcessing = true;
                set(source, 'checked', 'on');

                if verLessThan('matlab', '7.0.1')
                    if matlabpool('size') == 0
                        matlabpool('open');
                    end
                else
                    if isempty(gcp('nocreate'))
                        parpool;
                    end
                end
            end
        end

        function PanOn(self, varargin)
            set(self.ZoomToggleButton, 'state', 'off');
            pan on;
        end

        function ZoomOn(self, varargin)
            set(self.PanToggleButton, 'state', 'off');
            zoom on;
        end

        function HandleChannelSelectionEvent(self, varargin)
            disp('Channels selected');

            ecgData = self.ChannelSelectionView.GetECGDataToAnalyze();

            self.PaceBlankingView.SetECGData(ecgData);
        end

        function HandlePaceBlankingEvent(self, varargin)
            disp('Pace detection completed');

            blankedECGData = self.PaceBlankingView.GetBlankedECGData();

            self.PreProcessingView.SetECGData(blankedECGData);
        end

        function HandlePreProcessingEvent(self, varargin)
            disp('Pre-processing completed');

            ecgData = self.PreProcessingView.GetPreProcessedECGData();

            self.DepolarizationRepolarizationView.SetECGData(ecgData);
            self.DepolarizationDetectionView.SetECGData(ecgData);            
        end

        function HandleIntrinsicDeflectionEvent(self, varargin)
            disp('Depolarization determined');

            deflections = self.DepolarizationDetectionView.DetectedDeflections;

            self.RepolarizationDetectionView.SetECGData(self.DepolarizationDetectionView.EcgData);
            self.RepolarizationDetectionView.SetDepolarizations(deflections);
            
            self.DepolarizationRepolarizationView.SetECGData(self.DepolarizationDetectionView.EcgData);
            self.DepolarizationRepolarizationView.SetDepolarizations(deflections);
        end

        function HandleRepolarizationEvent(self, varargin)
            disp('Repolarization determined');

            repolarizations = self.RepolarizationDetectionView.DetectedRepolarizations;

            self.DepolarizationRepolarizationView.SetRepolarizations(repolarizations);
        end

        function Close(self, varargin)
            self.RemoveTabs();

            self.EcgData = [];

            delete(self.ControlHandle);
        end
    end

    methods (Static)
        function ecgData = LoadBDFData(filename)
            ecgData = InputOutputPkg.readBDF(filename);
        end

        function ecgData = LoadPACEMAPEcg(filename)
            ecgData = InputOutputPkg.readEcgFile(filename);
        end
    end
end
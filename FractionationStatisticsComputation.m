function [AFCL FI DF] = FractionationStatisticsComputation(fileAFCL, fileFI, fileDF)
percentileValues = [5 50 95];

numberOfFiles = numel(fileAFCL);
AFCL = struct(...
    'mean', NaN(numberOfFiles, 1),...
    'percentiles', NaN(numberOfFiles, numel(percentileValues)),...
    'values',{cell(numberOfFiles, 1)},...
    'means', {cell(numberOfFiles, 1)},...
    'medians', {cell(numberOfFiles, 1)});

FI = AFCL;
DF = AFCL;
for fileIndex = 1:numberOfFiles
% AFCL
AFCLData = fileAFCL{fileIndex};
allIntervals = vertcat(AFCLData{:});
AFCL.mean(fileIndex) = mean(allIntervals);
AFCL.percentiles(fileIndex, :) = prctile(allIntervals, percentileValues); % p5, p50 (median), p95
AFCL.values{fileIndex} = allIntervals;
AFCL.means{fileIndex} = cellfun(@mean, AFCLData);
AFCL.medians{fileIndex} = cellfun(@median, AFCLData);
% Fractionation Index
FIData = fileFI{fileIndex};
FI.mean(fileIndex) = mean(FIData);
FI.percentiles(fileIndex, :) = prctile(FIData,percentileValues); % p5, p50 (median), p95
FI.values{fileIndex}= FIData;
% Dominant frequency
DFData = fileDF{fileIndex};
DF.mean(fileIndex) = mean(DFData);
DF.percentiles(fileIndex, :) = prctile(DFData,percentileValues); % p5, p50 (median), p95
DF.values{fileIndex}= DFData;
end
end


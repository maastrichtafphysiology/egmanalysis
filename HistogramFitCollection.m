classdef HistogramFitCollection < handle
   properties
     CurrentFitFunction
   end
   
   properties (SetAccess = private)
      Beta 
   end
   
   properties (Constant)
      AvailableFitFunctions = {'Gaussian', 'Double Sigmoidal'}; 
   end
   
   methods
       function self = HistogramFitCollection()
           self.CurrentFitFunction = self.AvailableFitFunctions{1};
       end
       
       function PerformFit(self, xValues, yValues, xLim)
            initialBetaValues = self.InitialParameterValues(xLim, yValues);
            try
%                 warning off matlab:nlinfit;
%                 self.Beta = nlinfit(xValues,yValues,@self.CalculateFitValues,initialBetaValues);
                self.Beta = fminsearch(@(x) self.CalculateSE(x, xValues, yValues), initialBetaValues);
            catch error
                disp(error);
            end
       end
       
       function [type, beta] = GetFitTypeAndParameters(self)
          type = self.CurrentFitFunction;
          beta = self.Beta;
       end
       
       function yValues = YValues(self, xValues)
           yValues = self.CalculateFitValues(self.Beta, xValues);
       end
       
       function functionHandle = GetFunctionHandle(self)
           switch self.CurrentFitFunction
             case self.AvailableFitFunctions{1}
                functionHandle = @(x) Gaussian(self.Beta, x);
             case self.AvailableFitFunctions{2}
                 functionHandle = @(x) DoubleSigmoidal(self.Beta, x);
           end
       end
   end
   
   methods (Access = protected)
       function self = InitializeFit(self, type, beta)
           % Add check if beta and type correspond
           self.Beta = beta;
           self.CurrentFitFunction = type;
       end 
   end
   
   methods (Access = private)
       function beta = InitialParameterValues(self, xLim, yValues)
           mu = mean(xLim);
           sigma = (xLim(2) - xLim(1))/2;
           
           switch self.CurrentFitFunction
              case self.AvailableFitFunctions{1}
                beta(1) = max(yValues);
                beta(2) = mu;
                beta(3) = sigma;
              case self.AvailableFitFunctions{2}
                beta(1) = max(yValues)/2;
                beta(2) = mu;
                beta(3) = mu;
                beta(4) = sigma;
                beta(5) = sigma;
           end
       end
       
       function SE = CalculateSE(self, parameterValues, xValues, yValues)
           switch self.CurrentFitFunction
             case self.AvailableFitFunctions{1}
                y = self.Gaussian(parameterValues, xValues);
             case self.AvailableFitFunctions{2}
                y = self.DoubleSigmoidal(parameterValues, xValues);
           end
           
           SE = sum((yValues - y).^2);
       end
       
       function y = CalculateFitValues(self, beta, x)
           switch self.CurrentFitFunction
             case self.AvailableFitFunctions{1}
                y = self.Gaussian(beta, x);
             case self.AvailableFitFunctions{2}
                y = self.DoubleSigmoidal(beta, x);
           end
       end
   end
   
   methods (Static)
       function y = DoubleSigmoidal(beta,x)
            y = DoubleSigmoidal(beta, x);
       end  
       
       function y = Gaussian(beta,x)
            y = Gaussian(beta, x);
       end
       
       function histogramFitCollection = FitTypeWithParameters(type, beta)
           histogramFitCollection = HistogramFitCollection();
           histogramFitCollection.InitializeFit(type, beta);
       end
    end
end
classdef TemplateEditor < UserInterfacePkg.CustomFigure
    properties (Access = public)
        
    end

    properties (Access = private)
        CurrentTemplateIndex  
        CurrentRowIndex
        
        TemplateValues
        AddRowButton
        RemoveRowButton
        
        EditMode
        
        Templates
        TemplateAxes
        TemplateMenu
        
        AddButton
        RemoveButton
        AvailableTemplateList
        SelectedTemplateList
        
        TemplateNameEdit
        jTemplateNameEdit 
        
        NewButton
        TemporaryTemplate
        
        NewToolbarButton
        EditToolbarButton
        SaveToolbarButton
        RemoveToolbarButton
    
        jScrollBar
        TemplatesFile
		
		Toolbar
		Label1
		Label2
    end
    
    methods
        function self = TemplateEditor()
            self = self@UserInterfacePkg.CustomFigure([200 200 800 400]);
            self.CurrentRowIndex = [];
            self.EditMode = 'Finished';
            
            if ispc
                homePath = getenv('USERPROFILE'); 
            else
                homePath = getenv('HOME');
            end
            self.TemplatesFile = fullfile(homePath, 'templates.mat');
            if exist(self.TemplatesFile, 'file')
                load(self.TemplatesFile);
            else
                templates = struct('Name', 'Template 1', 'Selected', 1, 'x', [-1 -0.5 0.5 1], 'y', [0.8 1 -1 -0.8]);
                templates(2) = struct('Name', 'Template 2', 'Selected', 0, 'x', [-1 -0.5 0.5 1], 'y', [1 1 -1 -1]);
            end
            self.Templates = templates;
            self.TemporaryTemplate = struct('Name', {}, 'Selected', {}, 'x', {}, 'y', {});
            self.TemplateValues = [self.Templates(1).x; self.Templates(1).y]';
        end
        
        function Create(self)
            Create@UserInterfacePkg.CustomFigure(self);
            self.BuildGUI();
            self.SetTemplateListItems();
            set(self.ControlHandle, 'CloseRequestFcn', @self.Close);
            set(self.ControlHandle, 'Visible', 'off');
        end
        
        function HiddenCreate(self)
            HiddenCreate@UserInterfacePkg.CustomFigure(self);
            self.BuildGUI();
            self.SetTemplateListItems();
            set(self.ControlHandle, 'CloseRequestFcn', @self.Close);
            set(self.ControlHandle, 'Visible', 'off');
        end
        
        function delete(self)
			delete(self.AvailableTemplateList);
			delete(self.SelectedTemplateList);
			delete(self.TemplateValues);
			delete(self.RemoveRowButton);
			delete(self.AddRowButton);
			delete(self.TemplateNameEdit);
			delete(self.AddButton);
			delete(self.RemoveButton);
			delete(self.Label2);
			delete(self.Label1);
			delete(self.TemplateAxes);
			delete(self.RemoveToolbarButton);
			delete(self.EditToolbarButton);
			delete(self.SaveToolbarButton);
			delete(self.NewToolbarButton);
			delete(self.Toolbar);	
		
            for i = 1 : 1 : numel(self.CustomControlCollection)
				if isobject(self.CustomControlCollection{i})
					delete(self.CustomControlCollection{i});
				end
            end
            delete(self.ControlHandle);
        end
                
        function Show(self)
            Show@UserInterfacePkg.CustomFigure(self);
            self.ShowTemplate(struct('Name', '', 'Selected', [], 'x', [], 'y', []));
        end
        
        function templates = SelectedTemplates(self)
            templates = struct('Name', {}, 'Selected', {}, 'x', {}, 'y', {});
            selectedTemplates = get(self.SelectedTemplateList, 'string');
            for i = 1 : numel(selectedTemplates)
                globalIndex = strcmp(selectedTemplates{i}, {self.Templates.Name});
                if any(globalIndex)
                    templates(end + 1) = self.Templates(globalIndex); %#ok<AGROW>
                end
            end
        end
        
        function set.Templates(self, templates)
            self.Templates = templates;
        end
        
        function BuildGUI(self)
            % Toolbar
            self.Toolbar = uitoolbar(...
                'parent', self.ControlHandle);
            toolbarIconData = load('toolbarIcons.mat');
            self.NewToolbarButton = uipushtool(...
                'parent', self.Toolbar,...
                'CData', toolbarIconData.newDataCData,...
                'TooltipString', 'New Template',...
                'ClickedCallback', @self.NewTemplate);
            
            self.SaveToolbarButton = uipushtool(...
                'parent', self.Toolbar,...
                'CData', toolbarIconData.saveCData,...
                'TooltipString', 'Save Template',....
                'enable', 'off',...
                'ClickedCallback', @self.SaveTemplate);
            
            self.EditToolbarButton = uipushtool(...
                'parent', self.Toolbar,...
                'CData', toolbarIconData.editCData,...
                'TooltipString', 'Edit Template',...
                'ClickedCallback', @self.EditTemplate);
            
            self.RemoveToolbarButton = uipushtool(...
                'parent', self.Toolbar,...
                'CData', toolbarIconData.removeCData,...
                'TooltipString', 'Remove Template',...
                'ClickedCallback', @self.DeleteTemplate);
            
            self.TemplateAxes = axes(...
                'parent', self.ControlHandle,...
                'units', 'normalized',...
                'position', [0.06 0.5 .20 .40]);
           
            self.Label1 = uicontrol(...
                'style', 'text',...
                'parent', self.ControlHandle,...
                'string', 'Available templates',...
                'units', 'normalized',...
                'position', [.29 .93 .15 0.05]);
           
            self.Label2 = uicontrol(...
                'style', 'text',...
                'parent', self.ControlHandle,...
                'string', 'Selected templates',...
                'units', 'normalized',...
                'position', [.67 .93 .15 0.05]);
            
            self.RemoveButton = uicontrol(...
                'style', 'pushbutton',...
                'parent', self.ControlHandle,...
                'string', '<',...
                'units', 'normalized',...
                'position', [.615 .45 .05 0.05],...
                'callback', @self.RemoveTemplate);
            
            self.AddButton = uicontrol(...
                'style', 'pushbutton',...
                'parent', self.ControlHandle,...
                'string', '>',...
                'units', 'normalized',...
                'position', [.615 .55 .05 0.05],...
                'callback', @self.AddTemplate);
            
            self.TemplateNameEdit =...
                uicontrol('style', 'edit',...
                'parent', self.ControlHandle,...
                'units', 'normalized',...
                'position', [.06 .39 .20 .05],...
                'BackgroundColor', 'white',...
                'string', '',...
                'enable', 'inactive'); % ,...
            
            self.jTemplateNameEdit = UtilityPkg.findjobj(self.TemplateNameEdit);
            self.jTemplateNameEdit.CaretUpdateCallback = @self.TemplateNameUpdated;                
                        
            self.AddRowButton = uicontrol(...
                'style', 'pushbutton',...
                'parent', self.ControlHandle,...
                'string', '+',...
                'units', 'normalized',...
                'position', [.23 .05 .03 0.05],...
                'enable', 'off',...
                'callback', @self.AddRow);
            
            self.RemoveRowButton = uicontrol(...
                'style', 'pushbutton',...
                'parent', self.ControlHandle,...
                'string', '-',...
                'units', 'normalized',...
                'position', [.20 .05 .03 0.05],...
                'enable', 'off',...
                'callback', @self.RemoveRow);
            
            self.TemplateValues = uitable(...
                'parent', self.ControlHandle,...
                'data', self.TemplateValues,...
                'columnName', {'x', 'y'},... 
                'columnEditable', [true true],...
                'units', 'normalized',...
                'position', [0.06 0.10 .20 .28],...
                'rowname', [],...
                'celleditcallback', @self.TemplateValuesUpdated,...
                'cellSelectionCallback', @self.TemplateValueSelected,...
                'enable', 'inactive');
            
            self.SelectedTemplateList = uicontrol(...
                'style', 'listbox',...
                'parent', self.ControlHandle,...
                'units', 'normalized',...
                'position',[.68 0.1 .3 .8],...
                'backgroundcolor', 'white',...
                'Max', 1,'Min', 0, ...
                'callback', @self.ShowCurrentTemplate);
            
            self.AvailableTemplateList = uicontrol(...
                'style', 'listbox',...
                'parent', self.ControlHandle,...
                'units', 'normalized',...
                'position',[.3 0.1 .3 .8],...
                'backgroundcolor', 'white',...
                'Max', 1,'Min', 0, ...
                'callback', @self.ShowCurrentTemplate);
        end
    end
    
    methods (Access = private)
        
        function AddRow(self, varargin)
            data = get(self.TemplateValues, 'data');
            currentSize = size(data);
            updatedData = zeros(currentSize(1) + 1, 2);

            if self.CurrentRowIndex > currentSize(1)
                self.CurrentRowIndex = currentSize(1);
            elseif isempty(self.CurrentRowIndex)
                self.CurrentRowIndex = currentSize(1);
            end
            
            if (self.CurrentRowIndex < currentSize(1) && self.CurrentRowIndex ~= 1)
                updatedData(1:self.CurrentRowIndex, :) = [self.TemporaryTemplate.x(1:self.CurrentRowIndex); 
                    self.TemporaryTemplate.y(1:self.CurrentRowIndex)]';
                updatedData(self.CurrentRowIndex+1, :) = [0 0];
                updatedData(self.CurrentRowIndex+2:end, :) = [self.TemporaryTemplate.x(self.CurrentRowIndex+1:end);
                    self.TemporaryTemplate.y(self.CurrentRowIndex+1:end)]';
            elseif (self.CurrentRowIndex == 1)
                updatedData(1, :) = [0 0];
                updatedData(2:end, :) = [self.TemporaryTemplate.x(1:end); 
                self.TemporaryTemplate.y(1:end)]';
            elseif (self.CurrentRowIndex == currentSize(1))
                updatedData(1:end-1, :) = [self.TemporaryTemplate.x(1:end); 
                self.TemporaryTemplate.y(1:end)]';
                updatedData(end, :) = [0 0];
            end
                
            self.TemporaryTemplate.x = updatedData(:,1)';
            self.TemporaryTemplate.y = updatedData(:,2)';
            
            self.CurrentRowIndex = self.CurrentRowIndex + 1;
            
            self.ShowTemplate(self.TemporaryTemplate);
        end
        
        function RemoveRow(self, varargin)
            data = get(self.TemplateValues, 'data');
            currentSize = size(data);
            updatedData = zeros(currentSize(1) - 1, currentSize(2));
            
            if self.CurrentRowIndex > currentSize(1)
                self.CurrentRowIndex = currentSize(1);
            elseif (currentSize(1) == 0)
                return;
            elseif isempty(self.CurrentRowIndex)
                self.CurrentRowIndex = currentSize(1);
            end
            
            if (self.CurrentRowIndex < currentSize(1) && self.CurrentRowIndex ~= 1)
                updatedData(1:self.CurrentRowIndex-1, :) = [self.TemporaryTemplate.x(1:self.CurrentRowIndex-1);
                    self.TemporaryTemplate.y(1:self.CurrentRowIndex-1)]';
                updatedData(self.CurrentRowIndex:end, :) = [self.TemporaryTemplate.x(self.CurrentRowIndex+1:end);
                    self.TemporaryTemplate.y(self.CurrentRowIndex+1:end)]';
            elseif (self.CurrentRowIndex == 1)
                updatedData(:, :) = [self.TemporaryTemplate.x(2:end);
                    self.TemporaryTemplate.y(2:end)]';
            elseif (self.CurrentRowIndex == currentSize(1))
                updatedData(:, :) = [self.TemporaryTemplate.x(1:end - 1);
                    self.TemporaryTemplate.y(1:end - 1)]';
            end
            
            self.TemporaryTemplate.x = updatedData(:,1)';
            self.TemporaryTemplate.y = updatedData(:,2)';
            
            self.ShowTemplate(self.TemporaryTemplate);
        end
        
        function EnableSelectionComponents(self)
            set(self.SelectedTemplateList, 'enable', 'on');
            set(self.AvailableTemplateList, 'enable', 'on');
            set(self.RemoveToolbarButton, 'enable', 'on')
            set(self.EditToolbarButton, 'enable', 'on')
            set(self.NewToolbarButton, 'enable', 'on')
            set(self.AddButton, 'enable', 'on');
            set(self.RemoveButton, 'enable', 'on');    
            set(self.TemplateNameEdit, 'enable', 'inactive');
            set(self.TemplateValues, 'enable', 'inactive');
            set(self.RemoveRowButton, 'enable', 'off');
            set(self.AddRowButton, 'enable', 'off');
            set(self.EditToolbarButton, 'enable', 'on');
            set(self.SaveToolbarButton, 'enable', 'off');
        end
        
        function DisableSelectionComponents(self)
            set(self.SelectedTemplateList, 'enable', 'off');
            set(self.AvailableTemplateList, 'enable', 'off');
            set(self.RemoveToolbarButton, 'enable', 'off')
            set(self.AddButton, 'enable', 'off');
            set(self.RemoveButton, 'enable', 'off');
            set(self.TemplateNameEdit, 'enable', 'on');
            set(self.TemplateValues, 'enable', 'on');
            set(self.RemoveRowButton, 'enable', 'on');
            set(self.AddRowButton, 'enable', 'on');
            set(self.EditToolbarButton, 'enable', 'off');
            set(self.SaveToolbarButton, 'enable', 'on');
        end
        
        function NewTemplate(self, varargin)
            self.EditMode = 'New';
            self.TemporaryTemplate = struct('Name', 'New Template', 'Selected', 0, 'x', 0, 'y', 0);
            set(self.TemplateNameEdit, 'string', self.TemporaryTemplate.Name);
            set(self.TemplateValues, 'data', [self.TemporaryTemplate.x; self.TemporaryTemplate.y]');
            self.DisableSelectionComponents();
            set(self.NewToolbarButton, 'enable', 'off');
            set(self.EditToolbarButton, 'enable', 'off');
            self.ShowTemplate(self.TemporaryTemplate);
        end
        
        function SaveTemplate(self, varargin)
            msgBoxTitle = 'Template Editor';
            %self.TemporaryTemplate.Name = get(self.TemplateNameEdit, 'string');
            switch (self.ValidateTemplate())
                case 'InvalidName'
                    msgbox('Invalid template name.', msgBoxTitle, 'help');
                    return
                case 'InvalidTemplate'
                    msgbox('Template should be centered around zero.', msgBoxTitle, 'help');
                    return
                case 'ValidTemplate'
                    switch (self.EditMode)
                        case 'New'
                            self.SaveNewTemplate();
                        case 'Existing'
                            self.UpdateExistingTemplate();
                    end
            end
            self.EnableSelectionComponents();
            self.SetTemplateListItems();
            self.EditMode = 'Finished';
        end
        
        function SaveNewTemplate(self)
            self.Templates(end+1) = self.TemporaryTemplate;
        end
        
        function UpdateExistingTemplate(self)
            self.Templates(self.CurrentTemplateIndex) = self.TemporaryTemplate;
        end
        
        function EditTemplate(self, varargin)
            if isempty(self.CurrentTemplateIndex)
                return
            end
            currentTemplate = self.Templates(self.CurrentTemplateIndex);
            self.TemporaryTemplate = currentTemplate;
            set(self.TemplateValues, 'data', [currentTemplate.x; currentTemplate.y]');
            
            self.EditMode = 'Existing';
            self.DisableSelectionComponents();
            set(self.EditToolbarButton, 'enable', 'off');
            set(self.NewToolbarButton, 'enable', 'off');
            self.ShowTemplate(self.TemporaryTemplate);
        end

        function TemplateNameUpdated(self, varargin)
            if ~isempty(self.TemporaryTemplate)
                self.TemporaryTemplate.Name = self.jTemplateNameEdit.text; %get(self.TemplateNameEdit, 'string');
            end
        end
        
        function TemplateValuesUpdated(self, src, event)
            updatedData = get(src, 'data');
            self.TemporaryTemplate.x = updatedData(:, 1)';
            self.TemporaryTemplate.y = updatedData(:, 2)';
            self.ShowTemplate(self.TemporaryTemplate);
        end
        
        function TemplateValueSelected(self, src, event)
            if numel(event.Indices)
                self.CurrentRowIndex = event.Indices(1);
            end
        end
        
        function result = ValidateTemplate(self)
            if (any(strcmp(self.TemporaryTemplate.Name, {self.Templates.Name})) && strcmp(self.EditMode,'New'))
                result = 'InvalidName';
            elseif ~self.IsTemplateValid()
                result = 'InvalidTemplate';
            else
                result = 'ValidTemplate';
            end
        end
        
        function result = IsTemplateValid(self)
           result = true;
        end
            
        function AddTemplate(self, varargin)
           templateIndex = get(self.AvailableTemplateList, 'value');
           if (templateIndex == 0 || any(strcmp(get(self.AvailableTemplateList, 'string'), '')))
                return;
           else
               set(self.AvailableTemplateList, 'value', max([1 templateIndex - 1]));
           end
           availableTemplates = get(self.AvailableTemplateList, 'string');
           currentTemplate = availableTemplates{templateIndex};
           for i = 1 : numel(self.Templates)
               if strcmp(self.Templates(i).Name, currentTemplate)
                   self.Templates(i).Selected = true;
               end
           end
           self.SetTemplateListItems();
        end
        
        function RemoveTemplate(self, varargin)
            templateIndex = get(self.SelectedTemplateList, 'value');
            if (templateIndex == 0 || any(strcmp(get(self.SelectedTemplateList, 'string'), '')))
                return;
            else
                set(self.SelectedTemplateList, 'value', max([1 templateIndex - 1]));
            end
            selectedTemplates = get(self.SelectedTemplateList, 'string');
            currentTemplate = selectedTemplates{templateIndex};
            for i = 1 : numel(self.Templates)
                if strcmp(self.Templates(i).Name, currentTemplate)
                    self.Templates(i).Selected = false;
                end
            end
            self.SetTemplateListItems();
        end
        
        function DeleteTemplate(self, source, varargin)
            currentTemplateIndex = self.CurrentTemplateIndex;
            if (currentTemplateIndex < numel(self.Templates) && currentTemplateIndex ~= 1)
                self.Templates = [self.Templates(1:currentTemplateIndex - 1) 
                    self.Templates(currentTemplateIndex + 1, end)];
            elseif (currentTemplateIndex == 1)
                self.Templates = self.Templates(2:end);
            elseif (currentTemplateIndex == numel(self.Templates))
                self.Templates = self.Templates(1:end-1);
            end
            self.SetTemplateListItems();
            self.ShowTemplate(struct('Name', '', 'Selected', [], 'x', [], 'y', []));
            self.CurrentTemplateIndex = [];
        end
        
        function ShowCurrentTemplate(self, source, varargin)
           templateIndex = get(source, 'value');
           if isempty(templateIndex)
               return
           end
           
           templateInCurrentList = get(source, 'string');
           currentTemplate = templateInCurrentList{templateIndex};
           for i = 1 : numel(self.Templates)
               if strcmp(self.Templates(i).Name, currentTemplate)
                   self.CurrentTemplateIndex = i;
               end
           end
           self.ShowTemplateAtIndex(self.CurrentTemplateIndex);
        end
        
        function ShowTemplateAtIndex(self, index)
            self.ShowTemplate(self.Templates(index));
        end
        
        function ShowTemplate(self, template)
           cla(self.TemplateAxes);
           plot(self.TemplateAxes, template.x, template.y, '.-');
           self.SetTemplateAxes();

           set(self.TemplateNameEdit, 'string', template.Name);
           
           set(self.TemplateValues, 'data', [template.x; template.y]');
        end
        
        function SetTemplateListItems(self) 
            availableTemplates = [];
            selectedTemplates = [];
            for i = 1 : numel(self.Templates)
                currentTemplate = self.Templates(i);
                if currentTemplate.Selected
                    selectedTemplates{end + 1} = currentTemplate.Name; %#ok<AGROW>
                else
                    availableTemplates{end + 1} = currentTemplate.Name; %#ok<AGROW>
                end
            end
            set(self.AvailableTemplateList, 'string', availableTemplates);
            set(self.AvailableTemplateList, 'value', ...
                min([max([1 get(self.AvailableTemplateList, 'value')]) numel(availableTemplates)]));
            
            set(self.SelectedTemplateList, 'string', selectedTemplates);
            set(self.SelectedTemplateList, 'value', ...
                min([max([1 get(self.SelectedTemplateList, 'value')]) numel(selectedTemplates)]));
        end
        
        function SetTemplateAxes(self)
            if ~isempty(self.TemporaryTemplate)
                xlim = max([abs(cell2mat({self.Templates.x})) abs(self.TemporaryTemplate.x)]);
                ylim = max([abs(cell2mat({self.Templates.y})) abs(self.TemporaryTemplate.y)]);
            else
                xlim = max(abs(cell2mat({self.Templates.x})));
                ylim = max(abs(cell2mat({self.Templates.y})));
            end
            if (isempty(xlim) || isempty(ylim))
                xlim = 1;
                ylim = 1;
            elseif (0 == xlim)
                xlim = 1;
            elseif (0 == ylim)
                ylim = 1;     
            end
            set(self.TemplateAxes, 'XLim', [-1.5 * xlim 1.5 * xlim]);
            set(self.TemplateAxes, 'YLim', [-1.5 * ylim 1.5 * ylim]);
            box(self.TemplateAxes, 'on');
            grid(self.TemplateAxes, 'on');
        end
        
        function Close(self, varargin)
            templates = self.Templates;
            self.EditMode = 'Finished';
            save(self.TemplatesFile, 'templates');
            self.Hide();
        end
    end
end
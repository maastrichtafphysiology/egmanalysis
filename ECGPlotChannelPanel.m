classdef ECGPlotChannelPanel < UserInterfacePkg.CustomPanel
    properties
        EcgData
        SelectedChannels
        SelectedLeadChannels
        
        FilterControlPanel
    end
    
    properties (Access = protected)
        ControlPanel
        ChannelPanel
        VisibleChannelPanel
        ChannelListbox
        LeadPanel
        LeadListbox
        
        AxesPanel
        MainAxes
        ECGLines
        ScrollSlider
        ZoomSlider
        
        % Axes properties
        AxesWidthInCm
        AxesWidthInPixels
        AxesSpeedInMMPerSecond
        SliderSpeedInMMPerSecond
        NumberOfSamples
        NumberOfDataPoints
        ZoomFactor
        TimeStep
        
        % Signal visualization
        VisualizedSignals
        
        % Filter settings
        ApplyBandpass
        BandpassFrequencies
        ApplyNotch
        NotchFrequency
        NotchWidth
        
        FilteredChannelData
        FilteredLeadData
    end
    
    properties (Constant)
        NUMBER_OF_DISPLAYED_CHANNELS = 3;
    end
    
    methods
        function self = ECGPlotChannelPanel(position)
            self = self@UserInterfacePkg.CustomPanel(position);
            self.SelectedChannels = [];
            self.SelectedLeadChannels = [];
            
            self.AxesWidthInCm = 100;
            self.AxesWidthInPixels = 100;
            self.AxesSpeedInMMPerSecond = 100;
            self.SliderSpeedInMMPerSecond = self.AxesSpeedInMMPerSecond;
            
            self.ApplyBandpass = false;
            self.BandpassFrequencies = [0.05, 150];
            self.ApplyNotch = false;
            self.NotchFrequency = 50;
            self.NotchWidth = 2;
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            
            self.CreateControlPanel();
            self.CreateECGAxes();
        end
        
        function SetECGData(self, ecgData)
            self.EcgData = ecgData;
            
            if isempty(self.SelectedChannels)
                if self.EcgData.GetNumberOfChannels < ECGChannelPanel.NUMBER_OF_DISPLAYED_CHANNELS
                    self.SelectedChannels = 1:self.EcgData.GetNumberOfChannels;
                else
                    self.SelectedChannels = 1:ECGChannelPanel.NUMBER_OF_DISPLAYED_CHANNELS;
                end
            elseif numel(self.SelectedChannels) > self.EcgData.GetNumberOfChannels
                self.SelectedChannels = 1:self.EcgData.GetNumberOfChannels;
            end
            
            self.SelectedLeadChannels = [];
            
            self.SetChannelList();
            self.InitializeControls();
            self.UpdateFilteredData(false);
            self.ShowECGData();
        end
        
        function delete(self)
            self.EcgData = [];
        end
    end
    
    methods (Access = protected)
        function CreateECGAxes(self)
            self.AxesPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.2 0 .8 1]);
            
            self.MainAxes = axes(...
                'Parent', self.AxesPanel,...
                'TickLabelInterpreter','none',...
                'ActivePositionProperty','Position');
            
            self.ScrollSlider = uicontrol(...
                'Parent', self.AxesPanel,...
                'Style','slider',...
                'Min', 0,...
                'Value', 0,...
                'Max', 1,...
                'SliderStep',[1e-4,.07],...
                'TooltipString','Click the slider trough or ouse mouse wheel to page forward one screen width.',...
                'Interruptible','on',...
                'Callback', {@self.ReDraw, true});
            addlistener(handle(self.ScrollSlider), 'ContinuousValueChange', @self.ReDraw);
            %             addlistener(handle(self.ScrollSlider), 'Value', 'PostSet', @self.ReDraw);
            
            self.ZoomSlider = uicontrol(...
                'Parent',self.AxesPanel,...
                'Style','slider',...
                'Min',0,...
                'Value',0.5,...
                'Max',1,...
                'SliderStep',[1e-4,.07],...
                'TooltipString','Zoom',...
                'Interruptible','on',...
                'Callback',{@self.ReDraw, true});
            addlistener(handle(self.ZoomSlider), 'ContinuousValueChange', @ZoomCallback);
            
            function ZoomCallback(varargin)
                self.ReDraw(varargin{:});
                self.SliderSpeedInMMPerSecond = (self.AxesWidthInCm * 10) /...
                    (self.NumberOfSamples * self.TimeStep);
            end
        end
        
        function CreateControlPanel(self)
            self.CreateChannelSelection();
            self.CreateFilterControlPanel();
        end
        
        function CreateChannelSelection(self)
            self.ControlPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0 0 .2 1]);
            
            self.ChannelPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [0 .25 1 .74],...
                'borderType', 'none');
            
            self.VisibleChannelPanel = uipanel('parent', self.ChannelPanel,...
                'units', 'normalized', ...
                'position', [0 .2 .5 .8],...
                'title', 'Visible channels',...
                'titlePosition', 'centertop',...
                'borderType', 'none');
            self.ChannelListbox = uicontrol(...
                'parent', self.VisibleChannelPanel,...
                'units', 'normalized',...
                'position', [0 0 1 1],...
                'style', 'listbox',...
                'backgroundColor', [1 1 1],...
                'min', 0, 'max', 10,...
                'callback', @self.SetSelectedChannels,...
                'busyAction', 'cancel');
            
            self.LeadPanel = uipanel('parent', self.ChannelPanel,...
                'units', 'normalized', ...
                'position', [0 0 .5 .19],...
                'title', 'Reference',...
                'titlePosition', 'centertop',...
                'borderType', 'none');
            self.LeadListbox = uicontrol(...
                'parent', self.LeadPanel,...
                'units', 'normalized',...
                'position', [0 0 1 1],...
                'style', 'listbox',...
                'backgroundColor', [1 1 1],...
                'min', 0, 'max', 10,...
                'callback', @self.SetLeadChannels,...
                'busyAction', 'cancel');
        end
        
        function CreateFilterControlPanel(self)
            if ishandle(self.FilterControlPanel)
                delete(self.FilterControlPanel)
            end
            
            self.FilterControlPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [0 .05 1 .2],...
                'title', 'Visualization filters',...
                'titlePosition', 'centertop');
            
            uicontrol('parent', self.FilterControlPanel,...
                'units', 'normalized',...
                'position', [.025 .9 .95 .1],...
                'style', 'checkbox',...
                'string', 'Bandpass filter',...
                'value', false,...
                'callback', @self.EnableBandpassFilter);
            bandpassPanel = uipanel('parent', self.FilterControlPanel,...
                'units', 'normalized', ...
                'position', [0 .5 1 .4],...
                'borderType', 'none');
            uicontrol('parent', bandpassPanel,...
                'units', 'normalized',...
                'position', [.1 .1 .4 .8],...
                'style', 'edit',...
                'string', num2str(self.BandpassFrequencies(1)),...
                'tooltipString', 'High pass frequency (Hz)',...
                'callback', @self.SetBandpassStart);
            uicontrol('parent', bandpassPanel,...
                'units', 'normalized',...
                'position', [.5 .1 .4 .8],...
                'style', 'edit',...
                'string', num2str(self.BandpassFrequencies(2)),...
                'tooltipString', 'Low pass frequency (Hz)',...
                'callback', @self.SetBandpassStop);
        
            uicontrol('parent', self.FilterControlPanel,...
                'units', 'normalized',...
                'position', [.025, .4, .95 .1],...
                'style', 'checkbox',...
                'string', 'Notch filter',...
                'value', self.ApplyNotch,...
                'callback', @self.EnableNotchFilter);
            notchPanel = uipanel('parent', self.FilterControlPanel,...
                'units', 'normalized', ...
                'position', [0, 0, 1 .4],...
                'borderType', 'none');
            uicontrol('parent', notchPanel,...
                'units', 'normalized',...
                'position', [.1 .1 .4 .8],...
                'style', 'edit',...
                'tooltipString', 'Notch frequency (Hz)',...
                'string', num2str(self.NotchFrequency),...
                'callback', @self.SetNotchFrequency);
            uicontrol('parent', notchPanel,...
                'units', 'normalized',...
                'position', [.5 .1 .4 .8],...
                'style', 'edit',...
                'tooltipString', 'Notch width (Hz)',...
                'string', num2str(self.NotchWidth),...
                'callback', @self.SetNotchWidth);
        end
        
        function InitializeControls(self, varargin)
            space = 0.01;
            sliderHeight = 0.05;
            
            time = self.EcgData.GetTimeRange();
            self.TimeStep = median(diff(time),'omitnan');
            
            self.NumberOfDataPoints = self.EcgData.GetNumberOfSamples;
            self.ZoomFactor = -log(self.NumberOfDataPoints / 7);
            
            yPos = 0;
            
            sliderBorderPanel = uipanel('units', 'normalized', 'Position', [0,0,1,1],...
                'parent', self.AxesPanel);
            % Zoom slider
            pos = [space, yPos, 1 - 2 * space, sliderHeight];
            pos = [pos(1), pos(2), max(0, pos(3)), max(0, pos(4))];
            set(self.ZoomSlider, 'units', 'normalized', 'Position', pos)
            zoomPos = pos;
            
            yPos = pos(2) + pos(4) + space;
            
            % Scroll slider
            pos = [space, yPos, 1 - 2 * space, sliderHeight];
            pos = [pos(1), pos(2), max(0,pos(3)), max(0, pos(4))];
            set(self.ScrollSlider, 'units', 'normalized', 'Position', pos);
            scrollPos = pos;
            
            extent = pos(4) * 0.10;
            set(sliderBorderPanel, 'position',...
                [zoomPos(1) - extent, zoomPos(2),...
                zoomPos(3) + extent * 2,...
                zoomPos(4) + scrollPos(4) + space + extent * 2]);
            set(self.ZoomSlider, 'parent', sliderBorderPanel,...
                'position', [0,0,1,.5]);
            set(self.ScrollSlider, 'parent', sliderBorderPanel,...
                'position', [0,.5,1,.5]);
            
            yPos = pos(2) + pos(4) + 3 * space;
            
            % Axis
            insets = get(self.MainAxes,'TightInset');
            pos = [space, yPos, max(0, 1 - 3 * space), max(0, 1 - yPos)];
            pos = [pos(1) + insets(1), pos(2) + insets(2),...
                pos(3) - insets(1) - insets(3), pos(4) - insets(2) - insets(4)];
            pos = [pos(1), pos(2), max(0, pos(3)), max(0, pos(4))];
            set(self.MainAxes, 'units', 'normalized', 'Position',pos)
            
            % Update AxesWidthInCm and AxesWidthInPixels
            set(self.MainAxes, 'units','centimeters');
            self.AxesWidthInCm = get(self.MainAxes, 'Position');
            self.AxesWidthInCm = self.AxesWidthInCm(3); 
            self.AxesWidthInCm = max(self.AxesWidthInCm, 0);
            set(self.MainAxes, 'Units', 'pixels');
            self.AxesWidthInPixels = get(self.MainAxes,'Position');
            self.AxesWidthInPixels = round(self.AxesWidthInPixels(3));
            self.AxesWidthInPixels = max(self.AxesWidthInPixels, 0);
            set(self.MainAxes, 'Units', 'normalized');
            
            % Change zooming such that mmPerSec stays the same
            self.NumberOfSamples = self.AxesWidthInCm * 10 / (self.SliderSpeedInMMPerSecond * self.TimeStep);
            zoomValue = log(self.NumberOfSamples/ self.NumberOfDataPoints)/self.ZoomFactor;
            zoomValue = max(zoomValue, 0);
            zoomValue = min(zoomValue, 1);
            set(self.ZoomSlider, 'Value', zoomValue);
        end
        
        function ShowECGData(self)
            lineData = [self.EcgData.Data(:, self.SelectedChannels),...
                self.EcgData.ReferenceData(:, self.SelectedLeadChannels)];
            lineLabels = [self.EcgData.ElectrodeLabels(self.SelectedChannels);...
                self.EcgData.ReferenceLabels(self.SelectedLeadChannels)];
            
            self.ECGLines = plot(self.MainAxes,...
                1, 1:numel(self.SelectedChannels),...
                '-');
            
            self.MainAxes.XAxis.Exponent = 0;
            self.MainAxes.XAxis.ExponentMode = 'manual';
            self.MainAxes.YAxis.Exponent = 0;
            self.MainAxes.YAxis.ExponentMode = 'manual';
            
            self.MainAxes.XLabel.String = 'Time (seconds)';
            self.MainAxes.TickLength = [0.001,0.001];
%             self.MainAxes.YLabel.String = 'Channel';
            
            [signalPosition, signalAddition] = ECGPlotChannelPanel.AutoStackNoOverlap(lineData);
            
            self.MainAxes.YTick = flip(signalPosition);
            self.MainAxes.YTickLabel = flip(lineLabels(:));
            self.MainAxes.TickLabelInterpreter = 'none';
            
            self.VisualizedSignals = bsxfun(@plus, lineData, signalAddition);
            signalRange = [min(self.VisualizedSignals(:)), max(self.VisualizedSignals(:))];
            dlt = diff(signalRange) / 50;
            signalRange(1) = signalRange(1) - dlt;
            signalRange(2) = signalRange(2) + dlt;
            self.MainAxes.YLimMode = 'manual';
            self.MainAxes.YLim = signalRange;
            
            self.ReDraw();
        end
        
        function ReDraw(self, varargin)
            lineData = [self.FilteredChannelData(:, self.SelectedChannels),...
                self.FilteredLeadData(:, self.SelectedLeadChannels)];
            lineLabels = [self.EcgData.ElectrodeLabels(self.SelectedChannels);...
                self.EcgData.ReferenceLabels(self.SelectedLeadChannels)];
            
            self.PlotLines(lineData, lineLabels, self.MainAxes, self.ECGLines);
        end
        
        function PlotLines(self, lineData, lineLabels, lineAxes, lineHandles)
            scrollValue = get(self.ScrollSlider, 'Value');
            zoomValue = get(self.ZoomSlider, 'Value');
            
            % N * exp(d*x)
            self.NumberOfSamples = self.NumberOfDataPoints *...
                exp(self.ZoomFactor * zoomValue);
            self.NumberOfSamples = round(self.NumberOfSamples);
            self.NumberOfSamples = max(self.NumberOfSamples, 2);
            
            startIndex = (self.NumberOfDataPoints - self.NumberOfSamples) *...
                scrollValue + 1;
            endIndex = startIndex + self.NumberOfSamples;
            
            time = self.EcgData.GetTimeRange();
            timeBoundary = [time(1), time(end)];
            startTime = timeBoundary(1) + self.TimeStep * (startIndex - 1);
            [~, startIndex] = min(abs(startTime - time));
            endTime = timeBoundary(1)+ self.TimeStep * (endIndex - 1);
            [~, endIndex] = min(abs(endTime - time));
            
            startIndex = round(startIndex);
            endIndex = round(endIndex);
            startIndex = max(startIndex, 1);
            endIndex = min(endIndex, self.NumberOfDataPoints);
            
            % Maximum factor_max values per pixel,
            % so very long signals don't hang Matlab
            factor = round(self.NumberOfSamples / max(1, self.AxesWidthInPixels));
            factor_max = 1000; % increase this if you want to find the "needle in the haystack" (single outlier sample)
            if factor > factor_max
                spc = floor(factor / factor_max);
            else
                spc = 1;
            end
            plottedIndices = startIndex:spc:endIndex;
            
            XData = time(plottedIndices);
            YData = lineData(plottedIndices, :);
            
            % Don't show much more samples than pixels.
            % Make sure that minimum and maximum data is shown anyway
            % (except if factor is > factor_max, for responsiveness)
            maxSamplesPerPixel = 2;
            if size(YData, 1) / (self.AxesWidthInPixels * maxSamplesPerPixel) > 2
                factor = ceil(size(YData, 1)/(max(1, self.AxesWidthInPixels) * maxSamplesPerPixel));
                remove = mod(size(YData,1), factor);
                XData = XData(1:(end - remove));
                YData = YData(1:(end - remove),:);
                XData = reshape(XData, factor, []);
                XData = [min(XData, [], 1); max(XData, [], 1)];
                XData = XData(:)';
                YData = permute(YData,[3, 1, 2]);
                YData = reshape(YData, factor, [], size(YData, 3));
                YData = [min(YData, [], 1, 'includenan');max(YData, [], 1, 'includenan')]; % preserves 'NaN' separations
                YData = [min(YData, [], 1);max(YData, [], 1)];
                YData = reshape(YData, [], size(YData, 3));
            end
            
            % On the other hand, if there are much less samples than pixels,
            % show additional dots
            factor = size(YData, 1) / max(1, self.AxesWidthInPixels);
            if factor < 0.2
                set(lineHandles, 'Marker', 'o');
            else
                set(lineHandles, 'Marker','none');
            end
            
            [signalPosition, signalAddition] = ECGPlotChannelPanel.AutoStack(YData);
            lineAxes.YTick = flip(signalPosition);
            lineAxes.YTickLabel = flip(lineLabels);
            lineAxes.TickLabelInterpreter = 'none';
            
            YData = bsxfun(@plus, YData, signalAddition);
            
            set(lineHandles, 'XData', XData);
            for lineIndex = 1:size(YData, 2)
                set(lineHandles(lineIndex), 'YData', YData(:, lineIndex));
            end
            set(lineAxes, 'XLim', [startTime, endTime]);
            minY = min(YData(:));
            maxY = max(YData(:));
            delta = (maxY - minY) / 50;
            minY = minY - delta;
            maxY = maxY + delta;
            if nnz(signalPosition) > 1
                minY = min([minY; signalPosition(:)]);
                maxY = max([maxY; signalPosition(:)]);
            end
            
            set(lineAxes, 'YLim', [minY, maxY]);
            
            duration = diff(timeBoundary);
            if self.NumberOfDataPoints <= self.NumberOfSamples
                majorStep = Inf;
                minorStep = .1;
            else
                majorStep = max(1e-6, self.NumberOfSamples /...
                    (self.NumberOfDataPoints - self.NumberOfSamples));
                % 100 steps per screen width
                minorStep = max(1e-6, (endTime - startTime)/(100 * duration));
            end
            set(self.ScrollSlider, 'SliderStep', [minorStep, majorStep]);
        end
        
        function UpdateFilteredData(self, redrawPlot)
            self.FilteredChannelData = self.EcgData.Data;
            self.FilteredLeadData = self.EcgData.ReferenceData;
            if self.ApplyBandpass
                bandpassFilter = designfilt('bandpassiir', 'FilterOrder', 4,...
                    'HalfPowerFrequency1', self.BandpassFrequencies(1),...
                    'HalfPowerFrequency2', self.BandpassFrequencies(2),...
                    'SampleRate', self.EcgData.SamplingFrequency);
                self.FilteredChannelData = filtfilt(bandpassFilter, self.FilteredChannelData);
                self.FilteredLeadData = filtfilt(bandpassFilter, self.FilteredLeadData);
            end
            
            if self.ApplyNotch
                for notchIndex = 1:numel(self.NotchFrequency)
                    notchFrequency = self.NotchFrequency(notchIndex);
                    bandstopFilter = designfilt('bandstopiir', 'FilterOrder',2,...
                        'HalfPowerFrequency1', notchFrequency  - self.NotchWidth / 2,...
                        'HalfPowerFrequency2', notchFrequency + self.NotchWidth / 2,...
                        'SampleRate', self.EcgData.SamplingFrequency);
                    self.FilteredChannelData = filtfilt(bandstopFilter, self.FilteredChannelData);
                    self.FilteredLeadData = filtfilt(bandstopFilter, self.FilteredLeadData);
                end
            end
            
            if redrawPlot
                self.ReDraw();
            end
        end
        
        function SetChannelList(self)
            set(self.ChannelListbox, 'string', self.EcgData.ElectrodeLabels);
            set(self.ChannelListbox, 'value', find(self.SelectedChannels));
            
            set(self.LeadListbox, 'string', self.EcgData.ReferenceLabels);
            set(self.LeadListbox, 'value', find(self.SelectedLeadChannels));
        end
        
        function SetSelectedChannels(self, varargin)
            self.SelectedChannels = get(self.ChannelListbox, 'value');
            self.ShowECGData();
        end
        
        function SetLeadChannels(self, varargin)
            self.SelectedLeadChannels = get(self.LeadListbox, 'value');
            self.ShowECGData();
        end
        
        function EnableBandpassFilter(self, source, varargin)
            self.ApplyBandpass = get(source, 'value') > 0;
            self.UpdateFilteredData(true);
        end
        
         function EnableNotchFilter(self, source, varargin)
            enable = get(source, 'value') > 0;
            self.ApplyNotch = enable;
            self.UpdateFilteredData(true);
         end
        
        function SetBandpassStart(self, source, varargin)
            value = str2double(get(source, 'string'));
            if value >= 0 && value < self.BandpassFrequencies(2)
               self.BandpassFrequencies(1) = value; 
            end
            set(source, 'string', num2str(self.BandpassFrequencies(1)));
            self.UpdateFilteredData(true);
        end
        
        function SetBandpassStop(self, source, varargin)
            value = str2double(get(source, 'string'));
            if value > self.BandpassFrequencies(1)
               self.BandpassFrequencies(2) = value; 
            end
            set(source, 'string', num2str(self.BandpassFrequencies(2)));
            self.UpdateFilteredData(true);
        end
        
        function SetNotchFrequency(self, source, varargin)
            value = str2double(get(source, 'string'));
            if all(value > 0)
               self.NotchFrequency = value; 
            end
            set(source, 'string', num2str(self.NotchFrequency));
            self.UpdateFilteredData(true);
        end
        
        function SetNotchWidth(self, source, varargin)
            value = str2double(get(source, 'string'));
            self.NotchWidth = value; 
            set(source, 'string', num2str(self.NotchWidth));
            self.UpdateFilteredData(true);
        end
    end
    
    methods (Static)
        function [signalPosition,signalAddition] = AutoStackNoOverlap(signals)
            % Stacks Signals Horizontally with strictly no overlap.
            % Used for 'AutoStackSignals' with 'YLimMode' set to 'fixed'.
            
            signalMedian = median(signals, 1, 'omitnan');
            signals = bsxfun(@minus, signals, signalMedian);
            overlap = signals;
            overlap = min(overlap(:,1:end-1),[],1) - max(overlap(:,2:end),[],1);
            overlap(isnan(overlap)) = 0;
            signalSpacing = -overlap * 1.01;
            
            if isempty(overlap)
                signalPosition = signalMedian;
                signalAddition = 0;
                return;
            end
            
            % Increas very small spacings
            signalSpacing = max(signalSpacing, max(signalSpacing)./...
                1000. * ones(size(signalSpacing)));
            signalSpacing = max(eps, signalSpacing);
            signalPosition = -cumsum([0, signalSpacing]);
            signalAddition = signalPosition - signalMedian;
        end
        
        function [signalPosition, signalAddition] = AutoStack(signals)
            % Stacks Signals Horizontally with little overlap
            % Used after each scroll/zoom action for 'AutoStackSignals'
            % with 'YLimMode' set to 'dynamic'.
            
            signalMedian = median(signals, 1, 'omitnan');
            signals = bsxfun(@minus, signals, signalMedian);
            overlap = signals;
            overlap = diff(overlap, 1, 2); % positive values are overlap
            overlap(isnan(overlap)) = 0;
            
            if isempty(overlap)
                signalPosition = signalMedian;
                signalAddition = 0;
                return;
            end
            
            overlapS = sort(overlap, 1, 'descend');
            index = max(1, round(size(overlapS, 1) * .007));
            signalSpacing = overlapS(index, :) * 1.1;
            stdd = std(signals, 1, 1);
            stdd = min(stdd(1:end-1),stdd(2:end));
            signalSpacing = max(signalSpacing, median(signalSpacing,'omitnan') * .5 );
            signalSpacing = max(signalSpacing, stdd * 4);
            % Increas very small spacings
            signalSpacing = max(signalSpacing, max(signalSpacing) ./...
                1000 .* ones(size(signalSpacing)));
            signalSpacing = max(eps,signalSpacing);
            signalPosition = -cumsum([0, signalSpacing]);
            signalAddition = signalPosition - signalMedian;
        end
    end
end
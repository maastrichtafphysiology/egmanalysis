classdef BipolarLaplacianPanel < UserInterfacePkg.CustomPanel
    properties
        EcgData
        WavemapData
        LaplacianEcgData
        
        SelectedChannelIndex
        AnimationPosition
        MinimumSpacing
    end
    
    properties (Access = private)
        WavemapControl
        ElectrodeMapControl
        ECGGridMapControl
        
        SignalPanel
        % Unipolar axes
        UnipolarAxes
        FarFieldDeflectionLine
        IntrinsicDeflectionLine
        SignalLine
        WavemapSignalLine
        ReferenceLine
        AnimationPositionLine
        SelectedChannelText
        
        % Laplacian axes
        LaplacianAxes
        LaplacianLine
        LaplacianIntrinsicDeflectionLine
        LaplacianFarFieldDeflectionLine
        
        % Bipolar axes
        BipolarAxesNS
        BipolarLineNS
        BipolarAxesWE
        BipolarLineWE
        BipolarAxesNWSE
        BipolarLineNWSE
        BipolarAxesSWNE
        BipolarLineSWNE
        
        AnimationSlider
        AnimationSliderListener
        AnimationPositionLabel
    end
    
    events
        LaplacianECGComputed
    end
    
    methods
        function self = BipolarLaplacianPanel(position)
            self = self@UserInterfacePkg.CustomPanel(position);
            
            self.SelectedChannelIndex = NaN;
        end
        
         function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            set(self.ControlHandle, 'visible', 'on');
            
            self.CreateControlPanel();
            
            self.CreateWavemapPanel();
            
            self.CreateSignalPanel();
         end
         
         function SetECGData(self, ecgData)
             self.EcgData = ecgData;
%              self.EcgData.Data = bsxfun(@rdivide, self.EcgData.Data, std(self.EcgData.Data));
             self.SelectedChannelIndex = 1;
             self.MinimumSpacing = self.ComputeMinimumSpacing();
             
             if isempty(self.WavemapData)
                  self.InitializeElectrodeMap();
             end
             self.InitializeECGGridMap();
             
             self.InitializeSignalAxes();
             self.InitializeLaplacianAxes();
             self.InitializeBipolarAxes();
             self.ShowSelectedChannel();
             
             self.AnimationPosition = 1;
             numberOfSamples = self.EcgData.GetNumberOfSamples();
             set(self.AnimationSlider, 'min', 1, 'max', numberOfSamples,...
                 'value', self.AnimationPosition,...
                 'sliderStep', [1 / (numberOfSamples - 1), 1 / (numberOfSamples - 1)]);
         end
         
         function SetWavemapData(self, wavemapData)
             self.WavemapData = wavemapData;
             self.WavemapData.ecgData.Data = bsxfun(@rdivide, self.WavemapData.ecgData.Data, std(self.WavemapData.ecgData.Data));
             self.InitializeWavemap();
         end
         
         function laplacianEcgData = GetLaplacianECG(self)
             laplacianEcgData = self.ComputeCompleteLaplacian();
         end
         
         function delete(self)
             self.EcgData = [];
             if ~isempty(self.WavemapData)
              self.WavemapData.ecgData = [];
              self.WavemapData.detectedWaves = [];
             end
             self.LaplacianEcgData = [];
         end
    end
    
    methods (Access = private)
        function CreateControlPanel(self)
            uicontrol('parent', self.ControlHandle,...
                'units', 'normalized',...
                'position', [.05 .05 .2 .05],...
                'style', 'pushbutton',...
                'string', 'Use Laplacian ECG for template matching',...
                'callback', @(src, event) notify(self, 'LaplacianECGComputed'));
        end
        
        function CreateWavemapPanel(self)
            mapPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0 .1 .3 .9]);
            
            self.WavemapControl = ExtendedUIPkg.GridWavemapControl([0 .525 1 .475]);
            self.WavemapControl.Name = 'Waves';
            self.WavemapControl.Create(mapPanel);
            addlistener(self.WavemapControl, 'MapClicked', @self.HandleMapClick);
            
            self.AnimationSlider = uicontrol(...
                'parent', mapPanel,...
                'units', 'normalized',...
                'position', [.1 .475 .7 .05],...
                'style', 'slider',...
                'callback', @self.AnimationSliderCallback);
            
            addlistener(handle(self.AnimationSlider), 'Value', 'PostSet', @self.ContinuousAnimationSliderCallback);
            
            self.AnimationPositionLabel = uicontrol(...
                'parent', mapPanel,...
                'units', 'normalized',...
                'position', [.7 .5 .1 .025],...
                'style', 'text');
            
            self.ElectrodeMapControl = ExtendedUIPkg.ElectrodeMapControl([0 .525 1 .475]);
            self.ElectrodeMapControl.Name = 'Electrode map';
            self.ElectrodeMapControl.Create(mapPanel);
            addlistener(self.ElectrodeMapControl, 'MapClicked', @self.HandleMapClick);
            
            self.ECGGridMapControl = ExtendedUIPkg.ECGGridMapControl([0 0 1 .475]);
            self.ECGGridMapControl.Name = 'Electrode signals';
            self.ECGGridMapControl.Create(mapPanel);
        end
        
        function CreateSignalPanel(self)
            self.SignalPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.3 0 .7 1]);
            
            self.CreateUnipolarAxes();
            self.CreateLaplacianAxes();
            self.CreateBipolarAxes();
            
            axesHandles = [self.UnipolarAxes, self.LaplacianAxes,...
                self.BipolarAxesNS, self.BipolarAxesWE,...
                self.BipolarAxesNWSE, self.BipolarAxesSWNE];
            
            set(axesHandles, {'looseInset'}, get(axesHandles, 'tightInset'));
            set(axesHandles, {'YAxisLocation'}, {'right'});
            
            linkaxes(axesHandles, 'x');
        end
        
        function CreateUnipolarAxes(self)
            self.UnipolarAxes = axes('parent', self.SignalPanel,...
                'units', 'normalized',...
                'outerPosition', [0 5/6 1 1/6]);
%             set(self.UnipolarAxes, 'looseInset', get(self.UnipolarAxes, 'tightInset'));
            
%             set(self.UnipolarAxes, 'YAxisLocation', 'right');
        end
        
        function CreateLaplacianAxes(self)
            self.LaplacianAxes = axes('parent', self.SignalPanel,...
                'units', 'normalized',...
                'outerPosition', [0 4/6 1 1/6]);
%             set(self.LaplacianAxes, 'looseInset', get(self.LaplacianAxes, 'tightInset'));
%             
%             set(self.LaplacianAxes, 'YAxisLocation', 'right');
            
            text('units', 'normalized',...
                'position', [0, 1],...
                'string', 'Laplacian',...
                'verticalAlignment', 'top',...
                'parent', self.LaplacianAxes,...
                'backgroundColor', [1 1 1],...
                'edgeColor', [0 0 0]);
        end
        
        function CreateBipolarAxes(self)
            self.BipolarAxesNS = axes('parent', self.SignalPanel,...
                'units', 'normalized',...
                'outerPosition', [0 3/6 1 1/6]);
            self.BipolarAxesWE = axes('parent', self.SignalPanel,...
                'units', 'normalized',...
                'outerPosition', [0 2/6 1 1/6]);
            self.BipolarAxesNWSE = axes('parent', self.SignalPanel,...
                'units', 'normalized',...
                'outerPosition', [0 1/6 1 1/6]);
            self.BipolarAxesSWNE = axes('parent', self.SignalPanel,...
                'units', 'normalized',...
                'outerPosition', [0 0/6 1 1/6]);
            
            text('units', 'normalized',...
                'position', [0, 1],...
                'string', 'N -> S',...
                'verticalAlignment', 'top',...
                'parent', self.BipolarAxesNS,...
                'backgroundColor', [1 1 1],...
                'edgeColor', [0 0 0]);
            set(self.BipolarAxesNS, 'xTick', []);
            text('units', 'normalized',...
                'position', [0, 1],...
                'string', 'W -> E',...
                'verticalAlignment', 'top',...
                'parent', self.BipolarAxesWE,...
                'backgroundColor', [1 1 1],...
                'edgeColor', [0 0 0]);
            set(self.BipolarAxesWE, 'xTick', []);
            text('units', 'normalized',...
                'position', [0, 1],...
                'string', 'NW -> SE',...
                'verticalAlignment', 'top',...
                'parent', self.BipolarAxesNWSE,...
                'backgroundColor', [1 1 1],...
                'edgeColor', [0 0 0]);
            set(self.BipolarAxesNWSE, 'xTick', []);
            text('units', 'normalized',...
                'position', [0, 1],...
                'string', 'SW -> NE',...
                'verticalAlignment', 'top',...
                'parent', self.BipolarAxesSWNE,...
                'backgroundColor', [1 1 1],...
                'edgeColor', [0 0 0]);
        end
        
        function InitializeWavemap(self)
            if isempty(self.EcgData), return; end
            self.WavemapControl.SamplingFrequency = self.EcgData.SamplingFrequency;
            self.WavemapControl.SetData(self.WavemapData.detectedWaves, self.WavemapData.ecgData.ElectrodePositions);
            self.WavemapControl.Show();
            self.ElectrodeMapControl.Hide();
        end
        
        function InitializeElectrodeMap(self)
            self.ElectrodeMapControl.SetData(self.EcgData.ElectrodeLabels, self.EcgData.ElectrodePositions);
            self.ElectrodeMapControl.Show();
        end
        
        function InitializeECGGridMap(self)
            self.ECGGridMapControl.SetData(self.EcgData);
            self.ECGGridMapControl.Show();
        end
        
        function InitializeSignalAxes(self)
            if isempty(self.EcgData), return; end
            
            time = self.EcgData.GetTimeRange();
            signal = self.EcgData.Data(:, self.SelectedChannelIndex);
            if isempty(self.WavemapData)
                wavemapSignal = NaN(size(signal));
            else
                wavemapSignal = self.WavemapData.ecgData.Data(:, self.SelectedChannelIndex);
            end
            
            if ishandle(self.FarFieldDeflectionLine)
                delete(self.FarFieldDeflectionLine);
            end
            self.FarFieldDeflectionLine = line(...
                'xData', [], 'yData', [],...
                'color', [0 0 1],...
                'lineWidth', 2,...
                'lineSmoothing', 'on',...
                'parent', self.UnipolarAxes);
            
            if ishandle(self.IntrinsicDeflectionLine)
                delete(self.IntrinsicDeflectionLine);
            end
            self.IntrinsicDeflectionLine = line(...
                'xData', [], 'yData', [],...
                'color', [1 0 0],...
                'lineWidth', 3,...
                'lineSmoothing', 'on',...
                'parent', self.UnipolarAxes);
            
            if ishandle(self.ReferenceLine)
                delete(self.ReferenceLine);
            end
            self.ReferenceLine = line(...
                'xData', [], 'yData', [],...
                'color', [.8 .8 .8],...
                'lineWidth', 1,...
                'lineSmoothing', 'on',...
                'parent', self.UnipolarAxes);
            
            if ishandle(self.SignalLine)
                delete(self.SignalLine);
            end
            self.SignalLine = line('parent', self.UnipolarAxes,...
                'xData', time, 'yData', signal / 1000, 'color', [0 1 0], 'lineSmoothing', 'on');
            
            if ishandle(self.WavemapSignalLine)
                delete(self.WavemapSignalLine);
            end
            self.WavemapSignalLine = line('parent', self.UnipolarAxes,...
                'xData', time, 'yData', wavemapSignal / 1000, 'color', [0 0 0], 'lineSmoothing', 'on');
            
            self.AnimationPositionLine = line(...
                'xData', [0 0], 'yData', [0 0],...
                'color', [1 0 0],...
                'lineWidth', 1,...
                'parent', self.UnipolarAxes);
            
            self.SelectedChannelText = text(...
                'units', 'normalized',...
                'position', [0, 1],...
                'string', '',...
                'verticalAlignment', 'top',...
                'parent', self.UnipolarAxes,...
                'backgroundColor', [1 1 1],...
                'edgeColor', [0 0 0]);
        end
        
        function InitializeLaplacianAxes(self)
            if isempty(self.EcgData), return; end

            if ishandle(self.LaplacianFarFieldDeflectionLine)
                delete(self.LaplacianFarFieldDeflectionLine);
            end
            self.LaplacianFarFieldDeflectionLine = line(...
                'xData', [], 'yData', [],...
                'color', [0 0 1],...
                'lineWidth', 2,...
                'lineSmoothing', 'on',...
                'parent', self.LaplacianAxes);
            
            if ishandle(self.LaplacianIntrinsicDeflectionLine)
                delete(self.LaplacianIntrinsicDeflectionLine);
            end
            self.LaplacianIntrinsicDeflectionLine = line(...
                'xData', [], 'yData', [],...
                'color', [1 0 0],...
                'lineWidth', 3,...
                'lineSmoothing', 'on',...
                'parent', self.LaplacianAxes);
            
            if ishandle(self.LaplacianLine)
                delete(self.LaplacianLine);
            end
            self.LaplacianLine = line(...
                'xData', [], 'yData', [],...
                'color', [0 0 0],...
                'lineWidth', 1,...
                'lineSmoothing', 'on',...
                'parent', self.LaplacianAxes);
        end
        
        function InitializeBipolarAxes(self)
            if isempty(self.EcgData), return; end
            
            if ishandle(self.BipolarLineNS)
                delete(self.BipolarLineNS);
            end
            self.BipolarLineNS = line(...
                'xData', [], 'yData', [],...
                'color', [0 0 0],...
                'lineWidth', 1,...
                'lineSmoothing', 'on',...
                'parent', self.BipolarAxesNS);
            
            if ishandle(self.BipolarLineWE)
                delete(self.BipolarLineWE);
            end
            self.BipolarLineWE = line(...
                'xData', [], 'yData', [],...
                'color', [0 0 0],...
                'lineWidth', 1,...
                'lineSmoothing', 'on',...
                'parent', self.BipolarAxesWE);
            
            if ishandle(self.BipolarLineNWSE)
                delete(self.BipolarLineNWSE);
            end
            self.BipolarLineNWSE = line(...
                'xData', [], 'yData', [],...
                'color', [0 0 0],...
                'lineWidth', 1,...
                'lineSmoothing', 'on',...
                'parent', self.BipolarAxesNWSE);
            
            if ishandle(self.BipolarLineSWNE)
                delete(self.BipolarLineSWNE);
            end
            self.BipolarLineSWNE = line(...
                'xData', [], 'yData', [],...
                'color', [0 0 0],...
                'lineWidth', 1,...
                'lineSmoothing', 'on',...
                'parent', self.BipolarAxesSWNE);
        end
        
        function SetUnipolarAxes(self)
            set(self.SelectedChannelText, 'string',...
                ['Channel: ', self.EcgData.ElectrodeLabels{self.SelectedChannelIndex}]);
            
           % signal
           signal = self.EcgData.Data(:, self.SelectedChannelIndex);
            
            time = self.EcgData.GetTimeRange();
            
           if isempty(self.WavemapData)
               set(self.WavemapSignalLine, 'yData', NaN(size(signal)));
           else
                waveMapSignal = self.WavemapData.ecgData.Data(:, self.SelectedChannelIndex);
                set(self.WavemapSignalLine, 'yData', waveMapSignal / 1000);
           end
            
            set(self.SignalLine, 'yData', signal / 1000);
            
            yLimits = [min(signal) max(signal)] / 1000;
            
            % reference signal
            if ~isempty(self.EcgData.ReferenceData)
                referenceData = self.EcgData.ReferenceData(:, 1);
                referenceData = (referenceData - min(referenceData)) / (max(referenceData) - min(referenceData));
                referenceData = referenceData * (yLimits(end) - yLimits(1)) + yLimits(1);
                set(self.ReferenceLine, 'xData', time, 'yData', referenceData);
                uistack(self.ReferenceLine, 'bottom');
            end
            
            if isempty(self.WavemapData), return; end
            deflectionData = self.WavemapData.deflectionData{self.SelectedChannelIndex};
            
            % intrinsic deflections
            [xData yData] = DeflectionECGChannelPanel.ComputeDeflectionLineData(...
                time, waveMapSignal,...
                deflectionData.templates,...
                deflectionData.templatePeakTemplates(deflectionData.PrimaryDeflections, :),...
                deflectionData.templatePeakRanges(deflectionData.PrimaryDeflections, :),...
                deflectionData.templatePeakCoefficients(deflectionData.PrimaryDeflections, :));
            
            set(self.IntrinsicDeflectionLine, 'xData', xData, 'yData', yData / 1000);
            
            % far-field deflections
            [xData yData] = DeflectionECGChannelPanel.ComputeDeflectionLineData(...
                time, waveMapSignal,...
                deflectionData.templates,...
                deflectionData.templatePeakTemplates(~deflectionData.PrimaryDeflections, :),...
                deflectionData.templatePeakRanges(~deflectionData.PrimaryDeflections, :),...
                deflectionData.templatePeakCoefficients(~deflectionData.PrimaryDeflections, :));
            
            set(self.FarFieldDeflectionLine, 'xData', xData, 'yData', yData / 1000);
            self.SetSignalWindow(self.AnimationPosition);
        end
        
        function SetLaplacianAxes(self)
            if isempty(self.EcgData), return; end
            
            time = self.EcgData.GetTimeRange();
            laplacianSignal = self.ComputeLaplacian();
            
            set(self.LaplacianLine, 'xData', time, 'yData', laplacianSignal / 1000);
            
            if isempty(self.WavemapData), return; end
            deflectionData = self.WavemapData.deflectionData{self.SelectedChannelIndex};
            
            yLimits = get(self.LaplacianAxes, 'yLim');
            % intrinsic deflections
            intrinsicDeflectionIndices = deflectionData.templatePeakIndices(deflectionData.PrimaryDeflections, :);
            xData = NaN(3 * numel(intrinsicDeflectionIndices), 1);
            xData(1:3:end) = time(intrinsicDeflectionIndices);
            xData(2:3:end) = time(intrinsicDeflectionIndices);
            yData = NaN(3 * numel(intrinsicDeflectionIndices), 1);
            yData(1:3:end) = yLimits(1);
            yData(2:3:end) = yLimits(end);
            
            set(self.LaplacianIntrinsicDeflectionLine, 'xData', xData, 'yData', yData);
            
            % far-field deflections
            farFieldDeflectionIndices = deflectionData.templatePeakIndices(~deflectionData.PrimaryDeflections, :);
            xData = NaN(3 * numel(farFieldDeflectionIndices), 1);
            xData(1:3:end) = time(farFieldDeflectionIndices);
            xData(2:3:end) = time(farFieldDeflectionIndices);
            yData = NaN(3 * numel(farFieldDeflectionIndices), 1);
            yData(1:3:end) = yLimits(1);
            yData(2:3:end) = yLimits(end);
            
            set(self.LaplacianFarFieldDeflectionLine, 'xData', xData, 'yData', yData);
        end
        
        function SetBipolarAxes(self)
            if isempty(self.EcgData), return; end
            
            time = self.EcgData.GetTimeRange();
            [NS, WE, NWSE, SWNE, electrodeIndices] = self.ComputeBipolarSignals();
            
            set(self.BipolarLineNS, 'xData', time, 'yData', NS / 1000); 
            set(self.BipolarLineWE, 'xData', time, 'yData', WE / 1000);
            set(self.BipolarLineNWSE, 'xData', time, 'yData', NWSE / 1000); 
            set(self.BipolarLineSWNE, 'xData', time, 'yData', SWNE / 1000);
            
            self.WavemapControl.HighlightElectrodes(electrodeIndices(~isnan(electrodeIndices)));
            self.ElectrodeMapControl.HighlightElectrodes(electrodeIndices(~isnan(electrodeIndices)));
            self.ECGGridMapControl.SetGridElectrodes(electrodeIndices);
        end
        
        function HandleMapClick(self, ~, eventData)
            self.SelectedChannelIndex = eventData.Position;
            self.ShowSelectedChannel();
        end
        
        function ShowSelectedChannel(self)
            self.SetUnipolarAxes();
            self.SetLaplacianAxes();
            self.SetBipolarAxes();
        end
        
        function AnimationSliderCallback(self, varargin)
            self.AnimationPosition = round(get(self.AnimationSlider, 'value'));
            self.SetStaticFrame();
        end
        
        function ContinuousAnimationSliderCallback(self, varargin)
            self.AnimationPosition = round(get(self.AnimationSlider, 'value'));
            self.SetAnimationInfo();
            
            if ~isempty(self.EcgData)
                self.ECGGridMapControl.SetFramePosition(self.AnimationPosition);
                self.SetSignalWindow(self.AnimationPosition);
            end
        end
        
        function SetStaticFrame(self)
            self.SetAnimationInfo();
            
            if ~isempty(self.WavemapData)
                self.WavemapControl.SetStaticFramePosition(self.AnimationPosition);
            end
            if ~isempty(self.EcgData)
                self.ECGGridMapControl.SetStaticFramePosition(self.AnimationPosition);
                self.SetSignalWindow(self.AnimationPosition);
            end
        end
        
        function SetAnimationInfo(self)
            timePosition = round(1000 * self.AnimationPosition / self.EcgData.SamplingFrequency);
            set(self.AnimationPositionLabel, 'string', [ num2str(timePosition) ' ms']);
        end
        
        function SetSignalWindow(self, position)
            timePosition = position / self.EcgData.SamplingFrequency;
            timeWindow = [timePosition - 0.5, timePosition + 0.5];
            xTickSpacing = 0.1;
            xTicks = (timeWindow(1) + (xTickSpacing - mod(timeWindow(1), xTickSpacing))):(xTickSpacing):(timeWindow(end) - mod(timeWindow(end), xTickSpacing));
            
            set(self.UnipolarAxes,...
                'xLim', timeWindow,...
                'xTick', xTicks,...
                'xMinorTick', 'on');
            
            yLimits = get(self.UnipolarAxes, 'yLim');
            set(self.AnimationPositionLine, 'xData', [timePosition, timePosition], 'yData', yLimits);
        end
        
        function [NS, WE, NWSE, SWNE, electrodeIndices] = ComputeBipolarSignals(self)
            NS = NaN(self.EcgData.GetNumberOfSamples(), 1);
            WE = NS;
            NWSE = NS;
            SWNE = NS;
            if isnan(self.SelectedChannelIndex), return; end
            
            electrodePositions = self.EcgData.ElectrodePositions;
            
            electrodePosition = electrodePositions(self.SelectedChannelIndex, : );
            positionDifferences = bsxfun(@minus, electrodePositions, electrodePosition);
            
            tolerance = 1e3 * eps;
            positionDifferences(abs(positionDifferences(:, 3)) > tolerance, :) = NaN;
            
            westIndex = find(abs(positionDifferences(:, 1) + self.MinimumSpacing) < tolerance &...
                abs(positionDifferences(:, 2)) < tolerance);
            eastIndex = find(abs(positionDifferences(:, 1) - self.MinimumSpacing) < tolerance &...
                abs(positionDifferences(:, 2)) < tolerance);
            northIndex = find(abs(positionDifferences(:, 1)) < tolerance &...
                abs(positionDifferences(:, 2) + self.MinimumSpacing) < tolerance);
            southIndex = find(abs(positionDifferences(:, 1)) < tolerance &...
                abs(positionDifferences(:, 2) - self.MinimumSpacing) < tolerance);
            
            northWestIndex = find(abs(positionDifferences(:, 1) + self.MinimumSpacing) < tolerance &...
                abs(positionDifferences(:, 2) + self.MinimumSpacing) < tolerance);
            
            southWestIndex = find(abs(positionDifferences(:, 1) + self.MinimumSpacing) < tolerance &...
                abs(positionDifferences(:, 2) - self.MinimumSpacing) < tolerance);
            
            northEastIndex = find(abs(positionDifferences(:, 1) - self.MinimumSpacing) < tolerance &...
                abs(positionDifferences(:, 2) + self.MinimumSpacing) < tolerance);
            
            southEastIndex = find(abs(positionDifferences(:, 1) - self.MinimumSpacing) < tolerance &...
                abs(positionDifferences(:, 2) - self.MinimumSpacing) < tolerance);
            
            electrodeIndices = [self.SelectedChannelIndex, westIndex, eastIndex, northIndex, southIndex,...
                northWestIndex, southWestIndex, northEastIndex, southEastIndex];
            
            % bipolar bandpass filter
            nyquistFrequency = self.EcgData.SamplingFrequency / 2;
            [b, a] = cheby2(3, 20,...
                    30 / nyquistFrequency, 'high');
            
            % North -> South
            if ~(isempty(northIndex) || isempty(southIndex))
               NS =  self.EcgData.Data(:, southIndex) - self.EcgData.Data(:, northIndex);
               NS = filtfilt(b, a, NS);
            end
            % West -> East
            if ~(isempty(westIndex) || isempty(eastIndex))
               WE =  self.EcgData.Data(:, eastIndex) - self.EcgData.Data(:, westIndex);
               WE = filtfilt(b, a, WE);
            end
            % NorthWest -> SouthEast
            if ~(isempty(northWestIndex) || isempty(southEastIndex))
               NWSE =  self.EcgData.Data(:, southEastIndex) - self.EcgData.Data(:, northWestIndex);
               NWSE = filtfilt(b, a, NWSE);
            end
            % SouthWest -> NorthEast
            if ~(isempty(southWestIndex) || isempty(northEastIndex))
               SWNE =  self.EcgData.Data(:, northEastIndex) - self.EcgData.Data(:, southWestIndex);
               SWNE = filtfilt(b, a, SWNE);
            end
        end
        
        function data = ComputeLaplacian(self)
            if isempty(self.EcgData), return; end
            
            electrodeIndex = self.SelectedChannelIndex;
            electrodePositions = self.EcgData.ElectrodePositions;
            minimumSpacing = self.ComputeMinimumSpacing();
            
            data = self.ComputeSingleLaplacian(electrodeIndex, electrodePositions, minimumSpacing);
        end
        
        function ecgData = ComputeCompleteLaplacian(self)
            if isempty(self.EcgData),
                ecgData = DCMData.empty(1,0);
                return; 
            end
            
            electrodePositions = self.EcgData.ElectrodePositions;
            minimumSpacing = self.ComputeMinimumSpacing();
            
            data = NaN(self.EcgData.GetNumberOfSamples, self.EcgData.GetNumberOfChannels);
            for electrodeIndex = 1:self.EcgData.GetNumberOfChannels()
                data(:, electrodeIndex) = self.ComputeSingleLaplacian(electrodeIndex, electrodePositions, minimumSpacing);
            end
            validChannels = ~all(isnan(data));
            ecgData = self.EcgData.Copy(validChannels);
            ecgData.Data = data(:, validChannels);
        end
        
        function data = ComputeSingleLaplacian(self, electrodeIndex, electrodePositions, minimumSpacing)
           radius = sqrt(2 * (minimumSpacing + 1e3 * eps) ^ 2);
           L = 1.5;
           rho = .4;
            
           electrodeNeighbors = BipolarLaplacianPanel.GetElectrodeNeighbors(...
                electrodeIndex, electrodePositions, radius);
            plusNeighbors = electrodeNeighbors.distances <= minimumSpacing;
            crossNeigbors = electrodeNeighbors.distances > minimumSpacing;
            weights = NaN(9, 1);
            if numel(find(plusNeighbors)) + numel(find(crossNeigbors)) == 9
                validNeighbors = true(size(electrodeNeighbors.positions));
                weights(plusNeighbors) = 4;
                weights(crossNeigbors) = 1;
                weights(electrodeNeighbors.positions == electrodeIndex) = -20;
                denominator = 6 * rho * L^2;
            else
                if numel(find(plusNeighbors)) == 5
                    validNeighbors = plusNeighbors;
                    weights(plusNeighbors) = 1;
                    weights(electrodeNeighbors.positions == electrodeIndex) = -4;
                    denominator = rho * L^2;
                else
                    if numel(find(crossNeigbors)) == 4
                        validNeighbors = crossNeigbors;
                        validNeighbors(electrodeNeighbors.positions == electrodeIndex) = true;
                        weights(crossNeigbors) = 1;
                        weights(electrodeNeighbors.positions == electrodeIndex) = -4;
                        denominator = 2 * rho * L^2;
                    else
                        data = NaN(self.EcgData.GetNumberOfSamples, 1);
                        return;
                    end
                end
            end
            data = -(self.EcgData.Data(:, electrodeNeighbors.positions(validNeighbors)) *...
                weights(validNeighbors)) / denominator; 
        end
        
        function minimumSpacing = ComputeMinimumSpacing(self)
            distanceMatrix = pdist(self.EcgData.ElectrodePositions);
            distanceMatrix = distanceMatrix(distanceMatrix ~= 0);
            minimumSpacing = min(distanceMatrix);
        end
    end
    
    methods (Static)
        function neighbors = GetElectrodeNeighbors(electrodeIndex, electrodePositions, radius)
            electrodePosition = electrodePositions(electrodeIndex, : );
            positionDifferences = bsxfun(@minus, electrodePositions, electrodePosition);
            electrodeDistances = sqrt(positionDifferences(:, 1).^2 + positionDifferences(:, 2).^2 + positionDifferences(:, 3).^2);
            validNeigbors = electrodeDistances <= radius;
            
            neighbors = struct(...
                'positions', find(validNeigbors),...
                'distances', electrodeDistances(validNeigbors));
        end
    end
end
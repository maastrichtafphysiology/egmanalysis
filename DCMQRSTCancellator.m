classdef DCMQRSTCancellator < QRSTCancellator
    properties
        SpatialRadius
    end
    
    properties (Constant)
        CANCELLATION_METHODS = {...
            'ABS Clustered',...
            'Global Single Beat', 'Local Single Beat',...
            'Global Component', 'Local Component',...
            'Multipolar', 'Windowed Multipolar'}
    end
    
    methods
        function self = DCMQRSTCancellator()
            self.VentricularRefractoryPeriod = 0.2;
            self.CancellationMethod = DCMQRSTCancellator.CANCELLATION_METHODS{2};
            self.QRSTRange = [.3 .3];
            self.ClusterSensitivity = 0.9;
            self.ShowDiagnostics = false;
            
            self.TemplateShift = 0.02;
            self.RPeakSearchWindow = [0.03 0.03];
            
            self.InterpolationFactor = 1;
            self.SpatialRadius = 2;
            
            self.HighpassPostFilter = 1;
        end
        
        function rWaveIndices = DetectRWaves(self, ecgData, referenceChannel)
            self.SamplingFrequency = ecgData.SamplingFrequency;
            data = ecgData.ReferenceData(:, referenceChannel);
            
            [~, differentationFilters] = sgolay(3, 51);
            derivateSignal = filtfilt(differentationFilters(:, 2), 1, abs(data));
            [peakValues, candidateIndices] = findpeaks(derivateSignal, 'minPeakDistance', round(self.VentricularRefractoryPeriod * self.SamplingFrequency));
            
            rWaveIndices = candidateIndices(peakValues > median(peakValues) / 2);
            
            if self.ShowDiagnostics
                time = ecgData.GetTimeRange();
                figure;
                plotHandles = NaN(3,1);
                plotHandles(1) = subplot(3, 1, 1);
                plot(time, data);
                title('Original');
                line('xData', time(rWaveIndices), 'yData', data(rWaveIndices),...
                    'parent', plotHandles(1), 'lineStyle', 'none',...
                    'marker', 'o');
                
                plotHandles(2) = subplot(3, 1, 2);
                plot(time, abs(data));
                
                plotHandles(3) = subplot(3, 1, 3);
                plot(time, derivateSignal);
                line('xData', time(rWaveIndices), 'yData', derivateSignal(rWaveIndices),...
                    'parent', plotHandles(3), 'lineStyle', 'none',...
                    'marker', 'o');
                
                validHandles = ishandle(plotHandles);
                
                linkaxes(plotHandles(validHandles), 'x');
            end
        end
    end
    
    methods (Access = protected)
        function data = CancelQRSTMethod(self, data, ecgData, rWaveWindowIndices, memberIndices)
            oldTemplateShift = self.TemplateShift;
            
            switch self.CancellationMethod
                case DCMQRSTCancellator.CANCELLATION_METHODS{1}
                    self.TemplateShift = 0.005;
                    
                    for channelIndex = 1:ecgData.GetNumberOfChannels
                        channelData = data(:, channelIndex);
                        channelData = self.ComputeClusteredABSCancellation(channelData, rWaveWindowIndices, memberIndices);
                        data(:, channelIndex) = channelData;
                    end
                case DCMQRSTCancellator.CANCELLATION_METHODS{2}
                    self.TemplateShift = 0.005;
                    data = self.ComputeSingleBeatCancellation(data, rWaveWindowIndices);
                case DCMQRSTCancellator.CANCELLATION_METHODS{3}
                    self.TemplateShift = 0.001;
                    data = self.ComputeSpatialCancellation(data, ecgData, rWaveWindowIndices);
                case DCMQRSTCancellator.CANCELLATION_METHODS{4}
                    data = self.ComputeGlobalComponentCancellation(data);
                case DCMQRSTCancellator.CANCELLATION_METHODS{5}
                    data = self.ComputeLocalComponentCancellation(ecgData);
                case DCMQRSTCancellator.CANCELLATION_METHODS{6}
                    data = self.ComputeMultiPolarCancellation(ecgData);
                case DCMQRSTCancellator.CANCELLATION_METHODS{7}
                    data = self.ComputeWindowedMultiPolarCancellation(ecgData);
            end
            
            self.TemplateShift = oldTemplateShift;
        end
        
        function cancelledSignal = ComputeSingleBeatCancellation(self, signals, rWaveWindowIndices)
            cancelledSignal = signals;
            for windowIndex = 1:size(rWaveWindowIndices, 2)
                validWindowIndices = rWaveWindowIndices(:, windowIndex) >= 1 &...
                    rWaveWindowIndices(:, windowIndex) <= size(signals, 1);
                currentWindowIndices = rWaveWindowIndices(validWindowIndices, windowIndex);
                beatData = signals(currentWindowIndices, :);
                beatData = bsxfun(@minus, beatData, mean(beatData));
                [u, s, v] = svd(beatData');
                qrstComponent = v(:, 1);
                M = u(:, 1) * s(1);
                %                 newSignals = u * s(:, 2:end) * v(:, 2:end)';
                for signalIndex = 1:size(signals, 2)
                    signalQrsComponent = M(signalIndex) * qrstComponent;
                    cancelledSignal(:, signalIndex) = self.SubtractQRST(cancelledSignal(:, signalIndex), signalQrsComponent, currentWindowIndices);
                end
            end
            
        end
        
        function cancelledSignal = ComputeSpatialCancellation(self, signal, ecgData, rWaveWindowIndices)
            minimumSpacing = ecgData.ComputeMinimumSpacing();
            radius = self.SpatialRadius * sqrt(2 * (minimumSpacing + 1e3 * eps)^2);
            
            cancelledSignal = signal;
            numberOfChannels = ecgData.GetNumberOfChannels;
            neighbors = ecgData.GetElectrodeNeighbors(1:numberOfChannels, radius);
            
            oldTemplateShift = self.TemplateShift;
            self.TemplateShift = 0;
            
            for electrodeIndex = 1:numberOfChannels
                selectedSignals = signal(:, neighbors(:, electrodeIndex));
                for windowIndex = 1:size(rWaveWindowIndices, 2)
                    validWindowIndices = rWaveWindowIndices(:, windowIndex) >= 1 &...
                        rWaveWindowIndices(:, windowIndex) <= size(signal, 1);
                    currentWindowIndices = rWaveWindowIndices(validWindowIndices, windowIndex);
                    beatData = selectedSignals(currentWindowIndices, :);
                    beatData = bsxfun(@minus, beatData, mean(beatData));
                    [u, s, v] = svd(beatData', 'econ');
                    qrstComponent = v(:, 1);
                    %                     M = u(:, 1) * s(1);
                    cancelledSignal(:, electrodeIndex) = self.SubtractQRST(cancelledSignal(:, electrodeIndex), qrstComponent, currentWindowIndices);
                end
            end
            
            
            self.TemplateShift = oldTemplateShift;
        end
        
        function cancelledSignal = ComputeGlobalComponentCancellation(self, signals)
            numberOfChannels = size(signals, 2);
            signalMean = mean(signals);
            signalSD = std(signals);
            signals = bsxfun(@minus, signals, signalMean);
            signals = bsxfun(@rdivide, signals, signalSD);
            
            signals = signals';
            sampleMean = mean(signals);
            sampleSD = std(signals);
            signals = bsxfun(@minus, signals, sampleMean);
            signals = bsxfun(@rdivide, signals, sampleSD);
            
            [u, s, v] = svd(signals, 'econ');
            
            componentRange = 2:(numberOfChannels - 1);
            cancelledSignal = ((u(:, componentRange) * s(componentRange, componentRange)) * v(:, componentRange)');
            cancelledSignal = bsxfun(@times, cancelledSignal, sampleSD);
            cancelledSignal = bsxfun(@plus, cancelledSignal, sampleMean);
            cancelledSignal = cancelledSignal';
            
            cancelledSignal = bsxfun(@times, cancelledSignal, signalSD);
            cancelledSignal = bsxfun(@plus, cancelledSignal, signalMean);
        end
        
        function cancelledSignal = ComputeLocalComponentCancellation(self, ecgData)
            minimumSpacing = ecgData.ComputeMinimumSpacing();
            radius = self.SpatialRadius * sqrt(2 * (minimumSpacing + 1e3 * eps)^2);
            
            allSignals = ecgData.Data;
            cancelledSignal = allSignals;
            numberOfChannels = ecgData.GetNumberOfChannels;
            neighbors = ecgData.GetElectrodeNeighbors(1:numberOfChannels, radius);
            
            for electrodeIndex = 1:numberOfChannels
                electrodeNeighbors = find(neighbors(:, electrodeIndex));
                electrodePosition = electrodeNeighbors == electrodeIndex;
                selectedSignals = allSignals(:, neighbors(:, electrodeIndex));
                numberOfSignals = size(selectedSignals, 2);
                signals = selectedSignals';
                
                signalMean = mean(signals);
                signalSD = std(signals);
                signals = bsxfun(@minus, signals, signalMean);
                signals = bsxfun(@rdivide, signals, signalSD);
            
                [u, s, v] = svd(signals, 'econ');
            
                componentRange = 2:(numberOfSignals - 1);
                electrodeCancelledSignal =...
                    ((u(electrodePosition, componentRange) * s(componentRange, componentRange)) * v(:, componentRange)');
                
                electrodeCancelledSignal = electrodeCancelledSignal .* signalSD;
                electrodeCancelledSignal = electrodeCancelledSignal + signalMean;
                cancelledSignal(:, electrodeIndex) = electrodeCancelledSignal';
            end
        end
        
        function cancelledSignal = ComputeMultiPolarCancellation(self, ecgData)
            electrodePositions = ecgData.ElectrodePositions;
            minimumSpacing = ecgData.ComputeMinimumSpacing();
            
            electrodeDistances = squareform(pdist(electrodePositions));
            
            neighborDistance = sqrt(2) * minimumSpacing * 2;
            tolerance = 1e3 * eps;
            electrodeNeighbors = (electrodeDistances - tolerance) <= neighborDistance;
            
            orginalData = ecgData.Data;
%             orginalData = self.ComputeGlobalComponentCancellation(ecgData.Data);
            
            numberOfChannels = ecgData.GetNumberOfChannels;
            numberOfSamples = ecgData.GetNumberOfSamples();
            improving = true;
            improvementThreshold = 0.01;
            iteration = 1;
            while improving
                disp(['Iteration ' num2str(iteration)]);
                orginalDataRMSE = sqrt(sum(orginalData.^2) / numberOfSamples)';
                cancelledSignal = orginalData;
                neighborRMSE = zeros(numberOfChannels, 1);
                
                parfor electrodeIndex = 1:numberOfChannels
                    currentNeigbors = electrodeNeighbors(:, electrodeIndex);
                    currentNeigbors(electrodeIndex) = false;
                    neighborSignals = orginalData(:, currentNeigbors);
                    
                    [neighborCoefficients, residualNorm, residual] = lsqnonneg(neighborSignals, orginalData(:, electrodeIndex));
                    cancelledSignal(:, electrodeIndex) = residual;
                    neighborRMSE(electrodeIndex) = sqrt(residualNorm / numberOfSamples);
                    
%                     neighborCoefficients = robustfit(neighborSignals, orginalData(:, electrodeIndex));
%                     cancelledSignal(:, electrodeIndex) = orginalData(:, electrodeIndex) -...
%                         neighborCoefficients(1) * ones(numberOfSamples, 1) -...
%                         neighborSignals * neighborCoefficients(2:end);
%                     neighborRMSE(electrodeIndex) = sqrt(sum(cancelledSignal(:, electrodeIndex).^2) / numberOfSamples);
                end
                
                rmseReduction = (orginalDataRMSE - neighborRMSE) ./ orginalDataRMSE;
                clear originalData;
                disp(['Max RMS: ', num2str(max(neighborRMSE))]);
                disp(['Max RMS reduction: ', num2str(max(rmseReduction))]);
                if max(rmseReduction) < improvementThreshold
                    improving = false;
                else
                    orginalData = cancelledSignal;
                    clear cancelledSignal;
                    iteration = iteration + 1;
                end
            end
            
            %             [bHigh, aHigh] = butter(2, [40 250] / (self.SamplingFrequency / 2));
            %             [bLow, aLow] = butter(2, 20 / (self.SamplingFrequency / 2), 'low');
            %
            %             cancelledSignal = filtfilt(bHigh, aHigh,cancelledSignal);
            %             cancelledSignal = abs(cancelledSignal);
            %             cancelledSignal = [zeros(100, numberOfChannels); cancelledSignal; zeros(100, numberOfChannels)];
            %             cancelledSignal = filtfilt(bLow, aLow, cancelledSignal);
            %             cancelledSignal = cancelledSignal(101:(end-100), :);
            %
            %
            %             for electrodeIndex = 1:numberOfChannels
            %                 cancelledSignal(:, electrodeIndex) = gradient(cancelledSignal(:, electrodeIndex));
            %             end
        end
        
        function cancelledSignal = ComputeWindowedMultiPolarCancellation(self, ecgData)
            electrodePositions = ecgData.ElectrodePositions;
            minimumSpacing = ecgData.ComputeMinimumSpacing();
            
            electrodeDistances = squareform(pdist(electrodePositions));
            
            neighborDistance = sqrt(2) * minimumSpacing;
            tolerance = 1e3 * eps;
            electrodeNeighbors = (electrodeDistances - tolerance) <= neighborDistance;
            
            orginalData = ecgData.Data;
            
            numberOfChannels = ecgData.GetNumberOfChannels;
            numberOfSamples = ecgData.GetNumberOfSamples();
            
            windowLength = 200;
            windows = 1:(windowLength / 2):(numberOfSamples - windowLength / 2);
            numberOfWindows = numel(windows);
            
            cancelledSignal = orginalData;
            parfor electrodeIndex = 1:numberOfChannels
                currentNeigbors = electrodeNeighbors(:, electrodeIndex);
                currentNeigbors(electrodeIndex) = false;
                neighborSignals = orginalData(:, currentNeigbors);
                windowedCancelledSignal = NaN(numberOfSamples, numberOfWindows);
                for windowIndex = 1:numberOfWindows;    
                    windowIndices = windows(windowIndex):(windows(windowIndex) + windowLength - 1);
                    windowIndices = windowIndices(windowIndices <= numberOfSamples);
                    
                    [~, ~, residual] = lsqnonneg(neighborSignals(windowIndices, :), orginalData(windowIndices, electrodeIndex));
                    windowedCancelledSignal(windowIndices, windowIndex) = residual;
                end
                cancelledSignal(:, electrodeIndex) = nanmean(windowedCancelledSignal, 2);
            end
        end
    end
end
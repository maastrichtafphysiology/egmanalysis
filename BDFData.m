classdef BDFData < ECGData
    properties
        LAChannelData
        LAChannelLabel
        
        RAChannelData
        RAChannelLabel
        
        LLChannelData
        LLChannelLabel
        
        WCTCorrected
    end
   
    methods
        function self = BDFData(filename, data, samplingFrequency, varargin)
            self = self@ECGData(filename, data, samplingFrequency, varargin{:});
            self.WCTCorrected = false;
        end
        
        function Organize(self)
            self.Data = bsxfun(@minus, self.Data, mean(self.Data));
            [~, firstIndices, ~] = unique(self.ElectrodeLabels, 'first');
            firstIndices = sort(firstIndices);
            self.ElectrodeLabels = self.ElectrodeLabels(firstIndices);
            self.Data = self.Data(:, firstIndices);
            
            statusPosition = find(strcmp('Status', self.ElectrodeLabels));
            if ~isempty(statusPosition)
                self.ElectrodeLabels(statusPosition) = [];
                self.Data(:, statusPosition) = [];
            end
            
            LAChannelIndex = find(strcmp('LA', self.ElectrodeLabels));
            RAChannelIndex = find(strcmp('RA', self.ElectrodeLabels));
            LLChannelIndex = find(strcmp('LL', self.ElectrodeLabels));
            if (isempty(LAChannelIndex) || isempty(RAChannelIndex) || isempty(LLChannelIndex))
                LAChannelIndex = find(strcmp('EXG1', self.ElectrodeLabels));
                LLChannelIndex = find(strcmp('EXG2', self.ElectrodeLabels));
                RAChannelIndex = find(strcmp('EXG3', self.ElectrodeLabels));
            end
            
            if ~(isempty(LAChannelIndex) || isempty(RAChannelIndex) || isempty(LLChannelIndex))
                self.LAChannelData = self.Data(:, LAChannelIndex);
                self.LAChannelLabel = 'LA';
                
                self.RAChannelData = self.Data(:, RAChannelIndex);
                self.RAChannelLabel = 'RA';
                
                self.LLChannelData = self.Data(:, LLChannelIndex);
                self.LLChannelLabel = 'LL';
                
                self.Data(:, [LAChannelIndex, RAChannelIndex, LLChannelIndex]) = [];
                self.ElectrodeLabels([LAChannelIndex, RAChannelIndex, LLChannelIndex]) = [];
                
                self.SetReferenceData();
            end
        end
        
        function SetReferenceData(self)
            if isempty(self.LAChannelData) || isempty(self.RAChannelData) || isempty(self.LLChannelData), return; end
            
            LeadI = self.LAChannelData - self.RAChannelData;
            LeadII = self.LLChannelData - self.RAChannelData;
            LeadIII = self.LLChannelData - self.LAChannelData;
            WCT = self.ComputeWCT();
            
            self.ReferenceData = [self.LAChannelData, self.RAChannelData, self.LLChannelData,...
                LeadI, LeadII, LeadIII, WCT];
            self.ReferenceLabels = {self.LAChannelLabel; self.RAChannelLabel; self.LLChannelLabel;...
                'I'; 'II'; 'III'; 'WCT'};
        end
        
        function tf = ApplyWCT(self)
            WCT = self.ComputeWCT();
            if isnan(WCT), tf = false; return; end
                
            if self.WCTCorrected
                self.Data = bsxfun(@plus, self.Data, WCT);
                self.WCTCorrected = false;
            else
                self.Data = bsxfun(@minus, self.Data, WCT);
                self.WCTCorrected = true;
            end
            tf = self.WCTCorrected;
        end
        
        function newLALabel = SetLA(self, channelIndex)
            wctCorrected = self.WCTCorrected;
            if wctCorrected
                self.ApplyWCT();
            end
            
            channelData = self.Data(:, channelIndex);
            channelLabel = self.ElectrodeLabels{channelIndex};
            
            self.Data(:, channelIndex) = [];
            self.ElectrodeLabels(channelIndex) = [];
            
            if ~isempty(self.LAChannelLabel)
                self.Data = [self.Data, self.LAChannelData];
                self.ElectrodeLabels = [self.ElectrodeLabels; self.LAChannelLabel];
            end
            
            self.LAChannelData = channelData;
            self.LAChannelLabel = channelLabel;
            newLALabel = channelLabel;
            
            [~, firstIndices, ~] = unique(self.ElectrodeLabels, 'first');
            firstIndices = sort(firstIndices);
            self.ElectrodeLabels = self.ElectrodeLabels(firstIndices);
            self.Data = self.Data(:, firstIndices);
            
            if wctCorrected
                self.ApplyWCT();
            end
            
            self.SetReferenceData();
        end
        
        function newRALabel = SetRA(self, channelIndex)
            wctCorrected = self.WCTCorrected;
            if wctCorrected
                self.ApplyWCT();
            end
            
            channelData = self.Data(:, channelIndex);
            channelLabel = self.ElectrodeLabels{channelIndex};
            
            self.Data(:, channelIndex) = [];
            self.ElectrodeLabels(channelIndex) = [];
            
            if ~isempty(self.RAChannelLabel)
                self.Data = [self.Data, self.RAChannelData];
                self.ElectrodeLabels = [self.ElectrodeLabels; self.RAChannelLabel];
            end
            
            self.RAChannelData = channelData;
            self.RAChannelLabel = channelLabel;
            newRALabel = channelLabel;
            
            [~, firstIndices, ~] = unique(self.ElectrodeLabels, 'first');
            firstIndices = sort(firstIndices);
            self.ElectrodeLabels = self.ElectrodeLabels(firstIndices);
            self.Data = self.Data(:, firstIndices);
            
            if wctCorrected
                self.ApplyWCT();
            end
            
            self.SetReferenceData();
        end
        
        function newLLLabel = SetLL(self, channelIndex)
            wctCorrected = self.WCTCorrected;
            if wctCorrected
                self.ApplyWCT();
            end
            
            channelData = self.Data(:, channelIndex);
            channelLabel = self.ElectrodeLabels{channelIndex};
            
            self.Data(:, channelIndex) = [];
            self.ElectrodeLabels(channelIndex) = [];
            
            if ~isempty(self.LLChannelLabel)
                self.Data = [self.Data, self.LLChannelData];
                self.ElectrodeLabels = [self.ElectrodeLabels; self.LLChannelLabel];
            end
            
            self.LLChannelData = channelData;
            self.LLChannelLabel = channelLabel;
            newLLLabel = channelLabel;
            
            [~, firstIndices, ~] = unique(self.ElectrodeLabels, 'first');
            firstIndices = sort(firstIndices);
            self.ElectrodeLabels = self.ElectrodeLabels(firstIndices);
            self.Data = self.Data(:, firstIndices);
            
            if wctCorrected
                self.ApplyWCT();
            end
            
            self.SetReferenceData();
        end
        
        function WCT = ComputeWCT(self)
            if isempty(self.LAChannelLabel) || isempty(self.LAChannelLabel) || isempty(self.LAChannelLabel)
                WCT = NaN;
                return;
            else
                WCT = mean([self.LAChannelData, self.RAChannelData, self.LLChannelData], 2);
            end
        end
    end
end
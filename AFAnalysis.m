classdef AFAnalysis < UserInterfacePkg.CustomFigure
    properties (Access = protected)
        EcgData
        
        TabGroup
        PreProcessingTab
        QRSTCancellationTab
        TemplateMatchingTab
        DeflectionAnalysisTab
        WaveMapTab
        
        ChannelSelectionView
        PaceBlankingView
        PreProcessingView
        QRSTCancellationView
        TemplateMatchingView
        DeflectionAnalysisView
        FractionationAnalysisView
        WaveMapView
        BipolarLaplacianView
        
        WaveAnalysisFigure
        PropertyMapFigure
        BatchAnalysisFigure
        
        ApplicationSettings
        ParallelProcessing
        
        ZoomToggleButton
        PanToggleButton
        
        AnalysisStatusBar
        
        GUIMode
        
        MappedFileTypeIndices
    end
    
    properties (Constant)
        FILETYPEFILTER = {...
            '*.*', 'DCM (PACEMAP Binary) (*.E**)';...
            '*.DATA', 'IDEEQ Data (*.DATA)';...
            '*.bdf', 'BioSemi data format (*.bdf)';...
            '*.csv', 'Basket Export (*.csv)';...
            '*.*', 'DCM (PACEMAP Binary Series) (*.E**)';...
            '*.ASC', 'DCM (PACEMAP ASCII) (*.ASC)';...
            '*.mat', 'MAT-file containing ecgData (*.mat)'};
    end
    
    methods
        function self = AFAnalysis(position)
            if nargin < 1
                warning off MATLAB:uitabgroup:OldVersion;
                screenSize = get(0,'Screensize');
                position = [screenSize(3) / 5, screenSize(4) / 5,...
                    3 * screenSize(3) / 5, 3 * screenSize(4) / 5];
            end
            
            self = self@UserInterfacePkg.CustomFigure(position);
            self.Name = 'AF Analysis';
            self.ParallelProcessing = false;
            self.GUIMode = 'Release';
            self.MappedFileTypeIndices = 1:size(AFAnalysis.FILETYPEFILTER, 1);
%             self.GUIMode = 'Research';

            if nargin < 1            
                self.Create();
            end
        end
        
        function Create(self)
            Create@UserInterfacePkg.CustomFigure(self);
            
            set(self.ControlHandle,...
                'CloseRequestFcn', @self.Close);
            
            set(self.ControlHandle,...
                'colormap', flipud(gray(256)));
            
            self.Hide();
            
            self.InitializeApplicationSettings();
            
            self.CreateFileMenu();
            
            self.CreateSettingsMenu();
            
            self.CreateAnalysisMenu();
            
            self.CreateToolbar();
            
            self.CreateStatusBar();
            
            self.InitializeEvents();
            
            self.InitializeApplicationSettings();
            
            self.Show();
        end
        
        function delete(self)
            self.EcgData = [];
            if ishandle(self.ApplicationSettings)
                delete(self.ApplicationSettings);
            end
            clear self.ApplicationSettings;
        end
    end
    
    methods (Access = protected)
        function InitializeTabs(self)
            self.RemoveTabs();
            if ishandle(self.TabGroup)
                delete(self.TabGroup);
            end
            
            self.InitializeDCMTabs();
        end
        
        function ShowEmptyDCMTabs(self, varargin)
            self.EcgData = DCMData('Empty tabs', [], NaN);
            self.InitializeTabs();
            self.SetFilename();
        end
        
        function InitializeDCMTabs(self)
            self.Hide();
            
            self.TabGroup = uitabgroup(...
                'parent', self.ControlHandle,...
                'position', [0 .025 1 .975]);
            
            channelSelectionTab = uitab(self.TabGroup, 'title', 'Data');
            self.ChannelSelectionView = DCMVisualizationPanel([0 0 1 1]);
            self.ChannelSelectionView.Create(channelSelectionTab);
            self.ChannelSelectionView.Show();
            
            addlistener(self.ChannelSelectionView, 'ValidChannelsSelectionCompleted',...
                @self.HandleChannelSelectionEvent);
            
            paceBlankingTab = uitab(self.TabGroup, 'title','Pace detection');
            self.PaceBlankingView = PaceBlankingPanel([0 0 1 1]);
            self.PaceBlankingView.Create(paceBlankingTab);
            self.PaceBlankingView.Show();
            
            addlistener(self.PaceBlankingView, 'PaceBlankingCompleted',...
                @self.HandlePaceBlankingEvent);
            
            self.PreProcessingTab = uitab(self.TabGroup, 'title','Pre-processing');
            self.PreProcessingView = PreProcessingPanel([0 0 1 1]);
            self.PreProcessingView.Create(self.PreProcessingTab);
            self.PreProcessingView.Show();
            
            addlistener(self.PreProcessingView, 'PreProcessingCompleted',...
                @self.HandlePreProcessingEvent);
            
            self.QRSTCancellationTab = uitab(self.TabGroup, 'title','QRST Cancellation');
            self.QRSTCancellationView = DCMQRSTCancellationPanel([0 0 1 1]);
            self.QRSTCancellationView.Create(self.QRSTCancellationTab);
            self.QRSTCancellationView.Show();
            
            addlistener(self.QRSTCancellationView, 'QRSTCancellationCompleted',...
                @self.HandleQRSTCancellationEvent);
            
            self.TemplateMatchingTab = uitab(self.TabGroup, 'title','Template matching');
            self.TemplateMatchingView = TemplateMatchingPanel([0 0 1 1]);
            self.TemplateMatchingView.Create(self.TemplateMatchingTab);
            self.TemplateMatchingView.Show();
            
            addlistener(self.TemplateMatchingView, 'TemplateMatchingCompleted',...
                @self.HandleTemplateMatchingEvent);
            
            self.DeflectionAnalysisTab = uitab(self.TabGroup, 'title','Deflection analysis');
            self.DeflectionAnalysisView = DeflectionAnalysisPanel([0 0 1 1]);
            self.DeflectionAnalysisView.Create(self.DeflectionAnalysisTab);
            self.DeflectionAnalysisView.Show();
            
            addlistener(self.DeflectionAnalysisView, 'IntrinsicDeflectionsDetermined',...
                @self.HandleIntrinsicDeflectionEvent);
            
            self.WaveMapTab = uitab(self.TabGroup, 'title','Wave mapping');
            self.WaveMapView = WaveMapPanel([0 0 1 1]);
            self.WaveMapView.Create(self.WaveMapTab);
            self.WaveMapView.Show();
            
            addlistener(self.WaveMapView, 'WavemapComputed',...
                @self.HandleWavemapEvent);
            
            if strcmp(self.GUIMode, 'Research')
                bipolarLaplacianTab = uitab(self.TabGroup, 'title','Bipolar - Laplacian analysis');
                self.BipolarLaplacianView = BipolarLaplacianPanel([0 0 1 1]);
                self.BipolarLaplacianView.Create(bipolarLaplacianTab);
                self.BipolarLaplacianView.Show();
                
                addlistener(self.BipolarLaplacianView, 'LaplacianECGComputed',...
                    @self.HandleLaplacianEvent);
                
                fractionationAnalysisTab = uitab(self.TabGroup, 'title','Fractionation analysis');
                self.FractionationAnalysisView = FractionationAnalysisPanel([0 0 1 1]);
                self.FractionationAnalysisView.Create(fractionationAnalysisTab);
                self.FractionationAnalysisView.Show();
            end
            
            self.Show();
        end
        
        function RemoveTabs(self)
            if isobject(self.ChannelSelectionView)
                delete(self.ChannelSelectionView);
            end
            
            if isobject(self.PreProcessingView)
                delete(self.PreProcessingView);
            end
            
            if isobject(self.QRSTCancellationView)
                delete(self.QRSTCancellationView);
            end
            
            if isobject(self.TemplateMatchingView)
                delete(self.TemplateMatchingView);
            end
            
            if isobject(self.DeflectionAnalysisView)
                delete(self.DeflectionAnalysisView);
            end
            
            if isobject(self.WaveMapView)
                delete(self.WaveMapView);
            end
            
            if isobject(self.FractionationAnalysisView)
                delete(self.FractionationAnalysisView);
            end
            
            if isobject(self.BipolarLaplacianView)
                delete(self.BipolarLaplacianView);
            end
        end
        
        function CreateFileMenu(self)
            fileMenu = uimenu(...
                'parent', self.ControlHandle,...
                'label', 'File');
            uimenu('parent', fileMenu,...
                'label', 'Import New Signal',...
                'callback', @self.LoadSignal);
            uimenu('parent', fileMenu,...
                'label', 'Load Analysis',...
                'callback', @self.LoadAnalysis);
            uimenu('parent', fileMenu,...
                'label', 'Show Empty Tabs',...
                'callback', @self.ShowEmptyDCMTabs);
        end
        
        function CreateSettingsMenu(self)
            settingsMenu = uimenu(...
                'parent', self.ControlHandle,...
                'label', 'Settings');
            uimenu(...
                'parent', settingsMenu,...
                'label', 'Algorithm Settings',...
                'callback', @self.ShowApplicationSettings);
            uimenu(...
                'parent', settingsMenu,...
                'label', 'Parallel Processing',...
                'callback', @self.SetParallelProcessing);
        end
        
        function CreateAnalysisMenu(self)
            analysisMenu = uimenu(...
                'parent', self.ControlHandle,...
                'label', 'Analysis');
%             uimenu(...
%                 'parent', analysisMenu,...
%                 'label', 'Wave Analysis',...
%                 'callback', @self.ShowWaveAnalysis);
%             uimenu(...
%                 'parent', analysisMenu,...
%                 'label', 'Property Maps',...
%                 'callback', @self.ShowPropertyMaps);
            uimenu(...
                'parent', analysisMenu,...
                'label', 'Batch Analysis',...
                'callback', @self.ShowBatchAnalysis);
        end
        
        function CreateToolbar(self)
            toolbar = uitoolbar(...
                'parent', self.ControlHandle);
            toolbarIconData = load('toolbarIcons.mat');
            uipushtool(...
                'parent', toolbar,...
                'CData', toolbarIconData.newDataCData,...
                'tooltipString', 'Import new signal',...
                'clickedCallback', @self.LoadSignal);
            uipushtool(...
                'parent', toolbar,...
                'CData', toolbarIconData.opendoc,...
                'tooltipString', 'Load analysis',...
                'clickedCallback', @self.LoadAnalysis);
            self.ZoomToggleButton = uitoggletool(...
                'parent', toolbar,...
                'CData', toolbarIconData.zoomCData,...
                'offCallback', 'zoom off',...
                'onCallback', @self.ZoomOn,...
                'separator', 'on');
            self.PanToggleButton = uitoggletool(...
                'parent', toolbar,...
                'CData', toolbarIconData.panCData,...
                'offCallback', 'pan off',...
                'onCallback', @self.PanOn);
            uipushtool(...
                'parent', toolbar,...
                'CData', toolbarIconData.DCMButtonData,...
                'tooltipString', 'Show empty DCM tabs',...
                'clickedCallback', @self.ShowEmptyDCMTabs,...
                'separator', 'on');
        end
        
        function CreateStatusBar(self)
            self.AnalysisStatusBar = UserInterfacePkg.StatusBar([0 0 1 .025]);
            self.AddCustomControl(self.AnalysisStatusBar);
            self.AnalysisStatusBar.AddListener(UserInterfacePkg.StatusEventClass.Instance(), 'StatusChange');
            self.AnalysisStatusBar.Show();
        end
        
        function InitializeEvents(self)
            set(self.ControlHandle, 'KeyPressFcn', @self.NotifyKeyPress);
            set(self.ControlHandle, 'KeyReleaseFcn', @self.NotifyKeyRelease);
            
            set(self.ControlHandle, 'WindowButtonMotionFcn', @self.NotifyMouseMotion)
            set(self.ControlHandle, 'WindowButtonUpFcn', @self.NotifyMouseUp)
            set(self.ControlHandle, 'WindowButtonDownFcn', @self.NotifyMouseDown)
        end
        
        function NotifyKeyPress(self, ~, eventData)
            UserInterfacePkg.KeyEventClass.Instance().NotifyKeyPressed(UserInterfacePkg.KeyPressedEventData(eventData));
        end
        
        function NotifyKeyRelease(self, ~, eventData)
            UserInterfacePkg.KeyEventClass.Instance().NotifyKeyReleased(UserInterfacePkg.KeyPressedEventData(eventData));
        end
        
        function NotifyMouseMotion(self, varargin)
            UserInterfacePkg.MouseEventClass.Instance().NotifyMouseMotion();
        end
        
        function NotifyMouseUp(self, varargin)
            UserInterfacePkg.MouseEventClass.Instance().NotifyMouseUp();
        end
        
        function NotifyMouseDown(self, varargin)
            UserInterfacePkg.MouseEventClass.Instance().NotifyMouseDown();
        end
        
        function LoadSignal(self, varargin)
            fileTypeFilters = AFAnalysis.FILETYPEFILTER;
            mappedFiletypeFilters = fileTypeFilters(self.MappedFileTypeIndices, :);
            
            [fileName, pathName, filterIndex] = HelperFunctions.customUigetfile(...
                mappedFiletypeFilters,...
                'Select a signal file');
            if isequal(fileName, 0), return; end
            
            mappedFilterIndex = self.MappedFileTypeIndices(filterIndex);
            if filterIndex > 1
                fileTypeIndices = 1:size(AFAnalysis.FILETYPEFILTER, 1);
                fileTypeIndices(mappedFilterIndex) = [];
                self.MappedFileTypeIndices = [mappedFilterIndex, fileTypeIndices];
            end
            
            switch mappedFilterIndex
                case 1
                    ecgData = InputOutputPkg.ECGReader.ReadPACEMAPBinary(fullfile(pathName, fileName));
                case 2
                    ecgData = InputOutputPkg.ECGReader.ReadIDEEQ(fullfile(pathName, fileName));
                case 4
                    ecgData = InputOutputPkg.ECGReader.ReadBasketExportData(fullfile(pathName, fileName));
                case 5
                    ecgData = InputOutputPkg.ECGReader.ReadPACEMAPBinarySeries(fullfile(pathName, fileName));
                case 6
                    ecgData = InputOutputPkg.ECGReader.ReadPACEMAPEcg(fullfile(pathName, fileName));
                case 3
                    ecgData = InputOutputPkg.ECGReader.ReadDCMBDF(fullfile(pathName, fileName));
                    ecgData.SetElectrodeMap([]);
                case 7
                    fileData = load(fullfile(pathName, fileName));
                    ecgData = fileData.ecgData;
                    ecgData.Filename = fullfile(pathName, fileName);
                    if isempty(ecgData.ElectrodePositions)
                        ecgData.SetElectrodeMap([]);
                    end
                otherwise
                    errordlg('File Error', 'Unkown file type');
                    return;
            end
            
            self.EcgData = ecgData;
            self.SetFilename();
            self.InitializeTabs();
            
            self.ChannelSelectionView.SetECGData(self.EcgData);
        end
        
        function LoadAnalysis(self, varargin)
            try
                [fileName, pathName, filterIndex] = HelperFunctions.customUigetfile({...
                    '*.mat', 'Any analysis file';...
                    '*_PP.mat', 'Preprocessed file';...
                    '*_QRST.mat', 'QRST cancelllation file';...
                    '*_TM.mat', 'Template matching file';...
                    '*_DD.mat', 'Deflection detection file';...
                    '*_WM.mat', 'Wavemap file'},...
                    'Select an analysis file');
                if isequal(fileName, 0), return; end
                
                patterns = {'_PP.mat'; '_QRST.mat'; '_TM.mat'; '_DD.mat'; '_WM.mat'};
                types = {'PP'; 'QRST'; 'TM'; 'DD'; 'WM'};
                
                type = '';
                switch filterIndex
                    case 1
                        for typeIndex = 1:numel(patterns)
                            if ~isempty(strfind(fileName, patterns{typeIndex}))
                                type = types{typeIndex};
                                break;
                            end
                        end
                    otherwise
                        type = types{filterIndex - 1};
                end
                
                if isempty(type)
                    errordlg('File Error', 'Unkown file type');
                        return;
                end
                
                self.ShowEmptyDCMTabs();
                drawnow;
                
                switch(type)
                    case 'PP'
                        self.TabGroup.SelectedTab = self.PreProcessingTab;
                        ecgData = self.PreProcessingView.LoadData(fullfile(pathName, fileName));
                    case 'QRST'
                        self.TabGroup.SelectedTab = self.QRSTCancellationTab;
                        ecgData = self.QRSTCancellationView.LoadData(fullfile(pathName, fileName));
                    case 'TM'
                        self.TabGroup.SelectedTab = self.TemplateMatchingTab;
                        ecgData = self.TemplateMatchingView.LoadData(fullfile(pathName, fileName));
                    case 'DD'
                        self.TabGroup.SelectedTab = self.DeflectionAnalysisTab;
                        ecgData = self.DeflectionAnalysisView.LoadData(fullfile(pathName, fileName));
                    case 'WM'
                        self.TabGroup.SelectedTab = self.WaveMapTab;
                        ecgData = self.WaveMapView.LoadData(fullfile(pathName, fileName));
                end
            catch loadFileError
                errordlg(loadFileError.message, 'File error');
                disp(loadFileError);
            end
            
            self.EcgData = ecgData;
            self.SetFilename();
        end
        
        function SetFilename(self)
            if isempty(self.EcgData), return; end
            
            self.Name = ['AF Analysis - ', self.EcgData.Filename];
        end
        
        function InitializeApplicationSettings(self)
            self.ApplicationSettings = ApplicationSettingsFigure(self.Position);
            self.ApplicationSettings.Create();
            self.ApplicationSettings.Hide();
        end
        
        function ShowApplicationSettings(self, varargin)
            if ~isobject(self.ApplicationSettings)
                self.ApplicationSettings = ApplicationSettingsFigure(self.Position);
                self.ApplicationSettings.Create();
            end
            self.ApplicationSettings.Show();
        end
        
        function ShowWaveAnalysis(self, varargin)
            if isobject(self.WaveAnalysisFigure)
                delete(self.WaveAnalysisFigure);
            end
            
            if ~isobject(self.WaveMapView), return; end
            
            ecgData = self.WaveMapView.EcgData;
            waves = self.WaveMapView.DetectedWaves;
            deflections = self.WaveMapView.IntrinsicDeflectionData;
            
            if (isempty(ecgData) || isempty(waves)), return; end
            
            wavemapAnalysisView = AnalysisUIPkg.WaveStatisticsPanel([0 0 1 1]);
            
            figurePosition = [self.Position(1:2) + self.Position(3:4) / 5, self.Position(3:4) * (3/5)];
            self.WaveAnalysisFigure = UserInterfacePkg.CustomFigure(figurePosition);
            self.WaveAnalysisFigure.Create();
            self.WaveAnalysisFigure.Name = 'Wave Analysis';
            self.WaveAnalysisFigure.AddCustomControl(wavemapAnalysisView);
            wavemapAnalysisView.Show();
            
            wavemapAnalysisView.SetWavemapData(ecgData, waves, deflections);
        end
        
        function ShowPropertyMaps(self, varargin)
            if isobject(self.PropertyMapFigure)
                delete(self.PropertyMapFigure);
            end
            
            if ~isobject(self.DeflectionAnalysisView), return; end
            
            ecgData = self.DeflectionAnalysisView.EcgData;
            intrinsicDeflectionData = self.DeflectionAnalysisView.GetIntrinsicDeflections();
            
            if (isempty(ecgData) || isempty(intrinsicDeflectionData)), return; end
            
            propertyMapView = AnalysisUIPkg.PropertyMapPanel([0 0 1 1]);
            
            figurePosition = [self.Position(1:2) + self.Position(3:4) / 5, self.Position(3:4) * (3/5)];
            self.PropertyMapFigure = UserInterfacePkg.CustomFigure(figurePosition);
            self.PropertyMapFigure.Create();
            self.PropertyMapFigure.Name = 'Property Maps';
            self.PropertyMapFigure.AddCustomControl(propertyMapView);
            propertyMapView.Show();
            
            propertyMapView.SetECGData(ecgData, intrinsicDeflectionData);
        end
        
        function ShowBatchAnalysis(self, varargin)
            if isobject(self.BatchAnalysisFigure)
                delete(self.BatchAnalysisFigure);
            end
            
            screenSize = get(0,'Screensize');
            figurePosition = [screenSize(3) / 5, screenSize(4) / 5, 2 * screenSize(3) / 5, 4 * screenSize(4) / 5];
            self.BatchAnalysisFigure = BatchAnalysisPkg.BatchAnalysisFigure(figurePosition);
            self.BatchAnalysisFigure.Create();
        end
        
        function SetParallelProcessing(self, source, varargin)
            if self.ParallelProcessing
                self.ParallelProcessing = false;
                set(source, 'checked', 'off');
                
                if verLessThan('matlab', '7.0.1')
                    if matlabpool('size') > 0
                        matlabpool('close');
                    end
                else
                    delete(gcp('nocreate'));
                end
            else
                self.ParallelProcessing = true;
                set(source, 'checked', 'on');
                
                if verLessThan('matlab', '7.0.1')
                    if matlabpool('size') == 0
                        matlabpool('open');
                    end
                else
                    if isempty(gcp('nocreate'))
                        parpool;
                    end
                end
            end
        end
        
        function PanOn(self, varargin)
            set(self.ZoomToggleButton, 'state', 'off');
            pan on;
        end
        
        function ZoomOn(self, varargin)
            set(self.PanToggleButton, 'state', 'off');
            zoom on;
        end
        
        function HandleChannelSelectionEvent(self, varargin)
            disp('Channels selected');
            
            ecgData = self.ChannelSelectionView.GetECGDataToAnalyze();
            
            self.PaceBlankingView.SetECGData(ecgData);
        end
        
        function HandlePaceBlankingEvent(self, varargin)
            disp('Pace detection completed');
            
            blankedECGData = self.PaceBlankingView.GetBlankedECGData();
            
            self.PreProcessingView.SetECGData(blankedECGData);
        end
        
        function HandlePreProcessingEvent(self, varargin)
            disp('Pre-processing completed');
            
            ecgData = self.PreProcessingView.GetPreProcessedECGData();
            
            if strcmp(self.GUIMode, 'Research')
                if isa(self.EcgData, 'DCMData')
                    self.BipolarLaplacianView.SetECGData(ecgData);
                end
            end
            
            self.QRSTCancellationView.SetECGData(ecgData);
        end
        
        function HandleQRSTCancellationEvent(self, varargin)
            disp('QRST cancellation completed');
            
            ecgData = self.QRSTCancellationView.GetCancelledECGData();
                self.TemplateMatchingView.SetECGData(ecgData);
                self.DeflectionAnalysisView.SetECGData(ecgData);
                
            if strcmp(self.GUIMode, 'Research')
                if isa(self.EcgData, 'DCMData')
                    self.BipolarLaplacianView.SetECGData(self.QRSTCancellationView.EcgData);
                end
            end
        end
        
        function HandleTemplateMatchingEvent(self, varargin)
            disp('Template matching completed');
            
            self.DeflectionAnalysisView.SetECGData(self.TemplateMatchingView.EcgData);
            templateMatchingResult = self.TemplateMatchingView.GetTemplateMatchingResult();
            self.DeflectionAnalysisView.SetTemplateMatchingResult(templateMatchingResult);
        end
        
        function HandleIntrinsicDeflectionEvent(self, varargin)
            disp('Intrinsic deflections determined');
            
            intrinsicDeflectionData = self.DeflectionAnalysisView.GetIntrinsicDeflections();
            
            if strcmp(self.GUIMode, 'Research')
                self.FractionationAnalysisView.SetECGData(self.DeflectionAnalysisView.EcgData);
                self.FractionationAnalysisView.SetDeflectionData(intrinsicDeflectionData);
            end
            
            if isobject(self.WaveMapView)
                self.WaveMapView.SetECGData(...
                    self.DeflectionAnalysisView.EcgData,...
                    intrinsicDeflectionData);
            end
        end
        
        function HandleWavemapEvent(self, varargin)
            disp('Wavemap computed');
            
            wavemapData = struct(...
                'ecgData', self.WaveMapView.EcgData,...
                'detectedWaves', self.WaveMapView.DetectedWaves,...
                'deflectionData', {self.WaveMapView.IntrinsicDeflectionData});
            
            if strcmp(self.GUIMode, 'Research')
                self.BipolarLaplacianView.SetWavemapData(wavemapData);
            end
        end
        
        function HandleLaplacianEvent(self, varargin)
            disp('Laplacian ECG computed');
            
            ecgData = self.BipolarLaplacianView.GetLaplacianECG();
            if isempty(ecgData), return; end
            
            self.TemplateMatchingView.SetECGData(ecgData);
        end
        
        function Close(self, varargin)
            self.RemoveTabs();
            
            self.EcgData = [];
            delete(self.ApplicationSettings);

            if ishandle(self.ApplicationSettings)
                delete(self.ApplicationSettings);
            end
            clear self.ApplicationSettings;
            
            a = ApplicationSettings.Instance();
            delete(a);

            delete(self.ControlHandle);
        end
    end
    
    methods (Static)
        function ecgData = LoadBDFData(filename)
            ecgData = InputOutputPkg.readBDF(filename);
        end
        
        function ecgData = LoadPACEMAPEcg(filename)
            ecgData = InputOutputPkg.readEcgFile(filename);
        end
    end
end
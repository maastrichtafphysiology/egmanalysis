classdef  InterpolatedDeflectionMapControl < ExtendedUIPkg.GridActivationMapControl
    properties
        InterpolationFactor
    end
    
    properties (Access = private)
        XGrid
        YGrid
        NewXGrid
        NewYGrid
        MissingElectrodes
        Mask
    end
    
    methods
        function self = InterpolatedDeflectionMapControl(position)
            self = self@ExtendedUIPkg.GridActivationMapControl(position);
            
            self.Colormap = hot(self.Resolution);
            self.InterpolationFactor = 1;
            self.TimeLag = 50;
        end
        
        function colorData = GetFrame(self, framePosition)
            timePosition = 1000 * framePosition / self.SamplingFrequency;
            activationRange = [(timePosition - self.TimeLag), timePosition];
            
%             activations = cellfun(@(x) GetLastActivationWithinRange(x, activationRange), self.Activations);
            numberOfActivations = size(self.Activations, 1);
            activations = NaN(numberOfActivations, 1);
            for channelIndex = 1:numberOfActivations
                activationPosition = self.Activations{channelIndex} > activationRange(1) & self.Activations{channelIndex} <= activationRange(2);
                if any(activationPosition)
                    activation = self.Activations{channelIndex}(activationPosition);
                    activations(channelIndex) = activation(end);
                end
            end
            relativeColors = (timePosition - activations) / self.TimeLag;
%             validActivations = ~isnan(relativeColors);
            frameGrid = zeros(size(self.XGrid));
            frameGrid(self.ElectrodeImageIndices) = relativeColors;
            frameGrid(self.MissingElectrodes) = 1;
            interpolatedFrameGrid = frameGrid;
            if self.InterpolationFactor > 1
                try %#ok<TRYNC>
                    interpolatedFrameGrid = self.Mask .* interp2(self.XGrid, self.YGrid, frameGrid,...
                        self.NewXGrid, self.NewYGrid, 'linear');
                end
            end
            
            validValues = ~isnan(interpolatedFrameGrid);
            
            relativeColors = interpolatedFrameGrid(validValues);
            relativeColors(isnan(relativeColors)) = 1;
            relativeColors = round(relativeColors * (self.Resolution - 1)) + 1;
            colors = ind2rgb(relativeColors, self.Colormap);
            
            cData = ones([size(self.NewXGrid), 3]);
            rData = ones(size(cData, 1), size(cData, 2));
            rData(validValues) = colors(:, 1);
            
            gData = ones(size(cData, 1), size(cData, 2));
            gData(validValues) = colors(:, 2);
            
            bData = ones(size(cData, 1), size(cData, 2));
            bData(validValues) = colors(:, 3);
            
            colorData = cat(3, rData, gData, bData);
            
            function activation = GetLastActivationWithinRange(activations, range)
                %                 activationPosition = find(activations > range(1) & activations <= range(end), 1, 'last');
                %                 if isempty(activationPosition)
                %                     activation = NaN;
                %                 else
                %                     activation = activations(activationPosition);
                %                 end
                activation = NaN;
                activationPosition = activations > range(1) & activations <= range(2);
                if any(activationPosition)
                    activation = activations(activationPosition);
                    activation = activation(end);
                end
            end
        end
    end
    
    methods (Access = protected)
        function InitializeAxes(self)
            maxPosition = max(self.Positions);
            minPosition = min(self.Positions);
            minimumSpacing = self.ComputeMinimumSpacing();
            
            set(self.MapAxes,...
                'xlim', [minPosition(1) - minimumSpacing, maxPosition(1) + minimumSpacing],...
                'ylim', [minPosition(2) - minimumSpacing, maxPosition(2) + minimumSpacing]);
            set(self.MapAxes, 'XLimMode', 'manual', 'YLimMode', 'manual');
            
            xData = minPosition(1):minimumSpacing:maxPosition(1);
            yData = minPosition(2):minimumSpacing:maxPosition(2);
            
            newXData = linspace(xData(1), xData(end), numel(xData) * self.InterpolationFactor);
            newYData = linspace(yData(1), yData(end), numel(yData) * self.InterpolationFactor);
            [self.NewXGrid, self.NewYGrid] = meshgrid(newXData, newYData);
            
            tolerance = 1e6 * eps;
            [xMembers, xIndices] = UtilityPkg.ismemberf(self.Positions(:, 1), xData, 'tol', tolerance); %#ok<ASGLU>
            [yMembers, yIndices] = UtilityPkg.ismemberf(self.Positions(:, 2), yData, 'tol', tolerance); %#ok<ASGLU>
            self.ElectrodeImageIndices = sub2ind([numel(yData), numel(xData)], yIndices, xIndices);
            
            [self.XGrid, self.YGrid] = meshgrid(xData, yData);
            exampleFrame = NaN(size(self.XGrid));
            exampleFrame(self.ElectrodeImageIndices) = 1;
            self.MissingElectrodes = isnan(exampleFrame);
            self.Mask = interp2(self.XGrid, self.YGrid, exampleFrame, self.NewXGrid, self.NewYGrid, 'nearest');
        
            colors = ones(numel(newYData), numel(newXData), 3);
            self.ActivationImage = image(...
                'xData', newXData,...
                'yData', newYData,...
                'cData', colors,...
                'hitTest', 'off',...
                'parent', self.MapAxes);
            grid(self.MapAxes, 'minor');
            
            self.ElectrodeTextHandles = NaN(size(self.Positions, 1), 1);
            
            axis(self.MapAxes, 'image');
            set(self.MapAxes, 'yDir', 'reverse');
            set(self.MapAxes, 'clim', [0 1]);
            
            set(self.MapAxes, 'LooseInset', get(self.MapAxes, 'TightInset'));
        end 
    end
end
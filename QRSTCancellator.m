classdef QRSTCancellator < QRSTDetector
    properties
        CancellationMethod
        QRSTRange   % in fraction before/after
        ClusterSensitivity
        TemplateShift
        HighpassPostFilter
    end
    
    properties (Access = protected)
        InterpolationFactor
        MiniumClusterSize
    end
    
    methods
        function self = QRSTCancellator()
            self = self@QRSTDetector();
            
            self.InterpolationFactor = 4;
            self.HighpassPostFilter = 1;
            self.MiniumClusterSize = 20;
        end
        
        function cancelledEcgData = CancelQRST(self, ecgData, rWaveIndices, memberIndices)
            if nargin < 4 || isempty(memberIndices)
                memberIndices = ones(numel(rWaveIndices), 1);
            end
            
            self.SamplingFrequency = ecgData.SamplingFrequency;
            data = ecgData.Data;
%             referenceChannel = ecgData.ReferenceData(:, self.ReferenceChannelIndex);
            
            [rWaveIndices, indexInOriginal] = unique(rWaveIndices);
            memberIndices = memberIndices(indexInOriginal);
            
            % interpolation
%             self.InterpolationFactor = 4;
            if self.InterpolationFactor > 1
                self.SamplingFrequency = ecgData.SamplingFrequency * self.InterpolationFactor;
                data = ecgData.Data;
                time = ecgData.GetTimeRange();
                interpolatedTime = time(1):(1 / self.SamplingFrequency):time(end);
                data = spline(time, data', interpolatedTime)';
%                 referenceChannel = spline(time, referenceChannel', interpolatedTime)';
                rWaveIndices = (rWaveIndices - 1) * self.InterpolationFactor + 1;
            end
            % end interpolation
            
            rrMin = median(diff(rWaveIndices));
            windowStart= -round(rrMin * self.QRSTRange(1));
            windowStop = round(rrMin * self.QRSTRange(2));
            windowIndices = windowStart:windowStop;
            rWaveWindowIndices = bsxfun(@plus,...
                repmat(windowIndices(:), 1, numel(rWaveIndices)),...
                rWaveIndices(:)');
            
            data = self.CancelQRSTMethod(data, ecgData, rWaveWindowIndices, memberIndices);
            
            if self.HighpassPostFilter > 0
                data = self.FilterCancelledSignal(data, self.SamplingFrequency / 2);
            end
            
            % downsample
            cancelledEcgData = ecgData.Copy();
            cancelledEcgData.Data = downsample(data, self.InterpolationFactor);
            
            self.SamplingFrequency = ecgData.SamplingFrequency;
        end
        
        function rWaveWindowIndices = GetRWaveWindowIndices(self, rWaveIndices)
            rrMin = median(diff(rWaveIndices));
            windowStart= -round(rrMin * self.QRSTRange(1));
            windowStop = round(rrMin * self.QRSTRange(2));
            windowIndices = windowStart:windowStop;
            rWaveWindowIndices = bsxfun(@plus,...
                repmat(windowIndices(:), 1, numel(rWaveIndices)),...
                rWaveIndices(:)');
        end
        
        function filteredSignal = FilterCancelledSignal(self, signal, nyquistFrequency)
             if self.HighpassPostFilter > 0
                 [b, a] = cheby2(3, 20,...
                self.HighpassPostFilter / nyquistFrequency, 'high');
                filteredSignal = filtfilt(b, a, signal);
             else
                 filteredSignal = signal;
             end 
        end
    end
    
    methods (Access = protected, Abstract)
        data = CancelQRSTMethod(self, data, ecgData, rWaveWindowIndices, memberIndices);
    end
    
    methods (Access = protected)
        function cancelledSignal = ComputeABSCancellation(self, signal, rWaveWindowIndices)
            cancelledSignal = signal;
            
            validWindows = rWaveWindowIndices(1, :) > 0 &...
                rWaveWindowIndices(end, :) <= numel(signal);
            qrstWindows = signal(rWaveWindowIndices(:, validWindows));
            qrstWindows = bsxfun(@minus, qrstWindows, mean(qrstWindows));
            qrstAverage = mean(qrstWindows, 2);
            
            for windowIndex = 1:size(rWaveWindowIndices, 2)
                currentWindowIndices = rWaveWindowIndices(:, windowIndex);
                validPositions = currentWindowIndices > 0 & currentWindowIndices <= numel(signal);
                cancelledSignal = self.SubtractQRST(cancelledSignal, qrstAverage(validPositions), currentWindowIndices(validPositions));
            end
        end
        
        function cancelledSignal = ComputeSVDCancellation(self, signal, rWaveWindowIndices)
            cancelledSignal = signal;
            
            validWindows = rWaveWindowIndices(1, :) > 0 &...
                rWaveWindowIndices(end, :) <= numel(signal);
            qrstWindows = signal(rWaveWindowIndices(:, validWindows));
            qrstWindows = bsxfun(@minus, qrstWindows, mean(qrstWindows));
            [u, s, ~] = svd(qrstWindows);
            qrstComponent = u(:, 1) * s(1);
            
            for windowIndex = 1:size(rWaveWindowIndices, 2)
                currentWindowIndices = rWaveWindowIndices(:, windowIndex);
                validPositions = currentWindowIndices > 0 & currentWindowIndices <= numel(signal);
                cancelledSignal = self.SubtractQRST(cancelledSignal, qrstComponent(validPositions), currentWindowIndices(validPositions));
            end
        end
        
        function cancelledSignal = ComputeClusteredABSCancellation(self, signal, rWaveWindowIndices, clusterMemberIndices)
            cancelledSignal = signal;
            
            validWindows = rWaveWindowIndices(1, :) > 0 &...
                rWaveWindowIndices(end, :) <= numel(signal);
            
            qrstWindows = signal(rWaveWindowIndices(:, validWindows));
            qrstWindows = bsxfun(@minus, qrstWindows, mean(qrstWindows));
            
            numberOfClusters = numel(unique(clusterMemberIndices));
            clusterCentroids = NaN(size(rWaveWindowIndices, 1), numberOfClusters);
            minimumClusterSize = self.MiniumClusterSize;
            
            for clusterIndex = 1:numberOfClusters
                clusterMembers = clusterMemberIndices(validWindows) == clusterIndex;
                if any(clusterMembers)
                    if sum(clusterMembers) >= minimumClusterSize
                        [u, s, ~] = svd(qrstWindows(:, clusterMembers));
                        qrstComponent = u(:, 1) * s(1);
                    else
                        qrstComponent = trimmean(qrstWindows(:, clusterMembers), 10, 2);
                    end
                    clusterCentroids(:, clusterIndex) = qrstComponent;
                end
            end
            
            for windowIndex = 1:size(rWaveWindowIndices, 2)
                currentWindowIndices = rWaveWindowIndices(:, windowIndex);
                validPositions = currentWindowIndices > 0 & currentWindowIndices <= numel(signal);
                currentWindowIndices = currentWindowIndices(validPositions);
                qrstAverage = clusterCentroids(validPositions, clusterMemberIndices(windowIndex));
                cancelledSignal = self.SubtractQRST(cancelledSignal, qrstAverage, currentWindowIndices);
            end
        end
        
        function cancelledSignal = ComputeBayesianCancellation(self, signal, rWaveWindowIndices)
            cancelledSignal = signal;
            numberOfElectrodes = size(signal, 2);
            numberOfComponents = 5;
            [windowLength, numberOfWindows] = size(rWaveWindowIndices);
            
            validWindows = rWaveWindowIndices(1, :) > 0 &...
                rWaveWindowIndices(end, :) <= size(signal, 1);
            
            % X = [x1 ... xJ]
            qrstAverages = NaN(windowLength, numberOfElectrodes);
            for electrodeIndex = 1:numberOfElectrodes
                electrodeSignal = signal(:, electrodeIndex);
                qrstWindows = electrodeSignal(rWaveWindowIndices(:, validWindows));
                qrstWindows = bsxfun(@minus, qrstWindows, mean(qrstWindows));
                qrstAverages(:, electrodeIndex) = mean(qrstWindows, 2);
            end
            
            % A = [1 n]
            knownVectors = [ones(windowLength, 1), (1:windowLength)'];
            [U, S, V] = svd((eye(windowLength) - knownVectors * pinv(knownVectors)) * qrstAverages);
            % H = [K A]
            H = [U(:, 1:numberOfComponents) knownVectors];
            
            % LS estimated noise & covariance estimation
            pinvH = pinv(H);
            numberOfValidWindows = numel(find(validWindows));
            autoCorrelation = zeros(2 * windowLength - 1, numberOfElectrodes);
            for electrodeIndex = 1:numberOfElectrodes
                for windowIndex = 1:numberOfWindows
                    if validWindows(windowIndex)
                        currentWindowIndices = rWaveWindowIndices(:, windowIndex);
                        lsEstimate = signal(currentWindowIndices, electrodeIndex) - H * pinvH * signal(currentWindowIndices, electrodeIndex);
                        autoCorrelation(:, electrodeIndex) = autoCorrelation(:, electrodeIndex) +...
                            xcorr(lsEstimate, 'biased') ./ numberOfValidWindows;
                    end
                end
            end
            
            % BLUE estimate
            for electrodeIndex = 1:numberOfElectrodes
                correlationEstimate = autoCorrelation(:, electrodeIndex);
                Cb = toeplitz(correlationEstimate(windowLength:end) ./ windowLength);
                estimationMatrix = H * ((H' * (Cb \ H)) \ (H' / Cb));
                for windowIndex = 1:numberOfWindows
                    currentWindowIndices = rWaveWindowIndices(:, windowIndex);
                    validPositions = currentWindowIndices > 0 & currentWindowIndices <= numel(signal(:, electrodeIndex));
%                     cancelledSignal(currentWindowIndices(validPositions), electrodeIndex) = signal(currentWindowIndices(validPositions), electrodeIndex) -...
%                         estimationMatrix(validPositions, validPositions) * signal(currentWindowIndices(validPositions), electrodeIndex);
                    cancelledSignal(:, electrodeIndex) = self.SubtractQRST(cancelledSignal(:, electrodeIndex),...
                        estimationMatrix(validPositions, validPositions) * signal(currentWindowIndices(validPositions), electrodeIndex),...
                        currentWindowIndices(validPositions));
                end
            end
        end
        
        function subtractedSignal = SubtractQRST(self, signal, qrstTemplate, windowIndices)
            delta = ceil(self.TemplateShift * self.SamplingFrequency);
            r = xcorr(signal(windowIndices), qrstTemplate, delta, 'coeff');
            [~, position] = max(abs(r));
            
            shift = position - delta - 1;
            %             shift = 0;
            
            windowIndices = windowIndices + shift;
            validIndices = windowIndices > 0 & windowIndices <= numel(signal);
            
            currentWindowIndices = windowIndices(validIndices);
            currentQrstTemplate = qrstTemplate(validIndices);
            
            covariance = cov(currentQrstTemplate, signal(currentWindowIndices));
            amplification = covariance(2) / covariance(1);
            
%             baseline = - amplification * mean(currentQrstTemplate);
            baseline = 0;
            
            qrstTemplateLength = numel(currentQrstTemplate);
            
            qrstRamp = round(qrstTemplateLength / 5);
            qrstWindow = ones(size(currentQrstTemplate));
            
            qrstWindow(1:qrstRamp) = 0:(1 / (qrstRamp - 1)):1;
            qrstWindow((end - qrstRamp + 1):end) = 1:(-1 / (qrstRamp - 1)):0;
            
            qrstContribution = baseline + qrstWindow .* (amplification * currentQrstTemplate);
            
            currentWindowIndices = uint32(currentWindowIndices);
            windowStart = currentWindowIndices(1);
            windowEnd = currentWindowIndices(end);
            
            subtractedSignal = signal;
            subtractedSignal(windowStart:windowEnd) = signal(windowStart:windowEnd) - qrstContribution;
            
            %            if self.ShowDiagnostics
%                             figure;
%                             plot(signal(currentWindowIndices), 'k');
%                             hold on
%                             plot(subtractedSignal(currentWindowIndices), 'r');
%                             plot(qrstContribution, 'b--');
%                             plot(currentQrstTemplate, 'k--');
            %                 pause;
            %             end
        end
    end
    
    methods (Static)
        function re2 = byest(Y,R,Fs,leadAF,nbvec)
            
            % Y = n-leads input signal (signals organized on rows, samples on columns
            % (Y = (n x T) matrix))
            % R = R-wave time instant series (vector containing time instants of all R
            % waves in the signal)
            % Fs = sampling frequency
            % leadAF = number of the lead we want to obtain the AA extraction
            % (generally V1)
            % nbvec = number of principal components considered
            Y = Y';
            [ro,co] = size(Y);
            nblead = min(ro,co);
            if nargin < 5 |isempty(nbvec)
                nbvec = 5;%5
            end
            
            for i = 1:length(R)-1
                RR(i) = R(i+1)-R(i);
            end
            
            indmin = floor(0.04*Fs);
            indmax = floor(min(RR))-indmin-1;  % oppure finestra di 400 ms???
            
            if nargin < 4
                leadAF = 1;
            end
            
            if R(1)-indmin <= 0
                R(1) = [];
            end
            
            X = zeros(nblead,indmax+indmin+1);
            for k = 1:length(R)
                if R(k)+indmax < length(Y)
                    X = X + Y(:,R(k)-indmin:R(k)+indmax);
                else  X = X + [Y(:,R(k)-indmin:end) zeros(nblead,length(X)-length(Y(:,R(k)-indmin:end)))];
                end
            end
            X = X./length(R);
            
            [U,S,V] = svd(X);
            
            M = V(:,1:nbvec);
            
            X = X';
            
            Un=ones(indmax+indmin+1,1)/sqrt(indmax+indmin+1);
            A=[Un [1:indmax+indmin+1]'];                      % A = [A n]
            [U1,S,V]=svd((eye(indmax+indmin+1)-A*pinv(A))*X);
            M1 =[U1(:,1:nbvec) A];
            
            K = M1;     % K = [Ka A]
            
            % iqrs = find(kurtosis(U1)-3>2 & kurtosis(U1)-3<20);
            % iqrs
            % K = [U1(:,iqrs) A];
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % LS estimated noise
            
            IK = pinv(K);
            ri = [];
            
            L = length(R)-1;
            
            for ll = 1:L
                SI = Y(:,R(ll)-indmin:R(ll)+indmax)';
                SIest = K*IK*SI;
                AF = SI-SIest;
                ri = [ri AF(:,leadAF)];
            end
            
            rb=[Y(leadAF,1:R(1)-indmin-1)';ri(:,1)];
            for k=2:L,
                rb=[rb;Y(leadAF,R(k-1)+indmax+1:R(k)-indmin-1)';ri(:,k)];
            end
            
            rb=[rb' Y(leadAF,length(rb)+1:length(Y))];
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % covariance matrix estimation
            
            c = zeros((indmax+indmin)*2+1,1);
            LL = L;
            
            for k = 1:LL
                c = c + xcorr(ri(:,k),'biased');
            end
            
            c = c/LL;
            
            
            %prototype = input('use of prototype? ','s');
            %if prototype == 'y'
            %    xc=xcorr(diag(eye((indmax+indmin+1))-K*pinv(K)),'biased'); PROTOTIPO
            %    cll=c.*[[1:indmax+indmin]';[indmax+indmin+1:-1:1]']./xc;
            %    C=toeplitz(cll(indmax+indmin+1:2*(indmax+indmin)+1)/(indmax+indmin+1));
            %    IC=inv(toeplitz(cll(indmax+indmin+1:2*(indmax+indmin)+1)/(indmax+indmin+1)));
            %else
            IC=inv(toeplitz(c(indmax+indmin+1:2*(indmax+indmin)+1)/(indmax+indmin+1)));
            %end
            
            % BLUE estimation
            
            rib = [];
            IK = inv(K'*IC*K)*K'*IC;
            
            for ll = 1:LL
                SI = Y(:,R(ll)-indmin:R(ll)+indmax)';
                SIest = K*IK*SI;
                AF = SI-SIest;
                rib = [rib AF(:,leadAF)];
            end
            
            
            %reconstitution du signal AF continu
            re=[Y(leadAF,1:R(1)-indmin-1)';rib(:,1)];
            for k=2:L,
                re=[re;Y(leadAF,R(k-1)+indmax+1:R(k)-indmin-1)';rib(:,k)];
            end
            
            re=[re' Y(leadAF,length(re)+1:length(Y))];
            
            %reconstitution avec continuite aux points de raccordement
            re2=[Y(leadAF,1:R(1)-indmin-1)';rib(:,1)];
            for k=2:L,
                c1=rib(indmin+indmax+1,k-1);
                c2=rib(1,k);
                N=length((R(k-1)+indmax:R(k)-indmin));
                bet=(Y(leadAF,R(k)-indmin)-Y(leadAF,R(k-1)+indmax)-c2+c1)/(N-1);
                alp=Y(leadAF,R(k-1)+indmax)-c1-bet;
                vec=Y(leadAF,R(k-1)+indmax:R(k)-indmin)-(alp+bet*[1:N]);
                re2=[re2;vec(2:end-1)';rib(:,k)];
            end
            
            re_use = re2;
            re2=[re2; Y(leadAF,length(re2)+1:length(Y))'];
            
            % size(re2)
            % size(length(re2)+1:length(Y))
            % size(Y(leadAF,R(LL)+indmax+1:end))
            
            re2 = re2';
            % re2(length(re2)+1:length(Y)) = Y(leadAF,length(re2)+1:length(Y));
        end
    end
end
classdef DeflectionECGChannelPanel < ECGChannelPanel
    properties (Access = protected)
        DeflectionLineHandles
        DeflectionData
    end
    
    methods
        function self = DeflectionECGChannelPanel(position)
            self = self@ECGChannelPanel(position);
        end
        
        function SetDeflectionData(self, deflectionData)
            self.DeflectionData = deflectionData;
            self.ShowDeflections();
        end
    end
    
    methods (Access = protected)
        function ShowECGData(self)
            ShowECGData@ECGChannelPanel(self);
            
            self.ShowDeflections();
        end
        
        function ShowDeflections(self)
            validHandles = ishandle(self.DeflectionLineHandles);
            if any(validHandles)
                delete(self.DeflectionLineHandles(validHandles));
            end
            
            if isempty(self.DeflectionData), return; end
            
            lineData = self.EcgData.Data(:, self.SelectedChannels);
            resultData = self.DeflectionData.AnalysisResults(self.SelectedChannels);
            time = self.EcgData.GetTimeRange();
            self.DeflectionLineHandles = NaN(size(resultData));
            templateColor = [1 0 0];
            for channelIndex = 1:numel(resultData)
                currentResult = resultData{channelIndex};
                signal = lineData(:, channelIndex);
                
                try
                templates = currentResult.templates;
                ranges = currentResult.templatePeakRanges;
                coefficients = currentResult.templatePeakCoefficients;
                templatePeakTemplates = currentResult.templatePeakTemplates;
                
                [xData, yData] = DeflectionECGChannelPanel.ComputeDeflectionLineData(...
                    time, signal, templates, templatePeakTemplates, ranges, coefficients);
                
                self.DeflectionLineHandles(channelIndex) = line('xData', xData, 'yData', yData,...
                    'color', templateColor,...
                    'lineWidth', 2,...
                    'parent', self.AxesHandles(channelIndex),...
                    'lineSmoothing', 'on');
                catch
                end
            end
        end
    end
    
    methods (Static)
        function [xData, yData] = ComputeDeflectionLineData(time, signal, templates, templatePeakTemplates, ranges, coefficients)
            xData = [];
            yData = [];
            
            coefficients = coefficients(:);
            for i = 1 : numel(templatePeakTemplates(:,1))
                xData = [xData time(ranges(i, 1):ranges(i, 2)) NaN]; %#ok<AGROW>
                yData = [yData coefficients(i, 1) * templates{templatePeakTemplates(i, 1), templatePeakTemplates(i, 2)}...
                    + mean(signal(ranges(i, 1):ranges(i, 2))) - coefficients(i, 1)...
                    * mean(templates{templatePeakTemplates(i, 1), templatePeakTemplates(i, 2)}) NaN]; %#ok<AGROW>
            end
        end
        
        function EnableDeflections(deflectionLineHandles, enable)
            validHandles = ishandle(deflectionLineHandles);
            if any(validHandles)
               if enable
                   set(deflectionLineHandles(validHandles), 'visible', 'on');
               else
                   set(deflectionLineHandles(validHandles), 'visible', 'off');
               end
            end
        end
    end
end
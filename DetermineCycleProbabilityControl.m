classdef DetermineCycleProbabilityControl < UserInterfacePkg.CustomFigure
    properties (Access = protected)
        DeflectionDetectionResults
        EliminatedLines
        FilteredAnalysisResults
        CycleLengths
        Histogram
        OptimizationPanel
        
        CloseButton
        UpdateButton
    end
    
    methods
        function self = DetermineCycleProbabilityControl(position, analysisResults, eliminatedLines)
            self = self@UserInterfacePkg.CustomFigure(position);
            self.DeflectionDetectionResults = analysisResults;
            self.EliminatedLines = eliminatedLines;
        end
        
        function Create(self)
            Create@UserInterfacePkg.CustomFigure(self);
            
            self.BuildGUI();
            self.CreateEventNotifiers();
            self.UpdateCycleLengths();
        end
        
        function Show(self)
            Show@UserInterfacePkg.CustomFigure(self);
        end
    end
	
	methods (Access = private)
        function BuildGUI(self)
            buttonHeight = 0.03;
            
            self.OptimizationPanel = DeflectionFilterOptimizationPanel([0 0 .25 1]);
            self.OptimizationPanel.Create(self.ControlHandle);
            self.OptimizationPanel.Show();
            
            applicationSettings = ApplicationSettings.Instance();
            longIntervalThreshold = applicationSettings.DeflectionClustering.CycleLengthCutoff;
            intervalBins = 0:(longIntervalThreshold / 50):(longIntervalThreshold * 1.5);
            
            self.Histogram = ExtendedUIPkg.HistogramFitControl.BasicHistogramFitControl(....
                [.25 0 .65 1], 'Interval (s)', intervalBins);
            self.AddCustomControl(self.Histogram);
            
            buttonPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized',...
                'position', [.9 0 .1 1]);
            
            self.UpdateButton = UserInterfacePkg.CustomButtonControl([.1 .8 .8 buttonHeight],...
                @self.UpdateCycleLengths);
            self.UpdateButton.Name = 'Update';
            self.UpdateButton.Create(buttonPanel);
            
            self.CloseButton = UserInterfacePkg.CustomButtonControl([.1 .6 .8 buttonHeight],...
                @self.Close);
            self.CloseButton.Name = 'Close';
            self.CloseButton.Create(buttonPanel);
            
            set(self.ControlHandle, 'CloseRequestFcn', @self.CloseRequest);
        end
        
        function CreateEventNotifiers(self)
            set(self.ControlHandle, 'WindowButtonMotionFcn', @self.NotifyMouseMotion)
            set(self.ControlHandle, 'WindowButtonUpFcn', @self.NotifyMouseUp)
            set(self.ControlHandle, 'WindowButtonDownFcn', @self.NotifyMouseDown)
        end
        
        function NotifyMouseMotion(self, varargin)
            UserInterfacePkg.MouseEventClass.Instance().NotifyMouseMotion();
        end
        
        function NotifyMouseUp(self, varargin)
            UserInterfacePkg.MouseEventClass.Instance().NotifyMouseUp();
        end
        
        function NotifyMouseDown(self, varargin)
            UserInterfacePkg.MouseEventClass.Instance().NotifyMouseDown();
        end
        
        function UpdateCycleLengths(self, varargin)
            templateMatchingOptimizationFilter = AlgorithmPkg.TemplateMatchingOptimizationFilter(...
                    self.OptimizationPanel.Parameters, self.OptimizationPanel.OptimizationProtocol);
            
            settingsInstance = ApplicationSettings.Instance;
            originalSetting = settingsInstance.TemplateMatching.OverlapFilterEnabled;
            settingsInstance.TemplateMatching.OverlapFilterEnabled = false;
            
            self.FilteredAnalysisResults = templateMatchingOptimizationFilter.Apply(self.DeflectionDetectionResults.AnalysisResults);
            
            settingsInstance.TemplateMatching.OverlapFilterEnabled = originalSetting;
            
            self.CycleLengths = DeflectionDetectionResults.CalculateCycleLengths(...
                self.FilteredAnalysisResults, self.EliminatedLines); %#ok<PROPLC>
            
            self.Histogram.Show([], self.CycleLengths / self.FilteredAnalysisResults{1}.samplingFrequency);
        end
        
        function Close(self, varargin)
            [type, beta] = self.Histogram.GetFitTypeAndParameters();
            self.DeflectionDetectionResults.Fit = HistogramFitCollection.FitTypeWithParameters(type, beta);
            
            for i = 1:numel(self.CustomControlCollection)
                delete(self.CustomControlCollection{i});
            end
            delete(gcbf);
        end
        
        function CloseRequest(self, varargin)
            self.Close();
        end
	end
end
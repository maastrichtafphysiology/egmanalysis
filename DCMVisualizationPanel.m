classdef DCMVisualizationPanel < ECGVisualizationPanel
    methods
        function self = DCMVisualizationPanel(position)
            self = self@ECGVisualizationPanel(position);
        end
    end
    
    methods (Access = protected)
        function CreateChannelSelection(self)
            CreateChannelSelection@ECGVisualizationPanel(self);
            
            uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.1 .1 .8 .05],...
                'style', 'pushbutton',...
                'string', 'Mark selected as reference',...
                'callback', @self.AddReferenceChannel);
        end
    end
    
    methods (Access = private)
        function AddReferenceChannel(self, varargin)
            if ~any(self.SelectedChannels), return; end
            
            channelRange = 1:self.EcgData.GetNumberOfChannels();
            previousNumberOfReferenceChannels = numel(self.EcgData.ReferenceLabels);
            
            self.EcgData.AddReferenceChannel(self.SelectedChannels);
            
            selectedValidChannels = ismember(self.ValidChannels, self.SelectedChannels);
            if any(selectedValidChannels)
                self.ValidChannels(selectedValidChannels) = [];
            end
            channelRange(self.SelectedChannels) = [];
            self.ValidChannels = find(ismember(channelRange, self.ValidChannels));
            
            set(self.ValidChannelsListbox,...
                'value', self.ValidChannels,...
                'string', self.EcgData.ElectrodeLabels);
            
            self.SelectedChannels = [];
            set(self.ChannelListbox,...
                'string', self.EcgData.ElectrodeLabels,...
                'value', self.SelectedChannels);
            
            
            self.SelectedLeadChannels = (previousNumberOfReferenceChannels + 1):numel(self.EcgData.ReferenceLabels);
            set(self.LeadListbox,...
                'string', self.EcgData.ReferenceLabels,...
                'value', self.SelectedLeadChannels);
        end
    end
end
classdef FractionationAnalysisPanel < DeflectionECGChannelPanel
    properties (Access = private)
        AnalysisPanel
        TabGroup
        
        DeflectionComplexPanel
        DeflectionComplexAxes
        
        DeflectionAmplitudePanel
        IntrinsicDeflectionAmplitudeAxes
        FarFieldDeflectionAmplitudeAxes
        IntrinsicDeflectionSlopeAxes
        FarFieldDeflectionSlopeAxes
    end
    
    methods
        function self = FractionationAnalysisPanel(position)
            self = self@DeflectionECGChannelPanel(position);
        end
        
        function Create(self, parentHandle)
            Create@DeflectionECGChannelPanel(self, parentHandle);
            
            self.CreateAnalysisPanel();
        end
    end
    
    methods (Access = protected)
        function CreateECGAxes(self)
            CreateECGAxes@DeflectionECGChannelPanel(self);
            
            set(self.AxesPanel, 'position', [.2 .5 .8 .5]);
        end
        
        function CreateAnalysisPanel(self)
            self.AnalysisPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.2 0 .8 .5]);
            
            self.TabGroup = uitabgroup(...
                'parent', self.AnalysisPanel,...
                'position', [0 .025 1 .975]);
            
            deflectionComplexTab = uitab(self.TabGroup, 'title', 'Deflection complexes');
            self.DeflectionComplexPanel = uipanel('parent', deflectionComplexTab,...
                'units', 'normalized', ...
                'position', [0 0 1 1]);
            uicontrol('parent', self.DeflectionComplexPanel,...
                'units', 'normalized',...
                'position', [0 .9 .1 .1],...
                'style', 'pushbutton',...
                'string', 'Compute deflection complexes',...
                'callback', @self.ComputeDeflectionComplexes);
            self.DeflectionComplexAxes = axes('parent', self.DeflectionComplexPanel,...
                'units', 'normalized',...
                'outerPosition', [.1 0 .9 1]);
            
            deflectionAmplitudeTab = uitab(self.TabGroup, 'title', 'Deflection amplitude');
            self.DeflectionAmplitudePanel = uipanel('parent', deflectionAmplitudeTab,...
                'units', 'normalized', ...
                'position', [0 0 1 1]);
            uicontrol('parent', self.DeflectionAmplitudePanel,...
                'units', 'normalized',...
                'position', [0 .9 .05 .1],...
                'style', 'pushbutton',...
                'string', 'Compute deflection amplitudes',...
                'callback', @self.ShowDeflectionDistributions);
            self.IntrinsicDeflectionAmplitudeAxes = axes('parent', self.DeflectionAmplitudePanel,...
                'units', 'normalized',...
                'outerPosition', [.05 .5 .4 .5]);
            self.FarFieldDeflectionAmplitudeAxes = axes('parent', self.DeflectionAmplitudePanel,...
                'units', 'normalized',...
                'outerPosition', [.05 0 .4 .5]);
            self.IntrinsicDeflectionSlopeAxes = axes('parent', self.DeflectionAmplitudePanel,...
                'units', 'normalized',...
                'outerPosition', [.55 .5 .4 .5]);
            self.FarFieldDeflectionSlopeAxes = axes('parent', self.DeflectionAmplitudePanel,...
                'units', 'normalized',...
                'outerPosition', [.55 0 .4 .5]);
        end
    end
    
    methods (Access = private)
        function ComputeDeflectionComplexes(self, varargin)
            if isempty(self.DeflectionData), return; end
            
            analysisResult = self.DeflectionData.AnalysisResults;
            time = self.EcgData.GetTimeRange();
            
            fitReplicates = 10;
            [allDifferences, parameterEstimates, differencesFit] = AlgorithmPkg.AFComplexityCalculator.ComputeMicroAndMacroFractionation(analysisResult, time, fitReplicates);
            
            cla(self.DeflectionComplexAxes);
            bins = -150:2:150;
            [binCount xOut] = hist(allDifferences, bins);
            
            bar(self.DeflectionComplexAxes, xOut, binCount, 'faceColor', [0 0 1], 'edgeColor', [0 0 0]);
            
            fitXValues = min(allDifferences):1:max(allDifferences);
            fitYValues = differencesFit(fitXValues);
            
            line('xData', fitXValues, 'yData', fitYValues * numel(allDifferences) * 2,...
                'color', [1 0 0], 'lineWidth', 2,...
                'parent', self.DeflectionComplexAxes);
            xlabel(self.DeflectionComplexAxes, 'Interval (ms)');
            set(self.DeflectionComplexAxes, 'xlim', [bins(1) bins(end)]);
        end
        
        function ShowDeflectionDistributions(self, varargin)
            if isempty(self.DeflectionData), return; end
            
            self.ComputeDeflectionAmplitudes();
            self.ComputeDeflectionSlopes();
        end
        
        function ComputeDeflectionAmplitudes(self)
            analysisResult = self.DeflectionData.AnalysisResults;
            
            intrinsicDeflectionAmplitudes = cell(numel(analysisResult), 1);
            farFieldDeflectionAmplitudes = cell(numel(analysisResult), 1);
            for channelIndex = 1:numel(analysisResult)
                intrinsicDeflections = analysisResult{channelIndex}.PrimaryDeflections;
                intrinsicDeflectionAmplitudes{channelIndex} = analysisResult{channelIndex}.templatePeakAmplitudes(intrinsicDeflections);
                farFieldDeflectionAmplitudes{channelIndex} = analysisResult{channelIndex}.templatePeakAmplitudes(~intrinsicDeflections);
            end
            
            allIntrinsicAmplitudes = vertcat(intrinsicDeflectionAmplitudes{:});
            allFarFieldAmplitudes = vertcat(farFieldDeflectionAmplitudes{:});
            
            cla(self.IntrinsicDeflectionAmplitudeAxes);
            hist(self.IntrinsicDeflectionAmplitudeAxes, allIntrinsicAmplitudes / 1000, 100);
            xlabel(self.IntrinsicDeflectionAmplitudeAxes, 'Amplitude (mV)');
            title(self.IntrinsicDeflectionAmplitudeAxes, 'Intrinsic deflections');
            
            cla(self.FarFieldDeflectionAmplitudeAxes);
            hist(self.FarFieldDeflectionAmplitudeAxes, allFarFieldAmplitudes / 1000, 100);
            xlabel(self.FarFieldDeflectionAmplitudeAxes, 'Amplitude (mV)');
            title(self.FarFieldDeflectionAmplitudeAxes, 'Far-field deflections');
            
            linkaxes([self.IntrinsicDeflectionAmplitudeAxes, self.FarFieldDeflectionAmplitudeAxes], 'x');
        end
        
        function ComputeDeflectionSlopes(self)
            if isempty(self.DeflectionData), return; end
            
            analysisResult = self.DeflectionData.AnalysisResults;
            
            intrinsicDeflectionSlopes = cell(numel(analysisResult), 1);
            farFieldDeflectionSlopes = cell(numel(analysisResult), 1);
            for channelIndex = 1:numel(analysisResult)
                intrinsicDeflections = analysisResult{channelIndex}.PrimaryDeflections;
                intrinsicDeflectionSlopes{channelIndex} = analysisResult{channelIndex}.deflectionPeakSlopes(intrinsicDeflections);
                farFieldDeflectionSlopes{channelIndex} = analysisResult{channelIndex}.deflectionPeakSlopes(~intrinsicDeflections);
            end
            
            allIntrinsicSlopes = horzcat(intrinsicDeflectionSlopes{:});
            allFarFieldSlopes = horzcat(farFieldDeflectionSlopes{:});
            
            cla(self.IntrinsicDeflectionSlopeAxes);
            hist(self.IntrinsicDeflectionSlopeAxes, allIntrinsicSlopes / 1000, 100);
            xlabel(self.IntrinsicDeflectionSlopeAxes, 'Slope');
            title(self.IntrinsicDeflectionSlopeAxes, 'Intrinsic deflections');
            
            cla(self.FarFieldDeflectionSlopeAxes);
            hist(self.FarFieldDeflectionSlopeAxes, allFarFieldSlopes / 1000, 100);
            xlabel(self.FarFieldDeflectionSlopeAxes, 'Slope');
            title(self.FarFieldDeflectionSlopeAxes, 'Far-field deflections');
            
            linkaxes([self.IntrinsicDeflectionSlopeAxes, self.FarFieldDeflectionSlopeAxes], 'x');
        end
    end
end
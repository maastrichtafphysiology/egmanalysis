classdef QRSTDetector < handle
    properties
        IntegrationWindowLength
        VentricularRefractoryPeriod
        ReferenceChannelIndex
        ShowDiagnostics
        RPeakSearchWindow
        SelectProximalPeak
        SelectedPeakSign
        OriginalPanTompkins
    end
    
    properties (Access = protected)
        SamplingFrequency
    end
    
    methods
        function self = QRSTDetector()
            self.VentricularRefractoryPeriod = 0.2;
            self.ShowDiagnostics = false;
            self.RPeakSearchWindow = [0.03 0.03];
            %             self.IntegrationWindowLength = 0.05;
            self.IntegrationWindowLength = 0.1;
            self.SelectProximalPeak = true;
            self.SelectedPeakSign = 0;
            self.OriginalPanTompkins = false;
        end
        
        function rWaveIndices = DetectRWaves(self, ecgData, referenceChannel)
            samplingFrequency = ecgData.SamplingFrequency;
            signal = ecgData.ReferenceData(:, referenceChannel);
            
            rWaveIndices = self.DetectRWavesInSignal(signal, samplingFrequency);
            
%             %           Bandpass filter
%             [b, a] =  butter(3, [5, 15] / (self.SamplingFrequency / 2));
%             bandpassSignal = filtfilt(b, a, data);
%             
%             %             Derivative
%             b = [2 1 0 -1 -2] / 8;
%             derivativeSignal = filtfilt(b, 1, bandpassSignal);
%             
%             %             Squared
%             squaredSignal = derivativeSignal.^2;
%             
%             %             Integrated
%             integrationWindowSize = round(self.IntegrationWindowLength * self.SamplingFrequency);
%             b = ones(integrationWindowSize, 1) / integrationWindowSize;
%             integratedSignal = filtfilt(b, 1, squaredSignal);
%             
%             minPeakDistance = round(ecgData.SamplingFrequency * self.VentricularRefractoryPeriod);
%                 [peakHeights, candidatePeakPositions] = findpeaks(integratedSignal, 'minPeakDistance', minPeakDistance);
%                 peakSignal = zeros(size(data));
%                 peakSignal(candidatePeakPositions) = peakHeights;
%             
%             if self.OriginalPanTompkins
%                 rWaveIndices = self.DetectRPeaksPanTompkins(bandpassSignal, integratedSignal);
%             else
%                 rWaveIndices = self.DetectRPeaks(peakSignal, data);
%             end
%             
%             if self.ShowDiagnostics
%                 time = ecgData.GetTimeRange();
%                 figure;
%                 plotHandles = NaN(5,1);
%                 plotHandles(1) = subplot(5, 1, 1);
%                 plot(time, data);
%                 title('Original');
%                 line('xData', time(rWaveIndices), 'yData', data(rWaveIndices),...
%                     'parent', plotHandles(1), 'lineStyle', 'none',...
%                     'marker', 'o');
%                 
%                 plotHandles(2) = subplot(5, 1, 2);
%                 plot(time, bandpassSignal);
%                 title('Bandpass');
%                 
%                 plotHandles(3) = subplot(5, 1, 3);
%                 plot(time, derivativeSignal);
%                 title('Derivative');
%                 
%                 plotHandles(4) = subplot(5, 1, 4);
%                 plot(time, squaredSignal);
%                 title('Squared');
%                 
%                 plotHandles(5) = subplot(5, 1, 5);
%                 plot(time, integratedSignal);
%                 hold(plotHandles(5), 'on');
%                 stem(time(peakSignal > 0), peakSignal(peakSignal > 0));
%                 title('Integrated');
%                 
%                 validHandles = ishandle(plotHandles);
%                 
%                 linkaxes(plotHandles(validHandles), 'x');
%             end
        end
        
        function rWaveIndices = DetectRWavesInSignal(self, signal, samplingFrequency)
            self.SamplingFrequency = samplingFrequency;
            data = signal;
            
            %           Bandpass filter
            [b, a] =  butter(3, [5, 15] / (self.SamplingFrequency / 2));
            bandpassSignal = filtfilt(b, a, data);
            
            %             Derivative
            b = [2 1 0 -1 -2] / 8;
            derivativeSignal = filtfilt(b, 1, bandpassSignal);
            
            %             Squared
            squaredSignal = derivativeSignal.^2;
            
            %             Integrated
            integrationWindowSize = round(self.IntegrationWindowLength * self.SamplingFrequency);
            b = ones(integrationWindowSize, 1) / integrationWindowSize;
            integratedSignal = filtfilt(b, 1, squaredSignal);
            
            minPeakDistance = round(self.SamplingFrequency * self.VentricularRefractoryPeriod);
                [peakHeights, candidatePeakPositions] = findpeaks(integratedSignal, 'minPeakDistance', minPeakDistance);
                peakSignal = zeros(size(data));
                peakSignal(candidatePeakPositions) = peakHeights;
            
            if self.OriginalPanTompkins
                rWaveIndices = self.DetectRPeaksPanTompkins(bandpassSignal, integratedSignal);
            else
                rWaveIndices = self.DetectRPeaks(peakSignal, data);
            end
            
            if self.ShowDiagnostics
                numberOfSamples = numel(signal);
                time = (1:numberOfSamples) / self.SamplingFrequency;
                figure;
                plotHandles = NaN(5,1);
                plotHandles(1) = subplot(5, 1, 1);
                plot(time, data);
                title('Original');
                line('xData', time(rWaveIndices), 'yData', data(rWaveIndices),...
                    'parent', plotHandles(1), 'lineStyle', 'none',...
                    'marker', 'o');
                
                plotHandles(2) = subplot(5, 1, 2);
                plot(time, bandpassSignal);
                title('Bandpass');
                
                plotHandles(3) = subplot(5, 1, 3);
                plot(time, derivativeSignal);
                title('Derivative');
                
                plotHandles(4) = subplot(5, 1, 4);
                plot(time, squaredSignal);
                title('Squared');
                
                plotHandles(5) = subplot(5, 1, 5);
                plot(time, integratedSignal);
                hold(plotHandles(5), 'on');
                stem(time(peakSignal > 0), peakSignal(peakSignal > 0));
                title('Integrated');
                
                validHandles = ishandle(plotHandles);
                
                linkaxes(plotHandles(validHandles), 'x');
            end
        end
        
        function [qOnset, rIndex] = DetectQOnset(self, ecgData, rWaveIndices, validChannels)
            for channelIndex = validChannels
                [averageBeat(:,channelIndex), ~, rIndex] = ComputeAverageBeatNoBaseline(self, ecgData, rWaveIndices, channelIndex);
            end
            
            qWindowStart = rIndex-round(0.075*ecgData.SamplingFrequency);
            qWindowEnd = rIndex-round(0.020*ecgData.SamplingFrequency);
            
            % Loop qOnset detection for validchannels
            allQOnsets = nan(max(validChannels),1);
            for channelIndex = validChannels
                qOnsetCandidates=findchangepts(averageBeat(qWindowStart:qWindowEnd,channelIndex),...
                        'MinThreshold',mad(averageBeat(qWindowStart:qWindowEnd,channelIndex))/10,...
                        'Statistic','linear');
                if ~isempty(qOnsetCandidates)
                    allQOnsets(channelIndex,1) = qOnsetCandidates(1,1)+qWindowStart-1;
                end
            end
                   
            % remove outliers
            allQOnsets(isoutlier(allQOnsets))=[];
            qOnset = min(allQOnsets);            
        end
        
        function [averageBeat, beatStd] = ComputeAverageBeat(self, ecgData, rWaveIndices, channelIndex)
            numberOfChannels = ecgData.GetNumberOfChannels();
            numberOfReferenceChannels = size(ecgData.ReferenceData, 2);
            numberOfIntervals = numel(rWaveIndices) - 1;
            bandWidth = 10;
            if channelIndex <= numberOfChannels
                data = ecgData.Data(:, channelIndex);
            elseif channelIndex - numberOfChannels <= numberOfReferenceChannels
                data = ecgData.ReferenceData(:, channelIndex - numberOfChannels);
            else
                averageBeat = [];
                beatStd = [];
                return;
            end
            
            v0 = NaN(numberOfIntervals, 1);
            v1 = NaN(numberOfIntervals, 1);
            for intervalIndex = 1:numberOfIntervals
                t0 = rWaveIndices(intervalIndex);
                t1 = rWaveIndices(intervalIndex + 1);
                % average voltage over a small interval
                range1 = (t0 - bandWidth):(t0 + bandWidth);
                range2 = (t1 - bandWidth):(t1 + bandWidth);
                v0(intervalIndex) = mean(data(range1));
                v1(intervalIndex) = mean(data(range2));
            end
            
            for intervalIndex = 1:numberOfIntervals
                t0 = rWaveIndices(intervalIndex);
                t1 = rWaveIndices(intervalIndex + 1);
                tt = (t0:(t1-1))';
                baseline = v0(intervalIndex) * (t1-tt) / (t1-t0) + v1(intervalIndex) * (tt-t0) / (t1-t0);
                data(tt) = data(tt) - baseline;
            end
            
            % compute average
            rrMin = min(diff(rWaveIndices));
            windowStart= -round(rrMin * 0.3);
            windowStop = round(rrMin * 0.7);
            windowIndices = windowStart:windowStop;
            rWaveWindowIndices = bsxfun(@plus,...
                repmat(windowIndices(:), 1, numel(rWaveIndices)),...
                rWaveIndices(:)');
            validWindows = rWaveWindowIndices(1, :) > 0 &...
                rWaveWindowIndices(end, :) <= numel(data);
            
            qrstWindows = data(rWaveWindowIndices(:, validWindows));
            averageBeat = mean(qrstWindows, 2);
            beatStd = std(qrstWindows, 0, 2);
        end
        
        function [averageBeat, beatStd, rIndex] = ComputeAverageBeatNoBaseline(self, ecgData, rWaveIndices, channelIndex)
            numberOfChannels = ecgData.GetNumberOfChannels();
            numberOfReferenceChannels = size(ecgData.ReferenceData, 2);
            numberOfIntervals = numel(rWaveIndices) - 1;
            bandWidth = 10;
            if channelIndex <= numberOfChannels
                data = ecgData.Data(:, channelIndex);
            elseif channelIndex - numberOfChannels <= numberOfReferenceChannels
                data = ecgData.ReferenceData(:, channelIndex - numberOfChannels);
            else
                averageBeat = [];
                beatStd = [];
                return;
            end
            %                        
            % compute average
            rrMin = min(diff(rWaveIndices));
            windowStart= -round(rrMin * 0.5);
            if windowStart > -(0.25*ecgData.SamplingFrequency)
                windowStart = -(0.25*ecgData.SamplingFrequency);
            end
            windowStop = round(rrMin * 0.7);
            windowIndices = windowStart:windowStop;
            rWaveWindowIndices = bsxfun(@plus,...
                repmat(windowIndices(:), 1, numel(rWaveIndices)),...
                rWaveIndices(:)');
            validWindows = rWaveWindowIndices(1, :) > 0 &...
                rWaveWindowIndices(end, :) <= numel(data);
            
            qrstWindows = data(rWaveWindowIndices(:, validWindows));
            averageBeat = mean(qrstWindows, 2);
            beatStd = std(qrstWindows, 0, 2);
            rIndex = abs(windowStart);
        end
        
        function [clusterCentroids, clusterMemberIndices] = ComputeQRSTClusters(self, signal, rWaveWindowIndices)
            maxDistance = 1 - self.ClusterSensitivity;
            
            validWindows = rWaveWindowIndices(1, :) > 0 &...
                rWaveWindowIndices(end, :) <= numel(signal);
            
            qrstWindows = signal(rWaveWindowIndices(:, validWindows));
            qrstWindows = bsxfun(@minus, qrstWindows, mean(qrstWindows));
            
            % construct cluster tree from full windows
            clusterTree = linkage(qrstWindows', 'average', 'correlation');
            clusterMemberIndices = NaN(size(validWindows));
            clusterMemberIndices(validWindows) = cluster(clusterTree,...
                'cutoff', maxDistance, 'criterion', 'distance');
            numberOfClusters = numel(unique(clusterMemberIndices(validWindows)));
            numberOfClusterMembers = zeros(numberOfClusters, 1);
            
            % compute cluster centroids
            clusterCentroids = NaN(size(rWaveWindowIndices, 1), numberOfClusters);
            for clusterIndex = 1:numberOfClusters
                clusterMembers = clusterMemberIndices(validWindows) == clusterIndex;
                numberOfClusterMembers(clusterIndex) = numel(find(clusterMembers));
                if any(clusterMembers)
                    [u, s, ~] = svd(qrstWindows(:, clusterMembers));
                    qrstComponent = u(:, 1) * s(1);
                    %                     clusterCentroids(:, clusterIndex) = mean(qrstWindows(:, clusterMembers), 2);
                    clusterCentroids(:, clusterIndex) = qrstComponent;
                end
            end
            
            % assign unassigned windows to closest cluster
            if any(isnan(clusterMemberIndices))
                unassignedWindows = rWaveWindowIndices(:, isnan(clusterMemberIndices));
                bestClusterIndices = NaN(size(unassignedWindows, 2), 1);
                for windowIndex = 1:size(unassignedWindows, 2)
                    currentWindowIndices = unassignedWindows(:, windowIndex);
                    validPositions = currentWindowIndices > 0 & currentWindowIndices <= numel(signal);
                    currentWindowIndices = currentWindowIndices(validPositions);
                    partialClusterCentroids = clusterCentroids(validPositions, :);
                    correlations = zeros(size(partialClusterCentroids, 2), 1);
                    for clusterIndex = 1:size(partialClusterCentroids, 2)
                        correlations(clusterIndex) = xcorr(signal(currentWindowIndices), partialClusterCentroids(:, clusterIndex), 0, 'coeff');
                    end
                    [~, bestClusterIndices(windowIndex)] = max(correlations);
                end
                clusterMemberIndices(isnan(clusterMemberIndices)) = bestClusterIndices;
            end
            
            if self.ShowDiagnostics
                figure('name', 'Cluster information', 'numberTitle', 'off');
                plot(clusterCentroids);
                
                textString = {...
                    ['Number of clusters: ' num2str(numberOfClusters, '%.0f')];...
                    ['Number of R-peaks: ' num2str(size(rWaveWindowIndices, 2), '%.0f')]};
                
                text('parent', gca,...
                    'units', 'normalized',...
                    'position', [1 1],...
                    'verticalAlignment', 'top',...
                    'horizontalAlignment', 'right',...
                    'edgeColor', [0 0 0],...
                    'backgroundColor', [.8 .8 .8],...
                    'fontWeight', 'bold',...
                    'string', textString);
            end
        end
    end
    
    methods (Access = protected)
        function rPeaks = DetectRPeaks(self, peakSignal, originalSignal)
            candidatePeakPositions = find(peakSignal);
            peakHeights = peakSignal(candidatePeakPositions);
            
            peakExtremes = deal(prctile(peakHeights, [5, 95]));
            rThreshold = (peakExtremes(2) - peakExtremes(1)) / 2;
%             thresholdFound = false;
%             accuracy = 1e-10;
%             while ~thresholdFound
%                 validPeaks = peakHeights >= rThreshold;
%                 peakIntervals = diff(candidatePeakPositions(validPeaks));
%                 peakIntervals = peakIntervals / self.SamplingFrequency;
%                 if any(peakIntervals < self.VentricularRefractoryPeriod)
%                     newRThreshold = rThreshold + (upperThreshold - rThreshold) / 2;
%                     lowerThreshold = rThreshold;
%                 else
%                     newRThreshold = rThreshold - (rThreshold - lowerThreshold) / 2;
%                     upperThreshold = rThreshold;
%                 end
%                 
%                 if abs(rThreshold - newRThreshold) > accuracy
%                     rThreshold = newRThreshold;
%                 else
%                     thresholdFound = true;
%                 end
%             end
            
            validPeaks = peakHeights >= rThreshold;
            
            
            %             rThreshold = max(peakHeights);
            %             upperThreshold = max(peakHeights);
            %             lowerThreshold = min(peakHeights) / 2;
            %             thresholdFound = false;
            %             accuracy = 1e-10;
            %             while ~thresholdFound
            %                 validPeaks = peakHeights > rThreshold;
            %                 peakIntervals = diff(candidatePeakPositions(validPeaks));
            %                 peakIntervals = peakIntervals / self.SamplingFrequency;
            %                 if min(peakIntervals) < self.VentricularRefractoryPeriod
            %                     newRThreshold = rThreshold + (upperThreshold - rThreshold) / 2;
            %                     lowerThreshold = rThreshold;
            %                 else
            %                     newRThreshold = rThreshold - (rThreshold - lowerThreshold) / 2;
            %                     upperThreshold = rThreshold;
            %                 end
            %
            %                 if abs(rThreshold - newRThreshold) > accuracy
            %                     rThreshold = newRThreshold;
            %                 else
            %                     thresholdFound = true;
            %                 end
            %             end
            %
            %             validPeaks = peakHeights >= rThreshold;
            %
            peaksChanged = true;
            while peaksChanged
                peaksChanged = false;
                rPeaks = candidatePeakPositions(validPeaks);
                rPeaksHeight = peakHeights(validPeaks);
                
                noisePeakHeight = mean(peakHeights(~validPeaks));
                if isnan(noisePeakHeight)
                    noisePeakHeight = 0;
                end
                
                rrAverage = median(diff(rPeaks));
                longIntervalThreshold = (5/3) * rrAverage;
                shortIntervalThreshold = (1/2) * rrAverage;
                
                longIntervalPositions = find(diff(rPeaks) > longIntervalThreshold);
                for intervalIndex = 1:numel(longIntervalPositions)
                    intervalStart = rPeaks(longIntervalPositions(intervalIndex));
                    intervalStop = rPeaks(longIntervalPositions(intervalIndex) + 1);
                    candidatePeaks = find(candidatePeakPositions > intervalStart + shortIntervalThreshold &...
                        candidatePeakPositions < intervalStop - shortIntervalThreshold);
                    if any(candidatePeaks)
                        [maxPeakHeight, maxPeakPosition] = max(peakHeights(candidatePeaks));
                        if maxPeakHeight > noisePeakHeight
                            validPeaks(candidatePeaks(maxPeakPosition)) = true;
                            peaksChanged = true;
                            break;
                        end
                    end
                end
                
                if peaksChanged, continue; end
                
                shortIntervalPositions = find(diff(rPeaks) < shortIntervalThreshold);
                for intervalIndex = 1:numel(shortIntervalPositions)
                    firstPeakHeight = rPeaksHeight(shortIntervalPositions(intervalIndex));
                    secondPeakHeight = rPeaksHeight(shortIntervalPositions(intervalIndex) + 1);
                    if firstPeakHeight > secondPeakHeight
                        invalidPeakPosition = candidatePeakPositions == rPeaks(shortIntervalPositions(intervalIndex) + 1);
                        validPeaks(invalidPeakPosition) = false;
                    else
                        invalidPeakPosition = candidatePeakPositions == rPeaks(shortIntervalPositions(intervalIndex));
                        validPeaks(invalidPeakPosition) = false;
                    end
                    peaksChanged = true;
                    break;
                end
            end
            
            medianPeakHeight = mean(peakHeights(validPeaks));
            noisePeakHeight = mean(peakHeights(~validPeaks));
            medianAbsoluteDeviation = mad(peakHeights(validPeaks), 1);
            validPeaks(peakHeights < (medianPeakHeight - noisePeakHeight) / 2) = false;
            
            rPeaks = candidatePeakPositions(validPeaks);
            if ~self.SelectProximalPeak
                return;
            end
            
            newRPeaks = rPeaks;
            previousWindowSize = round(self.RPeakSearchWindow(1) * self.SamplingFrequency);
            nextWindowSize = round(self.RPeakSearchWindow(2) * self.SamplingFrequency);
            signalLength = numel(originalSignal);
            [b, a] = cheby2(3, 20,...
                1 / (self.SamplingFrequency / 2), 'high');
            filteredSignal = filtfilt(b, a, originalSignal);
            
            for peakIndex = 1:numel(rPeaks)
                peakWindowIndices = max([rPeaks(peakIndex) - previousWindowSize, 1]):min([rPeaks(peakIndex) + nextWindowSize, signalLength]);
                switch sign(self.SelectedPeakSign)
                    case 0
                        [~, maxIndex] = max(abs(filteredSignal(peakWindowIndices)));
                    case -1
                        [~, maxIndex] = min(filteredSignal(peakWindowIndices));
                    case 1
                        [~, maxIndex] = max(filteredSignal(peakWindowIndices));
                end
                newRPeaks(peakIndex) = peakWindowIndices(maxIndex);
            end
            rPeaks = newRPeaks;
        end
        
        function qrsIndexRaw = DetectRPeaksPanTompkins(self, bandpassSignal, integratedSignal)
            % Initialize
            rAmplitude =[]; %amplitude of R
            qrsIndex =[]; %index
            signalThresholdLevel = 0;
            noiseValues =[];
            noiseIndices =[];
            skip = 0; % becomes one when a T wave is detected
            notNoise = 0; % it is not noise when not_nois = 1
            selected_RR =[]; % Selected RR intervals
            currentRR = 0;
            meanRR = 0;
            qrsIndexRaw =[];
            qrsAmplitudeRaw=[];
            searchBack = 0;
            testRR = 0;
            signalThresholdLevelBuffer = [];
            noiseThresholdLevelBuffer = [];
            signalThresholdBuffer = [];
            signalThresholdBandpassLevelBuffer = [];
            noiseThresholdBandpassLevelBuffer = [];
            signalThresholdBandpassBuffer = [];
            ax = zeros(1,6);
            
            % initialize the training phase (2 seconds of the signal) to determine the THR_SIG and THR_NOISE
            signalThreshold = max(integratedSignal(1:2 * self.SamplingFrequency)) * 1/3; % 0.25 of the max amplitude
            noiseThreshold = mean(integratedSignal(1:2 * self.SamplingFrequency)) * 1/2; % 0.5 of the mean signal is considered to be noise
            signalThresholdLevel = signalThreshold;
            noiseThresholdLevel = noiseThreshold;
            
            % Initialize bandpath filter threshold(2 seconds of the bandpass signal)
            signalThresholdBandpass = max(bandpassSignal(1:2 * self.SamplingFrequency)) * 1/3; % 0.25 of the max amplitude
            noiseThresholdBandpass = mean(bandpassSignal(1:2 * self.SamplingFrequency)) * 1/2; %
            signalThresholdBandpassLevel = signalThresholdBandpass; % Signal level in Bandpassed filter
            noiseThresholdBandpassLevel = noiseThresholdBandpass; % Noise level in Bandpassed filter
            % Thresholding and online desicion rule
            
            minPeakDistance = round(self.SamplingFrequency * self.VentricularRefractoryPeriod);
            [peakHeights, candidatePeakPositions] = findpeaks(integratedSignal, 'minPeakDistance', minPeakDistance);
            for peakIndex = 1 : length(peakHeights)
                % locate the corresponding peak in the filtered signal
                if candidatePeakPositions(peakIndex) - round(0.150 * self.SamplingFrequency)>= 1 && candidatePeakPositions(peakIndex)<= length(bandpassSignal)
                    [maxPeakValue, maxPeakIndex] = max(bandpassSignal(candidatePeakPositions(peakIndex) - round(0.150 * self.SamplingFrequency):candidatePeakPositions(peakIndex)));
                else
                    if peakIndex == 1
                        [maxPeakValue,maxPeakIndex] = max(bandpassSignal(1:candidatePeakPositions(peakIndex)));
                        searchBack = 1;
                    elseif candidatePeakPositions(peakIndex)>= length(bandpassSignal)
                        [maxPeakValue,maxPeakIndex] = max(bandpassSignal(candidatePeakPositions(peakIndex)-round(0.150 * self.SamplingFrequency):end));
                    end
                    
                end
                
                % update the heart_rate (Two heart rate means one the most recent and the other selected)
                if length(rAmplitude) >= 9
                    
                    diffRR = diff(qrsIndex(end-8:end)); %calculate RR interval
                    meanRR = mean(diffRR); % calculate the mean of 8 previous R waves interval
                    latestRRInterval = qrsIndex(end) - qrsIndex(end-1);
                    
                    if latestRRInterval <= 0.92 * meanRR || latestRRInterval >= 1.16 * meanRR
                        % lower down thresholds to detect better in MVI
                        signalThreshold = 0.5 * (signalThreshold);
                        %THR_NOISE = 0.5*(THR_SIG);
                        % lower down thresholds to detect better in Bandpass filtered
                        signalThresholdBandpass = 0.5 * (signalThresholdBandpass);
                        %THR_NOISE1 = 0.5*(THR_SIG1);
                        
                    else
                        currentRR = meanRR; %the latest regular beats mean
                    end
                    
                end
                
                % calculate the mean of the last 8 R waves to make sure that QRS is not
                % missing(If no R detected , trigger a search back) 1.66*mean
                
                if currentRR
                    testRR = currentRR; %if the regular RR is availabe use it
                elseif meanRR && currentRR == 0
                    testRR = meanRR;
                else
                    testRR = 0;
                end
                
                if testRR
                    if (candidatePeakPositions(peakIndex) - qrsIndex(end)) >= round(1.66 * testRR)% it shows a QRS is missed
                        [testPeakValue,testPeakIndex] = ...
                            max(integratedSignal(qrsIndex(end)+ round(0.200 * self.SamplingFrequency):candidatePeakPositions(peakIndex) - round(0.200 * self.SamplingFrequency))); % search back and locate the max in this interval
                        testPeakIndex = qrsIndex(end)+ round(0.200 * self.SamplingFrequency) + testPeakIndex -1; %location
                        
                        if testPeakValue > noiseThreshold
                            rAmplitude = [rAmplitude, testPeakValue];
                            qrsIndex = [qrsIndex, testPeakIndex];
                            
                            % find the location in filtered sig
                            if testPeakIndex <= length(bandpassSignal)
                                [testMaxPeakValue, testMaxPeakIndex] =...
                                    max(bandpassSignal(testPeakIndex-round(0.150 * self.SamplingFrequency):testPeakIndex));
                            else
                                [testMaxPeakValue,testMaxPeakIndex] =...
                                    max(bandpassSignal(testPeakIndex-round(0.150 * self.SamplingFrequency):end));
                            end
                            % take care of bandpass signal threshold
                            if testMaxPeakValue > noiseThresholdBandpass
                                
                                qrsIndexRaw = [qrsIndexRaw testPeakIndex - round(0.150 * self.SamplingFrequency) + (testMaxPeakIndex - 1)];% save index of bandpass
                                qrsAmplitudeRaw =[qrsAmplitudeRaw testMaxPeakValue]; %save amplitude of bandpass
                                signalThresholdBandpassLevel = 0.25 * testMaxPeakValue + 0.75 * signalThresholdBandpassLevel; %when found with the second thres
                            end
                            
                            notNoise = 1;
                            signalThresholdLevel = 0.25 * testPeakValue + 0.75 * signalThresholdLevel ;  %when found with the second threshold
                        end
                        
                    else
                        notNoise = 0;
                    end
                end
                
                %  find noise and QRS peaks
                if peakHeights(peakIndex) >= signalThreshold
                    
                    % if a QRS candidate occurs within 360ms of the previous QRS
                    % ,the algorithm determines if its T wave or QRS
                    if length(rAmplitude) >= 3
                        if (candidatePeakPositions(peakIndex)-qrsIndex(end)) <= round(0.360 * self.SamplingFrequency)
                            Slope1 = mean(diff(integratedSignal(candidatePeakPositions(peakIndex)-round(0.075 * self.SamplingFrequency):candidatePeakPositions(peakIndex)))); %mean slope of the waveform at that position
                            Slope2 = mean(diff(integratedSignal(qrsIndex(end) - round(0.075 * self.SamplingFrequency):qrsIndex(end)))); %mean slope of previous R wave
                            if abs(Slope1) <= abs(0.5 * (Slope2))  % slope less then 0.5 of previous R
                                noiseValues = [noiseValues peakHeights(peakIndex)];
                                noiseIndices = [noiseIndices candidatePeakPositions(peakIndex)];
                                skip = 1; % T wave identification
                                % adjust noise level in both filtered and
                                % MVI
                                noiseThresholdBandpassLevel = 0.125 * maxPeakValue + 0.875 * noiseThresholdBandpassLevel;
                                noiseThresholdLevel = 0.125 * peakHeights(peakIndex) + 0.875 * noiseThresholdLevel;
                            else
                                skip = 0;
                            end
                        end
                    end
                    
                    if skip == 0  % skip is 1 when a T wave is detected
                        rAmplitude = [rAmplitude, peakHeights(peakIndex)];
                        qrsIndex = [qrsIndex, candidatePeakPositions(peakIndex)];
                        
                        % bandpass filter check threshold
                        if maxPeakValue >= signalThresholdBandpass
                            if searchBack
                                qrsIndexRaw = [qrsIndexRaw, maxPeakIndex];  % save index of bandpass
                            else
                                qrsIndexRaw = [qrsIndexRaw, candidatePeakPositions(peakIndex)-round(0.150 * self.SamplingFrequency)+ (maxPeakIndex - 1)];% save index of bandpass
                            end
                            qrsAmplitudeRaw =[qrsAmplitudeRaw, maxPeakValue];% save amplitude of bandpass
                            signalThresholdBandpassLevel = 0.125 * maxPeakValue + 0.875 * signalThresholdBandpassLevel;% adjust threshold for bandpass filtered sig
                        end
                        
                        % adjust Signal level
                        signalThresholdLevel = 0.125 * peakHeights(peakIndex) + 0.875 * signalThresholdLevel ;
                    end
                    
                elseif (noiseThreshold <= peakHeights(peakIndex)) && (peakHeights(peakIndex) < signalThreshold)
                    
                    %adjust Noise level in filtered sig
                    noiseThresholdBandpassLevel = 0.125 * maxPeakValue + 0.875 * noiseThresholdBandpassLevel;
                    %adjust Noise level in MVI
                    noiseThresholdLevel = 0.125 * peakHeights(peakIndex) + 0.875 * noiseThresholdLevel;
                    
                elseif peakHeights(peakIndex) < noiseThreshold
                    noiseValues = [noiseValues peakHeights(peakIndex)];
                    noiseIndices = [noiseIndices candidatePeakPositions(peakIndex)];
                    
                    % noise level in filtered signal
                    noiseThresholdBandpassLevel = 0.125 * maxPeakValue + 0.875 * noiseThresholdBandpassLevel;
                    %adjust Noise level in MVI
                    noiseThresholdLevel = 0.125 * peakHeights(peakIndex) + 0.875 * noiseThresholdLevel;
                end

                % adjust the threshold with SNR
                if noiseThresholdLevel ~= 0 || signalThresholdLevel ~= 0
                    signalThreshold = noiseThresholdLevel + 0.25 * (abs(signalThresholdLevel - noiseThresholdLevel));
                    noiseThreshold = 0.5 * (signalThreshold);
                end
                
                % adjust the threshold with SNR for bandpassed signal
                if noiseThresholdBandpassLevel ~= 0 || signalThresholdBandpassLevel ~= 0
                    signalThresholdBandpass = noiseThresholdBandpassLevel + 0.25 * (abs(signalThresholdBandpassLevel - noiseThresholdBandpassLevel));
                    noiseThresholdBandpass = 0.5 * signalThresholdBandpass;
                end
                
                % take a track of thresholds of smoothed signal
                signalThresholdLevelBuffer = [signalThresholdLevelBuffer signalThresholdLevel];
                noiseThresholdLevelBuffer = [noiseThresholdLevelBuffer noiseThresholdLevel];
                signalThresholdBuffer = [signalThresholdBuffer signalThreshold];
                
                % take a track of thresholds of filtered signal
                signalThresholdBandpassLevelBuffer = [signalThresholdBandpassLevelBuffer signalThresholdBandpassLevel];
                noiseThresholdBandpassLevelBuffer = [noiseThresholdBandpassLevelBuffer noiseThresholdBandpassLevel];
                signalThresholdBandpassBuffer = [signalThresholdBandpassBuffer signalThresholdBandpass];
                
                skip = 0; %reset parameters
                notNoise = 0; %reset parameters
                searchBack = 0;  %reset bandpass param
            end
        end
        
    end
end
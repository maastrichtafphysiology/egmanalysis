[pathname, ~] = fileparts(pwd);
customMatlabPackagesPath = fullfile(pathname, 'CustomMatlabPackages');
wavemapPath = fullfile(pathname, 'EGMAnalysis');

warning off MATLAB:rmpath:DirNotFound;
rmpath(customMatlabPackagesPath, wavemapPath);

addpath(customMatlabPackagesPath, wavemapPath);

warning off MATLAB:uitabgroup:OldVersion;
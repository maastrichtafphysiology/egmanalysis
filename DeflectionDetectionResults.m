classdef DeflectionDetectionResults < handle
    properties (Access = private)

    end
    
    properties (GetAccess = public)
        AnalysisResults
        FilteredAnalysisResults
        FilterSettings
        Fit
    end
    
    events
        FilterSettingsChanged
    end
    
    methods			
        function self = DeflectionDetectionResults(analysisResults)
            self.AnalysisResults = analysisResults;
        end
        
        function PreFilter(self, filter)
            [self.FilteredAnalysisResults, filterSettings] = filter.Apply(self.AnalysisResults);
            if ~isempty(filterSettings), self.FilterSettings = filterSettings; end
        end
        
        function ApplyFilter(self, filter)
            [self.FilteredAnalysisResults, filterSettings] = filter.Apply(self.FilteredAnalysisResults);
             if ~isempty(filterSettings), self.FilterSettings = filterSettings; end
        end
        
        function UpdateFilteredResults(self)
            filter = EcgMappingAlgorithms.DeflectionFilter(self.FilterSettings);
            [self.FilteredAnalysisResults, self.FilterSettings] = filter.Apply(self.AnalysisResults);
        end
        
        function analysisResult = GetAnalysisResult(self, lineIndex)
            analysisResult = self.AnalysisResults{lineIndex};
        end
        
        function filteredeAnalysisResult = GetFilteredAnalysisResult(self, lineIndex)
            filteredeAnalysisResult = self.FilteredAnalysisResults{lineIndex};
        end
        
        function filterSetting = GetFilterSetting(self, lineIndex)
            if iscell(self.FilterSettings)
                filterSetting = self.FilterSettings{lineIndex};
            else
                filterSetting = self.FilterSettings;
            end
        end
        
        function PublishFilterSettingsChangedChangedEvent(self, varargin)
            notify(self, 'FilterSettingsChanged');
        end
    end
    
    methods ( Access = private )
      
    end
    
    methods (Static)              
        function cycleLengths = CalculateCycleLengths(analysisResults, eliminatedLines)
            cycleLengths = [];
            for lineIndex = 1 : numel(analysisResults)
                if (~eliminatedLines(lineIndex))
                    cycleLengths = [cycleLengths; diff(analysisResults{lineIndex}.templatePeakIndices(:))]; %#ok<AGROW>
                end
            end
        end
        
        function cycleLengths = CalculatePrimaryCycleLengths(analysisResults, eliminatedLines)
            cycleLengths = [];
            for lineIndex = 1 : numel(analysisResults)
                if (~eliminatedLines(lineIndex))
                    currentIndices = analysisResults{lineIndex}.templatePeakIndices(analysisResults{lineIndex}.PrimaryDeflections);
                    cycleLengths = [cycleLengths; diff(currentIndices(:))]; %#ok<AGROW>
                end
            end
        end
    end
end
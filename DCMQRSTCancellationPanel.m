classdef DCMQRSTCancellationPanel < QRSTCancellationPanel
    properties
        SpatialRadiusPanel
    end
    
    methods
        function self = DCMQRSTCancellationPanel(position)
            self = self@QRSTCancellationPanel(position);
            self.QrstCancellator = DCMQRSTCancellator();
        end
    end
    
    methods (Access = protected)
        function CreateControlPanel(self)
            CreateControlPanel@QRSTCancellationPanel(self);
            
            self.SpatialRadiusPanel = uipanel('parent', self.QRSTRangePanel,...
                'units', 'normalized', ...
                'position', [.5, 0, .3, 1],...
                'borderType', 'none',...
                'visible', 'off');
            uicontrol('parent', self.SpatialRadiusPanel,...
                'units', 'normalized',...
                'position', [0, 2/3, 1, 1/3],...
                'style', 'text',...                
                'string', 'Spatial radius',...
                'tooltipString', 'Spatial radius (diagonal)');
            uicontrol('parent', self.SpatialRadiusPanel,...
                'units', 'normalized',...
                'position', [0, 0, 1, 2/3],...
                'style', 'edit',...
                'string', num2str(self.QrstCancellator.SpatialRadius),...
                'callback', @self.SetSpatialRadius);
            
            set(self.CancellationMethodPopup, 'string', DCMQRSTCancellator.CANCELLATION_METHODS);
            set(self.QRSTCancellationButton, 'enable', 'off');
        end
        
        function SetCancellationMethod(self, varargin)
            methodIndex = get(self.CancellationMethodPopup, 'value');
            self.QrstCancellator.CancellationMethod = DCMQRSTCancellator.CANCELLATION_METHODS{methodIndex};
            set(self.QRSTCancellationButton, 'enable', 'on');
            
            switch self.QrstCancellator.CancellationMethod
                case DCMQRSTCancellator.CANCELLATION_METHODS{1}
                    set(self.ClusterSensitivityPanel, 'visible', 'on');
                    set(self.SpatialRadiusPanel, 'visible', 'off');
                    set(self.QRSTCancellationButton, 'enable', 'off');
                case DCMQRSTCancellator.CANCELLATION_METHODS{3}
                    set(self.ClusterSensitivityPanel, 'visible', 'off');
                    set(self.SpatialRadiusPanel, 'visible', 'on');
                case DCMQRSTCancellator.CANCELLATION_METHODS{5}
                    set(self.ClusterSensitivityPanel, 'visible', 'off');
                    set(self.SpatialRadiusPanel, 'visible', 'on');
                otherwise
                    set(self.ClusterSensitivityPanel, 'visible', 'off');
                    set(self.SpatialRadiusPanel, 'visible', 'off');
            end
        end
        
        function SetSpatialRadius(self, source, varargin)
            value = str2double(get(source, 'string'));
            self.QrstCancellator.SpatialRadius = value;
        end
    end
end
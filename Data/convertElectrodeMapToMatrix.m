function [epiMatrix, endoMatrix] = convertElectrodeMapToMatrix(filename)

load(filename);
epiMatrix = NaN(electrodeMapSize);
endoMatrix = NaN(electrodeMapSize);

for electrodeIndex = 1:size(electrodeMap, 1)
    switch electrodeMap(electrodeIndex, 2)
        case 1
            epiMatrix(electrodeMap(electrodeIndex, 1)) = electrodeIndex;
        case - 1
            endoMatrix(electrodeMap(electrodeIndex, 1)) = electrodeIndex;
    end
end
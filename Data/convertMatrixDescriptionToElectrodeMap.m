function electrodeMap = convertMatrixDescriptionToElectrodeMap(matrixDescription, mapSize)
mapMatrix = NaN(mapSize);
for electrodeIndex = 1:size(matrixDescription, 1)
    rowPosition = matrixDescription(electrodeIndex, 2) + 1;
    columnPosition =  matrixDescription(electrodeIndex, 1) + 1;
    if rowPosition > 0 && columnPosition > 0
        mapMatrix(rowPosition, columnPosition) = electrodeIndex;
    end
end
[electrodeNumbers sortIndices] = sort(mapMatrix(:));
validElectrodeNumbers = ~isnan(electrodeNumbers);
electrodeMap = zeros(size(matrixDescription));

electrodeMap(electrodeNumbers(validElectrodeNumbers), :) =...
    [sortIndices(validElectrodeNumbers) ones(numel(electrodeNumbers(validElectrodeNumbers)), 1)];
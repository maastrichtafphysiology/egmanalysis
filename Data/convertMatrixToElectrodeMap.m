function electrodeMap = convertMatrixToElectrodeMap(epiMatrix, endoMatrix)
maxValue = max([max(epiMatrix(:)) max(endoMatrix(:))]);

electrodeMap = zeros(maxValue, 2);
for epiIndex = 1:numel(epiMatrix)
    if isnan(epiMatrix(epiIndex))
        continue;
    end
    
    electrodeMap(epiMatrix(epiIndex), 1) = epiIndex;
    electrodeMap(epiMatrix(epiIndex), 2) = 1;
end

for endoIndex = 1:numel(endoMatrix)
    if isnan(endoMatrix(endoIndex))
        continue;
    end
    
    electrodeMap(endoMatrix(endoIndex), 1) = endoIndex;
    electrodeMap(endoMatrix(endoIndex), 2) = -1;
end
end
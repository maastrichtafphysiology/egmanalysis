classdef ECGData < handle
    properties
        Filename
        
        Data
        ElectrodeLabels
        
        ReferenceData
        ReferenceLabels
        
        SamplingFrequency
        Type
    end
    
    methods
        function self = ECGData(filename, data, samplingFrequency, varargin)
            parser = inputParser;
            parser.addRequired('filename', @ischar);
            parser.addRequired('data', @isnumeric);
            parser.addRequired('samplingFrequency', @isnumeric);
            parser.addOptional('electrodeLabels', {}, @iscell);
            parser.parse(filename, data, samplingFrequency, varargin{:});
            
            self.Filename = filename;
            self.Data = data;
            self.SamplingFrequency = samplingFrequency;
            self.ElectrodeLabels = parser.Results.electrodeLabels;
        end
        
        function delete(self)
        end
        
        function value = GetNumberOfChannels(self)
            value = size(self.Data, 2);
        end
        
        function value = GetNumberOfSamples(self)
            value = size(self.Data, 1);
        end
        
        function timeRange = GetTimeRange(self)
            timeRange = 0:(1 / self.SamplingFrequency):((self.GetNumberOfSamples - 1)/ self.SamplingFrequency);
        end
        
        function ecgDataCopy = Copy(self, selectedChannels, timeRange)
            time = self.GetTimeRange();
            if nargin < 3
                timeRange = [time(1) time(end)];
                if nargin < 2
                    selectedChannels = true(size(self.Data, 2), 1);
                end
            end
            
            validTime = time >= timeRange(1) & time <= timeRange(2);
            data = self.Data(validTime, selectedChannels);
            labels = self.ElectrodeLabels(selectedChannels);
            ecgDataCopy = ECGData(self.Filename, data,...
                self.SamplingFrequency,...
                labels);
            
            if ~isempty(self.ReferenceData)
                ecgDataCopy.ReferenceData = self.ReferenceData(validTime, :);
                ecgDataCopy.ReferenceLabels = self.ReferenceLabels;
            end
            
            ecgDataCopy.Type = self.Type;
        end
    end
end
classdef FilterSettings < handle
    properties
        OverlapFilterEnabled
        correlationThreshold
        overlapThreshold
        amplitudeThreshold
        durationThreshold
        slopeThreshold
        amplitudeDividedByDurationThreshold
    end
   
    methods
        function self = FilterSettings()
			self.OverlapFilterEnabled = 0;
            self.correlationThreshold = 0;
            self.overlapThreshold = 0;
            self.amplitudeThreshold = 0;
            self.durationThreshold = inf;
            self.slopeThreshold = 0;
            self.amplitudeDividedByDurationThreshold = 0;
        end
    end
end
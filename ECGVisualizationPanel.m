classdef ECGVisualizationPanel < ECGChannelPanel
    properties
        ValidChannels
        ValidTime
    end
    
    properties (Access = protected)
        ValidChannelPanel
        ValidChannelsListbox
        
        ValidTimePanel
        ValidTimeStartEdit
        ValidTimeEndEdit
        
        ValidTimeLines
        UseSelectionButton
    end
    
    events
        ValidChannelsSelectionCompleted
    end
    
    methods
        function self = ECGVisualizationPanel(position)
            self = self@ECGChannelPanel(position);
            self.ValidTime = [NaN NaN];
        end
        
        function SetECGData(self, ecgData)
            self.ValidChannels = 1:ecgData.GetNumberOfChannels;
            time = ecgData.GetTimeRange();
            self.ValidTime = [time(1) time(end)];
            set(self.ValidTimeStartEdit, 'string', num2str(self.ValidTime(1)));
            set(self.ValidTimeEndEdit, 'string', num2str(self.ValidTime(end)));
            
            SetECGData@ECGChannelPanel(self, ecgData);
        end
        
        function ecgData = GetECGDataToAnalyze(self)
            if ~isempty(self.ValidChannels)
                ecgData = self.EcgData.Copy(self.ValidChannels, self.ValidTime);
            else
                ecgData = ECGData.empty(0);
            end
        end
    end
    
    methods (Access = protected)
        function CreateECGAxes(self)
            self.AxesPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.2 0 .8 1]);
        end
        
        function CreateChannelSelection(self)
            CreateChannelSelection@ECGChannelPanel(self);
            
            self.ValidChannelPanel = uipanel('parent', self.ChannelPanel,...
                'units', 'normalized', ...
                'position', [.5 .2 .5 .8],...
                'title', 'Channels to analyze',...
                'titlePosition', 'centertop',...
                'borderType', 'none');
            self.ValidChannelsListbox = uicontrol(...
                'parent', self.ValidChannelPanel,...
                'units', 'normalized',...
                'position', [0 0 1 1],...
                'style', 'listbox',...
                'backgroundColor', [1 1 1],...
                'min', 0, 'max', 10,...
                'callback', @self.SetValidChannels,...
                'busyAction', 'cancel');
            
            self.ValidTimePanel = uipanel('parent', self.ChannelPanel,...
                'units', 'normalized', ...
                'position', [.5 0 .5 .2],...
                'title', 'Time to analyze',...
                'titlePosition', 'centertop');
            
            uicontrol('parent', self.ValidTimePanel,...
                'units', 'normalized',...
                'position', [.1 .7 .4 .3],...
                'style', 'text',...                
                'string', 'Start (s)');
            self.ValidTimeStartEdit = uicontrol('parent', self.ValidTimePanel,...
                'units', 'normalized',...
                'position', [.1 .2 .4 .5],...
                'style', 'edit',...    
                'backgroundColor', [1 1 1],...
                'string', num2str(self.ValidTime(1)),...
                'callback', @self.SetValidTimeStart);
            
            uicontrol('parent', self.ValidTimePanel,...
                'units', 'normalized',...
                'position', [.5 .7 .4 .3],...
                'style', 'text',...                
                'string', 'End (s)');
            self.ValidTimeEndEdit = uicontrol('parent', self.ValidTimePanel,...
                'units', 'normalized',...
                'position', [.5 .2 .4 .5],...
                'style', 'edit',...
                'backgroundColor', [1 1 1],...
                'string', num2str(self.ValidTime(2)),...
                'callback', @self.SetValidTimeEnd);
            
            self.UseSelectionButton = uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.1 .05 .8 .05],...
                'style', 'pushbutton',...
                'string', 'Use selection for analysis',...
                'callback', @(src, event) notify(self, 'ValidChannelsSelectionCompleted'));
        end
        
        function ShowECGData(self)
            ShowECGData@ECGChannelPanel(self);
            
            channelIsValid = [ismember(self.SelectedChannels, self.ValidChannels) true(size(self.SelectedLeadChannels))];

            for axesIndex = 1:numel(self.AxesHandles)
                if channelIsValid(axesIndex)
                    axesColor = [1 1 1];
                else
                    axesColor = [.8 .8 .8];
                end
                set(self.AxesHandles(axesIndex), 'color', axesColor);
            end
            
            self.SetValidTimeLines();
        end
        
        function SetChannelList(self)
            SetChannelList@ECGChannelPanel(self);
            
            set(self.ValidChannelsListbox, 'string', self.EcgData.ElectrodeLabels);
            set(self.ValidChannelsListbox, 'value', find(self.ValidChannels));
        end
        
        function SetValidChannels(self, varargin)
            self.ValidChannels = get(self.ValidChannelsListbox, 'value');
            self.ShowECGData();
        end
        
        function SetValidTimeLines(self)
            validLines = ishandle(self.ValidTimeLines);
            if any(validLines)
                delete(self.ValidTimeLines(validLines));
            end
            self.ValidTimeLines = NaN(numel(self.AxesHandles), 2);
            
            for axesIndex = 1:numel(self.AxesHandles)
                yLimits = get(self.AxesHandles(axesIndex), 'yLim');
                self.ValidTimeLines(axesIndex, 1) = line(...
                    'xData', [self.ValidTime(1) self.ValidTime(1)],...
                    'yData', yLimits,...
                    'color', [0 1 0],...
                    'lineWidth', 2,...
                    'parent', self.AxesHandles(axesIndex));
                self.ValidTimeLines(axesIndex, 2) = line(...
                    'xData', [self.ValidTime(2) self.ValidTime(2)],...
                    'yData', yLimits,...
                    'color', [0 1 1],...
                    'lineWidth', 2,...
                    'parent', self.AxesHandles(axesIndex));
            end
        end
        
        function SetValidTimeStart(self, source, varargin)
            if isempty(self.EcgData), return; end
            time = self.EcgData.GetTimeRange();
            value = str2double(get(source, 'string'));
            if value >= time(1) && value < self.ValidTime(end)
                self.ValidTime(1) = value;
            end
            set(source, 'string', num2str(self.ValidTime(1)));
            self.SetValidTimeLines();
        end
        
        function SetValidTimeEnd(self, source, varargin)
            if isempty(self.EcgData), return; end
            time = self.EcgData.GetTimeRange();
            value = str2double(get(source, 'string'));
            if value <= time(end) && value > self.ValidTime(1)
                self.ValidTime(2) = value;
            end
            set(source, 'string', num2str(self.ValidTime(2)));
            self.SetValidTimeLines();
        end
    end
end
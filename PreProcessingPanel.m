classdef PreProcessingPanel < ECGChannelPanel
    properties (Access = public)
        EcgPeriodogramData
        EcgLeadPeriodogramData
        EcgPreProcessor
        BuzzFrequency
        
        ProcessedData
        ProcessedPeriodogramData
        ProcessedLeadPeriodogramData
        
        FrequencyAxesHandles
        
        LineColors
        FrequencyPanel
        
        FilterControlPanel
        BandpassPanel
        BandpassCheckbox
        BandpassStopEdit
        NotchCheckbox
        NotchEdit
        CombCheckbox
    end
    
    events
        PreProcessingCompleted
    end
    
    methods
        function self = PreProcessingPanel(position)
            self = self@ECGChannelPanel(position);
            
            self.EcgPreProcessor = ECGPreProcessor();
            self.EcgPreProcessor.ApplyBandpassFilter = false;
            self.ProcessedData = ECGData.empty(0,0);
            self.LineColors = lines(64);
        end
        
        function SetECGData(self, ecgData, preProcessor)
            if nargin < 3
             switch class(ecgData)
                case 'BDFData'
                    self.EcgPreProcessor.BandpassFrequencies(2) = ceil(ecgData.SamplingFrequency / 2);
                case 'DCMData'
                    self.EcgPreProcessor.BandpassFrequencies(2) = ceil(ecgData.SamplingFrequency / 2);
                case 'ECGData'
                    self.EcgPreProcessor.BandpassFrequencies(2) = ceil(ecgData.SamplingFrequency / 2);
                otherwise
                     self.EcgPreProcessor.BandpassFrequencies(2) = ceil(ecgData.SamplingFrequency / 2);
             end
            set(self.BandpassStopEdit, 'string', num2str(self.EcgPreProcessor.BandpassFrequencies(2)));
            else
                self.EcgPreProcessor = preProcessor;
                self.CreateFilterControlPanel();
            end
            
            [self.EcgPeriodogramData, self.EcgLeadPeriodogramData] =...
                self.EcgPreProcessor.EstimatePSD(ecgData);
            
            SetECGData@ECGChannelPanel(self, ecgData);
        end
        
        function preProcessedData = GetPreProcessedECGData(self)
            if isempty(self.ProcessedData)
                preProcessedData = self.EcgData;
            else
                preProcessedData = self.ProcessedData;
            end
        end
        
        function ecgData = LoadData(self, filename)
            ppData = load(filename);
            ecgData = ppData.ecgData;
            
            [pathString, nameString] = fileparts(filename);
            nameString = nameString(1:(end - 3));
            if ~isempty(ppData.ecgData)
                ppData.ecgData.Filename = fullfile(pathString, nameString);
            end
            
            if ~isempty(ppData.processedData)
                ppData.processedData.Filename = fullfile(pathString, nameString);
            end
            
            self.SetECGData(ppData.ecgData);
            
            self.ProcessedData = ppData.processedData;
            if isempty(self.ProcessedData), return; end
            [self.ProcessedPeriodogramData, self.ProcessedLeadPeriodogramData] =...
                self.EcgPreProcessor.EstimatePSD(self.ProcessedData);
            self.ShowECGData();
        end
        
        function preProcessedData = PreProcessData(self, bandpassFrequencies)
            if isempty(self.EcgData)
                preProcessedData = [];
                return;
            else
                self.EcgPreProcessor.BandpassFrequencies = bandpassFrequencies;
                preProcessedData = self.EcgPreProcessor.Filter(self.EcgData);
                self.ProcessedData = preProcessedData;
                [self.ProcessedPeriodogramData, self.ProcessedLeadPeriodogramData] =...
                    self.EcgPreProcessor.EstimatePSD(self.ProcessedData);
            end
            
            self.ShowECGData();
        end
        
        function DisableBandpassFilter(self)
            self.EcgPreProcessor.ApplyBandpassFilter = false;
            set(self.BandpassCheckbox, 'value', false);
            set(self.BandpassCheckbox, 'enable', 'off');
        end
    end
    
    methods (Access = protected)
        function CreateECGAxes(self)
            self.AxesPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.2 0 .6 1]);
            self.FrequencyPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.8 0 .2 1]);
        end
        
        function CreateControlPanel(self)
            CreateControlPanel@ECGChannelPanel(self);
            
            set(self.ChannelPanel, 'position', [0 .5 1 .49]);

            self.CreateFilterControlPanel();
            
            uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.1 .05 .8 .05],...
                'style', 'pushbutton',...
                'string', 'Pre-processing completed',...
                'callback', @(src, event) notify(self, 'PreProcessingCompleted'));
        end 
        
        function CreateFilterControlPanel(self)
            if ishandle(self.FilterControlPanel)
                delete(self.FilterControlPanel)
            end
            
            self.FilterControlPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [0 .1 1 .39],...
                'title', 'Filter settings',...
                'titlePosition', 'centertop');
            
            self.BandpassCheckbox = uicontrol('parent', self.FilterControlPanel,...
                'units', 'normalized',...
                'position', [.1 .9 .8 .05],...
                'style', 'checkbox',...
                'string', 'Bandpass filter',...
                'value', self.EcgPreProcessor.ApplyBandpassFilter,...
                'callback', @self.EnableBandpassFilter);
            self.BandpassPanel = uipanel('parent', self.FilterControlPanel,...
                'units', 'normalized', ...
                'position', [.05 .8 .9 .1]);
            uicontrol('parent', self.BandpassPanel,...
                'units', 'normalized',...
                'position', [.1 .1 .2 .8],...
                'style', 'edit',...
                'string', num2str(self.EcgPreProcessor.BandpassFrequencies(1)),...
                'tooltipString', 'High pass frequency',...
                'callback', @self.SetBandpassStart);
            self.BandpassStopEdit = uicontrol('parent', self.BandpassPanel,...
                'units', 'normalized',...
                'position', [.4 .1 .2 .8],...
                'style', 'edit',...
                'tooltipString', 'Low pass frequency',...
                'string', num2str(self.EcgPreProcessor.BandpassFrequencies(2)),...
                'callback', @self.SetBandpassStop);
            uicontrol('parent', self.BandpassPanel,...
                'units', 'normalized',...
                'position', [.7 .1 .2 .8],...
                'style', 'edit',...
                'tooltipString', 'Band pass order',...
                'string', num2str(self.EcgPreProcessor.BandpassOrder),...
                'callback', @self.SetBandpassOrder);
            
            self.NotchCheckbox = uicontrol('parent', self.FilterControlPanel,...
                'units', 'normalized',...
                'position', [.1 .6 .4 .05],...
                'style', 'checkbox',...
                'string', 'Notch filter',...
                'value', self.EcgPreProcessor.ApplyNotchFilter,...
                'callback', @self.EnableNotchFilter);
            self.CombCheckbox = uicontrol('parent', self.FilterControlPanel,...
                'units', 'normalized',...
                'position', [.5 .6 .4 .05],...
                'style', 'checkbox',...
                'string', 'Comb filter',...
                'value', self.EcgPreProcessor.ApplyCombFilter,...
                'callback', @self.EnableCombFilter);
            notchPanel = uipanel('parent', self.FilterControlPanel,...
                'units', 'normalized', ...
                'position', [.05 .5 .9 .1]);
            self.NotchEdit = uicontrol('parent', notchPanel,...
                'units', 'normalized',...
                'position', [.1 .1 .2 .8],...
                'style', 'edit',...
                'tooltipString', 'Notch frequency',...
                'string', num2str(self.EcgPreProcessor.NotchFrequency),...
                'callback', @self.SetNotchFrequency);
            uicontrol('parent', notchPanel,...
                'units', 'normalized',...
                'position', [.7 .1 .2 .8],...
                'style', 'edit',...
                'tooltipString', 'Notch precision',...
                'string', num2str(self.EcgPreProcessor.NotchOrder),...
                'callback', @self.SetNotchOrder);
            
            uicontrol('parent', self.FilterControlPanel,...
                'units', 'normalized',...
                'position', [.05 .3 .85 .1],...
                'style', 'pushbutton',...
                'string', 'Apply',...
                'callback', @self.PreProcess);
            
            uicontrol('parent', self.FilterControlPanel,...
                'units', 'normalized',...
                'position', [.05 .15 .4 .1],...
                'style', 'pushbutton',...
                'string', 'Save to .mat',...
                'callback', @self.SavePreProcessing);
            uicontrol('parent', self.FilterControlPanel,...
                'units', 'normalized',...
                'position', [.5 .15 .4 .1],...
                'style', 'pushbutton',...
                'string', 'Load from .mat',...
                'callback', @self.LoadPreProcessing);
            
            uicontrol('parent', self.FilterControlPanel,...
                'units', 'normalized',...
                'position', [.05 0.05 .4 .1],...
                'style', 'pushbutton',...
                'string', 'Save to new .mat',...
                'callback', @self.SaveNewPreProcessing);
            uicontrol('parent', self.FilterControlPanel,...
                'units', 'normalized',...
                'position', [.5 0.05 .4 .1],...
                'style', 'pushbutton',...
                'string', 'Load from new .mat',...
                'callback', @self.LoadNewPreProcessing); 
        end
        
        function ShowECGData(self)
            ShowECGData@ECGChannelPanel(self);
            
            self.ShowPeriodogramData();
            self.ShowFilteredSignals();
        end
        
        function ShowFilteredSignals(self)
            time = self.EcgData.GetTimeRange();
            for lineIndex = 1:numel(self.SelectedChannels)                
                for filteredDataIndex = 1:numel(self.ProcessedData)
                    lineColorPosition = max([1, mod(filteredDataIndex, length(self.LineColors))]);
                    line('xData', time, 'yData', self.ProcessedData(filteredDataIndex).Data(:, self.SelectedChannels(lineIndex)),...
                        'color', self.LineColors(lineColorPosition, :),...
                        'parent', self.AxesHandles(lineIndex),...
                        'lineSmoothing', 'on');
                end
            end
            
            for lineIndex = 1:numel(self.SelectedLeadChannels)                
                for filteredDataIndex = 1:numel(self.ProcessedData)
                    lineColorPosition = max([1, mod(filteredDataIndex, length(self.LineColors))]);
                    line('xData', time, 'yData', self.ProcessedData(filteredDataIndex).ReferenceData(:, self.SelectedLeadChannels(lineIndex)),...
                        'color', self.LineColors(lineColorPosition, :),...
                        'parent', self.AxesHandles(lineIndex + numel(self.SelectedChannels)),...
                        'lineSmoothing', 'on');
                end
            end
        end
        
        function ShowPeriodogramData(self)
            existingAxesHandles = findobj(get(self.FrequencyPanel, 'children'), 'type', 'axes');
            if ~isempty(existingAxesHandles)
                delete(existingAxesHandles);
            end
            
            numberOfAxes = numel(self.SelectedChannels) + numel(self.SelectedLeadChannels);
            self.FrequencyAxesHandles = ECGChannelPanel.TightSubplot(numberOfAxes, 1, self.FrequencyPanel,...
                0.0025, [0.05, 0.025], [0.025, 0.05]);
            for lineIndex = 1:numel(self.SelectedChannels)
%                 self.FrequencyAxesHandles(lineIndex) = subplot(numberOfAxes, 1, lineIndex,...
%                     'parent', self.FrequencyPanel);
                
                frequencies = self.EcgPeriodogramData(self.SelectedChannels(lineIndex)).Frequencies;
                if isempty(frequencies), continue; end
                
                powerDensity = self.EcgPeriodogramData(self.SelectedChannels(lineIndex)).Data;
                lineColor = [0 0 0];
                line('xData', frequencies,...
                    'yData',  pow2db(powerDensity),...
                    'color', lineColor,...
                    'parent', self.FrequencyAxesHandles(lineIndex));
                
                for filteredDataIndex = 1:numel(self.ProcessedData)
                    frequencies = self.ProcessedPeriodogramData(self.SelectedChannels(lineIndex)).Frequencies;
                    powerDensity = self.ProcessedPeriodogramData(self.SelectedChannels(lineIndex)).Data;
                    lineColorPosition = max([1, mod(filteredDataIndex, length(self.LineColors))]);
                    line('xData', frequencies,...
                        'yData',  pow2db(powerDensity),...
                        'color', self.LineColors(lineColorPosition, :),...
                        'parent', self.FrequencyAxesHandles(lineIndex));
                end

                set(self.FrequencyAxesHandles(lineIndex),...
                    'xlim', [0 frequencies(end)],...
                    'xTick', [],...
                    'YAxisLocation', 'right');
                ylabel(self.FrequencyAxesHandles(lineIndex), 'Magnitude (dB)');
            end
            
            numberOfSelectedChannels = numel(self.SelectedChannels);
            for lineIndex = 1:numel(self.SelectedLeadChannels)
%                 self.FrequencyAxesHandles(lineIndex + numberOfSelectedChannels) =...
%                     subplot(numberOfAxes, 1, lineIndex + numberOfSelectedChannels,...
%                     'parent', self.FrequencyPanel);
                
                frequencies = self.EcgLeadPeriodogramData(self.SelectedLeadChannels(lineIndex)).Frequencies;
                if isempty(frequencies), continue; end
                powerDensity = self.EcgLeadPeriodogramData(self.SelectedLeadChannels(lineIndex)).Data;
                lineColor = [0 0 0];
                line('xData', frequencies,...
                    'yData',  powerDensity,...
                    'color', lineColor,...
                    'parent', self.FrequencyAxesHandles(lineIndex + numberOfSelectedChannels));
                
                for filteredDataIndex = 1:numel(self.ProcessedData)
                    frequencies = self.ProcessedLeadPeriodogramData(self.SelectedLeadChannels(lineIndex)).Frequencies;
                    powerDensity = self.ProcessedLeadPeriodogramData(self.SelectedLeadChannels(lineIndex)).Data;
                    lineColorPosition = max([1, mod(filteredDataIndex, length(self.LineColors))]);
                    line('xData', frequencies,...
                        'yData',  pow2db(powerDensity),...
                        'color', self.LineColors(lineColorPosition, :),...
                        'parent', self.FrequencyAxesHandles(lineIndex + numberOfSelectedChannels));
                end

                set(self.FrequencyAxesHandles(lineIndex + numberOfSelectedChannels),...
                    'xlim', [0 frequencies(end)],...
                    'xTick', [],...
                    'YAxisLocation', 'right');
            end
            
            set(self.FrequencyAxesHandles(end),'xTickMode', 'auto');
            xlabel(self.FrequencyAxesHandles(end), 'Frequency (Hz)');           
            if numel(self.FrequencyAxesHandles) > 1
                set(self.FrequencyAxesHandles, {'looseInset'}, get(self.AxesHandles, 'tightInset'));
                if ishandle(self.FrequencyAxesHandles)
                   linkaxes(self.FrequencyAxesHandles, 'x');
                end
            else
                set(self.FrequencyAxesHandles, 'looseInset', get(self.FrequencyAxesHandles, 'tightInset'));
            end
        end
        
        function PreProcess(self, varargin)
            self.CheckPowerlineNoise();
            self.EcgPreProcessor.NotchFrequency =...
                [self.BuzzFrequency, 2 * self.BuzzFrequency, 3 * self.BuzzFrequency];
            set(self.NotchEdit,'string', num2str(self.EcgPreProcessor.NotchFrequency));

            self.ProcessedData = self.EcgPreProcessor.Filter(self.EcgData);
            [self.ProcessedPeriodogramData, self.ProcessedLeadPeriodogramData] =...
                self.EcgPreProcessor.EstimatePSD(self.ProcessedData);
            self.ShowECGData();
        end

        function [medianBuzzFrequency, PeriodogramData] = CheckPowerlineNoise(self)
             % Check powerline noise
            powerLineRange = [45, 55];
            [PeriodogramData, ~] = self.EcgPreProcessor.EstimatePSD(self.EcgData);
            numberOfChannels = self.EcgData.GetNumberOfChannels();
            parfor channelIndex = 1:numberOfChannels
                powerlineBand = find(PeriodogramData(channelIndex).Frequencies > powerLineRange(1) &...
                    PeriodogramData(channelIndex).Frequencies < powerLineRange(end));
                [powerlinePeakAmplitude, powerlinePeakIndex] = max(PeriodogramData(channelIndex).Data(powerlineBand));
                
                buzzFrequency(1,channelIndex) = PeriodogramData(channelIndex).Frequencies(powerlineBand(powerlinePeakIndex));
            end
            medianBuzzFrequency = median(buzzFrequency);
            
            self.BuzzFrequency = medianBuzzFrequency;
        end
        
        function EnableBandpassFilter(self, source, varargin)
            self.EcgPreProcessor.ApplyBandpassFilter = get(source, 'value') > 0;
        end
        
        function EnableBaselineCorrection(self, source, varargin)
            self.EcgPreProcessor.ApplyBaselineCorrection = get(source, 'value') > 0;
        end
        
        function EnableNotchFilter(self, source, varargin)
            enable = get(source, 'value') > 0;
            self.EcgPreProcessor.ApplyNotchFilter = enable;
            if enable && self.EcgPreProcessor.ApplyCombFilter
                self.EcgPreProcessor.ApplyCombFilter = false;
                set(self.CombCheckbox, 'value', false);
            end
        end
        
        function EnableCombFilter(self, source, varargin)
            enable = get(source, 'value') > 0;
            self.EcgPreProcessor.ApplyCombFilter = enable;
            if enable && self.EcgPreProcessor.ApplyNotchFilter
                self.EcgPreProcessor.ApplyNotchFilter = false;
                set(self.NotchCheckbox, 'value', false);
            end
        end
        
        function SetBandpassStart(self, source, varargin)
            value = str2double(get(source, 'string'));
            if value >= 0 && value < self.EcgPreProcessor.BandpassFrequencies(2)
               self.EcgPreProcessor.BandpassFrequencies(1) = value; 
            end
            set(source, 'string', num2str(self.EcgPreProcessor.BandpassFrequencies(1)));
        end
        
        function SetBandpassStop(self, source, varargin)
            value = str2double(get(source, 'string'));
            if value > self.EcgPreProcessor.BandpassFrequencies(1)
               self.EcgPreProcessor.BandpassFrequencies(2) = value; 
            end
            set(source, 'string', num2str(self.EcgPreProcessor.BandpassFrequencies(2)));
        end
        
        function SetBandpassOrder(self, source, varargin)
            value = round(str2double(get(source, 'string')));
            if value > 0
               self.EcgPreProcessor.BandpassOrder = value; 
            end
            set(source, 'string', num2str(self.EcgPreProcessor.BandpassOrder));
        end
        
        function SetNotchFrequency(self, source, varargin)
            value = str2num(get(source, 'string')); %#ok<ST2NM>
            if all(value > 0)
               self.EcgPreProcessor.NotchFrequency = value; 
            end
            set(source, 'string', num2str(self.EcgPreProcessor.NotchFrequency));
        end
        
        function SetNotchOrder(self, source, varargin)
            value = round(str2double(get(source, 'string')));
            if value > 0
               self.EcgPreProcessor.NotchOrder = value; 
            end
            set(source, 'string', num2str(self.EcgPreProcessor.NotchOrder));
        end
        
        function SavePreProcessing(self, varargin)
            [pathString, nameString] = fileparts(self.EcgData.Filename);
            filename = [pathString, filesep, nameString '_PP'];
            ecgData = self.EcgData; %#ok<NASGU>
            processedData = self.ProcessedData; %#ok<NASGU>
            preProcessor = self.EcgPreProcessor; %#ok<NASGU>
            save(filename,...
                'ecgData',...
                'processedData',...
                'preProcessor', '-v7.3');
        end
        
        function SaveNewPreProcessing(self, varargin)
            [filename, pathname] = HelperFunctions.customUiputfile('_PP.mat', 'Save preprocessing result as...');
            
            if isequal(filename, 0) || isequal(pathname, 0), return; end
            filename = fullfile(pathname, filename);
            [pathString, nameString] = fileparts(filename);
            
            if ~strcmp(nameString((end-2):end), '_PP')
                errordlg('Filename must end with "_PP"', 'File name error');
                return;
            end
            self.EcgData.Filename = fullfile(pathString, nameString(1:(end - 3)));
            
            if ~isempty(self.ProcessedData)
                self.ProcessedData.Filename = self.EcgData.Filename;
            end
            
            ecgData = self.EcgData; %#ok<NASGU>
            processedData = self.ProcessedData; %#ok<NASGU>
            preProcessor = self.EcgPreProcessor; %#ok<NASGU>
            save(filename,...
                'ecgData',...
                'processedData',...
                'preProcessor', '-v7.3');
        end
        
        function LoadPreProcessing(self, varargin)
            if isempty(self.EcgData)
                [filename, pathname] = HelperFunctions.customUigetfile('*_PP.mat', 'Select PP file');
                if isequal(filename, 0) || isequal(pathname, 0), return; end
                filename = fullfile(pathname, filename);
            else
                [pathString, nameString] = fileparts(self.EcgData.Filename);
                filename = [pathString, filesep, nameString '_PP.mat'];
            end
            
            self.LoadData(filename);
        end
        
        function LoadNewPreProcessing(self, varargin)
            [filename, pathname] = HelperFunctions.customUigetfile('*_PP.mat', 'Select PP file');
            if isequal(filename, 0) || isequal(pathname, 0), return; end
            filename = fullfile(pathname, filename);
            
            self.LoadData(filename);
        end
    end
end